package loecraftpack.logic.attributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.EntityCustomExp;
import loecraftpack.content.items.ItemBits;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.content.registery.LOE_Settings;
import loecraftpack.logic.areamapping.RankMapManager;
import loecraftpack.logic.attributes.title.HandlerTitle;
import loecraftpack.network.packets.PacketExtendedMobData;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityCaveSpider;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class HandlerAttribute
{
	
	Random rand = new Random();
	boolean cleanMode = false;//used to remove old & invalid data, rather than ignore it.
	
	/* Notes:
	 *   of mob grade and talent
	 *   MetaData [0] the min. Rank needed to be used
	 *   MetaData [1] looting bonus (multiplier)
	 */
	
	//slime death map
	public static List<DeathMap> slimeDeaths = new ArrayList<DeathMap>();
	
	class DeathMap
	{
		public final EntitySlime slime;
		public final LoeEntityLivingData extendedData;
		public int age = 0;
		
		DeathMap(EntitySlime slime)
		{
			this.slime = slime;
			extendedData = (LoeEntityLivingData)slime.getExtendedProperties("LoeEntityLiving");
		}
	}
	
	/************************/
	/******* Mob Loot *******/
	/************************/
	
	//CurveSeverity: values outside of range [0.0 , 1.0]  cause the curve function to behave weirdly
	
	//**bits
	HashMap<String, int[]> bitDropValues = new HashMap<String, int[]>();
	public int bitRarity = 25;// 1/25 mobs drop bits
	public float curveSeverityBits = 9.0f/19.0f;// a/(a+b)
	
	//**loot
	public float vanillalLootWeight = 0.05f;//weight of normal vanilla loot, indirectly acts a var C in the curve
	public float maxAverageSuccesRate = 1.00f;//average success rate of dropping loot at max rank (includes vanilla weight)
	public float maxAttemptSuccesRate = 0.20f;
	public float curveSeverityLoot = 9.0f/19.0f;// a/(a+b)
	
	static WeightedRandomChestContent[] loot;
	
	/************************/
	/******* Mob Rank *******/
	/************************/
	
	public static final Map<Integer, ModifierSet> mobRanks = new HashMap<Integer, ModifierSet>();
	public static int maxRank = 999;
	public static float rankDistance = 100.1f;
	
	/**************************/
	/******* Mob Grade ********/
	/**************************/
	
	public static final Map<String, ModifierSet> mobGrades = new HashMap<String, ModifierSet>();
	public static float chanceApplyMobGrade = 0.2f;
	public static int weightTotalMobGrade = 0;
	
	/**Mob Minion*****/
	/*****************/
	public static final ModifierSet modMinion = new ModifierSet("Minion");
	static {mobGrades.put(modMinion.name, modMinion);}
	
	/**Mob Improved***/
	/*****************/
	public static final ModifierSet modImproved = new ModifierSet("Improved");
	static {mobGrades.put(modImproved.name, modImproved);}
	
	/**Mob Brute******/
	/*****************/
	public static final ModifierSet modBrute = (new ModifierSet("Brute"));
	static {mobGrades.put(modBrute.name, modBrute);}
	
	/**Mob Solo*******/
	/*****************/
	public static final ModifierSet modSolo = new ModifierSet("Solo");
	static {mobGrades.put(modSolo.name, modSolo);}
	
	
	
	/**************************/
	/******* Mob Talent *******/
	/**************************/
	
	public static final Map<String, ModifierSet> mobTalents = new HashMap<String, ModifierSet>();
	public static float chanceApplyMobTalent = 0.1f;
	public static int weightTotalMobTalent = 0;
	
	/**Mob Skirmisher*/
	/*****************/
	public static final ModifierSet modSkirmisher = new ModifierSet("Skirmisher");
	static {mobTalents.put(modSkirmisher.name, modSkirmisher);}
	
	/**Mob Speedy*****/
	/*****************/
	public static final ModifierSet modSpeedy = new ModifierSet("Speedy");
	static {mobTalents.put(modSpeedy.name, modSpeedy);}
	
	/**Mob Leader*****/
	/*****************/
	public static final ModifierSet modLeader = new ModifierSet("Leader");
	static {mobTalents.put(modLeader.name, modLeader);}
	
	
	

	/******************************/
	/******* Passive Grade ********/
	/******************************/
	
	public static final Map<String, ModifierSet> passiveGrades = new HashMap<String, ModifierSet>();
	public static float chanceApplyPassiveGrade = 0.15f;
	public static int weightTotalPassiveGrade = 0;
	
	/**Healthy********/
	/*****************/
	public static final ModifierSet modHealthy = new ModifierSet("Healthy");
	static {passiveGrades.put(modHealthy.name, modHealthy);}
	
	/**Super**********/
	/*****************/
	public static final ModifierSet modSuper = new ModifierSet("Super");
	static {passiveGrades.put(modSuper.name, modSuper);}
	
	
	
	/******************************/
	/******* Passive Talent *******/
	/******************************/
	
	
	
	
	public HandlerAttribute()
	{
		/************************/
		/******* Mob Loot *******/
		/************************/
		
		//default - bits
		bitDropValues.put("DEFAULT", new int[]{5,200});
		
		//individual - bits
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntityCreeper.class), new int[]{5,200});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntitySkeleton.class), new int[]{5,200});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntitySpider.class), new int[]{2,150});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntityZombie.class), new int[]{5,200});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntitySlime.class), new int[]{1,35});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntityEnderman.class), new int[]{10,300});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntityCaveSpider.class), new int[]{2,150});
		bitDropValues.put((String)EntityList.classToStringMapping.get(EntitySilverfish.class), new int[]{2,200});
		//baseBitDropValues.put((String)EntityList.classToStringMapping.get(  .class), new int[]{5,200});
		
		
		loot = new WeightedRandomChestContent[]{new WeightedRandomChestContent(Items.ender_pearl, 0, 1, 3, 100),
												new WeightedRandomChestContent(Items.diamond, 0, 1, 3, 30),
												new WeightedRandomChestContent(Items.iron_ingot, 0, 1, 5, 100),
												new WeightedRandomChestContent(Items.gold_ingot, 0, 1, 3, 50),
												new WeightedRandomChestContent(Items.redstone, 0, 4, 9, 50),
												new WeightedRandomChestContent(Items.bread, 0, 2, 5, 150),
												new WeightedRandomChestContent(Items.apple, 0, 2, 5, 150),
												new WeightedRandomChestContent(Items.iron_pickaxe, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.iron_sword, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.iron_chestplate, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.iron_helmet, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.iron_leggings, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.iron_boots, 0, 1, 1, 50),
												new WeightedRandomChestContent(Items.golden_apple, 0, 1, 1, 10),
												new WeightedRandomChestContent(Items.golden_apple, 1, 1, 1, 1),
												new WeightedRandomChestContent(Items.name_tag, 0, 1, 1, 10),
												new WeightedRandomChestContent(Items.enchanted_book, 0, 1, 1, 100),
												new WeightedRandomChestContent(LOE_Items.appleFritter, 0, 2, 5, 150),
												new WeightedRandomChestContent(LOE_Items.gemStones, 1111/*random*/, -1/*random*/, -1/*random*/, 50),
												new WeightedRandomChestContent(LOE_Items.mask, 1111/*random*/, 1, 1, 100),
												new WeightedRandomChestContent(LOE_Items.tapestry, 1111/*random*/, 1, 1, 30),
												new WeightedRandomChestContent(Item.getItemFromBlock(LOE_Blocks.zapAppleSapling), 0, 1, 1, 5),
												new WeightedRandomChestContent(Item.getItemFromBlock(LOE_Blocks.appleBloomSapling), 0, 1, 1, 5),
												new WeightedRandomChestContent(LOE_Items.necklaceOfDreams, 0, 1, 1, 5),
												new WeightedRandomChestContent(LOE_Items.ringLife, 0, 1, 1, 10)};
		
		/************************/
		/******* Mob Rank *******/
		/************************/
		
		for (int i=1; i<=maxRank; i++)
		{
			ModifierSet rankset = new ModifierSet("R"+i);
			mobRanks.put(Integer.valueOf(i), rankset);
		}
		
		for (int i : mobRanks.keySet())
		{
			mobRanks.get(Integer.valueOf(i)).setDetails( 0, Stats.GetStatCurve(i, 0, 49, 1))
			/* (X15  at max) */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Rank "+i+")", Stats.GetStatCurve(i, 4.5f, 9.5f, 0), 2).setSaved(false))
			/* (X10  at max) */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Rank "+i+")", Stats.GetStatCurve(i, 1.75f, 7.25f, 0), 2).setSaved(false))
			/* (X2 at max) */.addMoveSpeedMod(new AttributeModifier(UUID.randomUUID(), "Speed (Rank "+i+")", Stats.GetStatCurve(i, 0.15f, 0.85f, 0), 2).setSaved(false));
		}
		
		
		
		/**************************/
		/******* Mob Grade ********/
		/**************************/
		
		modMinion.setDetails(0, 0.2f).addMetatData(new Float[]{0.0f, 0.10f})
		/*  X0.2 */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Minion)", -0.8d, 2).setSaved(false))
		/* -50% */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Minion)", -0.5d, 1).setSaved(false))
		/* -50% */.addKnockBackMod(new AttributeModifier(UUID.randomUUID(), "KnockBack resistance (Minion)", -0.5d, 1).setSaved(false));
		
		modImproved.setDetails(5, 1.3f).addMetatData(new Float[]{4.0f, 1.05f})
		/* +10% */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Improved)", 0.1d, 1).setSaved(false))
		/* +5% */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Improved)", 0.05d, 1).setSaved(false));
		
		modBrute.setDetails(2, 2.0f).addMetatData(new Float[]{12.0f, 1.10f})
		/* +30% */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Brute)", 0.3d, 1).setSaved(false))
		/* +10% */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Brute)", 0.1d, 1).setSaved(false))
		/* +50% */.addKnockBackMod(new AttributeModifier(UUID.randomUUID(), "KnockBack resistance (Brute)", +0.5d, 1).setSaved(false));
		
		modSolo.setDetails(1, 5.0f).addMetatData(new Float[]{20.0f, 1.2f})
		/* +75% */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Solo)", 0.75d, 1).setSaved(false))
		/* +25% */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Solo)", 0.25d, 1).setSaved(false))
		/* +100% */.addKnockBackMod(new AttributeModifier(UUID.randomUUID(), "KnockBack resistance (Solo)", 1.0d, 1).setSaved(false));
		
		
		for (ModifierSet set : mobGrades.values())
			weightTotalMobGrade+=set.weight;
		
		
		
		/**************************/
		/******* Mob Talent *******/
		/**************************/
		
		modSkirmisher.setDetails(5, 1.5f).addMetatData(new Float[]{12.0f, 1.05f})
		/*  X0.5 */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Skirmisher)", -0.5d, 2).setSaved(false))
		/*  X1.6 */.addMoveSpeedMod(new AttributeModifier(UUID.randomUUID(), "Move Speed (Skirmisher)", 0.6d, 2).setSaved(false))
		/*  X3   */.addFollowRangeMod(new AttributeModifier(UUID.randomUUID(), "detection Range (Skirmisher)",2.0d, 2).setSaved(false));
		
		modSpeedy.setDetails(5, 1.5f).addMetatData(new Float[]{12.0f, 1.05f})
		/*  X0.75 */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Speedy)", -0.25d, 2).setSaved(false))
		/*  X1.3  */.addMoveSpeedMod(new AttributeModifier(UUID.randomUUID(), "Move Speed (Speedy)", 0.3d, 2).setSaved(false));
		
		modLeader.setDetails(2, 3.0f).addMetatData(new Float[]{16.0f, 1.8f})
		/*  X2   */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Leader)", 1.0d, 2).setSaved(false))
		/*  X1.2 */.addAttackDamageMod(new AttributeModifier(UUID.randomUUID(), "Attack Damage (Leader)", 0.2d, 2).setSaved(false))
		/*  X2   */.addFollowRangeMod(new AttributeModifier(UUID.randomUUID(), "detection Range (Leader)", 1.0d, 2).setSaved(false));
		
		
		for (ModifierSet set : mobTalents.values())
			weightTotalMobTalent+=set.weight;
		
		
		/******************************/
		/******* Passive Grade ********/
		/******************************/
		
		modHealthy.setDetails( 5, 1.2f)
		/* +100% */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Healthy)", 1.0d, 1).setSaved(false));
		
		modSuper.setDetails( 1, 4.0f)
		/* +200% */.addMaxHealthMod(new AttributeModifier(UUID.randomUUID(), "Max Health (Super)", 2.0d, 1).setSaved(false))
		/* +40% */.addMoveSpeedMod(new AttributeModifier(UUID.randomUUID(), "Move Speed (Super)", 0.4d, 1).setSaved(false));
		
		
		for (ModifierSet set : passiveGrades.values())
			weightTotalPassiveGrade+=set.weight;
		
		
		
		/******************************/
		/******* Passive Talent *******/
		/******************************/
		
		
		
		
	}
	
	public static void cleanRecords()
	{
		int i = 0;
		while (i < slimeDeaths.size())
		{
			DeathMap death = slimeDeaths.get(i);
			if (death.slime.deathTime>=20)
				slimeDeaths.remove(i);
			else
				i++;
		}
	}
	
	/** This function is used to identify which entities are valid for applying modifiers too
	 */
	public static boolean isUsableEntity(Entity entity)
	{
		if (entity instanceof EntityLiving)
		{
			String name = EntityList.getEntityString(entity).toLowerCase();
			if (name.startsWith("npc")/*avoid custom npc's*/)
				return false;
			else
				return true;
		}
		return false;	
	}
	
	
	@SubscribeEvent
	public void bindCustomAttributes(EntityEvent.EntityConstructing event)
	{
		if (isUsableEntity(event.entity))
			event.entity.registerExtendedProperties("LoeEntityLiving", new LoeEntityLivingData());
	}
	/*
	@SubscribeEvent(priority=EventPriority.HIGH)
	public void onDeath(LivingDeathEvent event)
	{
		if (event.entity instanceof EntityLiving)
		{
			//non-player entities
			EntityLiving creature = (EntityLiving)event.entity;
			LoeEntityLivingData extendedData = (LoeEntityLivingData)creature.getExtendedProperties("LoeEntityLiving");
			
			if (extendedData != null && (extendedData.metaData&1)!=0)
			{
				if (creature instanceof IMob)
				{
					if (0 < extendedData.rank || extendedData.grade.length() > 0 || extendedData.talent.length() > 0)
					{
						creature.setCustomNameTag("");
					}
				}
				else if (creature instanceof IAnimals)
				{
					if (extendedData.grade.length() > 0 || extendedData.talent.length() > 0)
					{
						creature.setCustomNameTag("");
					}
				}
			}
		}
	}*/
	
	@SubscribeEvent
	public void onSpawn(EntityJoinWorldEvent event)
	{
		if (isUsableEntity(event.entity))
		{
			if (event.world.isRemote)
			{
				//request extended data packet
				LoECraftPack.packetHandler.sendToServer(new PacketExtendedMobData().INIT(event.entity.getEntityId()));
				return;
			}
			
			//non-player entities
			EntityLiving creature = (EntityLiving)event.entity;
			LoeEntityLivingData extendedData = (LoeEntityLivingData)creature.getExtendedProperties("LoeEntityLiving");
			
			if (extendedData != null)
			{
				boolean unprocessed = (extendedData.metaData&1)==0;
				
				/*********************/
				/*** EXAMINE ENTIY ***/
				/*********************/
				if (unprocessed)
				{
					if (creature.hasCustomNameTag()/*non-default*/)
					{
						//special mob - set processed & ignore XP
						extendedData.metaData |= 3;
					}
					else
					{
						//new mob - probably...
						boolean inheriter = false;
						
						//set processed
						extendedData.metaData |= 1;
						
						if (creature instanceof IMob)
						{
							if (creature instanceof EntitySlime)
							{
								//test for slime inheritance
								for (DeathMap death: slimeDeaths)
								{
									EntitySlime target = death.slime;
									//is correct size?
									if (target.getSlimeSize() == ((EntitySlime) creature).getSlimeSize()*2 && target.deathTime == 20)
									{
										//is in range?
										float d1 = (float)target.getSlimeSize()/4.0f;
										d1 = d1*d1 + 0.25f;
										float d2 = (float) creature.getDistanceSq(target.posX, target.posY, target.posZ);
										if (d1 > d2)
										{
											//inherent from assumed parent
											extendedData.clone(death.extendedData);
											inheriter = true;
											break;
										}
									}
								}
							}
							
							if (!inheriter)
							{
								//rank
								int rank = RankMapManager.getRankFromMap(creature);
								if (rank>=0)
								{
									extendedData.rank = rank;
								}
								
								//talent
								if (rand.nextFloat() < chanceApplyMobTalent)
								{
									int roll = rand.nextInt(this.weightTotalMobTalent);
									int increment = 0;
									for (ModifierSet set :this.mobTalents.values())
									{
										if (set == null) continue;
										
										increment += set.weight;
										if (roll < increment)
										{
											if (extendedData.rank >= ((Float)set.metaData[0]))
											{
												extendedData.talent = set.name;
											}
											break;
										}
									}
								}
								
								//grade
								if (rand.nextFloat() < chanceApplyMobGrade)
								{
									int roll = rand.nextInt(this.weightTotalMobGrade);
									int increment = 0;
									for (ModifierSet set :this.mobGrades.values())
									{
										if (set == null) continue;
										
										increment += set.weight;
										if (roll < increment)
										{
											if (extendedData.rank >= ((Float)set.metaData[0]))
											{
												extendedData.grade = set.name;
											}
											break;
										}
									}
								}
							}
						}
						else if (creature instanceof IAnimals)
						{
							if (!(creature instanceof EntityVillager))
							{
								//talent
								
								//grade
								if (rand.nextFloat() < chanceApplyPassiveGrade)
								{
									int roll = rand.nextInt(this.weightTotalPassiveGrade);
									int increment = 0;
									for (ModifierSet set :this.passiveGrades.values())
									{
										if (set == null) continue;
										
										increment += set.weight;
										if (roll < increment)
										{
											extendedData.grade = set.name;
											break;
										}
									}
								}
							}
						}
						
						if (!inheriter)
						{
							String specialTitle = HandlerTitle.getTitle(creature, extendedData, rand);
							if (specialTitle != null)
								extendedData.title = specialTitle;
						}
					}
				}
				
				/*******************/
				/** Clean Invalid **/
				/*******************/
				
				if (cleanMode)
					if (creature instanceof IMob)
					{
						if (extendedData.rank > maxRank)
							extendedData.rank = maxRank;
						
						if (extendedData.grade != "" && mobGrades.get(extendedData.grade) == null)
							extendedData.grade = "";
						
						if (extendedData.talent != "" && mobTalents.get(extendedData.talent) == null)
							extendedData.talent = "";
					}
					else if (creature instanceof IAnimals)
					{
						if (extendedData.grade != "" && passiveGrades.get(extendedData.grade) == null)
							extendedData.grade = "";
					}
				
				
				
				/******************/
				/*** APPLY MODS ***/
				/******************/
				
				if (LOE_Settings.Enabled_MobBoosts)
				{
					IAttributeInstance healthStat = creature.getEntityAttribute(SharedMonsterAttributes.maxHealth);
					IAttributeInstance attackStat = creature.getEntityAttribute(SharedMonsterAttributes.attackDamage);
					IAttributeInstance knockBStat = creature.getEntityAttribute(SharedMonsterAttributes.knockbackResistance);
					IAttributeInstance moveStat = creature.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
					IAttributeInstance senseStat = creature.getEntityAttribute(SharedMonsterAttributes.followRange);
					
					if (creature instanceof IMob)
					{
						if (healthStat != null && attackStat != null && knockBStat != null && moveStat != null && senseStat != null)
						{
							if (extendedData.rank > 0)
							{
								ModifierSet set = mobRanks.get(Integer.valueOf(extendedData.rank));
								if (set != null)
									set.applyModifiers(creature);
							}
							if (extendedData.grade != "")
							{
								ModifierSet set = mobGrades.get(extendedData.grade);
								if (set != null)
									set.applyModifiers(creature);
							}
							if (extendedData.talent != "")
							{
								ModifierSet set = mobTalents.get(extendedData.talent);
								if (set != null)
									set.applyModifiers(creature);
							}
							
							if (unprocessed)
								creature.setHealth(creature.getMaxHealth());
						}
					}
					else if (creature instanceof IAnimals)
					{
						if (healthStat != null && moveStat != null)
						{
							
							if (extendedData.grade != "")
							{
								ModifierSet set = passiveGrades.get(extendedData.grade);
								if (set != null)
									set.applyModifiers(creature);
							}
							
							if (unprocessed)
								creature.setHealth(creature.getMaxHealth());
						}
					}
				}
				
				
				/****************/
				/*** APPLY XP ***/
				/****************/
				
				if ((extendedData.metaData&2)==0/*use XP*/)
				{
					if (creature instanceof IMob)
					{
						//apply proper boosted XP value;
						float xp = creature.experienceValue;
						
						if (LOE_Settings.Enabled_MobBoosts)
						{
							if (extendedData.rank>0)
							{
								ModifierSet set = mobRanks.get(extendedData.rank);
								if (set != null)
									xp *= set.xpMod;
							}
							if (extendedData.grade!="")
							{
								ModifierSet set = mobGrades.get(extendedData.grade);
								if (set != null)
									xp *= set.xpMod;
							}
							if (extendedData.talent!="")
							{
								ModifierSet set = mobTalents.get(extendedData.talent);
								if (set != null)
									xp *= set.xpMod;
							}
						}
						
						extendedData.exp = (int) xp;
					}
					else if (creature instanceof IAnimals)
					{
						//apply proper boosted XP value;
						float xp = creature.experienceValue;
						
						if(LOE_Settings.Enabled_MobBoosts)
						{
							if (extendedData.grade!="")
							{
								ModifierSet set = passiveGrades.get(extendedData.grade);
								if (set != null)
									xp *= set.xpMod;
							}
						}
						
						extendedData.exp = (int) xp;
					}
				}
			}
			else
			{
				System.out.println("FAIL - extended data missing from a valid mob");
			}
		}
	}
	
	@SubscribeEvent
	public void onSlimeDeath(LivingDeathEvent event)
	{
		if (!event.entity.worldObj.isRemote && event.entityLiving instanceof EntitySlime)
		{
			EntitySlime slime = (EntitySlime) event.entityLiving;
			if (slime.getSlimeSize() > 1 && event.entityLiving.getExtendedProperties("LoeEntityLiving")!=null)
				slimeDeaths.add(new DeathMap(slime));
		}
	}
	
	@SubscribeEvent
	public void onDropLoot(LivingDropsEvent event)
	{
		LoeEntityLivingData extendedData = (LoeEntityLivingData)event.entityLiving.getExtendedProperties("LoeEntityLiving");
		
		if (extendedData != null && (extendedData.metaData&1) == 1)
		{
			if (event.entityLiving.recentlyHit>0 && !event.entityLiving.isChild() &&
					event.entityLiving instanceof IMob && event.entity.worldObj.getGameRules().getGameRuleBooleanValue("doMobLoot"))
			{
				if (LOE_Settings.Enabled_MobBoosts)
				{
					///apply bonus drops
					
					//random chance to drop bits (uncommon drop, except for leaders, amount scales with rank)
					if (extendedData.talent.equals(modLeader.name) || event.entity.rand.nextInt(bitRarity)==0)
					{
						String raceName = (String)EntityList.classToStringMapping.get(event.entityLiving.getClass());
						
						int[] minMaxDrop = null;
						if (raceName == null || (minMaxDrop = bitDropValues.get(raceName))== null)
							minMaxDrop = bitDropValues.get("DEFAULT");
						
						if (minMaxDrop != null)
						{
							int amount = (int)applyCurve_power2((float)extendedData.rank, curveSeverityBits, 0.0f, minMaxDrop[0], maxRank, minMaxDrop[1]);
							ItemBits.DropBits(amount, event.entity);
						}
					}
					
					int rank = extendedData.rank;
					
					EntityLivingBase entity = (EntityLivingBase)event.source.getEntity();
					float fortune = 1.0f + (entity != null ? 0.05f*EnchantmentHelper.getLootingModifier(entity) : 0);
					
					for (ModifierSet set :this.mobGrades.values())
					{
						if (set == null || !set.name.matches(extendedData.grade)) continue;
						fortune *= (Float)set.metaData[1];
						break;
					}
					for (ModifierSet set :this.mobTalents.values())
					{
						if (set == null || !set.name.matches(extendedData.talent)) continue;
						fortune *= (Float)set.metaData[1];
						break;
					}
					
					if (fortune>0 && event.entity instanceof EntitySlime? ((EntitySlime)event.entity).getSlimeSize() > 1: true)
					{
						float[] rates = calculateLootRates(rank, fortune);
						
						if (rates != null)
						{
							//Debug: loot
							System.out.println("Loot attepmts: "+rates.length);
							for(int i=0; i< rates.length; i++)
							{
								System.out.println("chance:"+rates[i]);
								if (event.entity.rand.nextFloat() < rates[i])
								{
									dropChestLoot(event.entity);
								}
							}
						}
					}
				}
				
				if (LOE_Settings.Enabled_StatsUsage)
				{
					//custom exp drop
					int exp = extendedData.exp;
					while (exp > 0)
					{
						int part = EntityXPOrb.getXPSplit(exp);
						exp -= part;
						event.entityLiving.worldObj.spawnEntityInWorld(new EntityCustomExp(event.entityLiving.worldObj,
								event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, part));
					}
				}
			}
		}
	}
	
	public float[] calculateLootRates(int rank, float fortune)
	{
		float success = applyCurve_power2(rank, curveSeverityLoot, 0.0f, 0.0f, maxRank, maxAverageSuccesRate - vanillalLootWeight)*fortune;//don't worry about this value exceeding 1.0f
		System.out.println();
		System.out.println("=====================");
		System.out.println("----chance--"+success);
		if (success<=0)
			return null;
		
		int attempts = 1;
		while (success>maxAttemptSuccesRate)
		{
			success -= maxAttemptSuccesRate;
			attempts++;
		}
		
		float[] spread = new float[attempts];
		for (int i2=0; i2<spread.length-1; i2++)
		{
			spread[i2] = maxAttemptSuccesRate;
		}
		spread[spread.length-1] = success;
		
		return spread;
	}
	
	/**
	 * 
	 * @param varX - input
	 * @param varCurve
	 * @param varXmin
	 * @param varYmin
	 * @param varXmax
	 * @param varYmax
	 * @return varY
	 */
	protected float applyCurve_power2(float varX, float varCurve, float varXmin, float varYmin, float varXmax, float varYmax)
	{
		float translatedX = (varX-varXmin)/(varXmax-varXmin);
		
		float a = varCurve*(varYmax - varYmin);
		
		float b = varYmax - (a+varYmin);
		
		float varY = a*translatedX*translatedX + b*translatedX + varYmin;
		
		return Math.min(varYmax, Math.max(varYmin, varY));
	}
	
	
	/**Enity Drops a new single unit of chest loot (could be music dics, 1+ redstone, potion(s))
	 * 
	 * @param entity
	 */
	protected void dropChestLoot(Entity entity)
	{
		WeightedRandomChestContent weightedrandomchestcontent = (WeightedRandomChestContent)WeightedRandom.getRandomItem(entity.rand, accessLootTable(entity.rand));
		//Debug: loot
		System.out.println("------loot- weight:"+weightedrandomchestcontent.itemWeight+"  ID:"+weightedrandomchestcontent.theItemId+"  min:"+weightedrandomchestcontent.theMinimumChanceToGenerateItem+" max:"+weightedrandomchestcontent.theMaximumChanceToGenerateItem);
		ItemStack[] stacks = ChestGenHooks.generateStacks(entity.rand, weightedrandomchestcontent.theItemId, weightedrandomchestcontent.theMinimumChanceToGenerateItem, weightedrandomchestcontent.theMaximumChanceToGenerateItem);
		for (ItemStack stack : stacks)
			entity.entityDropItem(stack, 0);
	}
	
	
	public static WeightedRandomChestContent[] accessLootTable(Random rand)
	{
		ArrayList<WeightedRandomChestContent> returnArray = new ArrayList<WeightedRandomChestContent>();
		
		for (WeightedRandomChestContent original : loot)
		{
			Item item = original.theItemId.getItem();
			
			if (item != null)
			{
				WeightedRandomChestContent newContent = item.getChestGenBase(null, rand, original);
				if (newContent != null)
				{
					returnArray.add(newContent);
				}
			}
		}
		return returnArray.toArray(new WeightedRandomChestContent[returnArray.size()]);
	}
}
