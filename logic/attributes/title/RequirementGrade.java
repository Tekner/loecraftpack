package loecraftpack.logic.attributes.title;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class RequirementGrade extends Requirement
{
	String name;
	
	RequirementGrade(String name)
	{
		this.name = name;
	}
	
	@Override
	public boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data) {
		return data.grade.contentEquals(name);
	}
}
