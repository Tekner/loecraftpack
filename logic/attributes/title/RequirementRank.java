package loecraftpack.logic.attributes.title;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class RequirementRank extends Requirement
{
	int min;
	int max;
	
	RequirementRank(int min, int max)
	{
		this.min = min;
		this.max = max;
	}
	
	@Override
	public boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data) {
		return data.rank > min && data.rank < max;
	}
	
}