package loecraftpack.logic.attributes.title;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class RequirementTalent extends Requirement
{
	String name;
	
	RequirementTalent(String name)
	{
		this.name = name;
	}
	
	@Override
	public boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data) {
		return data.talent.contentEquals(name);
	}
}
