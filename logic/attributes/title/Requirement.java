package loecraftpack.logic.attributes.title;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public abstract class Requirement
{
	boolean not = false;
	
	public boolean meets(EntityLiving entity, LoeEntityLivingData data)
	{
		if (not)
			return !meetsSUB(entity, data);
		else
			return meetsSUB(entity, data);
	}
	
	public Requirement not()
	{
		not = true;
		return this;
	}
	
	protected abstract boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data);
}
