package loecraftpack.logic.attributes.title;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class TitleData
{
	
	String name;
	int priority;
	float chance;//chance of being processed
	int weight;//chance against other candidates
	
	List<Requirement> requirements = new ArrayList<Requirement>();
	
	public TitleData (String name, int priority, float chance, int weight, Requirement... requirements)
	{
		this.name = name;
		this.priority = priority;
		this.chance = chance;
		this.weight = weight;
		for(Requirement req : requirements)
		{
			this.requirements.add(req);
		}
	}
	
	public boolean canApply(EntityLiving entity, LoeEntityLivingData data, Random rand)
	{
		if (chance<1.0f && rand.nextFloat() >= chance)
			return false;
		
		boolean flag = true;
		for (int i=0; i<requirements.size(); i++)
			if (!requirements.get(i).meets(entity, data))
				flag = false;
		return flag;
	}

}
