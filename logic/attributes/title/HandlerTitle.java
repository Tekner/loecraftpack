package loecraftpack.logic.attributes.title;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.util.StatCollector;

public class HandlerTitle {
	
	protected static List<TitleData> globalList = new ArrayList<TitleData>();
	protected static List<TitleData> monsterList = new ArrayList<TitleData>();
	protected static List<TitleData> passiveList = new ArrayList<TitleData>();
	
	public static String getTitle(EntityLiving entity, LoeEntityLivingData data, Random rand)
	{
		int priorityLevel = -1;
		int totalWeight = 0;
		String result = null;
		List<TitleData> candidates = new ArrayList<TitleData>();
		
		for (TitleData title : globalList)
		{
			if (title.priority>=priorityLevel && title.canApply(entity, data, rand))
			{
				if (priorityLevel < title.priority)
				{
					priorityLevel = title.priority;
					
					candidates = new ArrayList<TitleData>();
					totalWeight = 0;
				}
				candidates.add(title);
				totalWeight += title.weight;
			}
		}
		
		if (entity instanceof IMob)
			for (TitleData title : monsterList)
			{
				if (title.priority>=priorityLevel && title.canApply(entity, data, rand))
				{
					if (priorityLevel < title.priority)
					{
						priorityLevel = title.priority;
						
						candidates = new ArrayList<TitleData>();
						totalWeight = 0;
					}
					candidates.add(title);
					totalWeight += title.weight;
				}
			}
		
		if (entity instanceof IAnimals)
			for (TitleData title : passiveList)
			{
				if (title.priority>=priorityLevel && title.canApply(entity, data, rand))
				{
					if (priorityLevel < title.priority)
					{
						priorityLevel = title.priority;
						
						candidates = new ArrayList<TitleData>();
						totalWeight = 0;
					}
					candidates.add(title);
					totalWeight += title.weight;
				}
			}
		
		if (candidates.size()>0)
		{
			int roll = rand.nextInt(totalWeight);
			int step = 0;
			
			for (TitleData title : candidates)
			{
				if (roll<step+title.weight)
				{
					result = title.name;
					break;
				}
				step += title.weight;
			}
		}
		
		
		if (result != null)
		{
			if (result.contains("[Race]"))
			{
				String s = EntityList.getEntityString(entity);
				if (s==null)
					s = "generic";
				String translated = StatCollector.translateToLocal("entity." + s + ".name");
				if (translated.contentEquals("entity." + s + ".name"))
					translated = s;
				result = result.replace("[Race]", translated);
			}
		}
		
		return result;
	}
	
	static
	{
		//named titles
		passiveList.add(new TitleData("Speedy Gonzales", 20, 0.5f, 1, new RequirementRace(EntityChicken.class), new RequirementGrade("Super")));
		passiveList.add(new TitleData("Scootaloo"      , 20, 0.2f, 1, new RequirementRace(EntityChicken.class), new RequirementRank(0, 10)));
		
		//fancy titles
		monsterList.add(new TitleData("Ultimate [Race]", 50, 1.0f, 1, new RequirementGrade("Solo"), new RequirementRank(980, 999)));
		monsterList.add(new TitleData("King [Race]"    , 10, 1.0f, 1, new RequirementGrade("Solo"), new RequirementTalent("Leader")));
		monsterList.add(new TitleData("Rabble"         , 10, 1.0f, 1, new RequirementGrade("Minion"), new RequirementRank(0, 30)));
		
		//funny titles
		passiveList.add(new TitleData("FlutterBat"       , 50, 0.010f, 1, new RequirementRace(EntityBat.class)));
		monsterList.add(new TitleData("Suprise!!!!"      ,  5, 1.000f, 1, new RequirementRace(EntityCreeper.class), new RequirementGrade("Skirmisher")));
		globalList.add( new TitleData("Nameless Wanderer",  3, 0.005f, 1, (new RequirementTalent("Leader")).not()));
		passiveList.add(new TitleData("Boneless"         , 20, 0.200f, 1, new RequirementRace(EntityChicken.class)));
		
		
		//Generic titles - talent
		monsterList.add(new TitleData("Hunting [Race]"   , 2, 1.0f, 1, new RequirementTalent("Skirmisher")));
		monsterList.add(new TitleData("Unexpected [Race]", 2, 1.0f, 1, new RequirementTalent("Skirmisher")));
		
		monsterList.add(new TitleData("Speedy [Race]"    , 2, 1.0f, 1, new RequirementTalent("Speedy")));
		monsterList.add(new TitleData("Hyper [Race]"     , 2, 1.0f, 1, new RequirementTalent("Speedy")));
		
		monsterList.add(new TitleData("Noble [Race]"     , 2, 1.0f, 1, new RequirementTalent("Leader")));
		monsterList.add(new TitleData("Commanding [Race]", 2, 1.0f, 1, new RequirementTalent("Leader")));
		
		
		//Generic titles - grade
		monsterList.add(new TitleData("Wimpy"            , 1, 1.0f, 1, new RequirementGrade("Minion")));
		monsterList.add(new TitleData("Gimpy"            , 1, 1.0f, 1, new RequirementGrade("Minion")));
		monsterList.add(new TitleData("Stumpy"           , 1, 1.0f, 1, new RequirementGrade("Minion")));
		
		monsterList.add(new TitleData("Inspired [Race]"  , 1, 0.5f, 1, new RequirementGrade("Improved")));
		monsterList.add(new TitleData("Enhanced [Race]"  , 1, 0.5f, 1, new RequirementGrade("Improved")));
		
		monsterList.add(new TitleData("Dangerous [Race]" , 1, 0.5f, 1, new RequirementGrade("Brute")));
		monsterList.add(new TitleData("Brutish [Race]"   , 1, 0.5f, 1, new RequirementGrade("Brute")));
		
		monsterList.add(new TitleData("Master [Race]"    , 1, 1.0f, 1, new RequirementGrade("Solo")));
		monsterList.add(new TitleData("Massive [Race]"   , 1, 1.0f, 1, new RequirementGrade("Solo")));
		
		passiveList.add(new TitleData("Plump [Race]"     , 1, 0.5f, 1, new RequirementGrade("Healthy")));
		passiveList.add(new TitleData("Vigorous [Race]"  , 1, 0.5f, 1, new RequirementGrade("Healthy")));
		
		passiveList.add(new TitleData("Elusive [Race]"   , 1, 1.0f, 1, new RequirementGrade("Super")));
		passiveList.add(new TitleData("Skiddish [Race]"  , 1, 1.0f, 1, new RequirementGrade("Super")));
		
		
		//default title
		globalList.add(new TitleData("[Race]", 0, 1.0f, 1));
	}

}
