package loecraftpack.logic.attributes.title;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class AndSet extends Requirement
{
	List<Requirement> requirements = new ArrayList<Requirement>();
	
	public AndSet (Requirement... requirements)
	{
		for(Requirement req : requirements)
		{
			this.requirements.add(req);
		}
	}
	
	@Override
	public boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data) {
		boolean flag = false;
		for (int i=0; i<requirements.size(); i++)
			if (!requirements.get(i).meets(entity, data))
				flag = false;
		return flag;
	}
}
