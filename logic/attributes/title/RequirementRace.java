package loecraftpack.logic.attributes.title;

import loecraftpack.logic.attributes.LoeEntityLivingData;
import net.minecraft.entity.EntityLiving;

public class RequirementRace extends Requirement
{
	Class raceClass;
	
	RequirementRace(Class raceClass)
	{
		this.raceClass = raceClass;
	}
	
	@Override
	public boolean meetsSUB(EntityLiving entity, LoeEntityLivingData data) {
		return raceClass.isAssignableFrom(entity.getClass());
	}
	
}
