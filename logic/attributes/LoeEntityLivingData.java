package loecraftpack.logic.attributes;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class LoeEntityLivingData implements IExtendedEntityProperties {
	
	public int rank = 0;
	public String grade = "";
	public String talent = "";
	public String title = "";
	public int metaData = 0;
	public int exp = 0;
	/* 1: has been initialized
	 * 2: ignore XP attribute
	 */
	
	
	
	@Override
	public void saveNBTData(NBTTagCompound compound)
	{
		NBTTagCompound loeCompound = new NBTTagCompound();
		
		if (rank>0) loeCompound.setInteger("rank", rank);
		if (grade!="") loeCompound.setString("grade", grade);
		if (talent!="") loeCompound.setString("talent", talent);
		if (title!="") loeCompound.setString("title", title);
		loeCompound.setInteger("meta", metaData);
		loeCompound.setInteger("exp", exp);
		
		compound.setTag("LoeEntityLivingData", loeCompound);
	}
	
	@Override
	public void loadNBTData(NBTTagCompound compound)
	{
		NBTTagCompound loeCompound = compound.getCompoundTag("LoeEntityLivingData");
		
		rank = loeCompound.getInteger("rank");
		grade = loeCompound.getString("grade");
		talent = loeCompound.getString("talent");
		title = loeCompound.getString("title");
		metaData = loeCompound.getInteger("meta");
		exp = loeCompound.getInteger("exp");
	}
	
	@Override
	public void init(Entity entity, World world)
	{
	}
	
	public void clone(LoeEntityLivingData origin)
	{
		rank = origin.rank;
		grade = origin.grade;
		talent = origin.talent;
		title = origin.title;
		metaData = origin.metaData;
		exp = origin.exp;
	}
	
}
