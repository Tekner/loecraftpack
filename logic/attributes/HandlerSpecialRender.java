package loecraftpack.logic.attributes;

import java.util.HashMap;
import java.util.Map;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.IMob;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderLivingEvent;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/* Version: HandlerSpecialRender - forge passes offset variables in later version
 * vars were added on 12/25/2013
 */

@SideOnly(Side.CLIENT)
public class HandlerSpecialRender
{	
	static Map<String, ResourceLocation> gradeImages = new HashMap<String, ResourceLocation>();
	static Map<String, ResourceLocation> talentImages = new HashMap<String, ResourceLocation>();
	static ResourceLocation nameTagIcon = new ResourceLocation("textures/items/name_tag.png");
	static Minecraft mc;
	static Stats stats = null;
	
	static
	{
		gradeImages.put("Minion",new ResourceLocation("loecraftpack:misc/title/minion.png"));
		gradeImages.put("Improved",new ResourceLocation("loecraftpack:misc/title/improved.png"));
		gradeImages.put("Brute",new ResourceLocation("loecraftpack:misc/title/brute.png"));
		gradeImages.put("Solo",new ResourceLocation("loecraftpack:misc/title/solo.png"));
		
		talentImages.put("Skirmisher",new ResourceLocation("loecraftpack:misc/title/skirmisher.png"));
		talentImages.put("Speedy",new ResourceLocation("loecraftpack:misc/title/speedy.png"));
		talentImages.put("Leader",new ResourceLocation("loecraftpack:misc/title/leader.png"));
		
		gradeImages.put("Healthy",new ResourceLocation("loecraftpack:misc/title/healthy.png"));
		gradeImages.put("Super",new ResourceLocation("loecraftpack:misc/title/super.png"));
	}
	
	public HandlerSpecialRender()
	{
		mc = Minecraft.getMinecraft();
	}
	
	@SubscribeEvent
	public void renderInfo(RenderLivingEvent.Specials.Pre event)
	{
		RenderManager renderManager = RenderManager.instance;
		if (Minecraft.isGuiEnabled() && event.entity != renderManager.livingPlayer && !event.entity.isInvisibleToPlayer(Minecraft.getMinecraft().thePlayer) && event.entity.riddenByEntity == null)
		{
			if (HandlerAttribute.isUsableEntity(event.entity))
			{
				event.setCanceled(true);
				
				EntityLiving entityLiving = (EntityLiving)event.entity;
				if ( !(entityLiving.getAlwaysRenderNameTagForRender() || entityLiving == renderManager.field_147941_i))
					return;
				
				
				float f = 1.6F;
				float f1 = 0.016666668F * f;
				double d3 = event.entity.getDistanceSqToEntity(renderManager.livingPlayer);
				float f2 = event.entity.isSneaking()? RendererLivingEntity.NAME_TAG_RANGE_SNEAK: RendererLivingEntity.NAME_TAG_RANGE;
	
				if (d3 < (double)(f2 * f2))
				{
					///render Titles///
					
					//Version: remove this line
					double[] offset = getCameraOffset(event.entity);
					double offsetX = offset[0];//Version: event.x
					double offsetY = offset[1];//Version: event.y
					double offsetZ = offset[2];//Version: event.z
					
					Tessellator tessellator = Tessellator.instance;
					FontRenderer fontrenderer = renderManager.getFontRenderer();
					
					LoeEntityLivingData extendedData = (LoeEntityLivingData)entityLiving.getExtendedProperties("LoeEntityLiving");
					
					if ((extendedData.metaData&1)==0)
					{
						//wait for extended data packet
						return;
					}
					
					String gradeStr = extendedData.grade;
					String talentStr = extendedData.talent;
					String titleStr = extendedData.title;
					if (entityLiving.hasCustomNameTag())
						titleStr = entityLiving.getCustomNameTag();
					else if (titleStr.length()<1 && entityLiving instanceof IMob)
						titleStr = "+";
					
					ResourceLocation gradeBorder = gradeImages.get(gradeStr);
					ResourceLocation talentIcon = talentImages.get(talentStr);
					
					byte borderThinknessX = 2;
					byte borderThinknessY = 2;
					byte gradeEndCapX = 8;
					byte resolution = 3;
					byte gradeUmid = 12;
					
					byte stringXbuffer = (byte) (talentIcon!=null? 10: 1);
					
					//not including border
					int witdh = fontrenderer.getStringWidth(titleStr) + stringXbuffer;
					int witdhOffset = (witdh)/2;
					byte height = (byte) 9;
					byte offsetBarY = (byte) 0;
					
					float rankRatio = getEffectiveDifficulty(extendedData.rank);
					int titleColor = -1;
					if (entityLiving instanceof IMob)
					{
						//adjust title_color
						if(rankRatio <= 0.7f)
							titleColor = 404040;//gray
						else if (rankRatio < 1.4f)
							titleColor = 16777215;//white
						else if (rankRatio < 2.5f)
							titleColor = 65280;//green
						else if (rankRatio < 4.0f)
							titleColor = 16753920;//orange
						else
							titleColor = 16711680;//red
					}
					
					
					
					GL11.glPushMatrix();
					GL11.glTranslatef((float)offsetX + 0.0F, (float)offsetY + event.entity.height + 0.5F, (float)offsetZ);
					GL11.glNormal3f(0.0F, 1.0F, 0.0F);
					GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
					GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
					GL11.glScalef(-f1, -f1, f1);
					
					GL11.glDisable(GL11.GL_LIGHTING);
					GL11.glEnable(GL11.GL_BLEND);
					GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
					
					
					GL11.glDepthMask(false);
					GL11.glDisable(GL11.GL_DEPTH_TEST);
					
					//transparent background
					double minX = (double)(-witdhOffset);
					double minY = (double)(offsetBarY);
					double maxX = (double)(-witdhOffset + witdh);
					double maxY = (double)(offsetBarY + height);
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					tessellator.startDrawingQuads();
					tessellator.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
					tessellator.addVertex(minX, minY, 0.0D);
					tessellator.addVertex(minX, maxY, 0.0D);
					tessellator.addVertex(maxX, maxY, 0.0D);
					tessellator.addVertex(maxX, minY, 0.0D);
					tessellator.draw();
					GL11.glEnable(GL11.GL_TEXTURE_2D);
					
					//title
					fontrenderer.drawString(titleStr, -witdhOffset+stringXbuffer, offsetBarY+1, 553648127);
					GL11.glEnable(GL11.GL_DEPTH_TEST);
					GL11.glDepthMask(true);
					fontrenderer.drawString(titleStr, -witdhOffset+stringXbuffer, offsetBarY+1, titleColor);
					
					//grade border
					if (gradeBorder!=null)
					{
						
						double x1 = -witdhOffset-borderThinknessX;
						double x2 = x1 + gradeEndCapX;
						double x3 = witdh-witdhOffset+borderThinknessX-gradeEndCapX;
						double y = (double)(offsetBarY-borderThinknessY);
						
						byte height2 = (byte) (height+borderThinknessY*2);
						
						byte gradeEndCapXR = (byte) (gradeEndCapX*resolution);
						byte heightR = (byte) (height2*resolution);
						
						mc.renderEngine.bindTexture(gradeBorder);
						drawImage128x64(x1, y, (double)gradeEndCapX, (double)height2, 0, 0, gradeEndCapXR, heightR);
						if (witdh+borderThinknessX*2 > gradeEndCapX*2)
							drawImage128x64(x2, y, x3-x2, (double)height2, gradeEndCapXR, 0, gradeUmid, heightR);
						drawImage128x64(x3, y, (double)gradeEndCapX, (double)height2, gradeEndCapXR+gradeUmid, 0, gradeEndCapXR, heightR);
					}
					
					//talent icon
					if (talentIcon!=null)
					{
						mc.renderEngine.bindTexture(talentIcon);
						drawIcon((double)(-witdhOffset), (double)(offsetBarY), (double)9, (double)9);
					}
					
					//NameTag icon
					if (entityLiving.hasCustomNameTag())
					{
						mc.renderEngine.bindTexture(nameTagIcon);
						drawIcon2((double)(maxX), (double)(minY), (double)9, (double)9);
					}
					
					GL11.glEnable(GL11.GL_LIGHTING);
					GL11.glDisable(GL11.GL_BLEND);
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
					GL11.glPopMatrix();
				}
			}
		}
	}
	
	
	public static double[] getCameraOffset(Entity entity)
	{
		RenderManager renderManager = RenderManager.instance;
		
		double tpt = Minecraft.getMinecraft().timer.renderPartialTicks;
		double[] result = new double[3];
		result[0] = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * (double)tpt - renderManager.renderPosX;
		result[1] = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * (double)tpt - renderManager.renderPosY;
		result[2] = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * (double)tpt - renderManager.renderPosZ;
		return result;
	}
	
	public static float getEffectiveDifficulty(int mobRank)
	{
		//System.out.println("---");
		if (stats == null)
		{
			stats = (Stats) LoECraftPack.statHandler.stats.get(Minecraft.getMinecraft().thePlayer.getGameProfile().getId());
			if (stats == null)
				return 0;
		}
		
		float durabilityP = Stats.GetStatCurve(stats.getPonyLevel(), 2.25f, 9.25f, 1);
		float powerP = Stats.GetStatCurve(stats.getPonyLevel(), 1.75f, 7.25f, 1);
		
		float durabilityM = Stats.GetStatCurve(mobRank, 2.25f, 9.25f, 1);
		float powerM = Stats.GetStatCurve(mobRank, 1.75f, 7.25f, 1);
		
		float result = (durabilityM*powerM)/(durabilityP*powerP);
		
		//Debug: calc mob diff
		//System.out.println("diff "+mobRank+"|"+durabilityM+" "+powerM+"|"+durabilityP+" "+powerP+"|"+result);
		return result;
	}
	
	public static void drawIcon(double xPosition, double yPosition, double witdh, double height)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + height), 0.0D, 0.0D, 1.0D);
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + height), 0.0D, 1.0D, 1.0D);
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + 0), 0.0D, 1.0D, 0.0D);
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + 0), 0.0D, 0.0D, 0.0D);
		tessellator.draw();
	}
	
	//flip x, and slightly closer to screen
	public static void drawIcon2(double xPosition, double yPosition, double witdh, double height)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + height), -0.1D, 1.0D, 1.0D);
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + height), -0.1D, 0.0D, 1.0D);
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + 0), -0.1D, 0.0D, 0.0D);
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + 0), -0.1D, 1.0D, 0.0D);
		tessellator.draw();
	}
	
	//file size 128x64
	public static void drawImage128x64(double xPosition, double yPosition, double witdh, double height, int posU, int posV, int witdhU, int heightV)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		float f = 0.0078125F;
		float f1 = 0.015625F;
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + height), 0.0D, (double)((float)(posU + 0) * f), (double)((float)(posV + heightV) * f1));
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + height), 0.0D, (double)((float)(posU + witdhU) * f), (double)((float)(posV + heightV) * f1));
		tessellator.addVertexWithUV((double)(xPosition + witdh), (double)(yPosition + 0), 0.0D, (double)((float)(posU + witdhU) * f), (double)((float)(posV + 0) * f1));
		tessellator.addVertexWithUV((double)(xPosition + 0), (double)(yPosition + 0), 0.0D, (double)((float)(posU + 0) * f), (double)((float)(posV + 0) * f1));
		tessellator.draw();
	}
}
