package loecraftpack.logic.attributes;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;

public class ModifierSet {
	
	public final String name;
	public float xpMod;
	public int weight;
	public Object[] metaData = null;
	
	public final List<AttributeModifier> healthModifiers = new ArrayList<AttributeModifier>();
	public final List<AttributeModifier> attackModifiers = new ArrayList<AttributeModifier>();
	public final List<AttributeModifier> knockBModifiers = new ArrayList<AttributeModifier>();
	public final List<AttributeModifier> moveModifiers = new ArrayList<AttributeModifier>();
	public final List<AttributeModifier> senseModifiers = new ArrayList<AttributeModifier>();
	
	public ModifierSet(String name)
	{
		this.name = name;
	}
	
	public ModifierSet setDetails(int weight, float xpMod)
	{
		this.xpMod = xpMod;
		this.weight = weight;
		return this;
	}
	
	public ModifierSet addMetatData(Object[] metaData)
	{
		this.metaData = metaData;
		return this;
	}
	
	public ModifierSet addMaxHealthMod(AttributeModifier mod)
	{
		healthModifiers.add(mod);
		return this;
	}
	
	public ModifierSet addAttackDamageMod(AttributeModifier mod)
	{
		attackModifiers.add(mod);
		return this;
	}
	
	public ModifierSet addKnockBackMod(AttributeModifier mod)
	{
		knockBModifiers.add(mod);
		return this;
	}
	
	public ModifierSet addMoveSpeedMod(AttributeModifier mod)
	{
		moveModifiers.add(mod);
		return this;
	}
	
	public ModifierSet addFollowRangeMod(AttributeModifier mod)
	{
		senseModifiers.add(mod);
		return this;
	}
	
	public void applyModifiers(EntityLivingBase entity)
	{
		if (entity instanceof EntityLiving)
		{
			EntityLiving creature = (EntityLiving)entity;
			
			IAttributeInstance healthStat = creature.getEntityAttribute(SharedMonsterAttributes.maxHealth);
			IAttributeInstance attackStat = creature.getEntityAttribute(SharedMonsterAttributes.attackDamage);
			IAttributeInstance knockBStat = creature.getEntityAttribute(SharedMonsterAttributes.knockbackResistance);
			IAttributeInstance moveStat = creature.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
			IAttributeInstance senseStat = creature.getEntityAttribute(SharedMonsterAttributes.followRange);
			
			if (healthStat!=null)
				for (AttributeModifier mod :healthModifiers)
				{
					healthStat.removeModifier(mod);
					healthStat.applyModifier(mod);
				}
			if (attackStat!=null)
				for (AttributeModifier mod :attackModifiers)
				{
					attackStat.removeModifier(mod);
					attackStat.applyModifier(mod);
				}
			if (knockBStat!=null)
				for (AttributeModifier mod :knockBModifiers)
				{
					knockBStat.removeModifier(mod);
					knockBStat.applyModifier(mod);
				}
			if (moveStat!=null)
				for (AttributeModifier mod :moveModifiers)
				{
					moveStat.removeModifier(mod);
					moveStat.applyModifier(mod);
				}
			if (senseStat!=null)
				for (AttributeModifier mod :senseModifiers)
				{
					senseStat.removeModifier(mod);
					senseStat.applyModifier(mod);
				}
		}
	}
}
