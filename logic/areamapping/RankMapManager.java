package loecraftpack.logic.areamapping;

import loecraftpack.logic.attributes.HandlerAttribute;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

/**
 * this class handles compiling a layered Algorithm map for calculating ranks, and also can check for Zoning.
 */
public class RankMapManager
{	
	public static int getRankFromMap(EntityLiving creature)
	{
		return getRankFromMap(creature.worldObj, (int)creature.posX, (int)creature.posY, (int)creature.posZ);
	}
	
	public static int getRankFromMap(World world, int posX, int posY, int posZ)
	{
		ChunkCoordinates spawn = world.getSpawnPoint();
		float distance = MathHelper.sqrt_float(spawn.getDistanceSquared(posX, (int)spawn.posY/*ignore Y Coord*/, posZ));
		return MathHelper.clamp_int((int)((distance/HandlerAttribute.rankDistance) + SimplexNoise.noise(posX, posZ) * 5), 0, HandlerAttribute.maxRank);
	}
}
