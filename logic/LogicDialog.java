package loecraftpack.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import loecraftpack.LoECraftPack;
import loecraftpack.content.gui.GuiDialog;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.Minecraft;

import org.apache.commons.lang3.StringUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

enum Type {Dialog, Quest, Shop}

@SideOnly(Side.CLIENT)
public class LogicDialog
{
	public static String dialogTitle = "Error - No Title";
	public static String[] dialogText = {"Error - No Data"};
	public static String[] buttonText = new String[3];
	public static String[] buttons = new String[3];
	public static int index = 0; //Used to keep track of which message we're at.
	public static List<Message> messages = new ArrayList<Message>();
	private static boolean openDialog = false;
	
	private static final Pattern DialogPattern = Pattern.compile("~\\[(.*?)\\](.*)"),
            					 QuestPattern = Pattern.compile("~\\[(.*?)\\]`(.*?)`(.*?)`(.*)"),
            					 ShopPattern = Pattern.compile("~\\[(.*?)\\]``(.*?)`(.*?)`(.*)");
	private static final Matcher[] matcher = new Matcher[3];
	
	private static void SetMatchers(String msg)
	{
		matcher[0] = DialogPattern.matcher(msg);
        matcher[1] = QuestPattern.matcher(msg);
        matcher[2] = ShopPattern.matcher(msg);
	}
	
	private static boolean IsDialog()
	{
		m = matcher[0];
		return matcher[0].matches();
	}
	
	private static boolean IsQuest()
	{
		m = matcher[1];
		return matcher[1].matches();
	}
	
	private static boolean IsShop()
	{
		m = matcher[2];
		return matcher[2].matches();
	}
	
	private static void FixLastMessage()
	{
		FixLastMessage(false);
	}
	
	private static void FixLastMessage(boolean shop)
	{
		if (messages.size() > 1)
		{
			Message lastMessage = messages.get(messages.size() - 2);
			if (lastMessage.buttonText.length == 1 && lastMessage.buttonText[0].equals("Done") && lastMessage.buttons[0].endsWith("done"))
				lastMessage.SetNext(shop);
		}
	}
	
	public static Message getMessage()
	{
		if (messages == null || messages.size() == 0)
			return null;
		return messages.get(index);
	}
	
	private static Matcher m;
	public static void AddMessage(String msg)
	{
		SetMatchers(msg);
		if (IsDialog())
		{
			int count = StringUtils.countMatches(m.group(2), "`#");
			if (count == 0)
			{
				messages.add(new Message(m.group(0), m.group(1)));
				FixLastMessage();
			}
			else
			{
				String[] buttonText = new String[count];
				String[] buttons = new String[count];
				String[] lines = m.group(3).split("`");
				int i = 0;
				for(String line : lines)
				{
					if (i >= 3)
						break;
					else if (line.startsWith("#"))
					{
						String[] names = line.substring(1).split("\\|");
						buttonText[i] = names[0];
						buttons[i++] = names[1];
					}
				}
				
				messages.add(new Message(m.group(0), m.group(1), buttonText, buttons));
				FixLastMessage();
			}
		}
		else if (IsQuest())
		{
			messages.add(new Message(m.group(0) + ": " + m.group(1), m.group(2), m.group(3)));
			FixLastMessage();
			if (messages.size() == 1)
				LogicQuest.showQuest(messages.get(0));
		}
		else if (IsShop())
		{
			messages.add(new Message(m.group(1), m.group(2), m.group(3)));
			FixLastMessage(true);
			if (messages.size() == 1)
				LogicShop.showShop(messages.get(0));
		}
		
		if (Minecraft.getMinecraft().currentScreen == null && messages.size() == 1)
		{
			Minecraft.getMinecraft().thePlayer.openGui(LoECraftPack.instance, GuiIds.DIALOG.ordinal(), null, 0, 0, 0);
			ChangeMessage(false); //In this case (currentScreen == null), this is a shortcut to set up the static variables
		}
	}
	
	public static void ResetMessages()
	{
		messages.clear();
		index = 0;
	}
	
	public static void ChangeMessage(boolean forward)
	{
		if (forward)
			index++;
		else
			index--;
		
		if (index == -1)
			index = 0;
		else if (index >= messages.size())
			index = messages.size() - 1;
		
		dialogTitle = messages.get(index).dialogTitle;
		dialogText = messages.get(index).dialogText;
		buttonText = messages.get(index).buttonText;
		buttons = messages.get(index).buttons;
		
		if (Minecraft.getMinecraft().currentScreen instanceof GuiDialog)
			((GuiDialog)Minecraft.getMinecraft().currentScreen).changeButtons(buttonText, buttons);
	}
	
	public static String TranslateChatColor(String msg)
	{
		return msg.replaceAll("&((?i)[0-9a-fk-or])", "\u00A7$1");
	}
}
