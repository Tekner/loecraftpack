package loecraftpack.logic.helpers.inventoryaccess;

import net.minecraft.inventory.IInventory;
import net.minecraft.world.World;

/**because block container doesn't include this at default....*/
public interface IInventoryBlockAccess {
	
	public IInventory getInventory(World world, int xCoord, int yCoord, int zCoord);

}
