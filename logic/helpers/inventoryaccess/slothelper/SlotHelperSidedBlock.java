package loecraftpack.logic.helpers.inventoryaccess.slothelper;

import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;

public class SlotHelperSidedBlock extends SlotHelper
{
	public int[] getAccessableSlots()
	{
		return ((ISidedInventory)inventory).getAccessibleSlotsFromSide(side);
	}
	
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		return ((ISidedInventory)inventory).canInsertItem(slot, stack, side);
	}

}
