package loecraftpack.logic.helpers.inventoryaccess.slothelper;

import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;

public class SlotHelperNoReg extends SlotHelper
{
	public int[] getAccessableSlots()
	{
		int[] result = new int[inventory.getSizeInventory()];
		for (int i=0; i<result.length; i++)
			result[i] = i;
		return result;
	}
	
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		return inventory.isItemValidForSlot(slot, stack);
	}

}
