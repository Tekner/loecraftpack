package loecraftpack.logic.helpers.inventoryaccess.slothelper;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;

public abstract class SlotHelper
{
	IInventory inventory;
	int side;
	
	public static SlotHelper GetSlotHelper(IInventory inventory, int side)
	{
		SlotHelper helper;
		
		if (inventory instanceof ISidedInventory && side >= 0)
			helper = new SlotHelperSidedBlock();
		else
			helper = new SlotHelperNoReg();
		
		helper.inventory = inventory;
		helper.side = side;
		
		return helper;
	}
	
	public abstract int[] getAccessableSlots();
	
	public abstract boolean isItemValidForSlot(int slot, ItemStack stack);
}
