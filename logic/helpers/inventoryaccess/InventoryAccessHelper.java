package loecraftpack.logic.helpers.inventoryaccess;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.logic.helpers.inventoryaccess.slothelper.SlotHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class InventoryAccessHelper {
	
	
	  /*****************************************************************************************************/
	 /****** INVENTORY FINDERS ****************************************************************************/
	/*****************************************************************************************************/
	
	
	/**Returns the Inventory at the location (might not get the entire inventory; Ex: double chests from other mods)*/
	public static IInventory findInventory(World world, int xCoord, int yCoord, int zCoord)
	{
		IInventory inventory;
		
		//block containers
		inventory = findInventoryBlock(world, xCoord, yCoord, zCoord);
		if (inventory != null)
			return inventory;
		
		//unassigned? tile entity at location
		inventory = findInventoryTile(world, xCoord, yCoord, zCoord);
		if (inventory != null)
			return inventory;
		
		//entity chests
		inventory = findInventoryEntity(world, xCoord, yCoord, zCoord);
		if (inventory != null)
			return inventory;
		
		return null;
	}
	
	public static List<IInventory> findALLInventories(World world, int xCoord, int yCoord, int zCoord)
	{
		List<IInventory> inventories = new ArrayList<IInventory>();
		IInventory inventory;
		
		//block containers
		inventory = findInventoryBlock(world, xCoord, yCoord, zCoord);
		if (inventory != null)
			inventories.add(inventory);
		else
		{
			//unassigned? tile entity at location
			inventory = findInventoryTile(world, xCoord, yCoord, zCoord);
			if (inventory != null)
				inventories.add(inventory);
		}
		
		//entity chests
		inventories.addAll(findInventoryEntities(world, xCoord, yCoord, zCoord));
		
		
		if (inventories.size() > 0)
			return inventories;
		else
			return null;
	}
	
	
	/****** SUB METHODS *******************************************************************************/
	
	
	protected static IInventory findInventoryBlock(World world, int xCoord, int yCoord, int zCoord)
	{
		TileEntity tile = world.getTileEntity(xCoord, yCoord, zCoord);
		Block block = world.getBlock(xCoord, yCoord, zCoord);
		
		//vanilla Chests
		if (block instanceof BlockChest && tile != null && tile instanceof TileEntityChest)
		{
			return ((BlockChest)block).func_149951_m(world, xCoord, yCoord, zCoord);
		}
		//custom LoE storage (this function should have been included in the BlockContainer class)
		else if (block instanceof IInventoryBlockAccess)
		{
			return ((IInventoryBlockAccess)block).getInventory(world, xCoord, yCoord, zCoord);
		}
		
		return null;
	}
	
	protected static IInventory findInventoryTile(World world, int xCoord, int yCoord, int zCoord)
	{
		TileEntity tile = world.getTileEntity(xCoord, yCoord, zCoord);
		if (tile != null && tile instanceof IInventory)
		{
			return (IInventory)tile;
		}
		return null;
	}
	
	protected static IInventory findInventoryEntity(World world, int xCoord, int yCoord, int zCoord)
	{
		List list = findInventoryEntities(world, xCoord, yCoord, zCoord);
		
		if (list != null && list.size() > 0)
		{
			return (IInventory)list.get(world.rand.nextInt(list.size()));
		}
		
		return null;
	}
	
	protected static List<IInventory> findInventoryEntities(World world, int xCoord, int yCoord, int zCoord)
	{
		return world.getEntitiesWithinAABBExcludingEntity((Entity)null, 
				AxisAlignedBB.getAABBPool().getAABB((double)(xCoord), (double)(yCoord), (double)(zCoord),
													(double)(xCoord) + 1.0D, (double)(yCoord) + 1.0D, (double)(zCoord) + 1.0D), IEntitySelector.selectInventories);
	}
	
	
	
	
	
	  /*****************************************************************************************************/
	 /****** INVENTORY FILL *******************************************************************************/
	/*****************************************************************************************************/
	
	
	/**flood fill an entire list of stacks into an inventory (no regulations)
	 * @return the list of remaining item(stack)s that didn't get stored
	 */
	public static ItemStack[] dumpItemsIntoInventoryNoReg(IInventory inventory, ItemStack[] itemstacks)
	{
		return dumpItemsIntoInventory(inventory, itemstacks, -1);
	}
	
	/**flood fill an entire list of stacks into a block inventory
	 * @return the list of remaining item(stack)s that didn't get stored
	 */
	public static ItemStack[] dumpItemsIntoBlockInventory(IInventory inventory, ItemStack[] itemstacks, int side)
	{
		if (side < 0 || side >=6)
			return itemstacks;//invalid side
		
		return dumpItemsIntoInventory(inventory, itemstacks, side);
	}
	
	
	/****** SUB METHODS *******************************************************************************/
	
	
	/**flood fill an entire list of stacks into an inventory */
	protected static ItemStack[] dumpItemsIntoInventory(IInventory inventory, ItemStack[] itemstacks, int side)
	{
		//input validation and list assembly
		if (itemstacks == null || itemstacks.length <=0)
			return null;
		List<ItemStack> remaining = new ArrayList<ItemStack>();
		for (int index=0; index<itemstacks.length; index++)
		{
			if (itemstacks[index]!=null && itemstacks[index].stackSize>0)
			{
				remaining.add(itemstacks[index]);
			}
		}
		if (remaining.size()==0)
			return null;
		if (inventory == null)
			return remaining.toArray(new ItemStack[remaining.size()]);
		
		SlotHelper slotHelper = SlotHelper.GetSlotHelper(inventory, side);
		
		boolean invChange = false;
		int index;
		int slot;
		
		int[] accessableSlots = slotHelper.getAccessableSlots();
		
		//stack onto existing stacks, then null slots
		MasterLoop : for (int pass=0; pass<2; pass++)
			for (int slotI=0; slotI<accessableSlots.length; slotI++)
			{
				slot = accessableSlots[slotI];
				ItemStack targetStack = inventory.getStackInSlot(slot);
				if (pass==0? targetStack!=null: targetStack==null)
				{
					inputstacks : for (index=0; index<remaining.size(); index++)
					{
						//clean the list a bit
						while (remaining.get(index).stackSize<=0)
						{
							remaining.remove(index);
							if (remaining.size()==0)
								break MasterLoop;
							if (index==remaining.size())
								break inputstacks;
						}
						
						//attempt to insert a stack
						ItemStack stack = remaining.get(index);
						if (slotHelper.isItemValidForSlot(slot, stack))
						{
							if(attemptToAddToSlot(inventory, slot, stack))
								invChange = true;
						}
					}
				
				}
			}
		
		//clean list - final pass
		outputstacks : for (index=0; index<remaining.size(); index++)
		{
			while (remaining.get(index).stackSize<=0)
			{
				remaining.remove(index);
				if (index==remaining.size())
					break outputstacks;
			}
		}
		
		if (invChange)
			onInventoryChanged(inventory);
		
		if (remaining.size()>0)
			return remaining.toArray(new ItemStack[remaining.size()]);
		else
			return null;
	}
	
	
	
	
	  /*****************************************************************************************************/
	 /****** INVENTORY TRANSFER ***************************************************************************/
	/*****************************************************************************************************/
	
	//NOTE: these methods do not include the onInventoryChanged call.
	//      make sure to call the helper method once, afterwards, when one of the methods return true.
	
	
	public static boolean PutStackIntoInventoryNoReg(ItemStack stack, IInventory inventory)
	{
		return PutStackIntoInventory(stack, inventory, -1);
	}
	
	public static boolean PutStackIntoBlockInventory(ItemStack stack, IInventory inventory, int side)
	{
		if (side < 0 || side >=6)
			return false;
		
		return PutStackIntoInventory(stack, inventory, side);
	}
	
	protected static boolean PutStackIntoInventory(ItemStack stack, IInventory inventory, int side)
	{
		if (stack == null || inventory == null)
			return false;
		
		int size = stack.stackSize;
		
		SlotHelper slotHelper = SlotHelper.GetSlotHelper(inventory, side);
		
		int[] accesableSlots = slotHelper.getAccessableSlots();
		
		int slot;
		//stack onto existing stacks, then null slots
		for (int pass=0; pass<2; pass++)
			for (int slotI=0; slotI<accesableSlots.length; slotI++)
			{
				slot = accesableSlots[slotI];
				ItemStack targetStack = inventory.getStackInSlot(slot);
				if (pass==0? targetStack!=null: targetStack==null)
				{
					if (slotHelper.isItemValidForSlot(slot, stack))
					{
						if (attemptToAddToSlot(inventory, slot, stack) && stack.stackSize <= 0)
							return true;
					}
				}
			}
		
		return stack.stackSize != size;
	}
	
	
	
	
	  /**************************************************************************************************/
	 /****** SUPPORTING METHODS ************************************************************************/
	/**************************************************************************************************/
	
	
	public static void onInventoryChanged(IInventory inventory)
	{
		//if target was a hopper. update it's cooldown???
		if (inventory instanceof TileEntityHopper)
			((TileEntityHopper)inventory).func_145896_c(8);//setTransferCooldown
		
		inventory.markDirty();
	}
	
	protected static boolean attemptToAddToSlot(IInventory inventory, int slot, ItemStack sourceStack)
	{
		ItemStack targetStack = inventory.getStackInSlot(slot);
		
		boolean change = false;
		
		if (targetStack == null)
		{
			ItemStack copy = sourceStack.copy();
			inventory.setInventorySlotContents(slot, copy);
			sourceStack.stackSize -= copy.stackSize;
			change = true;
		}
		else if (areItemStacksEqualItem(targetStack, sourceStack))
		{
			int spaceAvailable = sourceStack.getMaxStackSize() - targetStack.stackSize;
			int ammountToTransfer = Math.min(sourceStack.stackSize, spaceAvailable);
			sourceStack.stackSize -= ammountToTransfer;
			targetStack.stackSize += ammountToTransfer;
			change = ammountToTransfer > 0;
		}
		
		return change;
	}
	
	protected static boolean areItemStacksEqualItem(ItemStack stack1, ItemStack stack2)
	{
		return stack1.getItem() != stack2.getItem() ? false : (stack1.getItemDamage() != stack2.getItemDamage() ? false : ItemStack.areItemStackTagsEqual(stack1, stack2));
	}
}
