package loecraftpack.logic.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import loecraftpack.logic.handlers.HandlerExtendedInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class UUIDHelper
{
	public static UUID applyUUIDtoStack(ItemStack stack)
	{
		UUID itemUUID = null;
		NBTTagCompound nbt;
		if (stack.hasTagCompound())
		{
			nbt = stack.getTagCompound();
		}
		else 
		{
			nbt = new NBTTagCompound();
			stack.setTagCompound(nbt);
		}
		
		
		if (!(nbt.hasKey("UUIDMostSig") && nbt.hasKey("UUIDLeastSig")))
		{
			itemUUID = UUID.randomUUID();
			nbt.setLong("UUIDMostSig", itemUUID.getMostSignificantBits());
			nbt.setLong("UUIDLeastSig", itemUUID.getLeastSignificantBits());
		}
		else
		{
			itemUUID = new UUID(nbt.getLong("UUIDMostSig"), nbt.getLong("UUIDLeastSig"));
		}
		
		return itemUUID;
	}
	
	public static ItemStack findUniqueItemStack(UUID itemUUID, EntityPlayer player)
	{
		//currently only detects our inventories
		return findUniqueItemStack(itemUUID, HandlerExtendedInventory.getAllInventories(player));
	}
	
	public static ItemStack findUniqueItemStack(UUID itemUUID, IInventory... inventories)
	{
		List<IInventory> invs = new ArrayList<IInventory>();
		for (int i=0; i<inventories.length; i++)
		{
			if (inventories[i] != null)
				invs.add(inventories[i]);
		}
		
		return findUniqueItemStack(itemUUID, invs);
	}
	
	public static ItemStack findUniqueItemStack(UUID itemUUID, List<IInventory> inventories)
	{
		if (itemUUID != null)
		{
			for (IInventory inventory: inventories)
			{
				if (inventory == null)
					continue;
				for (int i=0; i < inventory.getSizeInventory(); i++)
				{
					ItemStack itemStack = inventory.getStackInSlot(i);
					if (itemStack != null)
					{
						NBTTagCompound nbt = itemStack.getTagCompound();
						if (nbt != null && nbt.hasKey("UUIDMostSig") && nbt.hasKey("UUIDLeastSig"))
						{
							if (itemStack.getTagCompound().getLong("UUIDMostSig") == itemUUID.getMostSignificantBits() && itemStack.getTagCompound().getLong("UUIDLeastSig") == itemUUID.getLeastSignificantBits())
							{
								return itemStack;
							}
						}
					}
				}
			}
		}
		return null;
	}
}
