package loecraftpack.logic.helpers;

import java.util.Random;

import loecraftpack.content.entities.arrow.EntityCustomArrow;
import loecraftpack.content.entities.arrow.EntityIceArrow;
import loecraftpack.content.items.ICustomArrow;
import loecraftpack.content.items.ItemIceArrow;
import loecraftpack.content.items.ItemQuiver;
import loecraftpack.content.ponies.inventory.InventoryEquipment;
import loecraftpack.content.ponies.inventory.InventoryQuiver;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.referrance.enums.PlayerInventoryId;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;

public class ArrowHelper
{
	public static void LooseArrow(ArrowLooseEvent event)
	{
		EntityPlayer player = event.entityPlayer;
		Object[] searchResult = ArrowHelper.findAmmo(player);
		
		if (searchResult != null)
		{
			ItemStack ammo = (ItemStack)searchResult[0];
			InventoryQuiver invQ = (InventoryQuiver)searchResult[1];
			int slot = (Integer)searchResult[2];
			
			if (ammo != null)
			{
				event.setCanceled(true);//use an arrow that vanilla couldn't find
				
				ItemStack itemStackBow = event.bow;
				World world = player.worldObj;
				Random rand = new Random();
				boolean unlimited = player.capabilities.isCreativeMode || EnchantmentHelper.getEnchantmentLevel(Enchantment.infinity.effectId, itemStackBow) > 0;
				int charge = event.charge;
				
				float chargeLevel = (float)charge / 20.0F;
				chargeLevel = (chargeLevel * chargeLevel + chargeLevel * 2.0F) / 3.0F;
				
				if ((double)chargeLevel < 0.1D)
				{
					return;
				}
				
				if (chargeLevel > 1.0F)
				{
					chargeLevel = 1.0F;
				}
				if (ammo.getItem() instanceof ICustomArrow)
					fireCustom(((ICustomArrow)ammo.getItem()).createEntity(world, player, chargeLevel*2.0f), player, invQ, slot, itemStackBow, chargeLevel, ammo, unlimited);
				else
					fireVanilla(new EntityArrow(world, player, chargeLevel*2.0f), player, invQ, slot, itemStackBow, chargeLevel, ammo, unlimited);
			}
		}
	}
	
	
	public static void fireVanilla(EntityArrow entityarrow, EntityPlayer player, InventoryQuiver invQ, int slot, ItemStack itemStackBow, float chargeLevel, ItemStack ammo, boolean unlimited)
	{
		if (chargeLevel == 1.0F)
		{
			entityarrow.setIsCritical(true);
		}
		
		int enchPowerLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, itemStackBow);
		
		if (enchPowerLevel > 0)
		{
			entityarrow.setDamage(entityarrow.getDamage() + (double)enchPowerLevel * 0.5D + 0.5D);
		}
		
		int enchKnockbackLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, itemStackBow);
		
		if (enchKnockbackLevel > 0)
		{
			entityarrow.setKnockbackStrength(enchKnockbackLevel);
		}
		
		if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, itemStackBow) > 0)
		{
			entityarrow.setFire(100);
		}
		
		itemStackBow.damageItem(1, player);
		
		player.worldObj.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemStackBow.getItem().itemRand.nextFloat() * 0.4F + 1.2F) + chargeLevel * 0.5F);
		
		if (unlimited)
		{
			entityarrow.canBePickedUp = 2;
		}
		else
		{
			if (invQ != null)
				invQ.damageItemInSlot(slot, 1, player);
			else
				player.inventory.decrStackSize(slot, 1);
		}
		
		if (!player.worldObj.isRemote)
		{
			player.worldObj.spawnEntityInWorld(entityarrow);
		}
	}
	
	public static void fireCustom(EntityCustomArrow entityarrow, EntityPlayer player, InventoryQuiver invQ, int slot, ItemStack itemStackBow, float chargeLevel, ItemStack ammo, boolean unlimited)
	{
		if (chargeLevel == 1.0F)
		{
			entityarrow.setIsCritical(true);
		}
		
		int enchPowerLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, itemStackBow);
		
		if (enchPowerLevel > 0)
		{
			entityarrow.setDamage(entityarrow.getDamage() + (double)enchPowerLevel * 0.5D + 0.5D);
		}
		
		int enchKnockbackLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, itemStackBow);
		
		if (enchKnockbackLevel > 0)
		{
			entityarrow.setKnockbackStrength(enchKnockbackLevel);
		}
		
		if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, itemStackBow) > 0)
		{
			entityarrow.setFire(100);
		}
		
		itemStackBow.damageItem(1, player);
		
		player.worldObj.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemStackBow.getItem().itemRand.nextFloat() * 0.4F + 1.2F) + chargeLevel * 0.5F);
		
		if (unlimited)
		{
			entityarrow.canBePickedUp = 2;
		}
		else
		{
			if (invQ != null)
				invQ.damageItemInSlot(slot, 1, player);
			else
				player.inventory.decrStackSize(slot, 1);
		}
		
		if (!player.worldObj.isRemote)
		{
			player.worldObj.spawnEntityInWorld(entityarrow);
		}
	}


	public static ItemStack findQuiver(EntityPlayer player)
	{
		InventoryEquipment specialInv = (InventoryEquipment)HandlerExtendedInventory.getInventory(player, PlayerInventoryId.EQUIPMENT);
		if (specialInv != null)
		{
			for (int i=4; i<specialInv.getSizeInventory(); i++)
			{
				ItemStack itemStack = specialInv.getStackInSlot(i);
				if (itemStack != null)
				{
					if (itemStack.getItem() instanceof ItemQuiver)
						return itemStack;
				}
			}
		}
		
		for (int i=0; i < player.inventory.getSizeInventory(); i++)
		{
			ItemStack itemStack = player.inventory.getStackInSlot(i);
			if (itemStack != null)
			{
				if (itemStack.getItem() instanceof ItemQuiver)
					return itemStack;
			}
		}
		return null;
	}


	/**used to locate ammo in main inv, or ammo stored in quivers*/
	public static Object[] findAmmo(EntityPlayer player)
	{
		InventoryEquipment specialInv = (InventoryEquipment)HandlerExtendedInventory.getInventory(player, PlayerInventoryId.EQUIPMENT);
		if (specialInv != null)
		{
			for (int i=4; i<specialInv.getSizeInventory(); i++)
			{
				ItemStack itemStack = specialInv.getStackInSlot(i);
				if (itemStack != null)
				{
					if (itemStack.getItem() instanceof ItemQuiver)
					{
						Object[] ammoT = ArrowHelper.getAmmoFromQuiver(itemStack, player);
						if (ammoT != null)
							return ammoT;
					}
				}
			}
		}
		
		for (int slot=0; slot < player.inventory.getSizeInventory(); slot++)
		{
			ItemStack itemStack = player.inventory.getStackInSlot(slot);
			if (itemStack != null)
			{
				if (itemStack.getItem() instanceof ItemQuiver)
				{
					Object[] ammoT = ArrowHelper.getAmmoFromQuiver(itemStack, player);
					if (ammoT != null)
						return ammoT;
				}
				else if (itemStack.getItem() instanceof ICustomArrow || itemStack.getItem() == Items.arrow)
				{
					Object[] result = new Object[3];
					result[0] = itemStack;
					result[1] = null;
					result[2] = Integer.valueOf(slot);
					return result;
				}
			}
		}
		return null;
	}


	public static Object[] getAmmoFromQuiver(ItemStack quiver, EntityPlayer player)
	{
		Object[] result = new Object[3];
		if (quiver != null && quiver.getItem() instanceof ItemQuiver)
		{
			InventoryQuiver invTemp = new InventoryQuiver(quiver, player.worldObj.isRemote);
			int size = invTemp.getSizeInventory();
			for (int slot=0; slot<size; slot++)
			{
				ItemStack stack = invTemp.getStackInSlot(slot);
				if (stack != null)
				{
					System.out.println("ammo "+stack);
					result[0] = stack;
					result[1] = invTemp;
					result[2] = Integer.valueOf(slot);
					return result;
				}
			}
		}
		
		return null;
	}

}
