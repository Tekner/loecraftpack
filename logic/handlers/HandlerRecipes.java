package loecraftpack.logic.handlers;

import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.referrance.externalaccess.ExternalAccessHandler;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;

public class HandlerRecipes
{
	public static void init()
	{
		if (ExternalAccessHandler.isICPresent())
		{
			//HandlerIC2access.init();
		}
		
		if (ExternalAccessHandler.isNEIPresent())
		{
			//HandlerNEIaccess.init();
		}
	}
	
	
	public static void loadCommonRecipes()
	{
		//locate and remove old bed recipe
		HandlerColoredBed.cleanBedRecipe();
		
		//load all beds
		HandlerColoredBed.loadBeds();
		
		
		//Crystal Heart
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.crystalHeart, 1, 0), "XX", "XX",
												'X', new ItemStack(LOE_Items.gemStones, 1, 9));
		//Empty Bottle
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.emptyBottle, 1, 0), "GWG", "G G", "GGG",
												'G', new ItemStack(Item.getItemFromBlock(Blocks.glass), 1, 0), 'W', "plankWood"));
		//Zap Apple Jam
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.zapAppleJam, 1, 0), "ZZZ", "ZBZ", "ZZZ",
												'Z', new ItemStack(LOE_Items.zapApple, 1, 3), 'B', new ItemStack(LOE_Items.emptyBottle, 1, 0));
		
		//Big Apple
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.bigApple, 1, 0), "AA", "AA",
												'A', new ItemStack(Items.apple, 1, 0));
		
		//create planks from apple-bloom logs
		CraftingManager.getInstance().addRecipe(new ItemStack(Item.getItemFromBlock(Blocks.planks), 4, 0), new Object[] {"#",
												'#', new ItemStack(LOE_Blocks.appleBloomLog, 1, 0)});
		//cook zap apples
		for(int i = 0; i < 2; i++)
			FurnaceRecipes.smelting().func_151394_a(new ItemStack(LOE_Items.zapApple, 1, i), new ItemStack(LOE_Items.zapApple, 1, 2 + i), 1);
		
		//Apple Fritters
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.appleFritter, 2, 0),
				 										 new Object[] {Items.sugar, Items.wheat, Items.milk_bucket, Items.apple});
		
		//coffin
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Blocks.coffin, 1, 0), "SSS", "SCS", "SSS",
												'S', new ItemStack(Items.quartz, 1, 0), 'C', new ItemStack(Item.getItemFromBlock(Blocks.chest), 1, 0));
		
		//rainbow max
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.drink, 1, 3),
														 new Object[] {new ItemStack(LOE_Items.gemStones, 1, 14),
																	  Items.sugar, Items.sugar, Items.sugar, LOE_Items.emptyBottle});
		
		//gemstone cake
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Blocks.gemCake, 1, 0), "SRS", "SCS", "SSS",
				'R', new ItemStack(LOE_Items.gemStones, 1, 1), 'S', new ItemStack(LOE_Items.gemStones, 1, 0), 'C', new ItemStack(Items.cake, 1, 0));
		
		
		////tapestries
		
		//celly
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 0), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 0), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 1)));
		//luna
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 1), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 11), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 15)));
		//twi
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 2), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 2), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 10)));
		//rare
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 3), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 0), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 3)));
		//aj
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 4), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 2), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 4)));
		//pie
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 5), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 6), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 14)));
		//dash
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 6), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 3), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 14)));
		//shy
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 7), "S", "W", "w",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 4), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 6)));
		//--grand
		//faust
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.tapestry, 1, 8), "SSS", "WWW", "www",
				'S', "stickWood", 'W', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 0), 'w', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 14)));
		
		////masks
		
		//death
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.mask, 1, 0), "SSS", "MMM", "MMM",
				'S', new ItemStack(Items.string, 1, 0), 'M', new ItemStack(Items.bone, 1, 0));
		//happy
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.mask, 1, 1), "SSS", "MMM", "MMM",
				'S', new ItemStack(Items.string, 1, 0), 'M', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 5));
		//hungry
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.mask, 1, 2), "SSS", "MMM", "MMM",
				'S', new ItemStack(Items.string, 1, 0), 'M', new ItemStack(Items.rotten_flesh, 1, 0));
		//steve
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.mask, 1, 3), "SSS", "MMM", "MMM",
				'S', new ItemStack(Items.string, 1, 0), 'M', new ItemStack(Item.getItemFromBlock(Blocks.wool), 1, 6));
		
		
		//Gem pickaxe
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.pickaxeGem, 1, 0), "IDI", " s ", " s ",
				'I', Items.iron_ingot, 'D', Items.diamond, 's', Items.stick);
		
		//Dusts of Harmony
		for (int i=0; i<6; i++)
			CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ingredients, 3, i),
															 new Object[] {new ItemStack(LOE_Items.gemStones, 1, 10+i)});
		
		//harmony dust
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ingredients, 1, 6),
														 new Object[] {new ItemStack(LOE_Items.ingredients, 1, 0),
																	   new ItemStack(LOE_Items.ingredients, 1, 1),
																	   new ItemStack(LOE_Items.ingredients, 1, 2),
																	   new ItemStack(LOE_Items.ingredients, 1, 3),
																	   new ItemStack(LOE_Items.ingredients, 1, 4),
																	   new ItemStack(LOE_Items.ingredients, 1, 5)});
		
		//astral dust
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.ingredients, 1, 8), "HHH", "HDH", "HHH",
				'H', new ItemStack(LOE_Items.ingredients, 1, 6), 'D', "dustDiamond"));
		
		//Brewing Station
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Blocks.brewingStation, 1, 0), "LGG", "PPP",
				'L', "logWood", 'G', Item.getItemFromBlock(Blocks.glass), 'P', "plankWood"));
		
		//necklaces
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.necklace, 2, 0), "i i", "i i",
				" I ", 'i', Items.iron_ingot, 'I', Items.iron_ingot);
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.necklaceOfBling, 1, 0),
				   new Object[] {new ItemStack(LOE_Items.necklace, 1, 0),
								 new ItemStack(Items.gold_ingot, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 2),//citrine
								 new ItemStack(LOE_Items.gemStones, 1, 2),//citrine
								 new ItemStack(LOE_Items.gemStones, 1, 14)});//gem of loyalty
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.necklaceOfDreams, 1, 0),
				   new Object[] {new ItemStack(LOE_Items.necklace, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 4),//phantom amethyst
								 new ItemStack(LOE_Items.gemStones, 1, 4),//phantom amethyst
								 new ItemStack(LOE_Items.ingredients, 1, 4)});//dust of loyalty
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.alicornAmulet, 1, 0),
				   new Object[] {new ItemStack(LOE_Items.necklace, 1, 0),
								 new ItemStack(LOE_Items.ingredients, 1, 9)});//Astral Diamond
		
		//rings
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Items.ring, 8, 0), "III", "I I",
				"III", 'I', Items.iron_ingot);
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringPhantomArrow, 1, 0),
										   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
														 new ItemStack(LOE_Items.gemStones, 1, 4),//phantom amethyst
														 new ItemStack(LOE_Items.ingredients, 1, 3)});//dust of magic
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringLife, 1, 0),
										   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
														 new ItemStack(LOE_Items.gemStones, 1, 7),//onyx
														 new ItemStack(LOE_Items.ingredients, 1, 11),//tom
														 new ItemStack(Items.emerald, 1, 0),
														 new ItemStack(LOE_Items.ingredients, 1, 2)});//dust of kindness
		
		////Ring OF
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 0),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 15),
								 new ItemStack(LOE_Items.gemStones, 1, 15),
								 new ItemStack(LOE_Items.gemStones, 1, 15)});
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 1),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 13),
								 new ItemStack(LOE_Items.gemStones, 1, 13),
								 new ItemStack(LOE_Items.gemStones, 1, 13)});
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 2),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 14),
								 new ItemStack(LOE_Items.gemStones, 1, 14),
								 new ItemStack(LOE_Items.gemStones, 1, 14)});
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 3),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 10),
								 new ItemStack(LOE_Items.gemStones, 1, 10),
								 new ItemStack(LOE_Items.gemStones, 1, 10)});
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 4),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 12),
								 new ItemStack(LOE_Items.gemStones, 1, 12),
								 new ItemStack(LOE_Items.gemStones, 1, 12)});
		
		CraftingManager.getInstance().addShapelessRecipe(new ItemStack(LOE_Items.ringOf, 1, 5),
				   new Object[] {new ItemStack(LOE_Items.ring, 1, 0),
								 new ItemStack(LOE_Items.gemStones, 1, 11),
								 new ItemStack(LOE_Items.gemStones, 1, 11),
								 new ItemStack(LOE_Items.gemStones, 1, 11)});
		
		//clouds
		CraftingManager.getInstance().addRecipe(new ItemStack(LOE_Blocks.cloudThunder, 8, 0), "CCC", "CTC",
				"CCC", 'C', LOE_Blocks.cloud, 'T', new ItemStack(LOE_Items.throwableDrink, 1, 0));
	}
	
	
	
	public static void loadConditionalRecipes()
	{
		if (ExternalAccessHandler.isICPresent())
			ICpresent();
		else
			ICmissing();
		
	}
	
	protected static void ICpresent()
	{/*
		//Dusts of Harmony - via macerator (+1 dust)
		for (int i=0; i<6; i++)
			HandlerIC2access.createICRecipe_macerator(new ItemStack(LOE_Items.gemStones, 1, 10+i),
													  new ItemStack(LOE_Items.ingredients, 4, i));
		
		//Astral Diamond
		HandlerIC2access.createICRecipe_compressor(new ItemStack(LOE_Items.ingredients, 3, 8),
												   new ItemStack(LOE_Items.ingredients, 1, 9));
		
		//Astral Dust - backwards of diamond
		HandlerIC2access.createICRecipe_macerator(new ItemStack(LOE_Items.ingredients, 1, 9),
												  new ItemStack(LOE_Items.ingredients, 3, 8));
		
		//cheaper necklace recipe
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.necklace, 1, 0), "i i", "i i",
				" I ", 'i', "ironCable", 'I', Items.iron_ingot));
		
		//individual ring recipe
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.ring, 1, 0), " I ", "I I",
				" I ", 'I', "cableIron"));*/
	}
	
	protected static void ICmissing()
	{/*
		//diamond dust
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.ingredients, 1, 7), " O ", "ODO", " O ",
				'O', Item.getItemFromBlock(Blocks.obsidian, 'D', Item.diamond));
		
		//Astral Diamond
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.ingredients, 1, 9), "PSP", "DDD", "PSP",
				'P', Item.getItemFromBlock(Blocks.pistonBase, 'D', new ItemStack(LOE_Items.ingredients, 1, 8), 'S', Item.slimeBall));*/
	}
}
