package loecraftpack.logic.handlers.event;

import java.util.List;

import loecraftpack.content.items.accessories.ItemAccessory;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.inventory.InventoryCustom;
import loecraftpack.content.ponies.inventory.gui.TabManager;
import loecraftpack.logic.attributes.HandlerAttribute;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.logic.handlers.HandlerExtendedPotions;
import loecraftpack.referrance.enums.PlayerInventoryId;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.PlayerTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.RenderTickEvent;

public class HandlerTick
{
	//used for server ticks
	int autoEffectBufferS = 0;
	int autoEffectBufferC = 0;
	final int autoEffectBufferMax = 20;//one sec
	
	//randomly set falling value, that discerns when changingPlayerStat updates occur.
	int changingPlayerStatUpdateDelay = 100;
	
	@SubscribeEvent
	public void TickCommon(TickEvent event)
	{
		if(event.phase == TickEvent.Phase.START)
		{
			
		}
		else
		{
			////START ENDTICK////
			if (event.type == TickEvent.Type.SERVER)
			{
				//clean slime death record
				HandlerAttribute.cleanRecords();
			}
			////END ENDTICK////
		}
	}
	
	@SubscribeEvent
	public void TickPlayer(PlayerTickEvent event)
	{
		if(event.phase == TickEvent.Phase.START)
		{
			
		}
		else
		{
			////START ENDTICK////
			if (!event.player.worldObj.isRemote)//server
			{
				if(autoEffectBufferS++ >= autoEffectBufferMax)
				{
					autoEffectBufferS = 0;
					//apply auto effect for accessories
					InventoryCustom inv = HandlerExtendedInventory.getInventory(event.player, PlayerInventoryId.EQUIPMENT);
					List<Integer> accessorySlotIds = HandlerExtendedInventory.getAccessorySlotIds(inv);
					if (accessorySlotIds!=null)
					{
						for (Integer accessorySlotId : accessorySlotIds)
						{
							ItemStack accessory = inv.getStackInSlot(accessorySlotId);
							if (accessory != null)
								((ItemAccessory)accessory.getItem()).applyWornEffect(event.player, inv, accessorySlotId, accessory);
						}
					}
				}
				
				AbilityPlayerData data = AbilityPlayerData.Get(event.player.getGameProfile().getId());
				if (data != null)
				{
					data.onUpdateSERVER(event.player);
					
					if (--changingPlayerStatUpdateDelay == 0)
					{
						data.sendChangingPlayerStatPacket();
						changingPlayerStatUpdateDelay = 1000;
					}
				}
				
				HandlerExtendedInventory.informClientOfChanges(event.player);
			}
			else if (Minecraft.getMinecraft().thePlayer == event.player) //client
			{
				//clients player
				if (Minecraft.getMinecraft().thePlayer.getEntityId() == event.player.getEntityId())
				{
					//empty for now
				}//clients player
				
				
				AbilityPlayerData data = AbilityPlayerData.clientData;
				for(ActiveAbility ability : data.activeAbilities)
					ability.onUpdate(event.player);
				
				for(PassiveAbility ability : data.passiveAbilities)
					ability.onTick(event.player);
				
				if(autoEffectBufferC++ >= autoEffectBufferMax)
				{
					autoEffectBufferC = 0;
				}
				
				data.onUpdateCLIENT(event.player);
				data.fixRaceStats();
				
			}//client side
			
			////END ENDTICK////
		}
	}
	
	@SubscribeEvent
	public void TickRender (RenderTickEvent event)
	{
		if(event.phase == TickEvent.Phase.START)
		{
			
		}
		else
		{
			////START ENDTICK////
			if (event.type == TickEvent.Type.RENDER && Minecraft.getMinecraft().theWorld != null) 
			{
				GuiScreen gui = Minecraft.getMinecraft().currentScreen;
				if (!Minecraft.getMinecraft().gameSettings.hideGUI)
				{
					TabManager.applyTabs(gui);
					
					if (gui instanceof InventoryEffectRenderer)
					{
						HandlerExtendedPotions.renderExtendedPotionEffects((InventoryEffectRenderer)gui);
					}
				}
			}
			
			////END ENDTICK////
		}
	}
}
