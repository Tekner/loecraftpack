package loecraftpack.logic.handlers.event;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class HandlerKey
{
	public static String ID = "key.categories."+LoECraftPack.MODID;
	public static KeyBinding renderMonolithKeybind = new KeyBinding("RenderMonolith", Keyboard.KEY_F12, ID);
	public static KeyBinding modifierKey = new KeyBinding("TestFireball", Keyboard.KEY_LCONTROL, ID);
	public static KeyBinding testFireball = new KeyBinding("TestFireball", Keyboard.KEY_1, ID);
	public static KeyBinding testTeleport = new KeyBinding("TestTeleport", Keyboard.KEY_2, ID);
	//public static AdvancedKeyBinding displayTabs = new AdvancedKeyBinding("DisplayTabs", Keyboard.KEY_LCONTROL, ID);
	public static KeyBinding jump = Minecraft.getMinecraft().gameSettings.keyBindJump;
	public static KeyBinding sneak = Minecraft.getMinecraft().gameSettings.keyBindSneak;
	public static KeyBinding forward = Minecraft.getMinecraft().gameSettings.keyBindForward;
	public static KeyBinding back = Minecraft.getMinecraft().gameSettings.keyBindBack;
	public static KeyBinding left = Minecraft.getMinecraft().gameSettings.keyBindLeft;
	public static KeyBinding right = Minecraft.getMinecraft().gameSettings.keyBindRight;
	
	protected static Map<KeyBinding,KeyState> keyState;
	
	public enum KeyState
	{
		DOWN,
		HELD,
		UP,
		REST
	}
	
	public static boolean renderMonolith = false;
	
	public HandlerKey() {
		ClientRegistry.registerKeyBinding(renderMonolithKeybind);
		ClientRegistry.registerKeyBinding(modifierKey);
		ClientRegistry.registerKeyBinding(testFireball);
		ClientRegistry.registerKeyBinding(testTeleport);
		
		keyState = new HashMap<KeyBinding,KeyState>();
		mapKey(renderMonolithKeybind);
		mapKey(modifierKey);
		mapKey(testFireball);
		mapKey(testTeleport);
		mapKey(jump);
		mapKey(sneak);
		mapKey(forward);
		mapKey(back);
		mapKey(left);
		mapKey(right);
	}
	
	public static KeyState getKeyState(KeyBinding key)
	{
		return keyState.get(key);
	}
	
	public static boolean getKeyDown(KeyBinding key)
	{
		return keyState.get(key) == KeyState.DOWN;
	}

	public static boolean getKeyHeldDown(KeyBinding key)
	{
		return keyState.get(key) == KeyState.HELD;
	}

	public static boolean getKeyUp(KeyBinding key)
	{
		return keyState.get(key) == KeyState.UP;
	}
	
	public static boolean getKeyRest(KeyBinding key)
	{
		return keyState.get(key) == KeyState.REST;
	}

	@SubscribeEvent
	public void keyDown(InputEvent.KeyInputEvent event)
	{
		/*
		if (jump.isPressed())
		{
			EntityPlayer player = Minecraft.getMinecraft().thePlayer;
			if (LoECraftPack.statHandler.isRace(player, Race.EARTH) && player.motionY > 0)
				player.motionY *= 1.175f; //Adds an extra jump height of 1 block over time - stacks with jump boost potion effect
		}*/
		
		if (renderMonolithKeybind.getIsKeyPressed())
		{
			renderMonolith = !renderMonolith;
		}
		/*
		if (modifierKey.isPressed())
		{
			if (testFireball.isPressed())
				AbilityPlayerData.clientData.activeAbilities[ActiveAbility.getIdByName("Fireball")].castAbility(0.05f, true);
			if (testTeleport.isPressed())
				AbilityPlayerData.clientData.activeAbilities[ActiveAbility.getIdByName("Teleport")].castAbility(0.05f, true);
		}*/
		/*
		if (displayTabs.isPressed())
		{
			if (TabManager.inventoryOpen)
			{
				//turn inventory mode tabs  on/off
				TabManager.tabsDisplayed = !TabManager.tabsDisplayed;
			}
		}*/
	}
	
	@SubscribeEvent
	public void TickCommon(TickEvent event)
	{
		if (event.phase == TickEvent.Phase.START)
		{
			for (Entry<KeyBinding, KeyState> entry: keyState.entrySet())
			{
				if (entry.getKey().getIsKeyPressed())
				{
					if (entry.getValue().ordinal() > KeyState.HELD.ordinal())
					{
						entry.setValue(KeyState.DOWN);
					}
					else
					{
						entry.setValue(KeyState.HELD);
					}
				}
				else
				{
					if (entry.getValue().ordinal() < KeyState.UP.ordinal())
					{
						entry.setValue(KeyState.UP);
					}
					else
					{
						entry.setValue(KeyState.REST);
					}
					
				}
			}
		}
	}
	
	protected void mapKey(KeyBinding key)
	{
		keyState.put(key, KeyState.REST);
	}
}
