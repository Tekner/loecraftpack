package loecraftpack.logic.handlers.event;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.BlockProtectionMonolith;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.content.entities.EntityHarmonyTree;
import loecraftpack.content.entities.ai.EntityAIAvoidEntityLocation;
import loecraftpack.content.items.accessories.ItemAccessory;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.mechanics.PonyDodgedEvent;
import loecraftpack.content.ponies.inventory.SlotAccessory;
import loecraftpack.content.ponies.inventory.SlotAmmo;
import loecraftpack.content.ponies.inventory.gui.TabManager;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Enchantment;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.logic.LogicDialog;
import loecraftpack.logic.attributes.LoeEntityLivingData;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.logic.helpers.ArrowHelper;
import loecraftpack.network.packets.PacketMonolithUpdate;
import loecraftpack.proxies.ClientProxy;
import loecraftpack.referrance.enums.LivingEventId;
import net.minecraft.block.Block;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.BonemealEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class HandlerCommonEvent
{
	//tool for checking data values in the dev enviroment
	/*
	@ForgeSubscribe
	public void onExamineBlock(PlayerInteractEvent event)
	{
		if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK &&
				event.entityPlayer.getCurrentEquippedItem() != null &&
				event.entityPlayer.getCurrentEquippedItem().itemID == Item.leather.itemID)
		{
			int x = event.x, y = event.y, z = event.z;
			int id = event.entityPlayer.worldObj.getBlockId(x, y, z);
			int meta = event.entityPlayer.worldObj.getBlockMetadata(x, y, z);
			System.out.println("Block ID:"+id+"  Meta:"+meta);
		}
		
	}*/
	
	//creative mode tool that uses wool to set meta data. (has some legit purpose's)
	/*
	@ForgeSubscribe
	public void onSetMetaData(PlayerInteractEvent event)
	{
		if (event.action == PlayerInteractEvent.Action.LEFT_CLICK_BLOCK &&
				event.entityPlayer.capabilities.isCreativeMode &&
				event.entityPlayer.getCurrentEquippedItem() != null &&
				event.entityPlayer.getCurrentEquippedItem().itemID == Block.cloth.blockID)
		{
			int x = event.x, y = event.y, z = event.z;
			int id = event.entityPlayer.worldObj.getBlockId(x, y, z);
			int meta = event.entityPlayer.worldObj.getBlockMetadata(x, y, z);
			int newMeta = event.entityPlayer.getCurrentEquippedItem().getItemDamage();
			System.out.println("Block ID:"+id+"  Meta:"+meta+" new"+newMeta);
			event.entityPlayer.worldObj.setBlockMetadataWithNotify(x, y, z, newMeta, 3);
			event.setCanceled(true);
		}
		
	}
	*/
	
	
	
	
  /********************************************************************************************/
 /**  MONOLITH  ******************************************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void onMonolithInteract(PlayerInteractEvent event)
	{
		int x = event.x, y = event.y, z = event.z;
		if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK)
		{
			switch(event.face)
			{
				case 2:
					z --;
					break;
				case 3:
					z ++;
					break;
				case 4:
					x --;
					break;
				case 5:
					x ++;
					break;
			}
		}
		List<TileProtectionMonolith> list = BlockProtectionMonolith.monoliths.get(event.entityPlayer.worldObj.provider.dimensionId);
		
		if (list != null)
		{
			if (event.entityPlayer.worldObj.getBlock(x, y, z) != LOE_Blocks.monolith && list.contains(this))
				list.remove(this);
			
			for(TileProtectionMonolith te : list)
			{
				if (te.xCoord == event.x && te.yCoord == event.y && te.zCoord == event.z)
				{
					if (te.owners.size() > 0)
					{
						if (!te.owners.containsKey(event.entityPlayer.getGameProfile().getId()) && te.pointIsProtected(x, y, z))
						{
							if (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
							{
								if (!MinecraftServer.getServer().getConfigurationManager().getOps().contains(event.entityPlayer.getGameProfile().getId().toLowerCase().trim()))
									event.setCanceled(true);
							}
							else
								event.setCanceled(true);
							//event.entityPlayer.skinUrl = "http://skins.minecraft.net/MinecraftSkins/" + StringUtils.stripControlCodes("Tekner") + ".png";
							//Minecraft.getMinecraft().renderEngine.obtainImageData(event.entityPlayer.skinUrl, new ImageBufferDownload());
						}
					}
					else if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK && event.entityPlayer.getDistanceSq(te.xCoord, te.yCoord, te.zCoord) <= 100)
					{
						te.owners.put(event.entityPlayer.getGameProfile().getId(), event.entityPlayer.getGameProfile().getName());
					}
					
					if (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER)
					{
						Map<String, String> usernameMap = LoECraftPack.getPlayerID_UsernameMap();
						for (int i=0; i< te.owners.size(); i++)
						{
							String id = (String) te.owners.keySet().toArray()[i];
							String username = (String) te.owners.values().toArray()[i];
							if (usernameMap.containsKey(id) && usernameMap.get(id) != username)
							{
								//update username
								te.owners.put(id, username);
							}
						}
						
						//refresh Player's Client's tile entity
						LoECraftPack.packetHandler.sendTo(new PacketMonolithUpdate().INIT(event.x, event.y, event.z).setSize(te.width, te.length, te.offsetX, te.offsetZ).setOwners(te.owners), (EntityPlayerMP) event.entityPlayer);
					}
					
					break;
				}
			}
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  ABILITIES  *****************************************************************************/
/********************************************************************************************/
		
	@SubscribeEvent
	public void onSpawn(EntityJoinWorldEvent event)
	{
		// reconnect the proper EntityPlayer to the abilityData (for things like teleporting)
		if (event.entity instanceof EntityPlayer)
		{
			Stats stats = LoECraftPack.statHandler.stats.get(((EntityPlayer)event.entity).getGameProfile().getId());
			if (stats != null)
			{
				stats.abilityData.player = (EntityPlayer) event.entity;
				stats.abilityData.fixRaceStats();
			}
		}
	}
	
	private boolean PassEvent(LivingEvent event, boolean checkEntity)
	{
		if (!checkEntity || event.entityLiving instanceof EntityPlayer)
			return AbilityPlayerData.PassPassiveAbilityEvent(((EntityPlayer)event.entityLiving).getGameProfile().getId(), event);
		return false;
	}
	
	private boolean PassEvent(LivingEvent event)
	{
		return PassEvent(event, false);
	}
	
	@SubscribeEvent
	public void onAbilityInteract(PlayerInteractEvent event)
	{
		EntityPlayer player = event.entityPlayer;
	}
	
	@SubscribeEvent(priority=EventPriority.HIGHEST)
	public void FallEvent(LivingFallEvent event)
	{
		PassEvent(event, true);
	}
	
	@SubscribeEvent(priority=EventPriority.HIGHEST)
	public void HurtEvent(LivingHurtEvent event)
	{
		if (event.isCanceled())
			return;
		
		if (event.source.damageType.equals("player"))
		{
			Stats stats = (Stats)LoECraftPack.statHandler.stats.get(((EntityPlayer)event.source.getSourceOfDamage()).getGameProfile().getId());
			if (stats != null)
				event.ammount *= stats.getMeleeMultiplier();
		}
		else if (event.entityLiving instanceof EntityMob && event.source == DamageSource.fall && (event.ammount / (20 * (1 + ((LoeEntityLivingData)event.entityLiving.getExtendedProperties("LoeEntityLiving")).rank * 0.014d))) > 0.6f && (event.entityLiving.getHealth() / event.entityLiving.getMaxHealth()) >= 0.9f)
		{	
			event.entityLiving.recentlyHit = 0;
			event.entityLiving.setHealth(0);
			System.out.println("Killed mob to prevent spawn camping. (" + event.entityLiving.posX + ", " + event.entityLiving.posY + ", " + event.entityLiving.posZ + ")");
		}
		
		if (event.source instanceof EntityDamageSource && event.entityLiving instanceof EntityPlayer)
		{
			Stats stats = (Stats)LoECraftPack.statHandler.stats.get(((EntityPlayer)event.entityLiving).getGameProfile().getId());
			AbilityPlayerData playerData = AbilityPlayerData.Get(((EntityPlayer)event.entityLiving).getGameProfile().getId());
			float bonusEvasion = playerData.GetSharedFloat("evasion");
			if (Math.random() < stats.getEvasion() + bonusEvasion)
			{
				if (PassEvent(new PonyDodgedEvent(event)))
				{
					event.setCanceled(true);
					if (bonusEvasion > 0) //Wind Dance
					{
						playerData.addEnergy(-50, false);
						playerData.sendChangingPlayerStatPacket();
					}
				}
			}
		}
		PassEvent(event, true);
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void FoVEvent(FOVUpdateEvent event)
	{
		if (AbilityPlayerData.clientData != null && AbilityPlayerData.clientData.playerStats != null)
		{
			EntityPlayerSP player = event.entity;
			float speed = AbilityPlayerData.clientData.playerStats.getMoveSpeed();
			if (player.capabilities.getWalkSpeed() != speed)
			{
				player.capabilities.setPlayerWalkSpeed(speed);
			}
		}
	}
	
	@SubscribeEvent
	public void MineBlockEvent(BreakSpeed event)
	{
		if (AbilityPlayerData.HasPlayer(event.entityPlayer.getGameProfile().getId()))
		{
			Stats stats = AbilityPlayerData.Get(event.entityPlayer.getGameProfile().getId()).playerStats;
			if (stats != null)
				event.newSpeed *= stats.GetMiningSpeed();
		}
	}
	
	
	
	
  /*******************************************************************************************/
 /**  ABILITY ITEM  *************************************************************************/
/*******************************************************************************************/
	
	@SubscribeEvent
	public void onItemDrop(ItemTossEvent event)
	{
		Item item = event.entityItem.getEntityItem().getItem();
		if (item == LOE_Items.activeAbility || item == LOE_Items.passiveAbility)
			event.setCanceled(true);
	}
	
	
	
	
  /********************************************************************************************/
 /**  QUEST CHAT  ****************************************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void onChatReceived(ClientChatReceivedEvent event)
	{
		if (event.message.equals("npcchat"))
			event.setCanceled(true);
		else if (event.message.getFormattedText().length() > 0)
		{
			if (event.message.getFormattedText().startsWith("~"))
			{
				event.setCanceled(true);
				LogicDialog.AddMessage(event.message.getFormattedText());
			}
		}
	}
	
	
	
	
  /***********************************************************************************************/
 /**  BONE-MEAL ON CUSTOM SAPPLINGS  ************************************************************/
/***********************************************************************************************/
		
	@SubscribeEvent
	public void onBonemeal(BonemealEvent event)
	{
		if (!event.world.isRemote)
		{
			  /****************************/
			 /**Grow Apple-Bloom Sapling**/
			/****************************/
			
			if (Block.isEqualTo(event.block, LOE_Blocks.appleBloomSapling))
			{
				if ((double)event.world.rand.nextFloat() < 0.45D)
				{
					LOE_Blocks.appleBloomSapling.grow(event.world, event.x, event.y, event.z, event.world.rand);
				}
				event.setResult(Result.ALLOW);
			}
			
			  /**************************/
			 /**Grow Zap-Apple Sapling**/
			/**************************/
			
			if (Block.isEqualTo(event.block, LOE_Blocks.zapAppleSapling))
			{
				if ((double)event.world.rand.nextFloat() < 0.45D)
				{
					LOE_Blocks.zapAppleSapling.grow(event.world, event.x, event.y, event.z, event.world.rand);
				}
				event.setResult(Result.ALLOW);
			}
		}
	}
	
	
	  /************************/
	 /**Grow Harmony Sapling**/
	/************************/
	@SubscribeEvent
	public void onUseGemShard(PlayerInteractEvent event)
	{
		if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK &&
				event.entityPlayer.getCurrentEquippedItem() != null &&
				event.entityPlayer.getCurrentEquippedItem().getItem() == LOE_Items.ingredients)
		{
			int meta = event.entityPlayer.getCurrentEquippedItem().getItemDamage();
			World world = event.entityPlayer.worldObj;
			if (meta == 6/*harmony dust*/ && Block.isEqualTo(world.getBlock(event.x, event.y, event.z), LOE_Blocks.harmonySapling))
			{
				if (!world.isRemote)
				{
					world.playAuxSFX(2005, event.x, event.y, event.z, 0);
					if (!event.entityPlayer.capabilities.isCreativeMode)
						event.entityPlayer.getCurrentEquippedItem().stackSize--;
				}
				LOE_Blocks.harmonySapling.grow(world, event.x, event.y, event.z, world.rand);
			}
		}
		
	}
	
	
	
	
  /********************************************************************************************/
 /**  RENDERS  *******************************************************************************/
/********************************************************************************************/
	
		  /****************************/
		 /**Render Hidden Ore Vision**/
		/****************************/
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onfinalRender(RenderWorldLastEvent event)
	{
		ClientProxy.renderHiddenOre.drawBlockPhantomTexture(event);
	}
	
		  /******************/
		 /**Render MASK*****/
		/******************/
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onSpecialRender(RenderPlayerEvent.Specials.Pre event)
	{
		float f1 = 1.0F;
		GL11.glColor3f(f1, f1, f1);
		ItemStack itemstack = event.entityPlayer.inventory.armorItemInSlot(3);

		if (itemstack != null && event.renderHelmet)
		{
			GL11.glPushMatrix();
			event.renderer.modelBipedMain.bipedHead.postRender(0.0625F);
			if (itemstack.getItem() == LOE_Items.mask)
			{
				ClientProxy.renderMask.doRenderItemMask(itemstack, -0.0F, -0.25F, -0.2F, 1, 180.0F);
			}

			GL11.glPopMatrix();
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  GUI LOAD  ******************************************************************************/
/********************************************************************************************/
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onOpenGui(GuiOpenEvent event)
	{
		if (event.gui instanceof GuiInventory || event.gui instanceof GuiContainerCreative)
		{
			TabManager.currentTabID = 0;
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  CUSTOM ARROWS  *************************************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void onUseCustomArrows(ArrowLooseEvent event)
	{
		ArrowHelper.LooseArrow(event);
	}
	
	
	
	
  /********************************************************************************************/
 /**  Player Drops Inventory EVENT  **********************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void onPlayerLoot(PlayerDropsEvent event)
	{
		HandlerExtendedInventory.dropAllitems(event.entityPlayer);
	}
	
	
	
	
  /********************************************************************************************/
 /**  Enchantment Effect EVENT  **************************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void onAttackEchantmentEffect(LivingAttackEvent event)
	{
		Entity sourceEntity = event.source.getEntity();
		if (sourceEntity!= null && event.entityLiving != null)
		{
			int entityID = EntityList.getEntityID(event.entityLiving);
			//System.out.println("attack from "+sourceEntity);
			//System.out.println("target id"+IDEntity);
			
			EntityPlayer attackingPlayer = null;//if any
			ItemStack tool = null;
			
			if (sourceEntity instanceof EntityLivingBase)
			{
				if (sourceEntity instanceof EntityPlayer)
				{
					attackingPlayer = (EntityPlayer)sourceEntity;
					tool = attackingPlayer.getHeldItem();
				}
			}
			
			if (tool != null)
			{
				//System.out.println("The tool is "+tool.getItemName());
				
				  /****************************************/
				 /**Handle the effects of each type here**/
				/****************************************/
				
				int electricLevel = EnchantmentHelper.getEnchantmentLevel(LOE_Enchantment.enchantElectric.effectId, tool);
				//System.out.println("electric "+electricLevel);
				
				if (electricLevel > 0)
				{
					switch (entityID)
					{
					case 50://Creeper
						event.entityLiving.getDataWatcher().updateObject(17, Byte.valueOf((byte)1));
						break;
						
					case 90://pig
						if (!event.entityLiving.worldObj.isRemote)
						{
							EntityLivingBase pig = event.entityLiving;
							EntityPigZombie entitypigzombie = new EntityPigZombie(pig.worldObj);
							entitypigzombie.setLocationAndAngles(pig.posX, pig.posY, pig.posZ, pig.rotationYaw, pig.rotationPitch);
							pig.worldObj.spawnEntityInWorld(entitypigzombie);
							pig.setDead();
						}
						break;
					}
				}
			}
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  TEXTURE EVENT  *************************************************************************/
/********************************************************************************************/
	//accessory
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onItemTextureStitch(TextureStitchEvent.Pre event)
	{
		if (event.map.getTextureType()==1)
		{
			SlotAccessory.registerSlotIcons(event.map);
			SlotAmmo.registerSlotIcons(event.map);
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  ACCESSORY EVENT  ***********************************************************************/
/********************************************************************************************/
	
		  /*********/
		 /**Death**/
		/*********/
	@SubscribeEvent(priority=EventPriority.HIGHEST)
	public void onDeathPreEvent(LivingDeathEvent event)
	{
		ItemAccessory.applyLivingEvent(event, LivingEventId.LIVING_DEATH_PRE);
	}
	
	@SubscribeEvent
	public void onDeathEvent(LivingDeathEvent event)
	{
		ItemAccessory.applyLivingEvent(event, LivingEventId.LIVING_DEATH);
	}
	
		  /*********/
		 /**Sleep**/
		/*********/
	@SubscribeEvent
	public void onSleepEvent(PlayerSleepInBedEvent event)
	{
		ItemAccessory.applyLivingEvent(event, LivingEventId.PLAYER_SLEEP_IN_BED);
	}
	
		  /***************/
		 /**Loose Arrow**/
		/***************/
	@SubscribeEvent
	public void onArrowLoose(ArrowLooseEvent event)
	{
		ItemAccessory.applyLivingEvent(event, LivingEventId.ARROW_LOOSE);
	}
	
	
	
	
  /********************************************************************************************/
 /**  Potion Effects  ************************************************************************/
/********************************************************************************************/
	@SubscribeEvent(priority=EventPriority.LOWEST)
	public void onJump(LivingJumpEvent event)
	{
		if (event.entityLiving.isPotionActive(LoECraftPack.potionPetrified))
		{
			if (event.entityLiving.getActivePotionEffect(LoECraftPack.potionPetrified).getAmplifier()==0)
			{
				event.entityLiving.motionY *= 0.5;
			}
			else
			{
				event.entityLiving.motionY = 0;
			}
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  AI EVENT  ******************************************************************************/
/********************************************************************************************/
	
	@SubscribeEvent
	public void updateAI(EntityJoinWorldEvent event)
	{
		if (event.entity instanceof EntityCreature && event.entity instanceof IMob)
		{
			EntityCreature creature =(EntityCreature)event.entity;
			
			for (Object obj : creature.tasks.taskEntries)
			{
				EntityAIBase task = (EntityAIBase)((EntityAITaskEntry)obj).action;
				if (task instanceof EntityAIAvoidEntityLocation && ((EntityAIAvoidEntityLocation)task).targetEntityClass == EntityHarmonyTree.class)
					return;
			}
			
			creature.tasks.addTask(1, new EntityAIAvoidEntityLocation(creature, EntityHarmonyTree.class, 50.0F, 1.5D, 2.0D));
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  TEST CODE  *****************************************************************************/
/********************************************************************************************/
	
		  /***********************/
		 /**Ore Generation Info**/
		/***********************/
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		EntityPlayer player = event.entityPlayer;
		
		if (player.getHeldItem() != null)
		{
			if (player.getHeldItem().getItem() == LOE_Items.pickaxeGem)
			{
				if (event.action == Action.RIGHT_CLICK_BLOCK)
				{
					//Testing code: ore generation
					int countR = 0;
					int countC = 0;
					Chunk chunk = player.worldObj.getChunkFromChunkCoords(player.chunkCoordX, player.chunkCoordZ);
					for (int y = 0; y<256; y++)
					{
						for (int x = 0; x<16; x++)
						{
							for (int z = 0; z<16; z++)
							{
								if (Block.isEqualTo(chunk.getBlock(x, y, z), LOE_Blocks.gemOre))
								{
									if (chunk.getBlockMetadata(x, y, z)>7)
										countR++;
									else
										countC++;
								}
							}
						}
					}
					System.out.println("common ores: "+countC);
					System.out.println("rare   ores: "+countR);
				}
			}
		}
	}
	
	  /********************/
	 /**Check Protection**/
	/********************/
	@SubscribeEvent
	public void onBucket(FillBucketEvent event)
	{
		int x = event.target.blockX, y = event.target.blockY, z = event.target.blockZ;
		
		if (event.target.typeOfHit.ordinal() == 0)
		{
			switch(event.target.sideHit)
			{
				case 2:
					z --;
					break;
				case 3:
					z ++;
					break;
				case 4:
					x --;
					break;
				case 5:
					x ++;
					break;
			}
		}

		List<TileProtectionMonolith> list = BlockProtectionMonolith.monoliths.get(event.entityPlayer.worldObj.provider.dimensionId);
		if (list != null)
		for(TileProtectionMonolith te : list)
		{
			if (te.owners.size() > 0)
			{
				if (!te.owners.containsKey(event.entityPlayer.getGameProfile().getId()) && te.pointIsProtected(x, y, z))
					event.setCanceled(true);
			}
		}
	}
}
