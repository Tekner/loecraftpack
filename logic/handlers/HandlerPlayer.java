package loecraftpack.logic.handlers;

import loecraftpack.LoECraftPack;
import loecraftpack.content.items.accessories.ItemAccessory;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.referrance.enums.LivingEventId;
import net.minecraftforge.event.entity.living.LivingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;


public class HandlerPlayer
{
	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent event)
	{
		System.out.println("LOAD-------------------------------------------------------------------------------------------");
		//load Stats
		LoECraftPack.statHandler.addPlayer(event.player);
		//send clients packets
		
		//accessory event
		ItemAccessory.applyLivingEvent(new LivingEvent(event.player), LivingEventId.PLAYER_JOIN);
	}

	@SubscribeEvent
	public void onPlayerLogout(PlayerLoggedOutEvent event)
	{
		System.out.println("SAVE-------------------------------------------------------------------------------------------");
		//save Stats
		LoECraftPack.statHandler.savePlayer(event.player);
		
		//accessory event
		ItemAccessory.applyLivingEvent(new LivingEvent(event.player), LivingEventId.PLAYER_LEAVE);
	}

	@SubscribeEvent
	public void onPlayerChangedDimension(PlayerChangedDimensionEvent event)
	{
	}

	@SubscribeEvent
	public void onPlayerRespawn(PlayerRespawnEvent event)
	{
		AbilityPlayerData data = AbilityPlayerData.Get(event.player.getGameProfile().getId());
		data.setPlayer(event.player);
		data.fixRaceStats();
	}
}
