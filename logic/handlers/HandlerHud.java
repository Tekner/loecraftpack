package loecraftpack.logic.handlers;

import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.RenderHotBarOverlay;
import loecraftpack.content.ponies.abilities.passive.AbilityMagicBarrier;
import loecraftpack.content.ponies.stats.components.StatFlags;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.GuiIngameForge;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class HandlerHud extends GuiIngame
{
	ResourceLocation healthLoc = new ResourceLocation("loecraftpack:gui/overlay.png");
	
	public HandlerHud(Minecraft mc)
	{
		super(mc);
	}

	@SubscribeEvent
	public void PreOverlayEvent(RenderGameOverlayEvent.Pre event)
	{
		if (event.type == ElementType.HEALTH)
		{
			if (AbilityPlayerData.clientData != null && AbilityPlayerData.clientData.player != null)
			{
				event.setCanceled(true);
				bind(healthLoc);
				mc.mcProfiler.startSection("health");
				int left = event.resolution.getScaledWidth() / 2 - 91,
					top = event.resolution.getScaledHeight() - 39;
				float hpLeft = 10*(mc.thePlayer.getHealth()/mc.thePlayer.getMaxHealth()),
					  absorb = 10*(mc.thePlayer.getAbsorptionAmount()/AbilityMagicBarrier.getMaxShieldAmount(AbilityPlayerData.clientData.playerStats.getStatTotal(StatFlags.Alignment)));
				int offset = 0;
				if (mc.thePlayer.isPotionActive(Potion.poison))      offset = 10;
				else if (mc.thePlayer.isPotionActive(Potion.wither)) offset = 20;
				for(int i = 0; i < 10; i++)
				{
					drawTexturedModalRect(left + i*8, top, 0, 50, 9, 9);
					
					if (hpLeft - i >= 1)
						drawTexturedModalRect(left + i*8, top, offset, 40, 9, 9);
					else if (hpLeft > i)
						drawTexturedModalRect(left + i*8 + 1, top + 1 + offset, 1, 41, (int)((hpLeft%1)*7), 7);
				}
				for(int i = 0; i < 10; i++)
				{
					if (absorb - i >= 1)
						drawTexturedModalRect(left + i*8, top, 10, 50, 9, 9);
					else if (absorb > i)
						drawTexturedModalRect(left + i*8, top, 10, 50, (int)((absorb%1)*9), 9);
				}
				mc.mcProfiler.endSection();
			}
			GuiIngameForge.left_height += 10;
		}
		else if (event.type == ElementType.ARMOR)
			bind(icons);
	}
	
	@SubscribeEvent
	public void PostOverlayEvent(RenderGameOverlayEvent.Post event)
	{
		if (event.type == ElementType.HOTBAR)
			RenderHotBarOverlay.instance.renderHotBarOverlay(null, new Object[]{});
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	}
	
	private void bind(ResourceLocation res)
	{
		mc.getTextureManager().bindTexture(res);
	}
}
