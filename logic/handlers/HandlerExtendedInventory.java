package loecraftpack.logic.handlers;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.items.accessories.ItemAccessory;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.inventory.InventoryCustom;
import loecraftpack.content.ponies.inventory.InventoryEarth;
import loecraftpack.content.ponies.inventory.InventoryEquipment;
import loecraftpack.content.ponies.inventory.LoePlayerInventory;
import loecraftpack.referrance.enums.GuiIds;
import loecraftpack.referrance.enums.PlayerInventoryId;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.EntityEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class HandlerExtendedInventory
{
	public static InventoryEquipment ClientEquipment = null;
	public static InventoryEarth ClientExtended = null;
	
	/**
	 * gets the player's custom inventory
	 */
	public static InventoryCustom getInventory(EntityPlayer player, PlayerInventoryId id)
	{
		if(player.worldObj.isRemote)
		{
			switch (id)
			{
			case EQUIPMENT:
				if (ClientEquipment == null)
					ClientEquipment = new InventoryEquipment(player.getGameProfile().getId(), true);
				return ClientEquipment;
				
			case EARTH_PONY:
				if (ClientExtended == null)
					ClientExtended = new InventoryEarth();
				return ClientExtended;
				
			default:
				return null;
			}
		}
		else
		{
			LoePlayerInventory inv = (LoePlayerInventory)player.getExtendedProperties("LoePlayerInv");
			
			if (inv == null)
			{
				inv = new LoePlayerInventory();
			}
			
			switch (id)
			{
			case EQUIPMENT:
				return inv.getInvEquipment();
				
			case EARTH_PONY:
				return inv.getInvEarth();
				
			default:
				return null;
			}
		}
	}
	
	public static List<IInventory> getAllInventories(EntityPlayer player)
	{
		List<IInventory> result = new ArrayList<IInventory>();
		
		result.add(player.inventory);
		result.add(getInventory(player, PlayerInventoryId.EQUIPMENT));
		result.add(getInventory(player, PlayerInventoryId.EARTH_PONY));
		
		return result;
	}
	
	public static void dropAllitems(EntityPlayer player)
	{
		InventoryCustom inv;
		
		inv = getInventory(player, PlayerInventoryId.EQUIPMENT);
		if (inv != null) inv.dropAllItems(player);
		
		if (LoECraftPack.statHandler.isRace(player, Race.EARTH))
		{
			inv = getInventory(player, PlayerInventoryId.EARTH_PONY);
			if (inv != null) inv.dropAllItems(player);
		}
	}
	
	public static boolean canUseInv(EntityPlayer player, GuiIds newId)
	{
		switch (newId)
		{
		case MAIN_INV:
		case CREATIVE_INV:
		case EQUIPMENT_INV:
		case ABILITY_INV:
		case STATS:
			return true;
			
		case EARTH_INV:
			return (LoECraftPack.statHandler.isRace(player, Race.EARTH));
			
		default:
			return false;
		}
	}
	
	/**
	 * get the inventory position of any existing Accessories
	 */
	public static List<Integer> getAccessorySlotIds(InventoryCustom inv)
	{
		List<Integer> accessorySlotIds = new ArrayList<Integer>();
		ItemStack targetItem;
		for(int i=0; i<inv.getSizeInventory(); i++)
		{
			targetItem = inv.getStackInSlot(i);
			if (targetItem != null && (targetItem.getItem() instanceof ItemAccessory))
				accessorySlotIds.add(i);
		}
		if (accessorySlotIds.size()>0)
			return accessorySlotIds;
		return null;
	}
	
	
	@SubscribeEvent
	public void bindCustomINV(EntityEvent.EntityConstructing event)
	{
		if (event == null || event.entity == null || event.entity.worldObj == null || event.entity.worldObj.isRemote)
			return;
		
		if (event.entity instanceof EntityPlayer && event.entity.getExtendedProperties("LoePlayerInv")==null)
		{
			//Debug: Inv  (used to confirm that the player's inventory is being constructed)
			System.out.println("----BOOT PLAYER INV");
			event.entity.registerExtendedProperties("LoePlayerInv", new LoePlayerInventory());
		}
	}
	
	public static void informClientOfChanges(EntityPlayer player)
	{
		if (LoECraftPack.isSinglePlayer())
			return;
		
		InventoryCustom inv = getInventory(player, PlayerInventoryId.EQUIPMENT);
		if (inv.dirty)
		{
			inv.informClientOfChanges(player);
			inv.dirty = false;
			
			AbilityPlayerData.Get(player.getGameProfile().getId()).fixRaceStats();
		}
		//TODO: look into this
		/*
		inv = getInventory(player, PlayerInventoryId.EARTH_PONY);
		if (inv.inventoryChanged)
		{
			inv.informClientOfChanges(player);
			inv.inventoryChanged = false;
		}*/
	}
}
