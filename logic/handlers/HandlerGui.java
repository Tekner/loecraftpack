package loecraftpack.logic.handlers;

import loecraftpack.content.blocks.gui.ContainerPotionStation;
import loecraftpack.content.blocks.gui.ContainerProjectTable;
import loecraftpack.content.blocks.gui.GuiBank;
import loecraftpack.content.blocks.gui.GuiBrewingStation;
import loecraftpack.content.blocks.gui.GuiProjectTable;
import loecraftpack.content.blocks.gui.GuiProtectionMonolith;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.blocks.tile.TileProjectTable;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.content.gui.ContainerQuiver;
import loecraftpack.content.gui.GuiDialog;
import loecraftpack.content.gui.GuiQuest;
import loecraftpack.content.gui.GuiQuiver;
import loecraftpack.content.gui.GuiShop;
import loecraftpack.content.ponies.inventory.gui.ContainerAbilities;
import loecraftpack.content.ponies.inventory.gui.ContainerEarthInventory;
import loecraftpack.content.ponies.inventory.gui.ContainerEmpty;
import loecraftpack.content.ponies.inventory.gui.ContainerSpecialEquipment;
import loecraftpack.content.ponies.inventory.gui.GuiAbilities;
import loecraftpack.content.ponies.inventory.gui.GuiEarthPonyInventory;
import loecraftpack.content.ponies.inventory.gui.GuiSpecialEquipment;
import loecraftpack.content.ponies.inventory.gui.GuiStats;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class HandlerGui implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if(ID < GuiIds.values().length)
		{
			TileEntity tileEntity;
			switch(GuiIds.values()[ID])
			{
				case PROJECT_TABLE:
					tileEntity = world.getTileEntity(x, y, z);
					if(tileEntity instanceof TileProjectTable)
						return new ContainerProjectTable(player.inventory, (TileProjectTable)tileEntity, world, x, y, z);
					break;
					
				case BREWING_STATION:
					tileEntity = world.getTileEntity(x, y, z);
					if(tileEntity instanceof TileBrewingStation)
						return new ContainerPotionStation(player, (TileBrewingStation)tileEntity);
					break;
					
				case MAIN_INV:
					return player.inventoryContainer;
					
				case EQUIPMENT_INV:
					return new ContainerSpecialEquipment(player);
					
				case EARTH_INV:
					return new ContainerEarthInventory(player);
					
				case ABILITY_INV:
					return new ContainerAbilities(player);
					
				case STATS:
					return new ContainerEmpty();
					
				case QUIVER:
					return new ContainerQuiver(player);
					
				default:
					break;
			}
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if(ID < GuiIds.values().length)
		{
			TileEntity tileEntity;
			switch(GuiIds.values()[ID])
			{
				
				
				case MONOLITH:
					tileEntity = world.getTileEntity(x, y, z);
					if(tileEntity instanceof TileProtectionMonolith)
						return new GuiProtectionMonolith((TileProtectionMonolith) tileEntity);
					break;
					
				case PROJECT_TABLE:
					tileEntity = world.getTileEntity(x, y, z);
					if(tileEntity instanceof TileProjectTable)
						return new GuiProjectTable(player.inventory, world, x, y, z);
					break;
					
				case BREWING_STATION:
					tileEntity = world.getTileEntity(x, y, z);
					if(tileEntity instanceof TileBrewingStation)
						return new GuiBrewingStation(player, (TileBrewingStation)tileEntity);
					break;
					
				case DIALOG:
					return new GuiDialog();
			            
				case QUEST:
		            return new GuiQuest();
		            
				case SHOP:
		            return new GuiShop();
		            
				case MAIN_INV:
					return new GuiInventory(player);
					
				case EQUIPMENT_INV:
					return new GuiSpecialEquipment(player);
					
				case EARTH_INV:
					return new GuiEarthPonyInventory(player);
					
				case ABILITY_INV:
					return new GuiAbilities(player);
					
				case STATS:
					return new GuiStats(player);
					
				case BANK:
					return new GuiBank();
					
				case QUIVER:
					return new GuiQuiver(player);
					
				case CREATIVE_INV:
					break;
			}
		}
		return null;
	}
}
