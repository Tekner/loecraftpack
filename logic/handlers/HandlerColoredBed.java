package loecraftpack.logic.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import loecraftpack.content.registery.LOE_Items;
import loecraftpack.referrance.enums.Dye;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class HandlerColoredBed
{
	private HandlerColoredBed(){}
	
	public static int numBeds = 0;
	public static List<String> iconNames = new ArrayList<String>();
	public static Map<String, String[]> bedPairs = new HashMap<String, String[]>();
	
	private static void addBedRecipe(Dye color1, Dye color2, Dye color3)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(LOE_Items.beds, 1, numBeds++), "ABC", "XXX",
																 'A', new ItemStack(Blocks.wool, 1, color1.ordinal()),
																 'B', new ItemStack(Blocks.wool, 1, color2.ordinal()),
																 'C', new ItemStack(Blocks.wool, 1, color3.ordinal()),
																 'X', "plankWood"));
	}
	
	//locate and remove old bed recipe
	public static void cleanBedRecipe()
	{
    	Iterator r = CraftingManager.getInstance().getRecipeList().iterator();
    	while (r.hasNext())
    	{
    		IRecipe ir = (IRecipe)r.next();
    		//if the recipe outputs a bed, remove it
			if (ir.getRecipeOutput() != null && ir.getRecipeOutput().getItem() == Items.bed)
			{
				r.remove();
				break; //there really should only be one vanilla bed to remove, so stop once we find it
			}
    	}
	}
	
	public static void addCustomBed(String dataName, Dye color)
	{
		iconNames.add(dataName);
		addBedRecipe(color, color, color);
	}
	
	public static void addCustomBed(String dataName, Dye color1, Dye color2, Dye color3)
	{
		iconNames.add(dataName);
		addBedRecipe(color1, color2, color3);
	}
	
	public static void addBedPair(String name, String bedLeft, String bedRight)
	{
		bedPairs.put(name, new String[] {bedLeft, bedRight});
	}
	
	
	
	public static String getPairName(int id)
	{
		if (id >= 0 && id < bedPairs.size())
			return bedPairs.keySet().toArray(new String[0])[id];
		return "";
	}
	
	public static int getPairID(int bedLeftID, int bedRightID)
	{
		String bedLeftName = iconNames.get(bedLeftID);
		String bedRightName = iconNames.get(bedRightID);
		for (int i = 0; i < bedPairs.size(); i++)
		{
			String[][] pairs = bedPairs.values().toArray(new String[0][0]);
			
			if (pairs[i][0].equals(bedLeftName) && pairs[i][1].equals(bedRightName))
			{
				return i;
			}
		}
		
		return -1;
	}
	
	public static int findPairDirection(int pairID, int scourceID)
	{
		String scourceName = iconNames.get(scourceID);
		//return direction the partner would be in.  1 for right, -1 for left
		String[][] pairs = bedPairs.values().toArray(new String[0][0]);
		if (pairs[pairID][0].equals(scourceName))
			return 1; //to the right
		else if (pairs[pairID][1].equals(scourceName))
			return -1; //to the left
		return 0;//none
	}
	
	public static void loadBeds()
	{
		//Add base-color beds
    	HandlerColoredBed.addCustomBed("rarity", Dye.White);//0
    	HandlerColoredBed.addCustomBed("derpy", Dye.LightGray);
    	HandlerColoredBed.addCustomBed("octavia", Dye.Gray);
    	HandlerColoredBed.addCustomBed("discord", Dye.Black);
    	HandlerColoredBed.addCustomBed("big_mac", Dye.Red);
    	HandlerColoredBed.addCustomBed("applejack", Dye.Orange);
    	HandlerColoredBed.addCustomBed("fluttershy", Dye.Yellow);
    	HandlerColoredBed.addCustomBed("granny_smith", Dye.Lime);
    	HandlerColoredBed.addCustomBed("spike", Dye.Green);//8
    	HandlerColoredBed.addCustomBed("trixie", Dye.Cyan);
    	HandlerColoredBed.addCustomBed("rainbow_dash", Dye.LightBlue);
    	HandlerColoredBed.addCustomBed("luna", Dye.Blue);
    	HandlerColoredBed.addCustomBed("twilight_sparkle", Dye.Purple);
    	HandlerColoredBed.addCustomBed("Cheerilee", Dye.Magenta);
    	HandlerColoredBed.addCustomBed("pinkie_pie", Dye.Pink);
    	HandlerColoredBed.addCustomBed("muffin", Dye.Brown);
    	
    	//Add combo-color beds
    	HandlerColoredBed.addCustomBed("celestia", Dye.Lime, Dye.LightBlue, Dye.Pink);//16
    	HandlerColoredBed.addCustomBed("fausticorn", Dye.White, Dye.Red, Dye.White);
    	HandlerColoredBed.addCustomBed("cmc", Dye.Red, Dye.Blue, Dye.Yellow);
    	HandlerColoredBed.addCustomBed("sweetie_belle", Dye.White, Dye.Pink, Dye.Magenta);
    	HandlerColoredBed.addCustomBed("scootaloo", Dye.Orange, Dye.Purple, Dye.Orange);
    	HandlerColoredBed.addCustomBed("babs_seed", Dye.Brown, Dye.Red, Dye.Pink);
    	HandlerColoredBed.addCustomBed("apple_bloom", Dye.Yellow, Dye.Red, Dye.Yellow);
    	HandlerColoredBed.addCustomBed("silver_spoon", Dye.Gray, Dye.LightGray, Dye.LightBlue);
    	HandlerColoredBed.addCustomBed("diamond_tiara", Dye.Pink, Dye.Purple, Dye.White);//24
    	HandlerColoredBed.addCustomBed("lyra", Dye.Lime, Dye.Yellow, Dye.Cyan);
    	HandlerColoredBed.addCustomBed("bon_bon", Dye.Blue, Dye.Pink, Dye.Yellow);
    	HandlerColoredBed.addCustomBed("spitfire", Dye.Yellow, Dye.Orange, Dye.Yellow);
    	HandlerColoredBed.addCustomBed("shining_armor", Dye.White, Dye.LightBlue, Dye.Blue);
    	HandlerColoredBed.addCustomBed("cadence", Dye.Purple, Dye.Magenta, Dye.Yellow);
    	HandlerColoredBed.addCustomBed("colgate", Dye.White, Dye.Blue, Dye.Cyan);
    	HandlerColoredBed.addCustomBed("vinyl_scratch", Dye.White, Dye.Blue, Dye.LightBlue);
    	HandlerColoredBed.addCustomBed("zecora", Dye.Gray, Dye.LightGray, Dye.White);//32
    	
    	//Register bed pairs
    	HandlerColoredBed.addBedPair("alicorn_sisters", "celestia", "luna");
    	HandlerColoredBed.addBedPair("belle_sisters", "rarity", "sweetie_belle");
    	HandlerColoredBed.addBedPair("apple_sisters", "apple_jack", "apple_bloom");
    	HandlerColoredBed.addBedPair("wing_sisters", "rainbow_dash", "scootaloo");
    	HandlerColoredBed.addBedPair("double_trouble", "silver_spoon", "diamond_tiara");
    	HandlerColoredBed.addBedPair("fangasm", "lyra", "bon_bon");
    	HandlerColoredBed.addBedPair("royal_couple", "shining_armor", "cadence");
    	HandlerColoredBed.addBedPair("muffin_love", "derpy", "muffin");
    	HandlerColoredBed.addBedPair("chocolate_rain", "pinkie_pie", "discord");
    	HandlerColoredBed.addBedPair("music", "octavia", "vinyl_scratch");
	}
}
