package loecraftpack.logic.handlers;


import java.util.Collection;
import java.util.Iterator;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class HandlerExtendedPotions {
	
	public static final ResourceLocation defaultpotionEffects = new ResourceLocation("textures/gui/container/inventory.png");
	public static final ResourceLocation[] potionEffects = new ResourceLocation[128];
	
	public static void addPotion(Potion potion)
	{
		potionEffects[potion.id] = new ResourceLocation("loecraftpack:gui/potionEffects/"+potion.getName()+".png");
	}
	
	public static void renderExtendedPotionEffects(InventoryEffectRenderer gui)
	{
		Minecraft mc = Minecraft.getMinecraft();
		int i = gui.guiLeft - 124 + 6;
		int j = gui.guiTop + 7;
		boolean flag = true;
		Collection collection = mc.thePlayer.getActivePotionEffects();
		
		if (!collection.isEmpty())
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glDisable(GL11.GL_LIGHTING);
			int k = 33;
			
			if (collection.size() > 5)
			{
				k = 132 / (collection.size() - 1);
			}
			
			for (Iterator iterator = mc.thePlayer.getActivePotionEffects().iterator(); iterator.hasNext(); j += k)
			{
				PotionEffect potioneffect = (PotionEffect)iterator.next();
				int id = potioneffect.getPotionID();
				if (potionEffects[id]==null)
					continue;//ignore non-registered
				Potion potion = Potion.potionTypes[id];
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				mc.getTextureManager().bindTexture(defaultpotionEffects);
				gui.drawTexturedModalRect(i, j, 6, 173, 18, 18);
				
				mc.getTextureManager().bindTexture(potionEffects[id]);
				if (potion.hasStatusIcon())
				{
					int l = potion.getStatusIconIndex();
					drawIcon(gui, i, j, 0, 0, 18, 18, 18, 18);
				}
			}
		}
	}
	
	//TODO: possibly remove this
	public static void drawIcon(Gui gui, int posX, int posY, int posU, int posV, int sizeX, int sizeY, int resX, int resY)
	{
		gui.func_146110_a(posX, posY, posU, posV, sizeX, sizeY, resX, resY);
	}
}
