package loecraftpack.logic;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;

public class Message
{
	public String dialogTitle = "Error: No Title - Contact An Admin.";
	public String[] dialogText = {"Error: No Data", "Contact An Admin."};
	public String[] buttonText = {"Done"};
	public String[] buttons = {"done"};
	public String tag = "default";
	public Type type = Type.Dialog;
	
	public Message(String dialogTitle, String dialogText)
	{
		this(dialogTitle, dialogText, new String[] {"Done"}, new String[] {"next"});
	}
	
	public Message(String dialogTitle, String dialogText, String[] buttonText, String[] buttons)
	{
		this.dialogTitle = dialogTitle;
		this.buttonText = buttonText;
		this.buttons = buttons;
		
		dialogText = LogicDialog.TranslateChatColor(dialogText);
		String str = "";
		List<String> temp = new ArrayList<String>();
		for(int i = 0; i < dialogText.length(); i ++ )
		{
			if (dialogText.charAt(i) == '\\' && (i < dialogText.length()-1 && dialogText.charAt(i+1) == '\\'))
			{
				temp.add(str);
				str = "";
				i += 2;
			}
			else if (Minecraft.getMinecraft().fontRenderer.getStringWidth(str) > 248)
			{
				int indexOfLastSpace = str.lastIndexOf(' ');
				if (indexOfLastSpace != -1)
				{
					temp.add(str.substring(0, indexOfLastSpace));
					i -= str.length() - indexOfLastSpace - 1;
				}
				else
					temp.add(str);
				
				str = "";
			}
			
			str += dialogText.charAt(i);
		}
		temp.add(str);
		this.dialogText = temp.toArray(new String[0]);
	}
	
	public Message(String dialogTitle, String tasks, String rewards)
	{
		this.dialogTitle = dialogTitle;
		this.dialogText = new String[] {tasks, rewards};
		this.type = Type.Quest;
	}
	
	public void SetNext(boolean shop)
	{
		if (shop)
			buttonText = new String[] {"Enter Shop"};
		else
			buttonText = new String[] {"Next"};
		buttons = new String[] {"next"};
	}
}
