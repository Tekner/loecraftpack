package loecraftpack.network;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


/**
 * PacketBase class.
 * @author Rundo, 
 */
public abstract class PacketBase implements IMessage, IMessageHandler<PacketBase, IMessage>
{
	@Override
	public void fromBytes(ByteBuf buf) 
	{
		try {
			decodeInto(null, buf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		try {
			encodeInto(null, buf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public abstract void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception;

	public abstract void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception;
	
	
	@Override
	public IMessage onMessage(PacketBase message, MessageContext ctx) {
		if (ctx.side == Side.SERVER)
		{
			message.handleServerSide(ctx.getServerHandler().playerEntity);
		}
		else
		{
			message.handleClientSide(getClientPlayer());
		}
		
		//this would allow a reply?
		return null;
	}

	@SideOnly(Side.CLIENT)
	public abstract void handleClientSide(EntityPlayer player);

	public abstract void handleServerSide(EntityPlayer player);
	
	
	@SideOnly(Side.CLIENT)
	private EntityPlayer getClientPlayer()
	{
		return Minecraft.getMinecraft().thePlayer;
	}
}
