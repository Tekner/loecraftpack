package loecraftpack.network;

import loecraftpack.network.packets.*;
import net.minecraft.entity.player.EntityPlayerMP;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketHandler{
	
	protected SimpleNetworkWrapper wrapper;
	private int newIndex = 0;
	
	public void initialise(String channel)
	{
		wrapper = NetworkRegistry.INSTANCE.newSimpleChannel("channel");
		
		registerPacket(PacketAddPlayerToStatsHandler.class);
		registerPacket(PacketAppleBloomUpdate.class);
		registerPacket(PacketApplyStats.class);
		registerPacket(PacketBedUpdate.class);
		registerPacket(PacketCastAbility.class);
		registerPacket(PacketCustomInventory.class);
		registerPacket(PacketDenyAbility.class);
		registerPacket(PacketExtendedMobData.class);
		registerPacket(PacketFlightStatus.class);
		registerPacket(PacketMonolithEdit.class);
		registerPacket(PacketMonolithUpdate.class);
		registerPacket(PacketStatUpdate.class);
		registerPacket(PacketUpdateBrewingMode.class);
		registerPacket(PacketUpdateBrewingSelection.class);
		registerPacket(PacketUpdateInventory.class);
		registerPacket(PacketUpgradeStat.class);
		registerPacket(PacketUseAbility.class);
	}
	
	protected void registerPacket(Class packet)
	{
		wrapper.registerMessage(packet, packet, newIndex, Side.CLIENT);
		wrapper.registerMessage(packet, packet, newIndex, Side.SERVER);
		newIndex++;
	}
	
	public void sendToAll(PacketBase message)
	{
		wrapper.sendToAll(message);
	}
	
	public void sendTo(PacketBase message, EntityPlayerMP player)
	{
		wrapper.sendTo(message, player);
	}
	
	public void sendToAllAround(PacketBase message, NetworkRegistry.TargetPoint point)
	{
		wrapper.sendToAllAround(message, point);
	}
	
	public void sendToDimension(PacketBase message, int dimensionId)
	{
		wrapper.sendToDimension(message, dimensionId);
	}
	
	public void sendToServer(PacketBase message)
	{
		wrapper.sendToServer(message);
	}
}
