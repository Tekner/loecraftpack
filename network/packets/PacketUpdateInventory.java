package loecraftpack.network.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.content.ponies.inventory.InventoryCustom;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.network.PacketBase;
import loecraftpack.referrance.enums.PlayerInventoryId;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;

public class PacketUpdateInventory extends PacketBase
{
	PlayerInventoryId invID;
	Map<Integer, ItemStack> changes;
	int size;
	
	public PacketUpdateInventory INIT(PlayerInventoryId invID, Map<Integer, ItemStack> changes)
	{
		this.invID = invID;
		this.changes = changes;
		for (Map.Entry<Integer, ItemStack> change: changes.entrySet())
		{
			if (change != null && change.getKey()!=null)
			{
				size++;
			}
		}
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeByte(invID.ordinal());
		pktBuffer.writeByte(size);
		for (Map.Entry<Integer, ItemStack> change: changes.entrySet())
		{
			if (change != null && change.getKey()!=null)
			{
				pktBuffer.writeByte(change.getKey().byteValue());
				pktBuffer.writeItemStackToBuffer(change.getValue());
			}
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		int id = pktBuffer.readByte();
		if (id >= PlayerInventoryId.values().length)
			throw new Exception("invalid inventory id");
		invID = PlayerInventoryId.values()[id];
		size = pktBuffer.readByte();
		changes = new HashMap<Integer, ItemStack>();
		for (int i=0; i<size; i++)
		{
			changes.put(Integer.valueOf(pktBuffer.readByte()), pktBuffer.readItemStackFromBuffer());
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		InventoryCustom inv = HandlerExtendedInventory.getInventory((EntityPlayer)player, invID);
		if (inv!=null)
		{
			for (Map.Entry<Integer, ItemStack> change: changes.entrySet())
			{
				inv.setInventorySlotContents(change.getKey().intValue(), change.getValue());
			}
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}

}
