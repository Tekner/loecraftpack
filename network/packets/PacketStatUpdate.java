package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketStatUpdate extends PacketBase
{
	public byte id;
	public Object value;
	
	public PacketStatUpdate INIT(float energy)
	{
		id = -1;
		value = energy;
		
		return this;
	}
	
	public PacketStatUpdate INIT(byte statID, short value)
	{
		id = statID;
		this.value = value;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeByte(id);
		if (id == -1)
			pktBuffer.writeFloat((Float)value);
		else
			pktBuffer.writeShort((Short)value);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		id = pktBuffer.readByte();
		if (id == -1)
			value = pktBuffer.readFloat();
		else
			value = pktBuffer.readShort();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		AbilityPlayerData.recieveChangingPlayerStatPacket(this);
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}
	
	public int getID()
	{
		return id;
	}
	
	public Object getvalue()
	{
		return value;
	}
}
