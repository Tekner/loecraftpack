package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.abilities.passive.AbilityFlight;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class PacketFlightStatus extends PacketBase
{
	boolean isFlying;
	
	public PacketFlightStatus INIT(boolean isFlying)
	{
		this.isFlying = isFlying;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeBoolean(isFlying);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		isFlying = pktBuffer.readBoolean();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		if (!isFlying)
			AbilityPlayerData.clientData.PassPassiveAbilityEvent("", new LivingHurtEvent((EntityLivingBase)null, DamageSource.lava, 0));
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		for(PassiveAbility a : AbilityPlayerData.Get(player.getGameProfile().getId()).passiveAbilities)
		{
			if (a instanceof AbilityFlight)
			{
				((AbilityFlight)a).SetFlying(isFlying);
				break;
			}
		}
	}

}
