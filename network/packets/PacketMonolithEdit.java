package loecraftpack.network.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class PacketMonolithEdit extends PacketBase
{
	int xCoord, yCoord, zCoord;
	int width, length;
	int xOffset, zOffset;
	String id;
	String username;
	boolean setSize;
	boolean addOwner;
	
	public PacketMonolithEdit INIT(int xCoord, int yCoord, int zCoord)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		
		return this;
	}
	
	public PacketMonolithEdit setSize(int width, int length, int xOffset, int zOffset)
	{
		this.width = width;
		this.length = length;
		this.xOffset = xOffset;
		this.zOffset = zOffset;
		setSize = true;
		return this;
	}
	
	public PacketMonolithEdit AddOwner(String id, String username)
	{
		this.id = id;
		this.username = username;
		addOwner = true;
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		if (setSize)
		{
			pktBuffer.writeByte(1);
			pktBuffer.writeInt(width);
			pktBuffer.writeInt(length);
			pktBuffer.writeInt(xOffset);
			pktBuffer.writeInt(zOffset);
		}
		if (addOwner)
		{
			pktBuffer.writeByte(2);
			pktBuffer.writeStringToBuffer(id);
			pktBuffer.writeStringToBuffer(username);
		}
		
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		while (pktBuffer.isReadable())
		{
			switch (pktBuffer.readByte())
			{
			case 1:
				width = pktBuffer.readInt();
				length = pktBuffer.readInt();
				xOffset = pktBuffer.readInt();
				zOffset = pktBuffer.readInt();
				setSize = true;
				break;
				
			case 2:
				String ID = pktBuffer.readStringFromBuffer(256);
				String USER = pktBuffer.readStringFromBuffer(256);
				EntityPlayer player = MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(USER);
				if (player != null && player.getGameProfile() != null && player.getGameProfile().getId() == ID)
				{
					this.id = ID;
					this.username = USER;
					addOwner = true;
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player){}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		TileProtectionMonolith te = (TileProtectionMonolith)player.worldObj.getTileEntity(xCoord, yCoord, zCoord);
		if (te != null)
		{
			PacketMonolithUpdate packet = new PacketMonolithUpdate().INIT(xCoord, yCoord, zCoord);
			if (setSize)
			{
				te.width = width;
				te.length = length;
				te.offsetX = xOffset;
				te.offsetZ = zOffset;
				packet.setSize(width, length, xOffset, zOffset);
			}
			if (addOwner)
			{
				te.owners.put(id, username);
				packet.setOwners(te.owners);
			}
			LoECraftPack.packetHandler.sendTo(packet, (EntityPlayerMP) player);
		}
	}
}
