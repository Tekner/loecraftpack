package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;

public class PacketUpdateBrewingSelection extends PacketBase
{
	int xCoord, yCoord, zCoord;
	int id;
	
	public PacketUpdateBrewingSelection INIT(int xCoord, int yCoord, int zCoord, int id)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		this.id = id;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		pktBuffer.writeInt(id);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		id = pktBuffer.readInt();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		TileEntity tile = player.worldObj.getTileEntity(xCoord, yCoord, zCoord);
		if (tile instanceof TileBrewingStation)
		{
			CustomBrewingRecipe recipe = CustomBrewingRecipe.getRecipe(id);
			if (recipe != null)
				((TileBrewingStation)tile).selectRecipe(recipe);
		}
	}

}
