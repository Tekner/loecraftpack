package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;

public class PacketUpdateBrewingMode extends PacketBase
{
	int xCoord, yCoord, zCoord;
	boolean mode;
	
	public PacketUpdateBrewingMode INIT(int xCoord, int yCoord, int zCoord, boolean mode)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		this.mode = mode;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		pktBuffer.writeBoolean(mode);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		mode = pktBuffer.readBoolean();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		TileEntity tile = player.worldObj.getTileEntity(xCoord, yCoord, zCoord);
		if (tile instanceof TileBrewingStation)
		{
			((TileBrewingStation)tile).setAutoBrew(mode);
		}
	}

}
