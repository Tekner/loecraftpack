package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.LoECraftPack;
import loecraftpack.logic.attributes.LoeEntityLivingData;
import loecraftpack.network.PacketBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;

public class PacketExtendedMobData extends PacketBase
{
	int entityID;
	int rank;
	int metaData;
	String grade;
	String talent;
	String title;
	
	public PacketExtendedMobData INIT(int entityID)
	{
		this.entityID = entityID;
		
		return this;
	}
	
	public PacketExtendedMobData INIT(int entityID, int rank, int metaData, String grade, String talent, String title)
	{
		this.entityID = entityID;
		this.rank = rank;
		this.metaData = metaData;
		this.grade = grade;
		this.talent = talent;
		this.title = title;
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(entityID);
		if (metaData != 0)
		{
			pktBuffer.writeInt(rank);
			pktBuffer.writeInt(metaData);
			pktBuffer.writeStringToBuffer(grade);
			pktBuffer.writeStringToBuffer(talent);
			pktBuffer.writeStringToBuffer(title);
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		entityID = pktBuffer.readInt();
		if (pktBuffer.isReadable())
		{
			rank = pktBuffer.readInt();
			metaData = pktBuffer.readInt();
			grade = pktBuffer.readStringFromBuffer(256);
			talent = pktBuffer.readStringFromBuffer(256);
			title = pktBuffer.readStringFromBuffer(256);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		Entity entity = Minecraft.getMinecraft().thePlayer.worldObj.getEntityByID(entityID);
		if (entity!=null)
		{
			LoeEntityLivingData extendedData = (LoeEntityLivingData)entity.getExtendedProperties("LoeEntityLiving");
			if (extendedData!=null)
			{
				extendedData.rank = rank;
				extendedData.metaData = metaData;
				extendedData.grade = grade;
				extendedData.talent = talent;
				extendedData.title = title;
				
				//Debug: receive mob data
				//System.out.println("--- "+extendedData.rank+" "+extendedData.metaData+" "+extendedData.grade+"|"+extendedData.talent+"|"+extendedData.title);
			}
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		Entity entity = player.worldObj.getEntityByID(entityID);
		if (entity!=null)
		{
			LoeEntityLivingData extendedData = (LoeEntityLivingData)entity.getExtendedProperties("LoeEntityLiving");
			if (extendedData!=null)
			{
				LoECraftPack.packetHandler.sendTo(new PacketExtendedMobData().INIT(entityID, extendedData.rank, extendedData.metaData, extendedData.grade, extendedData.talent, extendedData.title), (EntityPlayerMP) player);
			}
		}
	}

}
