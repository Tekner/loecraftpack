package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketUseAbility extends PacketBase
{
	int activeID;
	PacketBuffer storedBuffer;
	
	public PacketUseAbility INIT(int activeID, float value)
	{
		PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
		buffer.writeFloat(value);
		INIT(activeID, buffer);
		return this;
	}
	
	public PacketUseAbility INIT(int activeID, PacketBuffer buffer)
	{
		this.activeID = activeID;
		this.storedBuffer = buffer;
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(activeID);
		if (storedBuffer != null)
		{
			int length = storedBuffer.writerIndex();
			for (int i=0; i<length; i++)
			{
				pktBuffer.writeByte(storedBuffer.getByte(i));
			}
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		storedBuffer = new PacketBuffer(buffer);
		
		activeID = storedBuffer.readInt();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
	}

	@Override
	public void handleServerSide(EntityPlayer player) 
	{
		AbilityPlayerData playerData = AbilityPlayerData.Get(player.getGameProfile().getId());
		
		playerData.activeAbilities[activeID].castSpellServerByHandler(player, storedBuffer);
	}

}
