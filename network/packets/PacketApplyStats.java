package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.network.PacketBase;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketApplyStats extends PacketBase
{
	String username;
	Race race;
	float energy;
	byte size;
	int[] values;
	
	public PacketApplyStats INIT(String username, Race race)
	{
		this.username = username;
		this.race = race;
		
		return this;
	}
	
	public PacketApplyStats INIT(String username, Stats stats)
	{
		this.username = username;
		energy = stats.abilityData.energy;
		race = stats.getRace();
		size = stats.getStatsSize();
		values = stats.getStatValues();
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeStringToBuffer(username);
		pktBuffer.writeByte(race.ordinal());
		if (size>0)
		{
			pktBuffer.writeFloat(energy);
			pktBuffer.writeByte(size);
			for (int value: values)
				pktBuffer.writeShort(value);
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		username = pktBuffer.readStringFromBuffer(256);
		int id = pktBuffer.readByte();
		if (id >= Race.values().length)
			throw new Exception("invalid race id");
		race = Race.values()[id];
		
		if (pktBuffer.isReadable())
		{
			energy = pktBuffer.readFloat();
			size = pktBuffer.readByte();
			values = new int[size];
			for (int i=0; i<size; i++)
			{
				values[i] = pktBuffer.readShort();
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		
		if (size>0)
			LoECraftPack.statHandler.updatePlayerData(username, race, energy, values);
		else
			LoECraftPack.statHandler.updatePlayerRace(username, race);
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}

}
