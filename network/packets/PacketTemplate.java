package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketTemplate extends PacketBase
{
	public PacketTemplate INIT()
	{
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}

}
