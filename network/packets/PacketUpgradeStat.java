package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.content.ponies.stats.components.StatFlags;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketUpgradeStat extends PacketBase
{
	int statID;
	
	public PacketUpgradeStat INIT(int statID)
	{
		this.statID = statID;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeByte(statID);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		statID = pktBuffer.readByte();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		Stats stats = (Stats)LoECraftPack.statHandler.stats.get(player.getGameProfile().getId());
		Stat stat = stats.getStatList()[statID];
		boolean ponylevel = stat.hasAnyFlag(StatFlags.PonyLevel);
		if ((ponylevel && stats.getPonyPoints() > 0) ||
			(stat.hasAnyFlag(StatFlags.Alignment) && stats.getAlignmentPoints() > 0))
		{
			stat.setValue(stat.getValue()+1);
			stats.abilityData.fixRaceStats();
		}
	}

}
