package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.network.PacketBase;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketAppleBloomUpdate extends PacketBase
{
	int xCoord, yCoord, zCoord;
	int newBlockID;
	
	public PacketAppleBloomUpdate INIT(int xCoord, int yCoord, int zCoord, Block newBlock)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		this.newBlockID = Block.getIdFromBlock(newBlock);
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		pktBuffer.writeInt(newBlockID);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		newBlockID  = pktBuffer.readInt();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		Minecraft.getMinecraft().theWorld.invalidateBlockReceiveRegion(xCoord, yCoord, zCoord, xCoord, yCoord, zCoord);
		Minecraft.getMinecraft().theWorld.setBlock(xCoord, yCoord, zCoord, Block.getBlockById(newBlockID), 0, 3);
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
	}

}
