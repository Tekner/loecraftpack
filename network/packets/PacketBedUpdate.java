package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.blocks.tile.TileColoredBed;
import loecraftpack.network.PacketBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketBedUpdate extends PacketBase
{
	int xCoord, yCoord, zCoord;
	int pairSide;
	int pairID;
	
	public PacketBedUpdate INIT(int xCoord, int yCoord, int zCoord, int pairID, int pairSide)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		this.pairID = pairID;
		this.pairSide = pairSide;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		pktBuffer.writeInt(pairSide);
		pktBuffer.writeInt(pairID);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		pairSide = pktBuffer.readInt();
		pairID = pktBuffer.readInt();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		TileColoredBed cb = (TileColoredBed)Minecraft.getMinecraft().theWorld.getTileEntity(xCoord, yCoord, zCoord);
		if (cb != null)
		{
			cb.pairID = pairID;
			cb.pairSide = pairSide;
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}

}
