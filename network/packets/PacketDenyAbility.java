package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;

public class PacketDenyAbility extends PacketBase
{
	int activeID;
	float energy;
	
	public PacketDenyAbility INIT(int activeID, float energy)
	{
		this.activeID = activeID;
		this.energy = energy;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(activeID);
		pktBuffer.writeFloat(energy);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		activeID = pktBuffer.readInt();
		energy = pktBuffer.readFloat();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		AbilityPlayerData.handleDeny(activeID, energy);
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		
	}

}
