package loecraftpack.network.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.network.PacketBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class PacketMonolithUpdate extends PacketBase
{
	int xCoord, yCoord, zCoord;
	int width, length;
	int xOffset, zOffset;
	Map<String, String> owners;
	boolean setSize;
	boolean setOwners;
	
	public PacketMonolithUpdate INIT(int xCoord, int yCoord, int zCoord)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
		
		return this;
	}
	
	public PacketMonolithUpdate setSize(int width, int length, int xOffset, int zOffset)
	{
		this.width = width;
		this.length = length;
		this.xOffset = xOffset;
		this.zOffset = zOffset;
		setSize = true;
		return this;
	}
	
	public PacketMonolithUpdate setOwners(Map<String, String> owners)
	{
		this.owners = owners;
		setOwners = true;
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(xCoord);
		pktBuffer.writeInt(yCoord);
		pktBuffer.writeInt(zCoord);
		if (setSize)
		{
			pktBuffer.writeByte(1);
			pktBuffer.writeInt(width);
			pktBuffer.writeInt(length);
			pktBuffer.writeInt(xOffset);
			pktBuffer.writeInt(zOffset);
		}
		if (setOwners)
		{
			pktBuffer.writeByte(2);
			pktBuffer.writeByte(owners.size());
			for (Entry<String, String> entry: owners.entrySet())
			{
				pktBuffer.writeStringToBuffer(entry.getKey());
				pktBuffer.writeStringToBuffer(entry.getValue());
			}
		}
		
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		xCoord = pktBuffer.readInt();
		yCoord = pktBuffer.readInt();
		zCoord = pktBuffer.readInt();
		while (pktBuffer.isReadable())
		{
			switch (pktBuffer.readByte())
			{
			case 1:
				width = pktBuffer.readInt();
				length = pktBuffer.readInt();
				xOffset = pktBuffer.readInt();
				zOffset = pktBuffer.readInt();
				setSize = true;
				break;
				
			case 2:
				owners = new HashMap<String, String>();
				int size = pktBuffer.readByte();
				for (int i=0; i< size; i++)
				{
					owners.put(pktBuffer.readStringFromBuffer(256), pktBuffer.readStringFromBuffer(256));
				}
				setOwners= true;
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		TileProtectionMonolith te = (TileProtectionMonolith)player.worldObj.getTileEntity(xCoord, yCoord, zCoord);
		if (te != null)
		{
			if (setSize)
			{
			te.width = width;
			te.length = length;
			te.offsetX = xOffset;
			te.offsetZ = zOffset;
			}
			if (setOwners)
				te.owners= owners;
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		TileProtectionMonolith te = (TileProtectionMonolith)player.worldObj.getTileEntity(xCoord, yCoord, zCoord);
		if (te != null)
			LoECraftPack.packetHandler.sendTo(new PacketMonolithUpdate().INIT(xCoord, yCoord, zCoord).setSize(te.width, te.length, te.offsetX, te.offsetZ).setOwners(te.owners), (EntityPlayerMP) player);
	}
}
