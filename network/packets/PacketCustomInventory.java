package loecraftpack.network.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import loecraftpack.LoECraftPack;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.network.PacketBase;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;

public class PacketCustomInventory extends PacketBase
{
	int guiID;
	
	public PacketCustomInventory INIT(int guiID)
	{
		this.guiID = guiID;
		
		return this;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		pktBuffer.writeInt(guiID);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception
	{
		PacketBuffer pktBuffer = new PacketBuffer(buffer);
		
		guiID = pktBuffer.readInt();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(EntityPlayer player)
	{
		Minecraft.getMinecraft().displayGuiScreen(new GuiContainerCreative(Minecraft.getMinecraft().thePlayer));
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		try
		{
			if (guiID < GuiIds.values().length)
			{
				GuiIds targetGui = GuiIds.values()[guiID];
				if (HandlerExtendedInventory.canUseInv(player, targetGui))
				{
					if (player.capabilities.isCreativeMode && targetGui==GuiIds.MAIN_INV)
					{
    					player.openContainer = player.inventoryContainer;
    					LoECraftPack.packetHandler.sendTo(new PacketCustomInventory().INIT(0), (EntityPlayerMP) player);
					}
					else
					{
    					player.openGui(LoECraftPack.instance,
    							guiID,
    							MinecraftServer.getServer().worldServerForDimension(player.dimension),
								(int)player.posX,
								(int)player.posY,
								(int)player.posZ);
					}
				}
			}
		}
		catch(IllegalArgumentException e)
		{
			//this allows the exception for reloading crafting inventory without closing it first, to be ignored.
			//currently only one crafting inventory that applies.
			if (! (guiID == GuiIds.MAIN_INV.ordinal()))
				throw e;
		}
	}

}
