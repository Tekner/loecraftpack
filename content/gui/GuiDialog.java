package loecraftpack.content.gui;

import java.awt.Color;

import loecraftpack.logic.LogicDialog;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiDialog extends GuiScreen
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/dialog.png");
	
	final int xSizeOfTexture = 256
			, ySizeOfTexture = 192
			, white = Color.white.getRGB();
	int posX
	  , posY;
	
	public void initGui()
	{
		this.buttonList.clear();
	
		posX = (this.width - xSizeOfTexture) / 2;
		posY = (this.height - ySizeOfTexture) / 2;
		
		this.buttonList.add(new GuiButton(0, posX, posY + 169, 256, 20, "Done"));
		this.buttonList.add(new GuiButton(1, posX, posY + 149, 256, 20, ""));
		this.buttonList.add(new GuiButton(0, posX, posY + 129, 256, 20, ""));
	}
	
	public void actionPerformed(GuiButton button)
	{
		if (buttons[button.id].equals("next"))
			LogicDialog.ChangeMessage(true);
		else if (buttons[button.id].equals("done"))
			mc.displayGuiScreen((GuiScreen)null);
		
		mc.thePlayer.sendChatMessage(buttons[button.id]);
	}
	
	@Override
	public void drawScreen(int x, int y, float f)
	{
		drawDefaultBackground();
	
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture(mainImage);
	
		int posX = (this.width - xSizeOfTexture) / 2;
		int posY = (this.height - ySizeOfTexture) / 2;
	
		drawTexturedModalRect(posX, posY, 0, 0, xSizeOfTexture, ySizeOfTexture);
		drawCenteredString(fontRendererObj, LogicDialog.dialogTitle, posX + xSizeOfTexture/2, posY + 9, white);
		
		for(int i = 0; i < LogicDialog.dialogText.length; i++ )
			drawCenteredString(fontRendererObj, LogicDialog.dialogText[i], posX + xSizeOfTexture/2, posY + 31 + i*11, white);
		
		super.drawScreen(x, y, f);
	}
	
	@Override
	public void onGuiClosed()
	{
		LogicDialog.ResetMessages();
	}
	
	private String[] buttons = new String[0];
	public void changeButtons(String[] buttonText, String[] buttons)
	{
		((GuiButton)buttonList.get(0)).visible = false;
		((GuiButton)buttonList.get(1)).visible = false;
		((GuiButton)buttonList.get(2)).visible = false;
		for(int i = 0; i < buttonText.length; i++)
		{
			GuiButton button = (GuiButton)buttonList.get(i);
			button.visible = true;
			button.displayString = buttonText[i];
		}
		
		this.buttons = buttons;
	}
}
