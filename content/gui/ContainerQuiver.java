package loecraftpack.content.gui;

import loecraftpack.content.ponies.inventory.InventoryQuiver;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerQuiver extends Container
{
	IInventory normalInv;
	InventoryQuiver quiverInv;
	
	public ContainerQuiver(EntityPlayer player)
	{
		normalInv = player.inventory;
		quiverInv = new InventoryQuiver(player.getHeldItem(), player.worldObj.isRemote);
		
		int v;
		int h;
		
		for (h = 0; h < 4; h++)
		{
			this.addSlotToContainer(new Slot(quiverInv, h, 52 + h * 18, 18));
		}
		
		for (v = 0; v < 3; ++v)
		{
			for (h = 0; h < 9; h++)
			{
				this.addSlotToContainer(new Slot(normalInv, h + v * 9 + 9, 8 + h * 18, 63 + v * 18));
			}
		}
		
		for (h = 0; h < 9; h++)
		{
			this.addSlotToContainer(new Slot(normalInv, h, 8 + h * 18, 121));
		}
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		return null;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer entityPlayer)
	{
		super.onContainerClosed(entityPlayer);
		quiverInv.relocateTrueItemStackAndSaveChanges(entityPlayer);
		quiverInv.markDirty();
	}
}
