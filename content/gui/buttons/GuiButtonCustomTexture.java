package loecraftpack.content.gui.buttons;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiButtonCustomTexture extends GuiButtonCustomBase {
	
	protected ResourceLocation mainImage;
	protected int posU, posV;
	
	public GuiButtonCustomTexture(int id, ResourceLocation mainImage, int xPos, int yPos, int posU, int posV, int width, int height, String displayText)
	{
		super(id, xPos, yPos, width, height, displayText);
		
		this.mainImage = mainImage;
		this.posU = posU;
		this.posV = posV;
	}
	
	@Override
	public void drawButton(Minecraft par1Minecraft, int posX, int posY)
	{
		if (this.visible)
		{
			par1Minecraft.getTextureManager().bindTexture(mainImage);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			int state = this.getHoverStateOfButton(posX, posY);
			this.drawTexturedModalRect(this.xPosition, this.yPosition, posU, posV+state*height, this.width, this.height);
			this.mouseDragged(par1Minecraft, posX, posY);
		}
	}

}
