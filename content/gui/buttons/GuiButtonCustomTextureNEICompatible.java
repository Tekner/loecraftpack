package loecraftpack.content.gui.buttons;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiButtonCustomTextureNEICompatible extends GuiButtonCustomBase {
	
	protected ResourceLocation mainImage;
	protected int posU, posV;
	protected GuiContainer parent;
	protected int xPos, yPos;
	
	public GuiButtonCustomTextureNEICompatible(int id, ResourceLocation mainImage, GuiContainer parent, int xPos, int yPos, int posU, int posV, int width, int height, String displayText)
	{
		super(id, parent.guiLeft+xPos, parent.guiTop+yPos, width, height, displayText);
		
		this.parent = parent;
		this.xPos = xPos;
		this.yPos = yPos;
		
		this.mainImage = mainImage;
		this.posU = posU;
		this.posV = posV;
	}
	
	@Override
	public void drawButton(Minecraft par1Minecraft, int posX, int posY)
	{
		this.xPosition = parent.guiLeft+xPos;
		this.yPosition = parent.guiTop+yPos;
		
		if (this.visible)
		{
			par1Minecraft.getTextureManager().bindTexture(mainImage);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			int state = this.getHoverStateOfButton(posX, posY);
			this.drawTexturedModalRect(this.xPosition, this.yPosition, posU, posV+state*height, this.width, this.height);
			this.mouseDragged(par1Minecraft, posX, posY);
		}
	}

}
