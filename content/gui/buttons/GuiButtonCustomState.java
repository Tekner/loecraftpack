package loecraftpack.content.gui.buttons;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class GuiButtonCustomState extends GuiButtonCustomTexture {
	
	public int state = 0;

	public GuiButtonCustomState(int id, ResourceLocation image, int xPos, int yPos, int posU, int posV, int width, int height, String displayText)
	{
		super(id, image, xPos, yPos, posU, posV, width, height, displayText);
	}
	
	@Override
	public void drawButton(Minecraft par1Minecraft, int posX, int posY)
	{
		if (this.visible)
		{
			par1Minecraft.getTextureManager().bindTexture(mainImage);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			this.drawTexturedModalRect(this.xPosition, this.yPosition, posU, posV+state*height, this.width, this.height);
			this.mouseDragged(par1Minecraft, posX, posY);
		}
	}

}
