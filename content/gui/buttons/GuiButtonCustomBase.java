package loecraftpack.content.gui.buttons;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public abstract class GuiButtonCustomBase extends GuiButton {
	
	public GuiButtonCustomBase(int id, int xPos, int yPos, int width, int height, String displayText)
	{
		super(id, xPos, yPos, width, height, displayText);
	}
	
	@Override
	public boolean mousePressed(Minecraft par1Minecraft, int posX, int posY)
	{
		return this.enabled && this.visible && getHoverOverButton(posX, posY);
	}
	
	public int getHoverStateOfButton(int posX, int posY)
	{
		this.field_146123_n = getHoverOverButton(posX, posY);
		return this.getHoverState(this.field_146123_n);
	}
	
	/**Ignores enabled state, and uses is drawn**/
	public boolean getHoverDrawnButton(int posX, int posY)
	{
		return this.visible && getHoverOverButton(posX, posY);
	}
	
	public boolean getHoverOverButton(int posX, int posY)
	{
		return posX >= this.xPosition && posY >= this.yPosition && posX < this.xPosition + this.width && posY < this.yPosition + this.height;
	}
	
}
