package loecraftpack.content.gui.buttons;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

public class GuiScrollButtonOld extends GuiButtonCustomTexture
{
	public GuiScreen parent;
	
	//bounding coords
	public int minX;
	public int minY;
	public int maxX;
	public int maxY;
	
	//default
	public int baseWidth;
	public int baseHeight;
	
	//modified
	public int offsetU;
	public int offsetV;
	
	public int state;
	
	public GuiScrollButtonOld(int id, GuiScreen parent, ResourceLocation mainImage, int minX, int minY, int maxX, int maxY, int posU, int posV, int width, int height)
	{
		super(id, mainImage, 0, 0, posU, posV, 0, 0, "---");
		this.parent = parent;
		
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		
		this.baseWidth = width;
		this.baseHeight = height;
		
		this.offsetU = 0;
		this.offsetV = 0;
		
		this.width = 0;
		this.height = 0;
		
		this.state = 0;
		
		this.visible = false;
		this.enabled = false;
	}
	
	public void updateButton(int subPosX, int subPosY, int state)
	{
		if (subPosX <= -baseWidth || subPosX >= maxX-minX || subPosY <= -baseHeight || subPosY >= maxY-minY)
		{
			this.visible = false;
			this.enabled = false;
		}
		else
		{
			if (subPosX<0)
			{
				xPosition = minX;
				offsetU = -subPosX;
				width = baseWidth - offsetU;
			}
			else
			{
				xPosition = minX + subPosX;
				offsetU = 0;
				width = Math.min(maxX-xPosition, baseWidth);
			}
			
			if (subPosY<0)
			{
				yPosition = minY;
				offsetV = -subPosY;
				height = baseHeight - offsetV;
			}
			else
			{
				yPosition = minY + subPosY;
				offsetV = 0;
				height = Math.min(maxY-yPosition, baseHeight);
			}
			
			this.visible = true;
			if(state>0)
				this.enabled = true;
		}
		
		this.state = state;
	}
	
	public void mute()
	{
		this.visible = false;
		this.enabled = false;
	}
	
	@Override
	public void drawButton(Minecraft mc, int posXmouse, int posYmouse)
	{
		if (this.visible)
		{
			ScaledResolution res = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
			
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glScissor(minX*res.getScaleFactor(), mc.displayHeight - maxY*res.getScaleFactor(), (maxX-minX)*res.getScaleFactor(), (maxY-minY)*res.getScaleFactor());
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			
			renderContent(mc, posXmouse, posYmouse);
			
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
			
			this.mouseDragged(mc, posXmouse, posYmouse);
		}
	}
	
	protected void renderContent(Minecraft mc, int posXmouse, int posYmouse)
	{
		FontRenderer fontRenderer = mc.fontRenderer;
		
		mc.getTextureManager().bindTexture(mainImage);
		this.drawTexturedModalRect(this.xPosition, this.yPosition, posU+offsetU, posV+offsetV+state*baseHeight, this.width, this.height);
		
		fontRenderer.drawString(StatCollector.translateToLocal(this.displayString), xPosition - offsetU + 4, yPosition - offsetV + 6, 16777215);
	}
	
	protected void renderPopout(Minecraft mc, int guiLeft, int guiTop, int posXmouse, int posYmouse){}
}
