package loecraftpack.content.gui.scroll;

import loecraftpack.content.gui.buttons.GuiButtonCustomTexture;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

public class GuiScrollButton extends GuiButtonCustomTexture
{
	public GuiScrollPage page;
	
	public int pagePosX;
	public int pagePosY;
	
	public int posU;
	public int posV;
	
	public int masterState;
	
	public GuiScrollButton(int id, GuiScrollPage page, ResourceLocation mainImage, int pagePosX, int pagePosY, int posU, int posV, int width, int height)
	{
		super(id, mainImage, 0, 0, posU, posV, 0, 0, "");
		this.page = page;
		page.linkedButtons.add(this);
		
		this.width = width;
		this.height = height;
		
		updateButtonPosition(pagePosX, pagePosY);
		
		this.posU = posU;
		this.posV = posV;
	}
	
	  /************************/
	 /***** updater's ********/
	/************************/
	
	public void updateButtonPosition(int pagePosX, int pagePosY)
	{
		this.pagePosX = pagePosX;
		this.pagePosY = pagePosY;
		
		updateVisability();
	}
	
	public void updateState(int state)
	{
		this.masterState = state;
		if (state <= 0)
			enabled = false;
		else
			enabled = visible;
	}
	
	public boolean updateVisability()
	{
		this.xPosition = grandPosX();
		this.yPosition = grandPosY();
		
		int x = pagePosX+page.offsetX;
		int y = pagePosY+page.offsetY;
		if (x <= -width || x >= page.width ||
			y <= -height || y>= page.height)
		{
			this.visible = false;
			this.enabled = false;
			return false;
		}
		else
		{
			this.visible = true;
			if(masterState>0)
				this.enabled = true;
			return true;
		}
	}
	
	public void mute()
	{
		this.visible = false;
		this.enabled = false;
	}
	
	  /************************/
	 /**** position calc's ***/
	/************************/
	
	public int grandPosX()
	{
		return page.guiLeft()+page.getXsubPos(pagePosX);
	}
	
	public int grandPosY()
	{
		return page.guiTop()+page.getYsubPos(pagePosY);
	}
	
	  /************************/
	 /***** mouse code *******/
	/************************/
	
	@Override
	public boolean getHoverOverButton(int posX, int posY)
	{
		return posX >= page.grandPosX() && posY >= page.grandPosY() && posX < page.grandPosX()+page.width && posY < page.grandPosY()+page.height &&
				posX >= this.xPosition && posY >= this.yPosition && posX < this.xPosition + this.width && posY < this.yPosition + this.height;
	}
	
	  /************************/
	 /***** drawer's *********/
	/************************/
	
	@Override
	public void drawButton(Minecraft mc, int posXmouse, int posYmouse)
	{
		xPosition = grandPosX();
		yPosition = grandPosY();
		
		if (this.visible)
		{
			FontRenderer fontRenderer = page.mc.fontRenderer;
			ScaledResolution res = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
			
			page.mc.getTextureManager().bindTexture(mainImage);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glScissor(page.grandPosX()*res.getScaleFactor(), mc.displayHeight - (page.grandPosY()+page.height)*res.getScaleFactor(), page.width*res.getScaleFactor(), page.height*res.getScaleFactor());
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			
			int hoverState = this.getHoverStateOfButton(posXmouse, posYmouse);
			this.drawTexturedModalRect(xPosition, yPosition, posU, posV+hoverState*height, width, height);
			
			fontRenderer.drawString(StatCollector.translateToLocal(this.displayString), xPosition + 4, yPosition + 6, 16777215);
			
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
			
			this.mouseDragged(mc, posXmouse, posYmouse);
		}
	}
	
	protected void renderPopout(Minecraft mc, int guiLeft, int guiTop, int posXmouse, int posYmouse){}
}
