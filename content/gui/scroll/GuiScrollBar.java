package loecraftpack.content.gui.scroll;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiScrollBar
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/components.png");
	
	protected Minecraft mc = Minecraft.getMinecraft();
	
	protected GuiScreen parent;
	public int xPos, yPos;
	public int width, length;
	public int scrollSize;
	public int scrollPos;
	public int scrollbuttonSize;
	public int scrollU, scrollV;
	public boolean vertical;
	public boolean buttons;
	public boolean mouseSensitiveScroll;
	public boolean isScrolling = false;
	
	public GuiScrollBar(GuiScrollPage parent, int xPos, int yPos, int width, int length, boolean vertical)
	{
		this(parent.parent, xPos, yPos, width, length, vertical);
		if (vertical)
			parent.yScroll= this;
		else
			parent.xScroll= this;
	}
	
	public GuiScrollBar(GuiScreen parent, int xPos, int yPos, int width, int length, boolean vertical)
	{
		this.parent = parent;
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.length = length;
		this.scrollSize = 100;
		this.scrollPos = 0;
		this.scrollbuttonSize = 11;
		this.vertical = vertical;
		this.buttons = false;
		this.mouseSensitiveScroll = true;
		initImage();
	}
	
	protected void initImage()
	{
		if (vertical)
		{
			scrollU = 0;
			scrollV = 54;
		}
		else
		{
			scrollU = 11;
			scrollV = 54;
		}
	}
	
	public ResourceLocation imageLocation()
	{
		return mainImage;
	}
	
	  /************************/
	 /**** position calc's ***/
	/************************/
	
	protected int guiLeft()
	{
		return ((parent instanceof GuiContainer)? ((GuiContainer)parent).guiLeft: 0);
	}
	
	protected int guiTop()
	{
		return ((parent instanceof GuiContainer)? ((GuiContainer)parent).guiTop: 0);
	}
	
	  /************************/
	 /***** drawer's *********/
	/************************/
	
	public void draw()
	{
		mc.renderEngine.bindTexture(imageLocation());
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		int offset = (int) (((float)length-(float)scrollbuttonSize)*((float)scrollPos/(float)scrollSize));
		if (vertical)
			parent.drawTexturedModalRect(xPos, yPos+offset, scrollU, scrollV, width, scrollbuttonSize);
		else
			parent.drawTexturedModalRect(xPos+offset, yPos, scrollU, scrollV, scrollbuttonSize, width);
	}
	
	  /************************/
	 /***** updater's ********/
	/************************/
	
	protected void setScroll(int newValue)
	{
		scrollPos = Math.min(scrollSize, Math.max(0, newValue));
	}
	
	  /************************/
	 /***** mouse code *******/
	/************************/
	
	public void handleMouseInput()
	{
		if (mouseSensitiveScroll)
		{
			int i = Mouse.getEventDWheel();
	
			if (i != 0)
			{
	
				if (i > 0)
				{
					i = 10;
				}
	
				if (i < 0)
				{
					i = -10;
				}
				
				setScroll(scrollPos - i);
			}
		}
	}
	
	public boolean mouseClicked(int posX, int posY)
	{
		int scrollBarPosX = guiLeft()+xPos;
		int scrollBarPosY = guiTop()+yPos;
		int scrollSizeX;
		int scrollSizeY;
		
		if (vertical)
		{
			scrollSizeX = width;
			scrollSizeY = length;
		}
		else
		{
			scrollSizeX = length;
			scrollSizeY = width;
		}
		
		if (posX >= scrollBarPosX && posY >= scrollBarPosY && posX < scrollBarPosX + scrollSizeX && posY < scrollBarPosY + scrollSizeY)
		{
			return isScrolling = true;
		}
		
		return false;
	}
	
	public void mouseMovedOrUp()
	{
		isScrolling = false;
	}
	
	public void mouseClickMove(int posX, int posY)
	{
		if (isScrolling)
		{
			if (vertical)
			{
				int scrollBarPos = guiTop() + yPos + scrollbuttonSize/2;
				float target = Math.max(0.0F, Math.min((float)(posY - scrollBarPos)/(float)(length-scrollbuttonSize), 1.0f));
				this.setScroll((int)((float)target * scrollSize));
			}
			else
			{
				int scrollBarPos = guiLeft() + xPos + scrollbuttonSize/2;
				float target = Math.max(0.0F, Math.min((float)(posX - scrollBarPos)/(float)(length-scrollbuttonSize), 1.0f));
				this.setScroll((int)((float)target * scrollSize));
			}
		}
	}
	
	
}
