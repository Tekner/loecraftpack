package loecraftpack.content.gui.scroll;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.inventory.Container;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public abstract class GuiScrollWithEffectRender extends InventoryEffectRenderer
{
	public final List<GuiScrollPage> pages = new ArrayList<GuiScrollPage>();
	
	public GuiScrollWithEffectRender(Container par1Container)
	{
		super(par1Container);
	}
	
	/**IMPORTANT: make sure to call this at the end of initGui()*/
	protected void initGuiScroll()
	{
		updateScrollComponents();
		updatePages();
	}
	
	public abstract void updateScrollComponents();
	
	private void updatePages()
	{
		for (GuiScrollPage page: pages)
		{
			if (page != null)
				page.update();
		}
	}
	
	@Override
	public void drawScreen(int posXmouse, int posYmouse, float par3)
	{
		updateScrollComponents();
		updatePages();
		super.drawScreen(posXmouse, posYmouse, par3);
	}
	
	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		for (GuiScrollPage page: pages)
		{
			if (page != null)
				page.handleMouseInput();
		}
	}
	
	@Override
	protected void mouseClicked(int posX, int posY, int par3)
	{
		boolean flag = false;
		for (GuiScrollPage page: pages)
		{
			if (page != null && page.mouseClicked(posX, posY))
				flag = true;
		}
		if (!flag)
			super.mouseClicked(posX, posY, par3);
	}
	
	@Override
	protected void mouseMovedOrUp(int par1, int par2, int par3)
	{
		super.mouseMovedOrUp(par1, par2, par3);
		for (GuiScrollPage page: pages)
		{
			if (page != null)
				page.mouseMovedOrUp();
		}
	}
	
	@Override
	protected void mouseClickMove(int posX, int posY, int par3, long time)
	{
		super.mouseClickMove(posX, posY, par3, time);
		for (GuiScrollPage page: pages)
		{
			if (page != null)
				page.mouseClickMove(posX, posY);
		}
	}
}
