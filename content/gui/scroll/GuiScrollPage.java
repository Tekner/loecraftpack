package loecraftpack.content.gui.scroll;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class GuiScrollPage
{
	protected Minecraft mc = Minecraft.getMinecraft();
	
	public ResourceLocation backgroundImage = null;
	
	protected GuiScreen parent;
	public int xPos, yPos;
	public int width, height;
	public int contentX, contentY;
	public int offsetX, offsetY;
	
	public GuiScrollBar xScroll = null;
	public int oldScrollX = 0;
	
	public GuiScrollBar yScroll = null;
	public int oldScrollY = 0;
	
	public final List<GuiScrollTexture> linkedTextures = new ArrayList<GuiScrollTexture>();
	public final List<GuiScrollString> linkedStrings = new ArrayList<GuiScrollString>();
	public final List<GuiScrollButton> linkedButtons = new ArrayList<GuiScrollButton>();
	
	public GuiScrollPage(GuiScreen parent, int xPos, int yPos, int width, int height, int contentX, int contentY)
	{
		this.parent = parent;
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.height = height;
		this.contentX = contentX;
		this.contentY = contentY;
		offsetX = 0;
		offsetY = 0;
	}
	
	  /************************/
	 /**** position calc's ***/
	/************************/
	
	protected int guiLeft()
	{
		return ((parent instanceof GuiContainer)? ((GuiContainer)parent).guiLeft: 0);
	}
	
	protected int guiTop()
	{
		return ((parent instanceof GuiContainer)? ((GuiContainer)parent).guiTop: 0);
	}
	
	public int grandPosX()
	{
		return guiLeft()+xPos;
	}
	
	public int grandPosY()
	{
		return guiTop()+yPos;
	}
	
	/**used by components*/
	public int getXsubPos(int subPosX)
	{
		return xPos+subPosX+offsetX;
	}
	
	/**used by components*/
	public int getYsubPos(int subPosY)
	{
		return yPos+subPosY+offsetY;
	}
	
	  /************************/
	 /***** drawer's *********/
	/************************/
	
	public void draw(int posXmouse, int posYmouse)
	{
		if (xScroll!=null)
			xScroll.draw();
		if (yScroll!=null)
			yScroll.draw();
		
		ScaledResolution res = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glScissor(grandPosX()*res.getScaleFactor(), mc.displayHeight - (grandPosY()+height)*res.getScaleFactor(), width*res.getScaleFactor(), height*res.getScaleFactor());
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		
		for (GuiScrollTexture texture : linkedTextures)
		{
			texture.draw();
		}
		
		for (GuiScrollString string : linkedStrings)
		{
			string.draw();
		}
		
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
	}
	
	public void drawBackground()
	{
		ScaledResolution res = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glScissor(grandPosX()*res.getScaleFactor(), mc.displayHeight - (grandPosY()+height)*res.getScaleFactor(), width*res.getScaleFactor(), height*res.getScaleFactor());
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		
		if (backgroundImage!= null)
		{
			mc.renderEngine.bindTexture(backgroundImage);
			parent.drawTexturedModalRect(grandPosX()+offsetX, grandPosY()+offsetY, 0, 0, contentX, contentY);
		}
		
		for (GuiScrollTexture texture : linkedTextures)
		{
			texture.drawBackground();
		}
		
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
	}
	
	  /************************/
	 /***** updater's ********/
	/************************/
	
	public void update()
	{
		boolean change = false;
		
		if (xScroll != null && xScroll.scrollPos != oldScrollX)
		{
			offsetX = -(int)((float)xScroll.scrollPos/(float)xScroll.scrollSize * (float)(contentX-width));
			oldScrollX = xScroll.scrollPos;
			change = true;
		}
		
		if (yScroll != null && yScroll.scrollPos != oldScrollY)
		{
			offsetY = -(int)((float)yScroll.scrollPos/(float)yScroll.scrollSize * (float)(contentY-height));
			oldScrollY = yScroll.scrollPos;
			change = true;
		}
		
		if (change)
		{
			for (GuiScrollTexture texture : linkedTextures)
			{
				texture.updateVisability();
			}
			
			for (GuiScrollString string : linkedStrings)
			{
				string.updateVisability();
			}
			
			for (GuiScrollButton button : linkedButtons)
			{
				button.updateVisability();
			}
		}
	}
	
	  /************************/
	 /***** mouse code *******/
	/************************/
	
	public void handleMouseInput()
	{
		if (xScroll!=null)
			xScroll.handleMouseInput();
		if (yScroll!=null)
			yScroll.handleMouseInput();
	}
	
	public boolean mouseClicked(int posX, int posY)
	{
		boolean result = false;
		if (xScroll!=null && xScroll.mouseClicked(posX, posY))
				result = true;
		if (yScroll!=null && yScroll.mouseClicked(posX, posY))
				result = true;
		return result;
	}
	
	public void mouseMovedOrUp()
	{
		if (xScroll!=null)
			xScroll.mouseMovedOrUp();
		if (yScroll!=null)
			yScroll.mouseMovedOrUp();
	}
	
	public void mouseClickMove(int posX, int posY)
	{
		if (xScroll!=null)
			xScroll.mouseClickMove(posX, posY);
		if (yScroll!=null)
			yScroll.mouseClickMove(posX, posY);
	}
}
