package loecraftpack.content.gui.scroll;

import net.minecraft.client.gui.FontRenderer;

public class GuiScrollString
{
	protected GuiScrollPage page;
	protected FontRenderer fontRenderer;
	
	protected int pagePosX, pagePosY;
	protected String value;
	public int color = 16777215;
	
	public int stringWidth;
	public boolean drawString;
	public boolean enabled = true;
	
	public GuiScrollString(GuiScrollPage page, FontRenderer fontRenderer)
	{
		this(page, fontRenderer, 0, 0, "");
	}
	
	public GuiScrollString(GuiScrollPage page, FontRenderer fontRenderer, int subPosX, int subPosY, String value)
	{
		this.page = page;
		page.linkedStrings.add(this);
		
		this.fontRenderer = fontRenderer;
		
		this.pagePosX = subPosX;
		this.pagePosY = subPosY;
		
		this.value = value;
		stringWidth = this.fontRenderer.getStringWidth(value);
		
		updateVisability();
	}
	
	  /************************/
	 /***** updater's ********/
	/************************/
	
	public GuiScrollString updateStringValue(String value)
	{
		this.value = value;
		stringWidth = this.fontRenderer.getStringWidth(value);
		
		updateVisability();
		return this;
	}
	
	public GuiScrollString updateStringPos(int subPosX, int subPosY)
	{
		this.pagePosX = subPosX;
		this.pagePosY = subPosY;
		
		updateVisability();
		return this;
	}
	
	public GuiScrollString setColor(int color)
	{
		this.color = color;
		return this;
	}
	
	public void updateVisability()
	{
		this.drawString = !(pagePosX+page.offsetX <= -stringWidth || pagePosX+page.offsetX >= page.width ||
							pagePosY+page.offsetY <= -fontRenderer.FONT_HEIGHT || pagePosY+page.offsetY >= page.height);
	}
	
	  /************************/
	 /***** drawer's *********/
	/************************/
	
	public void draw()
	{
		if (this.drawString)
		{
			fontRenderer.drawString(value, page.getXsubPos(pagePosX), page.getYsubPos(pagePosY), color);
		}
	}
}
