package loecraftpack.content.gui.scroll;

import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class GuiScrollTexture
{
	protected GuiScrollPage page;
	protected int pagePosX, pagePosY;
	
	protected ResourceLocation image;
	protected int posU, posV;
	
	protected int width, height;
	
	public boolean drawInBackground = false;
	protected boolean drawTexture = true;
	
	public GuiScrollTexture(GuiScrollPage page, ResourceLocation image, int pagePosX, int pagePosY, int posU, int posV, int width, int height)
	{
		page.linkedTextures.add(this);
		
		this.page = page;
		this.image = image;
		
		this.width = width;
		this.height = height;
		
		updatePosition(pagePosX, pagePosY);
		
		this.posU = posU;
		this.posV = posV;
		
		
	}
	
	public GuiScrollTexture inBackground()
	{
		drawInBackground = true;
		return this;
	}
	
	  /************************/
	 /***** updater's ********/
	/************************/
	
	public void updatePosition(int pagePosX, int pagePosY)
	{
		this.pagePosX = pagePosX;
		this.pagePosY = pagePosY;
		
		updateVisability();
	}
	
	public void updateVisability()
	{
		int x = pagePosX+page.offsetX;
		int y = pagePosY+page.offsetY;
		drawTexture = !(x <= -width || x >= page.width ||
						y <= -height || y>= page.height);
	}
	
	  /************************/
	 /***** drawer's *********/
	/************************/
	
	public void draw()
	{
		if (!drawInBackground && this.drawTexture)
		{
			int x = page.getXsubPos(pagePosX);
			int y = page.getYsubPos(pagePosY);
			
			drawContent(x, y);
		}
	}
	
	public void drawBackground()
	{
		if (drawInBackground && this.drawTexture)
		{
			int x = page.guiLeft()+page.getXsubPos(pagePosX);
			int y = page.guiTop()+page.getYsubPos(pagePosY);
			
			drawContent(x, y);
		}
	}
	
	protected void drawContent(int x, int y)
	{
		page.mc.getTextureManager().bindTexture(image);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		page.parent.drawTexturedModalRect(x, y, posU, posV, width, height);
	}
}
