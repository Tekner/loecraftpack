package loecraftpack.content.gui.tooltip;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class TooltipHandler
{
	/**
	 * 
	 * @param components
	 * @param guiLeft - the gui's x=0
	 * @param guiTop - the gui's y=0
	 * @param posX - the tooltip's x=0
	 * @param posY - the tooltip's y=0
	 */
	public static void draw(List<ComponentBase> components, int guiLeft, int guiTop, int posX, int posY)
	{
		if (components==null || components.size()==0)
			return;
		
		posX -= guiLeft;
		posY -= guiTop;
		
		for (ComponentBase component : components)
		{
			component.draw(posX, posY);
		}
	}
	
	/**
	 * 
	 * @param components
	 * @param guiLeft - the gui's x=0
	 * @param guiTop - the gui's y=0
	 * @param posX - the tooltip's x=0
	 * @param posY - the tooltip's y=0
	 * @param minX - bounding box
	 * @param minY - bounding box
	 * @param maxX - bounding box
	 * @param maxY - bounding box
	 */
	public static void drawWithBounds(List<ComponentBase> components, int guiLeft, int guiTop, int posX, int posY,
																	  int minX, int minY, int maxX, int maxY)
	{
		if (components==null || components.size()==0)
			return;
		
		int sizeWH[] = TooltipHandler.detectDimesions(components);
		
		if (sizeWH[2] > maxX - minX)
			posX = minX + MathHelper.ceiling_float_int(( (float)maxX - (float)minX - (float)sizeWH[2] )/2.0f);
		else if (maxX < posX + sizeWH[2])
			posX = maxX - sizeWH[2];
		else if (posX < minX)
			posX = minX;
		
		if (sizeWH[3] > maxY - minY)
			posY = minY + MathHelper.ceiling_float_int(( (float)maxY - (float)minY - (float)sizeWH[3] )/2.0f);
		else if (maxY < posY + sizeWH[3])
			posY = maxY - sizeWH[3];
		else if (posY < minY)
			posY = minY;
		
		draw(components, guiLeft, guiTop, posX, posY);
	}
	
	public static int[] detectDimesions(List<ComponentBase> components)
	{
		if (components==null || components.size()==0)
			return null;
		
		int[] result = {0, 0, 0, 0};
		
		result[0] = components.get(0).subPosX;
		result[1] = components.get(0).subPosY;
		result[2] = components.get(0).subPosX;
		result[3] = components.get(0).subPosY;
		
		for (ComponentBase component : components)
		{
			result[0] = Math.min(result[0], component.subPosX);
			result[1] = Math.min(result[1], component.subPosY);
			
			result[2] = Math.max(result[2], component.subPosX+component.width);
			result[3] = Math.max(result[3], component.subPosY+component.height);
		}
		
		return result;
	}
	
	public static List<ComponentBase> generateAndAddBorderAndBackgroundTexture(List<ComponentBase> components, ResourceLocation mainImage,
																			int minU, int minV, int sizeU, int sizeV, int textureScaleU, int textureScaleV,
																			int borderU, int borderV, int borderX, int borderY)
	{
		if (components==null || components.size()==0)
			return null;
		
		List<ComponentBase> result = new ArrayList<ComponentBase>();
		
		int bounds[] = TooltipHandler.detectDimesions(components);
		
		int u1 = minU; 
		int u2 = minU+borderU;
		int u3 = minU+sizeU-borderU;
		int midU = sizeU - borderU*2;
		
		int v1 = minV; 
		int v2 = minV+borderV;
		int v3 = minV+sizeV-borderV;
		int midV = sizeV - borderV*2;
		
		int x1 = bounds[0]-borderX; 
		int x2 = bounds[0];
		int x3 = bounds[2];
		int midX = bounds[2]-bounds[0];
		
		int y1 = bounds[1]-borderY; 
		int y2 = bounds[1];
		int y3 = bounds[3];
		int midY = bounds[3]-bounds[1];
		
		//top-left
		result.add(new ComponentTexture(mainImage, u1, v1, borderU, borderV, textureScaleU, textureScaleV, x1, y1, borderX, borderY));
		//top
		result.add(new ComponentTexture(mainImage, u2, v1,    midU, borderV, textureScaleU, textureScaleV, x2, y1,    midX, borderY));
		//top-right
		result.add(new ComponentTexture(mainImage, u3, v1, borderU, borderV, textureScaleU, textureScaleV, x3, y1, borderX, borderY));
		//left
		result.add(new ComponentTexture(mainImage, u1, v2, borderU,    midV, textureScaleU, textureScaleV, x1, y2, borderX,    midY));
		//center
		result.add(new ComponentTexture(mainImage, u2, v2,    midU,    midV, textureScaleU, textureScaleV, x2, y2,    midX,    midY));
		//right
		result.add(new ComponentTexture(mainImage, u3, v2, borderU,    midV, textureScaleU, textureScaleV, x3, y2, borderX,    midY));
		//bottem-left
		result.add(new ComponentTexture(mainImage, u1, v3, borderU, borderV, textureScaleU, textureScaleV, x1, y3, borderX, borderY));
		//bottom
		result.add(new ComponentTexture(mainImage, u2, v3,    midU, borderV, textureScaleU, textureScaleV, x2, y3,    midX, borderY));
		//bottom-right
		result.add(new ComponentTexture(mainImage, u3, v3, borderU, borderV, textureScaleU, textureScaleV, x3, y3, borderX, borderY));
		
		result.addAll(components);
		
		return result;
	}
}
