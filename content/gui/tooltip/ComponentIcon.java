package loecraftpack.content.gui.tooltip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.IIcon;

public class ComponentIcon extends ComponentBase
{
	public final IIcon icon;
	
	public ComponentIcon(IIcon icon, int subPosX, int subPosY, int width, int height)
	{
		this.icon = icon;
		
		this.subPosX = subPosX;
		this.subPosY = subPosY;
		this.width = width;
		this.height = height;
	}
	
	public ComponentIcon(IIcon icon, int subPosX, int subPosY, int width, int height, int rgba)
	{
		this(icon, subPosX, subPosY, width, height);
		this.rgba = rgba;
	}
	
	public void draw(int posX, int posY)
	{
		super.draw(posX, posY);
		
		int posX2= posX + subPosX;
		int posY2= posY + subPosY;
		double zLevel = 0;
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.locationItemsTexture);
		
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(posX2 + 0)    , (double)(posY2 + height), zLevel, icon.getMinU(), icon.getMaxV());
		tessellator.addVertexWithUV((double)(posX2 + width), (double)(posY2 + height), zLevel, icon.getMaxU(), icon.getMaxV());
		tessellator.addVertexWithUV((double)(posX2 + width), (double)(posY2 + 0)     , zLevel, icon.getMaxU(), icon.getMinV());
		tessellator.addVertexWithUV((double)(posX2 + 0)    , (double)(posY2 + 0)     , zLevel, icon.getMinU(), icon.getMinV());
		tessellator.draw();
	}
}
