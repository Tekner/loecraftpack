package loecraftpack.content.gui.tooltip;

import java.util.List;

public class ComponentSet extends ComponentBase {

	List<ComponentBase> components;
	
	public ComponentSet(List<ComponentBase> components)
	{
		this.components = components;
		int[] bounds = TooltipHandler.detectDimesions(components);
		if (bounds != null)
		{
			this.subPosX = bounds[0];
			this.subPosY = bounds[1];
			this.width = bounds[2]-subPosX;
			this.height = bounds[3]-subPosY;
		}
	}
	
	@Override
	public void draw(int offsetX, int offsetY)
	{
		for (ComponentBase target :components)
			target.draw(offsetX, offsetY);
	}

}
