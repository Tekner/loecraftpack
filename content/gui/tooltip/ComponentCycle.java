package loecraftpack.content.gui.tooltip;

import java.util.List;

public class ComponentCycle extends ComponentBase
{
	List<ComponentBase> components;
	int delay;
	
	public ComponentCycle(List<ComponentBase> components, int delay)
	{
		this.components = components;
		this.delay = delay;
		int[] bounds = TooltipHandler.detectDimesions(components);
		if (bounds != null)
		{
			this.subPosX = bounds[0];
			this.subPosY = bounds[1];
			this.width = bounds[2]-subPosX;
			this.height = bounds[3]-subPosY;
		}
	}

	@Override
	public void draw(int offsetX, int offsetY) 
	{
		ComponentBase target = components.get((int) ((System.currentTimeMillis()/delay)%components.size()));
		if (target != null)
			target.draw(offsetX, offsetY);
	}

}
