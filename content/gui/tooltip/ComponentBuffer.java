package loecraftpack.content.gui.tooltip;

/**purely a buffer**/
public class ComponentBuffer extends ComponentBase {

	public ComponentBuffer(int subPosX, int subPosY, int width, int height)
	{
		this.subPosX = subPosX;
		this.subPosY = subPosY;
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void draw(int offsetX, int offsetY){}//- Do nothing

}
