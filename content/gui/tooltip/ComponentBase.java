package loecraftpack.content.gui.tooltip;

import org.lwjgl.opengl.GL11;

public abstract class ComponentBase {
	
	public int subPosX;
	public int subPosY;
	public int width;
	public int height;
	public int rgba = 16777215;//white
	
	public void draw(int offsetX, int offsetY)
	{
		float red = (float)(rgba >> 16 & 255) / 255.0F;
		float green = (float)(rgba >> 8 & 255) / 255.0F;
		float blue = (float)(rgba & 255) / 255.0F;
		GL11.glColor4f(red, green, blue, 1.0F);
	}

}
