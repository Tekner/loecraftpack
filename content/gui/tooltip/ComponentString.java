package loecraftpack.content.gui.tooltip;

import net.minecraft.client.gui.FontRenderer;

public class ComponentString extends ComponentBase
{
	public final FontRenderer font;
	public final String text;
	
	public ComponentString(FontRenderer font, int subPosX, int subPosY, String text)
	{
		this.font = font;
		this.subPosX = subPosX;
		this.subPosY = subPosY;
		this.text = text;
		width = font.getStringWidth(text);
		height = font.FONT_HEIGHT+1;
	}

	@Override
	public void draw(int posX, int posY)
	{
		super.draw(posX, posY);
		
		font.drawStringWithShadow(text, posX+subPosX, posY+subPosY, -1);
	}

}
