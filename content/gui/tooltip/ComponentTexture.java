package loecraftpack.content.gui.tooltip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

public class ComponentTexture extends ComponentBase
{
	public final ResourceLocation mainImage;
	public final double u1;
	public final double v1;
	public final double u2;
	public final double v2;
	
	public ComponentTexture(ResourceLocation mainImage, int minU, int minV, int sizeU, int sizeV, int textureScaleU, int textureScaleV,
														int subPosX, int subPosY, int width, int height)
	{
		this.mainImage = mainImage;
		this.u1 = (double)minU / (double)textureScaleU;
		this.u2 = (double)(sizeU+minU) / (double)textureScaleU;
		this.v1 = (double)minV / (double)textureScaleV;
		this.v2 = (double)(sizeV+minV) / (double)textureScaleV;
		
		this.subPosX = subPosX;
		this.subPosY = subPosY;
		this.width = width;
		this.height = height;
	}
	
	public void draw(int posX, int posY)
	{
		super.draw(posX, posY);
		
		int posX2= posX + subPosX;
		int posY2= posY + subPosY;
		double zLevel = 0;
		
		Minecraft.getMinecraft().renderEngine.bindTexture(mainImage);
		
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(posX2 + 0)    , (double)(posY2 + height), zLevel, u1, v2);
		tessellator.addVertexWithUV((double)(posX2 + width), (double)(posY2 + height), zLevel, u2, v2);
		tessellator.addVertexWithUV((double)(posX2 + width), (double)(posY2 + 0)     , zLevel, u2, v1);
		tessellator.addVertexWithUV((double)(posX2 + 0)    , (double)(posY2 + 0)     , zLevel, u1, v1);
		tessellator.draw();
	}

}