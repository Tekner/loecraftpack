package loecraftpack.content.gui;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class CreativeTabBlock extends CreativeTabs
{
	protected static List<Block> loeSortedBlockList = new ArrayList<Block>();
	
	public CreativeTabBlock(String label)
	{
		super(label);
	}
	
	//Set the icon - Do: CreativeTab - Add new item with custom icon for use here 
	@Override
	public Item getTabIconItem() {
		return Items.writable_book;
	}
	
	@Override
	public void displayAllReleventItems(List par1List)
	{
		for (Block block : loeSortedBlockList)
		{
			if (block != null)
			{
				Item item = Item.getItemFromBlock(block);
				if (item != null)
					item.getSubItems(item, this, par1List);
			}
		}
		
		if (this.func_111225_m() != null)
		{
			this.addEnchantmentBooksToList(par1List, this.func_111225_m());
		}
	}
	
	public static void addToList(Block... blocks)
	{
		for (Block block : blocks)
		{
			if (block != null)
			{
				loeSortedBlockList.add(block);
			}
		}
	}

	
}
