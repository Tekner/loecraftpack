package loecraftpack.content.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

public class GuiQuiver extends GuiContainer
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/quiverInventory.png");
	
	public GuiQuiver(EntityPlayer player)
	{
		super(new ContainerQuiver(player));
		this.ySize = 145;
	}
	
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal("Quiver"), 52, 8, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, this.ySize - 96 + 4, 4210752);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(mainImage);
		int xPos = this.guiLeft;
		int yPos = this.guiTop;
		this.drawTexturedModalRect(xPos, yPos, 0, 0, this.xSize, this.ySize);
	}
}
