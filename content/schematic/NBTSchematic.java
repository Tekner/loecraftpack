package loecraftpack.content.schematic;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.World;

public class NBTSchematic {
	
	String filePath;
	
	//default from file
	short width = 0;//x
	short height = 0;//y
	short length = 0;//z
	
	Block[][][] blocks;
	int[][][] metaDatas;
	NBTTagList entities;
	NBTTagList tileEntities;
	
	//with any translations
	short widthT = 0;//x
	short heightT = 0;//y
	short lengthT = 0;//z
	
	Block[][][] blocksT;
	int[][][] metaDatasT;
	NBTTagList entitiesT;
	NBTTagList tileEntitiesT;
	
	int rotationCurrent;
	
	List<Block> ignoreIDsWhenPasting = new ArrayList<Block>();
	
	boolean valid = false;
	
	
	
	  /*******************************/
	 /*****CONSTRUCTORS**************/
	/*******************************/
	
	public NBTSchematic(String filePath)
	{
		this.filePath = filePath;
		NBTTagCompound nbtCompound = getNBTFromSchematicFile(filePath);
		if (nbtCompound!=null)
		{
			widthT = width = nbtCompound.getShort("Width");
			heightT = height = nbtCompound.getShort("Height");
			lengthT = length = nbtCompound.getShort("Length");
			
			String version = nbtCompound.getString("Materials");
			
			//yzx
			byte[] blockIDs = nbtCompound.getByteArray("Blocks");
			byte[] data = nbtCompound.getByteArray("Data");
			
			blocks = new Block[width][height][length];
			metaDatas = new int[width][height][length];
			
			
			int index = 0;
			for (int y=0; y<height; y++)
			{
				for (int z=0; z<length; z++)
				{
					for (int x=0; x<width; x++)
					{
						blocks[x][y][z] = getBlockConversion(blockIDs[index], version);
						metaDatas[x][y][z] = data[index];
						index++;
					}
				}
			}
			
			blocksT = blocks.clone();
			metaDatasT = metaDatas.clone();
			
			entitiesT = (NBTTagList) (entities = nbtCompound.getTagList("Entities", 10)).copy();
			tileEntitiesT = (NBTTagList) (tileEntities = nbtCompound.getTagList("TileEntities", 10)).copy();
			
			System.out.println("[INFO] loaded Schematic!  "+filePath+"     Mat.Ver. "+version);
			valid = true;
		}
		else
			System.out.println("[ERROR] failed to load Schematic!  "+filePath);
	}
	
	protected NBTTagCompound getNBTFromSchematicFile(String filePath)
	{
		try {
			ClassLoader loader = NBTSchematic.class.getClassLoader();
			InputStream stream = loader.getResourceAsStream("loecraftpack/content/schematic/"+filePath);
			if (stream!=null)
				return CompressedStreamTools.readCompressed(stream);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	
	
	  /*******************************/
	 /*****USAGE*********************/
	/*******************************/
	
	public boolean spawnNBTSchematic(World world, int coordX, int coordY, int coordZ)
	{
		if (!valid)
			return false;
		
		/***********************/
		/***add basic blocks****/
		/***********************/
		//support blocks first, the rest second.
		for (int pass = 0; pass<4; pass++)
		{
			//bottom up, across the x-axis first.
			for (int y=0; y<blocksT[0].length; y++)
			{
				for (int z=0; z<blocksT[0][0].length; z++)
				{
					for (int x=0; x<blocksT.length; x++)
					{
						Block block = blocksT[x][y][z];
						if (block == null)
						{
							//debug: bad IDs
							System.out.println(" "+(coordX+x)+" "+(coordY+y)+" "+(coordZ+z)+"   "+block);
							continue;
						}
						
						if (ignoreID(block))
							continue;//ignored blocks
						
						//print based on pass
						if (pass==getBlockPass(block))
						{
							printBlock(world, coordX+x, coordY+y, coordZ+z, block, metaDatasT[x][y][z], 3);
						}
					}
				}
			}
		}
		
		/***********************/
		/***add tile entities***/
		/***********************/
		
		for (int i=0; i<tileEntitiesT.tagCount(); i++)
		{
			NBTTagCompound Mastercompound = tileEntitiesT.getCompoundTagAt(i);
			if (!Mastercompound.hasNoTags())
			{
				NBTTagCompound compound = (NBTTagCompound)Mastercompound.copy();
				
				String id = compound.getString("id");
				int x = coordX + compound.getInteger("x");
				int y = coordY + compound.getInteger("y");
				int z = coordZ + compound.getInteger("z");
				compound.setInteger("x", x);
				compound.setInteger("y", y);
				compound.setInteger("z", z);
				
				Block block = world.getBlock(x, y, z);
				int metadata = world.getBlockMetadata(x, y, z);
				
				TileEntity tile;
				if (id.matches("Sign"))
				{
					tile = block.createTileEntity(world, metadata);
					tile.readFromNBT(compound);
				}
				else
					tile = TileEntity.createAndLoadEntity(compound);
				
				world.setTileEntity(x, y, z, tile);
			}
		}
		
		/***********************/
		/***add entities********/
		/***********************/
		
		for (int i=0; i<entitiesT.tagCount(); i++)
		{
			NBTTagCompound Mastercompound = entitiesT.getCompoundTagAt(i);
			if (!Mastercompound.hasNoTags())
			{
				NBTTagCompound compound = (NBTTagCompound)Mastercompound.copy();
				
				if (compound.getString("id")==null)
					continue;//do not spawn player entities.
				System.out.println(" adding entity "+compound.getString("id"));
				if (compound.hasKey("TileX"))
				{
					int x = coordX+compound.getInteger("TileX");
					int y = coordY+compound.getInteger("TileY");
					int z = coordZ+compound.getInteger("TileZ");
					compound.setInteger("TileX", x);
					compound.setInteger("TileY", y);
					compound.setInteger("TileZ", z);
				}
				if (compound.hasKey("Pos"))
				{
					int ID = compound.func_150299_b("Pos");
					NBTTagList pos;
					if (ID == 10)
					{
						pos = compound.getTagList("Pos", 10);
					}
					else
					{
						pos = new NBTTagList();
						String damagedTagList = compound.getString("Pos");
						String[] damagedElements = damagedTagList.trim().replace("[", "").replace("]", "").split(",", 3);
						for(String element : damagedElements)
						{
							pos.appendTag(new NBTTagDouble(Double.valueOf(element.split(":")[1].replace(",", ""))));
						}
						compound.setTag("Pos", pos);
					}
					
					double posX = coordX + pos.func_150309_d(0);
					double posY = coordY + pos.func_150309_d(1);
					double posZ = coordZ + pos.func_150309_d(2);
					pos.removeTag(2);
					pos.removeTag(1);
					pos.removeTag(0);
					pos.appendTag(new NBTTagDouble(posX));
					pos.appendTag(new NBTTagDouble(posY));
					pos.appendTag(new NBTTagDouble(posZ));
				}
				
				Entity spawnling = EntityList.createEntityFromNBT(compound, world);
				spawnling.forceSpawn = true;
				world.spawnEntityInWorld(spawnling);
			}
		}
		
		return false;
	}
	
	
	
	  /*******************************/
	 /*****MANIPULATORS**************/
	/*******************************/
	
	public void setIgnoreAir(boolean state)
	{
		if (state)
		{
			ignoreIDsWhenPasting.add(Blocks.air);
		}
		else
		{
			ignoreIDsWhenPasting.remove(Blocks.air);
		}
	}
	
	public void setToDefault()
	{
		widthT = width;
		heightT = height;
		lengthT = length;
		
		blocksT = blocks.clone();
		metaDatasT = metaDatas.clone();
		entitiesT = (NBTTagList) entities.copy();
		tileEntitiesT = (NBTTagList) tileEntities.copy();
		
		rotationCurrent = 0;
	}
	
	public void rotate(int rotation)
	{
		if (rotation < 1 || rotation > 3)
			return;
		/* 0:none
		 * 1:right 90
		 * 2:right 180
		 * 3:left 90
		 */
		
		Block[][][] blockIDsCopy = null;
		int[][][] metaDatasCopy = null;
		short temp;
		
		switch (rotation)
		{
		case 1:
			blockIDsCopy = new Block[lengthT][heightT][widthT];
			metaDatasCopy = new int[lengthT][heightT][widthT];
			for (int x=0; x<widthT; x++)
			{
				for (int y=0; y<heightT; y++)
				{
					for (int z=0; z<lengthT; z++)
					{
						blockIDsCopy[lengthT-1-z][y][x] = blocksT[x][y][z];
						metaDatasCopy[lengthT-1-z][y][x] = rotateYMeta(blocksT[x][y][z], metaDatasT[x][y][z], rotation);
					}
				}
			}
			break;
			
			
			
		case 2:
			blockIDsCopy = new Block[widthT][heightT][lengthT];
			metaDatasCopy = new int[widthT][heightT][lengthT];
			for (int x=0; x<widthT; x++)
			{
				for (int y=0; y<heightT; y++)
				{
					for (int z=0; z<lengthT; z++)
					{
						blockIDsCopy[widthT-1-x][y][lengthT-1-z] = blocksT[x][y][z];
						metaDatasCopy[widthT-1-x][y][lengthT-1-z] = rotateYMeta(blocksT[x][y][z], metaDatasT[x][y][z], rotation);
					}
				}
			}
			break;
			
			
			
		case 3:
			blockIDsCopy = new Block[lengthT][heightT][widthT];
			metaDatasCopy = new int[lengthT][heightT][widthT];
			for (int x=0; x<widthT; x++)
			{
				for (int y=0; y<heightT; y++)
				{
					for (int z=0; z<lengthT; z++)
					{
						blockIDsCopy[z][y][widthT-1-x] = blocksT[x][y][z];
						metaDatasCopy[z][y][widthT-1-x] = rotateYMeta(blocksT[x][y][z], metaDatasT[x][y][z], rotation);
					}
				}
			}
			break;
		}
		
		blocksT = blockIDsCopy;
		metaDatasT = metaDatasCopy;
		
		for (int i=0; i<tileEntitiesT.tagCount(); i++)
		{
			NBTTagCompound nbtcompound = tileEntitiesT.getCompoundTagAt(i);
			if (!nbtcompound.hasNoTags())
			{
				rotateYTile(nbtcompound, rotation);
			}
		}
		
		for (int i=0; i<entitiesT.tagCount(); i++)
		{
			NBTTagCompound nbtcompound = entitiesT.getCompoundTagAt(i);
			if (!nbtcompound.hasNoTags())
			{
				rotateYEntity(nbtcompound, rotation);
			}
		}
		
		if (rotation==1 || rotation==3)
		{
			temp = widthT;
			widthT = lengthT;
			lengthT = temp;
		}
		
		rotationCurrent = (rotationCurrent+rotation)&3;
	}
	
	/**Sub Processes**/  //these are public because they have no direct effect on the schematic.  thus can be used for other things if needed
	
	public static int rotateYMeta(Block blocksT2, int meta, int rotation)
	{
		int temp;
		
		//logs
		if (Block.isEqualTo(blocksT2, Blocks.log) || Block.isEqualTo(blocksT2, Blocks.log2) ||
				Block.isEqualTo(blocksT2, LOE_Blocks.appleBloomLog) ||
				Block.isEqualTo(blocksT2, LOE_Blocks.harmonyLog) ||
				Block.isEqualTo(blocksT2, LOE_Blocks.zapAppleLog) )
		{
			if (rotation == 1 || rotation == 3 )
				return ((meta & 8)>>1)|((meta & 4)<<1);
			else
				return meta;
		}
		
		//torch
		else if (Block.isEqualTo(blocksT2, Blocks.torch) || Block.isEqualTo(blocksT2, Blocks.unlit_redstone_torch) || Block.isEqualTo(blocksT2, Blocks.redstone_torch))
		{
			if ((meta&7) != 0 && (meta&7)<5)
			{
				int [] flow = {3, 1, 0, 2};
				int [] back = {2, 1, 3, 0};
				return back[(flow[(meta-1)&3]+rotation)&3]+1;
			}
		}
		
		//beds
		else if (Block.isEqualTo(blocksT2, Blocks.bed) || Block.isEqualTo(blocksT2, LOE_Blocks.bed))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//pistons
		else if (Block.isEqualTo(blocksT2, Blocks.sticky_piston) || Block.isEqualTo(blocksT2, Blocks.piston) ||
				 Block.isEqualTo(blocksT2, Blocks.piston_head))
		{
			if ((meta&7) < 2)
				return meta;
			return Direction.directionToFacing[(Direction.facingToDirection[meta&7]+rotation)&3] | (meta&8);
		}
		
		//stairs
		else if (Block.isEqualTo(blocksT2, Blocks.oak_stairs) || Block.isEqualTo(blocksT2, Blocks.stone_stairs) ||
				Block.isEqualTo(blocksT2, Blocks.brick_stairs) || Block.isEqualTo(blocksT2, Blocks.stone_brick_stairs) ||
				Block.isEqualTo(blocksT2, Blocks.nether_brick_stairs) || Block.isEqualTo(blocksT2, Blocks.sandstone_stairs) ||
				Block.isEqualTo(blocksT2, Blocks.spruce_stairs) || Block.isEqualTo(blocksT2, Blocks.birch_stairs) ||
				Block.isEqualTo(blocksT2, Blocks.jungle_stairs) ||Block.isEqualTo(blocksT2, Blocks.quartz_stairs) ||
				Block.isEqualTo(blocksT2, Blocks.acacia_stairs) || Block.isEqualTo(blocksT2, Blocks.dark_oak_stairs))
		{
			int [] flow = {3, 1, 0, 2};
			int [] back = {2, 1, 3, 0};
			return back[(flow[meta&3]+rotation)&3] | (meta&12);
		}
		
		//signs
		else if (Block.isEqualTo(blocksT2, Blocks.standing_sign))
		{
			return (meta + rotation*4)&15;
		}
		
		//doors
		else if (Block.isEqualTo(blocksT2, Blocks.wooden_door) || Block.isEqualTo(blocksT2, Blocks.iron_door))
		{
			if ((meta&8) != 0)
				return meta;
			
			return (((meta&3)+rotation)&3) | (meta&4);
		}
		
		//ladders - wall signs - furnace - Dispensers - Droppers - Hoppers
		else if (Block.isEqualTo(blocksT2, Blocks.ladder) || 
				Block.isEqualTo(blocksT2, Blocks.wall_sign) || 
				Block.isEqualTo(blocksT2, Blocks.furnace) || Block.isEqualTo(blocksT2, Blocks.lit_furnace) ||
				Block.isEqualTo(blocksT2, Blocks.dispenser) ||
				Block.isEqualTo(blocksT2, Blocks.dropper) ||
				Block.isEqualTo(blocksT2, Blocks.hopper))
		{
			if ((meta&7) < 2)
				return meta;
			return Direction.directionToFacing[(Direction.facingToDirection[meta&7]+rotation)&3] | (meta&8);
		}
		
		//chest
		else if (Block.isEqualTo(blocksT2, Blocks.chest) || Block.isEqualTo(blocksT2, Blocks.trapped_chest) ||
				 Block.isEqualTo(blocksT2, Blocks.ender_chest))
		{
			if ((meta&7) < 2)
				return meta;
			
			return Direction.directionToFacing[(Direction.facingToDirection[meta&7]+rotation)&3];
		}
		
		//levers (ignore ceiling and floor)
		else if (Block.isEqualTo(blocksT2, Blocks.lever))
		{
			if ((meta&7) < 1 || (meta&7) > 4)
				return meta;
			
			int [] flow = {3, 1, 0, 2};
			int [] back = {2, 1, 3, 0};
			return (back[(flow[(meta-1)&3]+rotation)&3]+1) | (meta&8);
		}
		
		//buttons
		else if (Block.isEqualTo(blocksT2, Blocks.stone_button) || Block.isEqualTo(blocksT2, Blocks.wooden_button))
		{
			int [] flow = {3, 1, 0, 2};
			int [] back = {2, 1, 3, 0};
			return (back[(flow[(meta-1)&3]+rotation)&3]+1) | (meta&8);
		}
		
		//pumpkin - jack o lantern
		else if (Block.isEqualTo(blocksT2, Blocks.pumpkin) || Block.isEqualTo(blocksT2, Blocks.lit_pumpkin))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//Redstone Repeater - Redstone Comparator
		else if (Block.isEqualTo(blocksT2, Blocks.unpowered_repeater) || Block.isEqualTo(blocksT2, Blocks.powered_repeater) ||
				Block.isEqualTo(blocksT2, Blocks.unpowered_comparator) || Block.isEqualTo(blocksT2, Blocks.powered_comparator))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//trap door
		else if (Block.isEqualTo(blocksT2, Blocks.trapdoor))
		{
			int [] flow = {3, 1, 0, 2};
			int [] back = {2, 1, 3, 0};
			return (back[(flow[(meta)&3]+rotation)&3]) | (meta&8);
		}
		
		//vines
		else if (Block.isEqualTo(blocksT2, Blocks.vine))
		{
			if (meta == 0)
				return meta;
			temp = meta&15;
			return ((temp>>(4-rotation)) | (temp<<rotation))&15;
		}
		
		//fence gate
		else if (Block.isEqualTo(blocksT2, Blocks.fence_gate))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//end portal block
		else if (Block.isEqualTo(blocksT2, Blocks.end_portal_frame))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//cocoa
		else if (Block.isEqualTo(blocksT2, Blocks.cocoa))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//trip wire hook
		else if (Block.isEqualTo(blocksT2, Blocks.tripwire_hook))
		{
			return (((meta&3)+rotation)&3) | (meta&12);
		}
		
		//heads
		else if (Block.isEqualTo(blocksT2, Blocks.skull))
		{
			//Debug : this might be wrong
			if ((meta&7) < 2)
				return meta;
			return Direction.directionToFacing[(Direction.facingToDirection[meta&7]+rotation)&3] | (meta&8);
		}
		
		//block of quartz
		else if (Block.isEqualTo(blocksT2, Blocks.quartz_block))
		{
			if (rotation == 1 || rotation == 3 )
			{
				if (meta == 3)
					return 4;
				if (meta == 4)
					return 3;
			}
		}
		
		//anvil
		else if (Block.isEqualTo(blocksT2, Blocks.anvil))
		{
			if (rotation == 1 || rotation == 3 )
			{
				if ((meta&1) == 0)
					return meta | 1;
				if ((meta&1) == 1)
					return meta & 14;
			}
		}
		
		return meta;
	}
	
	public void rotateYTile(NBTTagCompound compound, int rotation)
	{
		int tempI;
		
		int x = compound.getInteger("x");
		int z = compound.getInteger("z");
		
		switch(rotation)
		{
		case 1:
			tempI = x;
			x = lengthT-1 - z;
			z = tempI;
			break;
		case 2:
			x = widthT-1 - x;
			z = lengthT-1 - z;
			break;
		case 3:
			tempI = z;
			z = widthT-1 - x;
			x = tempI;
			break;
		}
		
		compound.setInteger("x", x);
		compound.setInteger("z", z);
	}
	
	public void rotateYEntity(NBTTagCompound compound, int rotation)
	{
		String id = compound.getString("id");
		if (id==null)
			return;//do not bother with players
		
		int tempI;
		double tempD;
		
		if (compound.hasKey("TileX"))
		{
			int x = compound.getInteger("TileX");
			int z = compound.getInteger("TileZ");
			switch(rotation)
			{
			case 1:
				tempI = x;
				x = lengthT-1 - z;
				z = tempI;
				break;
			case 2:
				x = widthT-1 - x;
				z = lengthT-1 - z;
				break;
			case 3:
				tempI = z;
				z = widthT-1 - x;
				x = tempI;
				break;
			}
			compound.setInteger("TileX", x);
			compound.setInteger("TileZ", z);
		}
		if (compound.hasKey("Pos"))
		{
			int ID = compound.func_150299_b("Pos");
			NBTTagList pos;
			if (ID == 10)
			{
				pos = compound.getTagList("Pos", 10);
			}
			else
			{
				pos = new NBTTagList();
				String damagedTagList = compound.getString("Pos");
				String[] damagedElements = damagedTagList.trim().replace("[", "").replace("]", "").split(",", 3);
				for(String element : damagedElements)
				{
					pos.appendTag(new NBTTagDouble(Double.valueOf(element.split(":")[1].replace(",", ""))));
				}
				compound.setTag("Pos", pos);
			}
			double posX = pos.func_150309_d(0);
			double posY = pos.func_150309_d(1);
			double posZ = pos.func_150309_d(2);
			switch(rotation)
			{
			case 1:
				tempD = posX;
				posX = lengthT - posZ;
				posZ = tempD;
				break;
			case 2:
				posX = widthT - posX;
				posZ = lengthT - posZ;
				break;
			case 3:
				tempD = posZ;
				posZ = widthT - posX;
				posX = tempD;
				break;
			}
			pos.removeTag(2);
			pos.removeTag(1);
			pos.removeTag(0);
			pos.appendTag(new NBTTagDouble(posX));
			pos.appendTag(new NBTTagDouble(posY));
			pos.appendTag(new NBTTagDouble(posZ));
		}
		if (compound.hasKey("Direction") || compound.hasKey("Dir"))
		{
			if (id.contentEquals("ItemFrame") || id.contentEquals("Painting"))
			{
				byte direction;
				
				if (compound.hasKey("Direction"))
					direction = compound.getByte("Direction");
				else
					direction = compound.getByte("Dir");
				
				direction = (byte) ((direction + rotation)&3);
				
				compound.setByte("Direction", direction);
			}
			else if (id.contentEquals("loecraftpack.pedestal"))
			{
				int direction = compound.getInteger("Direction");
				
				direction = (direction + (rotation*90));
				
				compound.setInteger("Direction", direction);
			}
		}
	}
	
	
	
	  /*******************************/
	 /*****Special Case**************/
	/*******************************/	
	
	protected boolean ignoreID(Block block)
	{
		for (Block ids : ignoreIDsWhenPasting)
		{
			if (Block.isEqualTo(block, ids))
				return true;
		}
		return false;
	}
	
	/** 0:dark support block, 1:light support block, 2: dark dependent, 3: light dependent */
	protected int getBlockPass(Block block)
	{
		if (Block.isEqualTo(block, Blocks.air))return 0;//air
		if (Block.isEqualTo(block, Blocks.glowstone))return 1;
		if (Block.isEqualTo(block, Blocks.lit_redstone_lamp))return 1;
		if (block.isOpaqueCube())return 0;//common simple building block
		if (Block.isEqualTo(block, Blocks.farmland))return 0;
		if (Block.isEqualTo(block, Blocks.trapdoor)) return 2;
		if (Block.isEqualTo(block, Blocks.unlit_redstone_torch)) return 2;
		if (Block.isEqualTo(block, Blocks.redstone_torch)) return 3;
		if (Block.isEqualTo(block, Blocks.stone_button)) return 2;
		if (Block.isEqualTo(block, Blocks.ladder)) return 2;
		if (Block.isEqualTo(block, Blocks.wall_sign)) return 2;
		if (Block.isEqualTo(block, Blocks.torch)) return 3;
		if (Block.isEqualTo(block, Blocks.tripwire)) return 2;
		if (Block.isEqualTo(block, Blocks.lever)) return 2;
		if (Block.isEqualTo(block, Blocks.wooden_door)) return 2;
		if (Block.isEqualTo(block, Blocks.iron_door)) return 2;
		if (Block.isEqualTo(block, Blocks.vine)) return 2;
		if (Block.isEqualTo(block, Blocks.wheat)) return 2;
		return 0;
	}

	protected void printBlock(World world, int x, int y, int z, Block block, int metaData, int flag)
	{
		if (Block.isEqualTo(block, Blocks.wooden_door) || Block.isEqualTo(block, Blocks.iron_door))
		{
			// bug fix: doors falling apart before the top part can be printed (typically because of double doors)
			world.setBlock(x, y, z, block, metaData, 2);
		}
		else
		{
			world.setBlock(x, y, z, block, metaData, flag);
			// bug fix: chest's changing their meta-data on placement
			if (Block.isEqualTo(block, Blocks.chest) || Block.isEqualTo(block, Blocks.trapped_chest))
				world.setBlockMetadataWithNotify(x, y, z, metaData, flag);
		}
	}
	
	
	
	  /*******************************/
	 /*****GETTERS*******************/
	/*******************************/
	
	public int getXSizeDefault()
	{
		return this.width;
	}
	
	public int getZSizeDefault()
	{
		return this.length;
	}
	
	public int getRotation()
	{
		return this.rotationCurrent;
	}
	
	public boolean isvalid()
	{
		return this.valid;
	}
	
	public String getFileLocation()
	{
		return this.filePath;
	}
	
	
	
	  /*******************************/
	 /*****DEBUG*********************/
	/*******************************/
	
	public void printSchematicDEBUG()
	{
		System.out.println("[[----------DEBUG: SCHEMATIC----------]]");
		System.out.println("FILE PATH: "+filePath);
		System.out.println("IS VALID:  "+valid);
		System.out.println("[X]Width:  "+width);
		System.out.println("[Y]Height: "+height);
		System.out.println("[Z]Length: "+length);
		
		System.out.println();
		System.out.print("NO PASTE IDS:");
		for (Block ids : ignoreIDsWhenPasting)
			System.out.print(" "+ids);
		System.out.println();
		
		System.out.println("ENTITIES TO LOAD: "+(entities!=null? entities.tagCount(): ""));
		System.out.println("TILE ENTITIES TO LOAD: "+(tileEntities!=null? tileEntities.tagCount(): ""));
		
		System.out.println("");
	}
	
	
	
	  /*******************************/
	 /*****Version Func.*************/
	/*******************************/
	
	public Block getBlockConversion(int blockID, String version)
	{
		if (blockID < 0)
		{
			blockID += 256;
		}
		
		if (blockID > 197)
		{
			System.out.println(" coverting ID "+blockID);
			switch(blockID)
			{
			/*case 665:
				return LOE_Blocks.bank;*/
			/*case 666:
				return LOE_Blocks.monolith;*/
			/*case 667:
				return LOE_Blocks.projectTable;*/
			case 607:
				return LOE_Blocks.bed;
			case 671:
				return LOE_Blocks.appleBloomSapling;
			case 672:
				return LOE_Blocks.zapAppleSapling;
			case 673:
				return LOE_Blocks.appleBloomLog;
			case 674:
				return LOE_Blocks.zapAppleLog;
			case 675:
				return LOE_Blocks.appleBloomLeaves;
			case 676:
				return LOE_Blocks.zapAppleLeaves;
			case 677:
				return LOE_Blocks.zapAppleLeavesCharged;
			case 678:
				return LOE_Blocks.gemOre;
			case 679:
				return LOE_Blocks.bossChest;
			case 680:
				return LOE_Blocks.coffin;
			case 681:
				return LOE_Blocks.harmonySapling;
			case 682:
				return LOE_Blocks.harmonyLog;
			case 683:
				return LOE_Blocks.harmonyLeaves;
			case 684:
				return LOE_Blocks.plunderVine;
			/*case 685:
				return LOE_Blocks.brewingStation;*/
			/*case 686:
				return LOE_Blocks.gemCake;*/
			/*case 687:
				return LOE_Blocks.liquidRainbow;*/
			case 688:
				return LOE_Blocks.cloud;
			case 689:
				return LOE_Blocks.cloudThunder;
			case 690:
				return LOE_Blocks.tomOre;
				
			}
		}
		
		return Block.getBlockById(blockID);
	}
}