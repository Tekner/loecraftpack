package loecraftpack.content.materials;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class CustomMaterial extends Material {

	protected boolean isTranslucent;
	
	public CustomMaterial(MapColor par1MapColor) {
		super(par1MapColor);
	}
	
	protected Material setTranslucent()
	{
		this.isTranslucent = true;
		return this;
	}
	
	@Override
	public boolean isOpaque()
	{
		return this.isTranslucent ? false : this.blocksMovement();
	}
}
