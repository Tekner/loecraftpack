package loecraftpack.content.materials;

import net.minecraft.block.material.MapColor;

public class MaterialCloud extends CustomMaterial{

	public MaterialCloud() {
		super(MapColor.airColor);
		setTranslucent();
	}

}
