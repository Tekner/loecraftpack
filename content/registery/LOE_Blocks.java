package loecraftpack.content.registery;

import loecraftpack.content.blocks.BlockAppleBloomLeaves;
import loecraftpack.content.blocks.BlockAppleBloomSapling;
import loecraftpack.content.blocks.BlockBank;
import loecraftpack.content.blocks.BlockBrewingStation;
import loecraftpack.content.blocks.BlockCloud;
import loecraftpack.content.blocks.BlockColoredBed;
import loecraftpack.content.blocks.BlockCustomLog;
import loecraftpack.content.blocks.BlockDungeonChest;
import loecraftpack.content.blocks.BlockGemCake;
import loecraftpack.content.blocks.BlockHarmonyLeaves;
import loecraftpack.content.blocks.BlockHarmonySapling;
import loecraftpack.content.blocks.BlockHiddenOre;
import loecraftpack.content.blocks.BlockPlunderVine;
import loecraftpack.content.blocks.BlockProjectTable;
import loecraftpack.content.blocks.BlockProtectionMonolith;
import loecraftpack.content.blocks.BlockTom;
import loecraftpack.content.blocks.BlockZapAppleLeaves;
import loecraftpack.content.blocks.BlockZapAppleLeavesCharged;
import loecraftpack.content.blocks.BlockZapAppleSapling;
import loecraftpack.content.gui.CreativeTabBlock;
import loecraftpack.content.items.ItemHiddenOre;
import loecraftpack.content.items.ItemLeavesAppleBloom;
import loecraftpack.content.items.ItemLeavesHarmony;
import net.minecraft.block.Block;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.registry.GameRegistry;

public class LOE_Blocks
{
	public static BlockHiddenOre gemOre;
	public static BlockTom tomOre;
	
	//public static BlockRainbowFluid liquidRainbow;
	
	public static BlockCloud cloud;
	public static BlockCloud cloudThunder;
	
	public static BlockAppleBloomSapling appleBloomSapling;
	public static BlockZapAppleSapling zapAppleSapling;
	public static BlockHarmonySapling harmonySapling;
	public static BlockCustomLog appleBloomLog;
	public static BlockCustomLog zapAppleLog;
	public static BlockCustomLog harmonyLog;
	public static BlockAppleBloomLeaves appleBloomLeaves;
	public static BlockZapAppleLeaves zapAppleLeaves;
	public static BlockZapAppleLeavesCharged zapAppleLeavesCharged;
	public static BlockHarmonyLeaves harmonyLeaves;
	public static BlockPlunderVine plunderVine;
	
	public static BlockBank bank;
	public static BlockProtectionMonolith monolith;
	public static BlockProjectTable projectTable;
	public static BlockColoredBed bed;
	public static BlockDungeonChest bossChest;
	public static BlockDungeonChest coffin;
	public static BlockBrewingStation brewingStation;
	public static BlockGemCake gemCake;
	
	static int ID_BedBlock;
	
	public static void init(Configuration config)
	{
		ID_BedBlock              = config.get("block", "Bed",                   670).getInt(); //NEW ID: 689
		
		
		gemOre = (BlockHiddenOre)(new BlockHiddenOre()).setHardness(1.5F).setResistance(10.0F).setStepSound(Block.soundTypeStone).setBlockName("oreGem");
		tomOre = (BlockTom)(new BlockTom()).setHardness(1.5F).setResistance(10.0F).setStepSound(Block.soundTypeStone).setBlockName("oreTom");
		
		//liquidRainbow = (BlockRainbowFluid)(new BlockRainbowFluid(ID_LiquidRainbow, LoECraftPack.rainbow, Material.water)).setBlockName("liquid_rainbow");
		
		cloud = (BlockCloud)(new BlockCloud()).setBlockName("cloud");
		cloudThunder = (BlockCloud)(new BlockCloud()).setBlockName("cloudThunder");
		
		appleBloomSapling = (BlockAppleBloomSapling)(new BlockAppleBloomSapling()).setHardness(0.0F).setStepSound(Block.soundTypeGrass).setBlockName("saplingBloom");
		zapAppleSapling = (BlockZapAppleSapling)(new BlockZapAppleSapling()).setHardness(0.0F).setStepSound(Block.soundTypeGrass).setBlockName("saplingZap");
		harmonySapling = (BlockHarmonySapling)(new BlockHarmonySapling()).setHardness(0.0F).setStepSound(Block.soundTypeGrass).setBlockName("saplingHarmony");
		appleBloomLog = (BlockCustomLog)(new BlockCustomLog("loecraftpack:trees/tree_applebloom_side", "loecraftpack:trees/tree_applebloom_top")).setHardness(2.0F).setStepSound(Block.soundTypeWood).setBlockName("logApple");
		zapAppleLog = (BlockCustomLog)(new BlockCustomLog( "loecraftpack:trees/tree_zapapple", "loecraftpack:trees/tree_zapapple_top" )).setHardness(2.0F).setStepSound(Block.soundTypeWood).setBlockName("logZap");
		harmonyLog = (BlockCustomLog)(new BlockCustomLog( "loecraftpack:trees/tree_harmony_side", "loecraftpack:trees/tree_harmony_top")).setHardness(4.0F).setStepSound(Block.soundTypeWood).setBlockName("logHarmony");
		appleBloomLeaves = (BlockAppleBloomLeaves)(new BlockAppleBloomLeaves()).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundTypeGrass);
		zapAppleLeaves = (BlockZapAppleLeaves)(new BlockZapAppleLeaves()).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundTypeGrass);
		zapAppleLeavesCharged = (BlockZapAppleLeavesCharged)(new BlockZapAppleLeavesCharged()).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundTypeGrass);
		harmonyLeaves = (BlockHarmonyLeaves)(new BlockHarmonyLeaves()).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundTypeGrass);
		plunderVine = (BlockPlunderVine)(new BlockPlunderVine()).setHardness(1.0F).setStepSound(Block.soundTypeLadder).setBlockName("plundervine").setBlockTextureName("plundervine");
		
		bank = (BlockBank) new BlockBank().setBlockName("bank");
		monolith = (BlockProtectionMonolith) new BlockProtectionMonolith().setBlockName("protectionMonolith");
		projectTable = (BlockProjectTable) new BlockProjectTable().setBlockName("projecttable");
		bed = (BlockColoredBed)new BlockColoredBed().setHardness(0.2F);
		bossChest = (BlockDungeonChest)(new BlockDungeonChest(0)).setBlockUnbreakable().setResistance(6000000.0F).setStepSound(Block.soundTypeStone).setBlockName("bossChest");
		coffin = (BlockDungeonChest)(new BlockDungeonChest(1)).setHardness(1.5F).setResistance(10.0F).setStepSound(Block.soundTypeStone).setBlockName("coffin");
		brewingStation = (BlockBrewingStation)(new BlockBrewingStation()).setHardness(0.5F).setLightLevel(0.125F).setBlockName("brewingStation").setBlockTextureName("Brewing_Station");
		gemCake = (BlockGemCake)(new BlockGemCake()).setBlockName("gemcake");
		
		CreativeTabBlock.addToList( gemOre, tomOre,/*
									liquidRainbow,*/ cloud, cloudThunder,
									appleBloomSapling, zapAppleSapling, harmonySapling,
									appleBloomLog, zapAppleLog, harmonyLog,
									appleBloomLeaves, zapAppleLeaves, zapAppleLeavesCharged, harmonyLeaves,
									plunderVine, bank, monolith, projectTable,
									bossChest, coffin, brewingStation);
	}
	
	public static void updateDependants()
	{
		gemOre.setItemSet(LOE_Items.gemStones);
	}
	
	public static void register() {
		GameRegistry.registerBlock(gemOre, ItemHiddenOre.class, "Ore^Hidden_Gem");
		GameRegistry.registerBlock(tomOre, "Ore^Tom");
		/*
		GameRegistry.registerBlock(liquidRainbow, "Liquid^Rainbow");
		LanguageRegistry.addName(liquidRainbow, "Liquid Rainbow");
		*/
		GameRegistry.registerBlock(cloud, "Building^Cloud");
		GameRegistry.registerBlock(cloudThunder, "Building^Cloud_Thunder");
		
		GameRegistry.registerBlock(appleBloomSapling,"Decor^Sapling_AppleBloom");
		GameRegistry.registerBlock(zapAppleSapling,"Decor^Sapling_ZapApple");
		GameRegistry.registerBlock(harmonySapling,"Decor^Sapling_Harmony");
		GameRegistry.registerBlock(appleBloomLog,"Building^Log_AppleBloom");
		GameRegistry.registerBlock(zapAppleLog,"Building^Log_ZapApple");
		GameRegistry.registerBlock(harmonyLog,"Building^Log_Harmony");
		GameRegistry.registerBlock(appleBloomLeaves, ItemLeavesAppleBloom.class, "Decor^Leaf_AppleBloom");
		GameRegistry.registerBlock(zapAppleLeaves, ItemLeavesAppleBloom.class, "Decor^Leaf_ZapApple");
		GameRegistry.registerBlock(zapAppleLeavesCharged, ItemLeavesAppleBloom.class, "Decor^Leaf_ZapApple_Charged");
		GameRegistry.registerBlock(harmonyLeaves, ItemLeavesHarmony.class, "Decor^Leaf_Harmony");
		GameRegistry.registerBlock(plunderVine, "Decor^PlunderVine");
		
		GameRegistry.registerBlock(bank, "Device^Bank");
		GameRegistry.registerBlock(monolith, "Device^Protection_Monolith");
		GameRegistry.registerBlock(projectTable, "Device^Project_Table");
		Block.blockRegistry.addObject(ID_BedBlock, "Decor^Colored_Bed", bed);
		//GameRegistry.registerBlock(bed, "Decor^Colored_Bed");
		GameRegistry.registerBlock(bossChest, "Device^Boss_Chest");
		GameRegistry.registerBlock(coffin, "Device^Coffin");
		GameRegistry.registerBlock(brewingStation, "Device^Brewing_Station");
		GameRegistry.registerBlock(gemCake, "Decor^Gem_Cake");
	}
}
