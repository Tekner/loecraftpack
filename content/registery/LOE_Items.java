package loecraftpack.content.registery;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.EntityItemMask;
import loecraftpack.content.entities.EntityItemTapestry;
import loecraftpack.content.items.ItemAppleFritter;
import loecraftpack.content.items.ItemBigApple;
import loecraftpack.content.items.ItemBits;
import loecraftpack.content.items.ItemColoredBed;
import loecraftpack.content.items.ItemCrystalHeart;
import loecraftpack.content.items.ItemDrink;
import loecraftpack.content.items.ItemGemStones;
import loecraftpack.content.items.ItemIceArrow;
import loecraftpack.content.items.ItemIngredients;
import loecraftpack.content.items.ItemIronArrow;
import loecraftpack.content.items.ItemMask;
import loecraftpack.content.items.ItemMusicDisc;
import loecraftpack.content.items.ItemPedestal;
import loecraftpack.content.items.ItemPickaxeGem;
import loecraftpack.content.items.ItemQuiver;
import loecraftpack.content.items.ItemTapestry;
import loecraftpack.content.items.ItemThrowableDrink;
import loecraftpack.content.items.ItemZapApple;
import loecraftpack.content.items.ItemZapAppleJam;
import loecraftpack.content.items.accessories.ItemAlicornAmulet;
import loecraftpack.content.items.accessories.ItemNecklace;
import loecraftpack.content.items.accessories.ItemNecklaceOfBling;
import loecraftpack.content.items.accessories.ItemNecklaceOfDreams;
import loecraftpack.content.items.accessories.ItemRing;
import loecraftpack.content.items.accessories.ItemRingLife;
import loecraftpack.content.items.accessories.ItemRingOf;
import loecraftpack.content.items.accessories.ItemRingPhantomArrow;
import loecraftpack.content.ponies.abilities.ItemActiveAbility;
import loecraftpack.content.ponies.abilities.ItemPassiveAbility;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class LOE_Items
{
	public static ItemActiveAbility activeAbility;
	public static ItemPassiveAbility passiveAbility;
	
	public static ItemPickaxeGem pickaxeGem;
	//public static ItemBucketLoECustom bucketRainbow;
	
	public static ItemMask mask;
	public static ItemNecklace necklace;
	public static ItemNecklaceOfDreams necklaceOfDreams;
	public static ItemNecklaceOfBling necklaceOfBling;
	public static ItemAlicornAmulet alicornAmulet;
	public static ItemRing ring;
	public static ItemRingLife ringLife;
	public static ItemRingPhantomArrow ringPhantomArrow;
	public static ItemRingOf ringOf;
	
	public static ItemGemStones gemStones;
	public static ItemIngredients ingredients;
	public static ItemBits bits;
	
	public static ItemZapApple zapApple;
	public static ItemBigApple bigApple;
	public static ItemZapAppleJam zapAppleJam;
	public static ItemAppleFritter appleFritter;
	public static ItemDrink drink;

	public static Item emptyBottle;
	public static ItemCrystalHeart crystalHeart;
	public static ItemQuiver quiver;
	
	public static ItemIronArrow ironArrow;
	public static ItemIceArrow iceArrow;
	public static ItemThrowableDrink throwableDrink;
	
	public static ItemPedestal pedestal;
	public static ItemTapestry tapestry;
	public static ItemColoredBed beds;
	
	public static ItemMusicDisc discCloudsdale;
	public static ItemMusicDisc discCutieMark;
	
	static int  ID_Bed;
	
	public static void init(Configuration config)
	{
		ID_Bed              = config.get("item", "Bed",              20672).getInt(); //OLD ID: 670
		
		
		activeAbility = new ItemActiveAbility();
		passiveAbility = new ItemPassiveAbility();
		
		pickaxeGem = (ItemPickaxeGem)(new ItemPickaxeGem()).setUnlocalizedName("pickaxeGem");
		//bucketRainbow = (ItemBucketLoECustom)(new ItemBucketLoECustom(ID_BucketRainbow, LOE_Blocks.liquidRainbow)).setUnlocalizedName("bucket_rainbow").setTextureName("rainbowBucket");
		
		mask = (ItemMask)(new ItemMask(EntityItemMask.class)).setUnlocalizedName("mask").setTextureName("masks");
		necklace = (ItemNecklace)(new ItemNecklace()).setUnlocalizedName("necklace");
		necklaceOfDreams = (ItemNecklaceOfDreams)(new ItemNecklaceOfDreams()).setUnlocalizedName("necklacedream");
		necklaceOfBling = (ItemNecklaceOfBling)(new ItemNecklaceOfBling()).setUnlocalizedName("necklacebling");
		alicornAmulet = (ItemAlicornAmulet)(new ItemAlicornAmulet()).setUnlocalizedName("alicorn_amulet");
		ring = (ItemRing)(new ItemRing()).setUnlocalizedName("ring");
		ringLife = (ItemRingLife)(new ItemRingLife()).setUnlocalizedName("ringlife");
		ringPhantomArrow = (ItemRingPhantomArrow)(new ItemRingPhantomArrow()).setUnlocalizedName("ringphantomarrow");
		ringOf = (ItemRingOf)(new ItemRingOf()).setUnlocalizedName("ring_of");
		
		gemStones = (ItemGemStones)(new ItemGemStones()).setUnlocalizedName("gemstones");
		ingredients = (ItemIngredients)(new ItemIngredients()).setUnlocalizedName("ingredient");
		bits = new ItemBits();
		
		
		zapApple = (ItemZapApple)(new ItemZapApple(4, 1.2F, true)).setAlwaysEdible().setUnlocalizedName("zapApple");
		bigApple = (ItemBigApple)(new ItemBigApple()).setUnlocalizedName("bigApple");
		zapAppleJam = (ItemZapAppleJam)(new ItemZapAppleJam(4, 1.2F, false)).setAlwaysEdible().setUnlocalizedName("zapAppleJam");
		appleFritter = (ItemAppleFritter)(new ItemAppleFritter()).setAlwaysEdible().setUnlocalizedName("appleFritter");
		drink = (ItemDrink)(new ItemDrink()).setUnlocalizedName("drink");
		
		emptyBottle = (new Item()).setUnlocalizedName("bottle").setCreativeTab(LoECraftPack.LoECraftTabItem).setTextureName("loecraftpack:food/empty_bottle");
		crystalHeart = (ItemCrystalHeart)(new ItemCrystalHeart()).setUnlocalizedName("cyrstalheart");
		quiver = (ItemQuiver)(new ItemQuiver()).setUnlocalizedName("quiver");
		
		ironArrow = (ItemIronArrow)(new ItemIronArrow()).setUnlocalizedName("arrow_iron");
		iceArrow = (ItemIceArrow)(new ItemIceArrow()).setUnlocalizedName("arrow_ice");
		throwableDrink = (ItemThrowableDrink)(new ItemThrowableDrink()).setUnlocalizedName("throwable_drink");

		pedestal = (ItemPedestal)(new ItemPedestal()).setUnlocalizedName("pedestal");
		tapestry = (ItemTapestry)(new ItemTapestry(EntityItemTapestry.class)).setUnlocalizedName("tapestry").setTextureName("tapestries");
		beds = new ItemColoredBed();
		
		discCloudsdale = (ItemMusicDisc)(new ItemMusicDisc("LoE", "cloudsdaleracetheme"));
		discCutieMark = (ItemMusicDisc)(new ItemMusicDisc("MLP:FiM", "whatmycutiemarkistellingme"));
	}
	
	public static void updateDependants(){}
	
	public static void register()
	{
		//Abilities
		GameRegistry.registerItem(activeAbility, "Ability^Active");
		GameRegistry.registerItem(passiveAbility, "Ability^Passive");
		
		//TOOLS
		GameRegistry.registerItem(pickaxeGem, "Tool^Pickaxe_Gem");
		/*
		GameRegistry.registerItem(bucketRainbow, "Tool^Bucket_Rainbows");
		LanguageRegistry.addName(bucketRainbow, "Bucket of Rainbow");
		FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack("rainbow", FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(bucketRainbow), new ItemStack(Item.bucketEmpty));
		*/
		GameRegistry.registerItem(mask, "Armor^Mask");
		GameRegistry.registerItem(necklace, "Necklace^Basic");
		GameRegistry.registerItem(necklaceOfDreams, "Necklace^Dream");
		GameRegistry.registerItem(necklaceOfBling, "Necklace^Bling");
		GameRegistry.registerItem(alicornAmulet, "Necklace^Alicorn");
		GameRegistry.registerItem(ring, "Ring^Basic");
		GameRegistry.registerItem(ringLife, "Ring^Life");
		GameRegistry.registerItem(ringPhantomArrow, "Ring^Phantom_Arrow");
		GameRegistry.registerItem(ringOf, "Ring^Stat_Boosts");

		//Resources
		GameRegistry.registerItem(gemStones, "Resource^Gem_Stones");
		
		GameRegistry.registerItem(ingredients, "Resource^Ingredients");
		GameRegistry.registerItem(bits, "Resource^Bits");
		

		//FOOD
		GameRegistry.registerItem(zapApple, "Food^Apple_Zap");
		GameRegistry.registerItem(bigApple, "Food^Apple_Big");
		GameRegistry.registerItem(zapAppleJam, "Food^Jam_Zap_Apple");
		GameRegistry.registerItem(appleFritter, "Food^Apple_Fritter");
		GameRegistry.registerItem(drink, "Food^Drinks");
		
		//Containers
		GameRegistry.registerItem(emptyBottle, "Container^Jar_glass");
		GameRegistry.registerItem(crystalHeart, "Container^Crystal_Heart");
		GameRegistry.registerItem(quiver, "Container^Quiver");
		
		//AMMO
		GameRegistry.registerItem(ironArrow, "Arrow^Iron");
		GameRegistry.registerItem(iceArrow, "Arrow^Ice");
		GameRegistry.registerItem(throwableDrink, "Projectile^Potions");
		
		//Decoration
		GameRegistry.registerItem(pedestal, "Decor^Pedestal");
		GameRegistry.registerItem(tapestry, "Decor^Tapestry");
		Item.itemRegistry.addObject(ID_Bed, "Decor^Colored_Bed", beds);
		
		//Music
		GameRegistry.registerItem(discCloudsdale,"MusicDisc^Cloudsdale");
		GameRegistry.registerItem(discCutieMark,"MusicDisc^CutieMark");
	}
}
