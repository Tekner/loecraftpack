package loecraftpack.content.registery;

import net.minecraftforge.common.config.Configuration;

public class LOE_Settings
{
	public static boolean Enabled_StatsUsage;
	public static boolean Enabled_MobBoosts;
	public static boolean Enabled_Structures;
	public static int Mode_GemOreDistribution;
	
	public static void init(Configuration config)
	{
		/**toggle on or off, if stats gives boost to players, and if abilities are dependent/boosted by it*/
		Enabled_StatsUsage             = config.get("Settings", "UseStats", true, null).getBoolean(true);
		/**toggle if mobs receive boosts, and drop bonus loot*/
		Enabled_MobBoosts             = config.get("Settings", "BoostMobs", true, null).getBoolean(true);
		
		/**toggle if worldGen will spawn Structures*/
		Enabled_Structures             = config.get("Settings", "SpawnStructures", true, null).getBoolean(true);
		
		/**switch between random scatter to biome type dependent, distribution*/
		Mode_GemOreDistribution             = config.get("Settings", "Gem_Ore_Distribution", 0, null).getInt();
	}
}