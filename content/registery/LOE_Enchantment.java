package loecraftpack.content.registery;

import loecraftpack.content.enchantment.EnchantmentBanish;
import loecraftpack.content.enchantment.EnchantmentElectric;
import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class LOE_Enchantment
{
	public static Enchantment enchantElectric;
	public static Enchantment enchantBanish;
	
	public static void init(Configuration config)
	{
		enchantElectric = new EnchantmentElectric(100, 2);
		enchantBanish = new EnchantmentBanish(101, 2);
	}
	
	public static void updateDependants(){}
	
	public static void register(){}
}
