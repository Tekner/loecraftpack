package loecraftpack.content.registery;

import java.util.Arrays;
import java.util.List;

//import loecraftpack.content.entities.EntityBreezy;
//import loecraftpack.content.entities.EntityCockatrice;
import loecraftpack.content.entities.EntityBreezy;
import loecraftpack.content.entities.EntityCockatrice;
import loecraftpack.content.entities.EntityTimberWolf;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.BiomeGenBase.SpawnListEntry;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class LOE_Mobs
{
	public static void init(Configuration config) {}
	
	public static void updateDependants() {}
	
	public static void register()
	{
		EntityRegistry.registerGlobalEntityID(EntityTimberWolf.class, "timberwolf", 127, 12422002, 5651507);
		EntityRegistry.registerGlobalEntityID(EntityBreezy.class, "breezy", 126, 3407871, 16724889);
		EntityRegistry.registerGlobalEntityID(EntityCockatrice.class, "cockatrice", 125, 894731, 16711680);
		
		for (BiomeGenBase biome: BiomeGenBase.getBiomeGenArray())
		{
			if (biome == null)
				continue;
			
			List<Type> types = Arrays.asList(BiomeDictionary.getTypesForBiome(biome));
			
			if (!types.contains(Type.NETHER) && !types.contains(Type.FROZEN))
			{
				List list = biome.getSpawnableList(EnumCreatureType.creature);
				TYPELOOP:
				for (Type type: types)
				{
					switch (type)
					{
					case PLAINS:
					case FOREST:
					case MAGICAL:
						list.add(new SpawnListEntry(EntityBreezy.class, 12, 8, 8));
						break TYPELOOP;
					}
				}
			}
		}
		
		BiomeGenBase.extremeHillsEdge.getSpawnableList(EnumCreatureType.creature).add(new SpawnListEntry(EntityBreezy.class, 12, 8, 8));
	}
}
