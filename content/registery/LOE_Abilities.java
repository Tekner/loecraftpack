package loecraftpack.content.registery;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.abilities.active.AbilityBuckTree;
import loecraftpack.content.ponies.abilities.active.AbilityBusterHoof;
import loecraftpack.content.ponies.abilities.active.AbilityDeepHeal;
import loecraftpack.content.ponies.abilities.active.AbilityFireBreath;
import loecraftpack.content.ponies.abilities.active.AbilityFireWard;
import loecraftpack.content.ponies.abilities.active.AbilityFireball;
import loecraftpack.content.ponies.abilities.active.AbilityFrostbolt;
import loecraftpack.content.ponies.abilities.active.AbilityHealingWind;
import loecraftpack.content.ponies.abilities.active.AbilityHomewardBound;
import loecraftpack.content.ponies.abilities.active.AbilityMagicBarrierRecharge;
import loecraftpack.content.ponies.abilities.active.AbilityOreVision;
import loecraftpack.content.ponies.abilities.active.AbilitySonicBreath;
import loecraftpack.content.ponies.abilities.active.AbilityTailwind;
import loecraftpack.content.ponies.abilities.active.AbilityTeleBreath;
import loecraftpack.content.ponies.abilities.active.AbilityTeleport;
import loecraftpack.content.ponies.abilities.active.AbilityWindDance;
import loecraftpack.content.ponies.abilities.passive.AbilityFlight;
import loecraftpack.content.ponies.abilities.passive.AbilityLeapingBound;
import loecraftpack.content.ponies.abilities.passive.AbilityMagicBarrier;
import loecraftpack.content.ponies.abilities.passive.AbilityNaturalHealthRegen;
import loecraftpack.content.ponies.abilities.passive.AbilityNightVision;
import loecraftpack.content.ponies.abilities.passive.AbilityToughHide;
import loecraftpack.content.ponies.abilities.passive.AbilityWeaving;
import loecraftpack.content.ponies.abilities.passive.AbilityWindDancePassive;
import net.minecraftforge.common.config.Configuration;



public class LOE_Abilities
{
	public static boolean Enabled_Fireball;
	public static boolean Enabled_Frostbolt;
	public static boolean Enabled_Teleport;
	public static boolean Enabled_OreVision;
	public static boolean Enabled_BuckTree;
	public static boolean Enabled_DeepHeal;
	public static boolean Enabled_HomewardBound;
	public static boolean Enabled_WindDance;
	public static boolean Enabled_Tailwind;
	public static boolean Enabled_MagicBarrierRecharge;
	public static boolean Enabled_FireWard;
	public static boolean Enabled_BusterHoof;
	public static boolean Enabled_FireBreath;
	public static boolean Enabled_SonicBreath;
	public static boolean Enabled_TeleBreath;
	public static boolean Enabled_HealingWind;
	
	public static boolean Enabled_Flight;
	public static boolean Enabled_LeapingBound;
	public static boolean Enabled_ToughHide;
	public static boolean Enabled_NightVision;
	public static boolean Enabled_MagicBarrier;
	public static boolean Enabled_HealthRegen;
	public static boolean Enabled_Weaving;
	
	
	public static void init(Configuration config)
	{
		//Active:
		Enabled_Fireball             = config.get("Abilities", "Fireball", true, null).getBoolean(true);
		Enabled_Frostbolt            = config.get("Abilities", "Frostbolt", true, null).getBoolean(true);
		Enabled_Teleport             = config.get("Abilities", "Teleport", true, null).getBoolean(true);
		Enabled_OreVision            = config.get("Abilities", "OreVision", true, null).getBoolean(true);
		Enabled_BuckTree             = config.get("Abilities", "BuckTree", true, null).getBoolean(true);
		Enabled_DeepHeal             = config.get("Abilities", "DeepHeal", true, null).getBoolean(true);
		Enabled_HomewardBound        = config.get("Abilities", "HomewardBound", true, null).getBoolean(true);
		Enabled_WindDance            = config.get("Abilities", "WindDance", true, null).getBoolean(true);
		Enabled_Tailwind             = config.get("Abilities", "Tailwind", true, null).getBoolean(true);
		Enabled_MagicBarrierRecharge = config.get("Abilities", "MagicBarrierRecharge", true, null).getBoolean(true);
		Enabled_FireWard             = config.get("Abilities", "FireWard", true, null).getBoolean(true);
		Enabled_BusterHoof           = config.get("Abilities", "BusterHoof", true, null).getBoolean(true);
		Enabled_FireBreath           = config.get("Abilities", "FireBreath", true, null).getBoolean(true);
		Enabled_SonicBreath          = config.get("Abilities", "SonicBreath", true, null).getBoolean(true);
		Enabled_TeleBreath           = config.get("Abilities", "TeleBreath", true, null).getBoolean(true);
		Enabled_HealingWind          = config.get("Abilities", "HealingWind", true, null).getBoolean(true);
		
		//Passive:
		Enabled_Flight               = config.get("Abilities", "Flight", true, null).getBoolean(true);
		Enabled_LeapingBound         = config.get("Abilities", "LeapingBound", true, null).getBoolean(true);
		Enabled_ToughHide            = config.get("Abilities", "ToughHide", true, null).getBoolean(true);
		Enabled_NightVision          = config.get("Abilities", "NightVision", true, null).getBoolean(true);
		Enabled_MagicBarrier         = config.get("Abilities", "MagicBarrier", true, null).getBoolean(true);
		Enabled_HealthRegen          = config.get("Abilities", "HealthRegen", true, null).getBoolean(true);
		Enabled_Weaving              = config.get("Abilities", "Weaving", true, null).getBoolean(true);
		
		
		
		ActiveAbility.RegisterAbilities(AbilityFireball.class, AbilityFrostbolt.class, AbilityTeleport.class, AbilityOreVision.class,
				AbilityBuckTree.class, AbilityDeepHeal.class, AbilityHomewardBound.class, AbilityWindDance.class,
				AbilityTailwind.class, AbilityMagicBarrierRecharge.class, AbilityFireWard.class, AbilityBusterHoof.class,
				AbilityFireBreath.class, AbilitySonicBreath.class, AbilityTeleBreath.class, AbilityHealingWind.class);
		PassiveAbility.RegisterAbilities(AbilityFlight.class, AbilityLeapingBound.class, AbilityToughHide.class, AbilityWindDancePassive.class,
				AbilityNightVision.class, AbilityMagicBarrier.class, AbilityNaturalHealthRegen.class,
				AbilityWeaving.class);
	}
	
	public static void updateDependants() {}
	
	public static void register() {}
}
