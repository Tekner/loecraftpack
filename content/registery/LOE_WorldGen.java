package loecraftpack.content.registery;

import loecraftpack.content.worldgen.BiomeGenEverFreeForest;
//import loecraftpack.content.worldgen.WorldGenCathedral;
import loecraftpack.content.worldgen.WorldGenCathedral;
import loecraftpack.content.worldgen.WorldGenCustomAppleTree;
import loecraftpack.content.worldgen.WorldGenCustomForest;
import loecraftpack.content.worldgen.WorldGenPlunderVine;
import loecraftpack.content.worldgen.WorldGenRuins;
import loecraftpack.content.worldgen.WorldGenTreeHouseTwilight;
import loecraftpack.content.worldgen.WorldGenTreeHouseZecora;
//import loecraftpack.content.worldgen.WorldGenRuins;
//import loecraftpack.content.worldgen.WorldGenTreeHouseTwilight;
//import loecraftpack.content.worldgen.WorldGenTreeHouseZecora;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeManager.BiomeEntry;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;

public class LOE_WorldGen
{
	public static BiomeGenEverFreeForest biomeGeneratorEverFreeForest;
	public static WorldGenCustomForest worldGeneratorZapAppleForest;
	public static WorldGenCustomAppleTree worldGeneratorAppleBloom;
	public static WorldGenTreeHouseZecora worldGeneratorZecoraHouse;
	public static WorldGenTreeHouseTwilight worldGeneratorTwilightHouse;
	public static WorldGenCathedral worldGeneratorCathedral;
	public static WorldGenRuins worldGeneratorSmallRuin;
	public static WorldGenPlunderVine worldGeneratorPlunderVine;
	
	public static void init(Configuration config)
	{
		biomeGeneratorEverFreeForest = (BiomeGenEverFreeForest)new BiomeGenEverFreeForest(70).setColor(25600).setBiomeName("EverFree").setTemperatureRainfall(0.5f, 0.7f);
		worldGeneratorZapAppleForest = new WorldGenCustomForest(false, LOE_Blocks.zapAppleSapling, LOE_Blocks.zapAppleLog, LOE_Blocks.zapAppleLeaves);
		worldGeneratorAppleBloom = new WorldGenCustomAppleTree(false, LOE_Blocks.appleBloomSapling, LOE_Blocks.appleBloomLog, LOE_Blocks.appleBloomLeaves, 5);
		worldGeneratorZecoraHouse = new WorldGenTreeHouseZecora();
		worldGeneratorTwilightHouse = new WorldGenTreeHouseTwilight();
		worldGeneratorCathedral = new WorldGenCathedral();/*
		worldGeneratorSmallRuin = new WorldGenRuins();*/
		worldGeneratorPlunderVine = new WorldGenPlunderVine(3,8);
	}
	
	public static void updateDependants(){}
	
	public static void register()
	{
		BiomeDictionary.registerBiomeType(biomeGeneratorEverFreeForest, BiomeDictionary.Type.FOREST, BiomeDictionary.Type.MAGICAL);
		BiomeManager.addSpawnBiome(biomeGeneratorEverFreeForest);
		BiomeManager.warmBiomes.add(new BiomeEntry(biomeGeneratorEverFreeForest, 100));
		//GameRegistry.registerWorldGenerator((IWorldGenerator) biomeGeneratorEverFreeForest, 5);
	}

}
