package loecraftpack.content.blocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockProtectionMonolith extends Block implements ITileEntityProvider
{
	public static Map<Integer, List<TileProtectionMonolith>> monoliths = new HashMap<Integer, List<TileProtectionMonolith>>();
	
	protected IIcon icon;
	
	public BlockProtectionMonolith()
	{
		super(Material.rock);
		setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.blockResistance = 2000;
	}
	
	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4, Block par5, int par6)
    {
		int dim = par1World.provider.dimensionId;
		List<TileProtectionMonolith> list = monoliths.get(dim);
		if (list == null)
			list = new ArrayList<TileProtectionMonolith>();
		else
			monoliths.remove(dim);
		
		for(int i = 0; i < list.size(); i++)
		{
			TileProtectionMonolith te = list.get(i);
			if (te.xCoord == par2 &&
				te.yCoord == par3 &&
				te.zCoord == par4)
			{
				list.remove(i);
				break;
			}
		}
		
		monoliths.put(dim, list);
		
        super.breakBlock(par1World, par2, par3, par4, par5, par6);
    }
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer playerEntity, int w, float px, float py, float pz)
	{
		if (((TileProtectionMonolith)world.getTileEntity(x, y, z)).isOwner(playerEntity.getGameProfile().getId()))
			playerEntity.openGui(LoECraftPack.instance, GuiIds.MONOLITH.ordinal(), world, x, y, z);
		return false;
	}
	
    public TileEntity createNewTileEntity(World world, int i)
    {
    	TileProtectionMonolith te = new TileProtectionMonolith();
    	if (world.isRemote)
    	{
    		int dim = world.getWorldInfo().getVanillaDimension();
    		
    		if (monoliths.get(dim) != null)
    			monoliths.get(dim).add(te);
    		else
    			monoliths.put(dim, new ArrayList<TileProtectionMonolith>(Arrays.asList(te)));
    	}
    	
    	return te;
    }
    
    public int getRenderType()
    {
        return -1;
    }
    
    public boolean renderAsNormalBlock()
    {
        return false;
    }
    
    public boolean isOpaqueCube()
    {
        return false;
    }
    
    @Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return icon;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("loecraftpack:blank");
	}
}
