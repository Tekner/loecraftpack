package loecraftpack.content.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.network.packets.PacketAppleBloomUpdate;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import static net.minecraftforge.common.util.ForgeDirection.*;


public class BlockAppleBloomLeaves extends BlockLeavesBase implements IShearable
{
	public Item apple;
	public int appleType = 0;//item damage
	public int bloomStage = 2;
	protected int saplingDropRate = 50;
	protected double growthChance = 80;//(an attempt every 68.2666...secs) average of 91 minutes(real time) to reach the next stage
	protected IIcon icon[] = new IIcon[4];
	/**flags, etc.**/
	protected int[] adjacentTreeBlocks;
	protected static boolean sheared = false;
	protected boolean graphicsLevel = false;;
	
	
	public BlockAppleBloomLeaves()
	{
		super(Material.leaves, false);
		this.setTickRandomly(true);
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.setBlockName("leavesAppleBloom");
		this.apple = Items.apple;
	}
	
	//no color change - inventory
	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderColor(int par1)
	{
		return 16777215;
	}
	
	//no color change - world
	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess blockAccess, int coordX, int coordY, int coordZ)
	{
		return 16777215;
	}
	
	/*
	//inventory 
	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderColor(int par1)
	{
		return ColorizerFoliage.getFoliageColorBasic();
	}
	
	//world
	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
	{
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		
		for (int l1 = -1; l1 <= 1; ++l1)
		{
			for (int i2 = -1; i2 <= 1; ++i2)
			{
				int j2 = par1IBlockAccess.getBiomeGenForCoords(par2 + i2, par4 + l1).getBiomeFoliageColor();
				i1 += (j2 & 16711680) >> 16;
				j1 += (j2 & 65280) >> 8;
				k1 += j2 & 255;
			}
		}
		
		return (i1 / 9 & 255) << 16 | (j1 / 9 & 255) << 8 | k1 / 9 & 255;
	}
	*/
	
	@Override
	public boolean onBlockActivated(World world, int xCoord, int yCoord, int zCoord, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if (!world.isRemote && (meta & 3) >= bloomStage)
		{
			int fortune = 0;
			
			float chanceSapling = saplingDropRate;
			
			if (fortune > 0)
			{
				chanceSapling -= (int)(chanceSapling/10) << fortune;
				if (chanceSapling < 10)
				{
					chanceSapling = 10;
				}
			}
			
			if (world.rand.nextFloat() < 1.0f/chanceSapling)
			{
				Item sapling = this.getItemDropped(meta, world.rand, fortune);
				this.dropBlockAsItem(world, xCoord, yCoord, zCoord, new ItemStack(sapling, 1, this.damageDropped(meta)));
			}
			
			int amount = world.rand.nextInt(fortune + 1)+1;
			this.dropAppleThruTree(world, xCoord, yCoord, zCoord, new ItemStack(apple, amount, appleType));
			
			world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, 2);
			
			return true;
		}
		return false;
    }
	
	@Override
	public void breakBlock(World world, int xCoord, int yCoord, int zCoord, Block oldBlock, int oldMeta)
	{
		byte effectRange = 1;
		int requiredChunkRange = effectRange + 1;
		
		if (world.checkChunksExist(xCoord - requiredChunkRange, yCoord - requiredChunkRange, zCoord - requiredChunkRange, xCoord + requiredChunkRange, yCoord + requiredChunkRange, zCoord + requiredChunkRange))
		{
			for (int offX = -effectRange; offX <= effectRange; ++offX)
			{
				for (int offY = -effectRange; offY <= effectRange; ++offY)
				{
					for (int offZ = -effectRange; offZ <= effectRange; ++offZ)
					{
						Block adjBlock = world.getBlock(xCoord + offX, yCoord + offY, zCoord + offZ);
						
						if (adjBlock != null)
						{
							adjBlock.beginLeavesDecay(world, xCoord + offX, yCoord + offY, zCoord + offZ);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void updateTick(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		if (!world.isRemote)
		{
			int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
			boolean flag = true;
			
			if ((meta & 8) != 0 && (meta & 4) == 0)
			{
				byte scanRange = 4;
				int requiredChunkRange = scanRange + 1;
				byte arraySize = 32;
				int xSequanceMod = arraySize * arraySize;
				int arrayCenterMod = arraySize / 2;
				
				if (this.adjacentTreeBlocks == null)
				{
					this.adjacentTreeBlocks = new int[arraySize * arraySize * arraySize];
				}
				
				if (world.checkChunksExist(xCoord - requiredChunkRange, yCoord - requiredChunkRange, zCoord - requiredChunkRange, xCoord + requiredChunkRange, yCoord + requiredChunkRange, zCoord + requiredChunkRange))
				{
					int offX;
					int offY;
					int offZ;
					
					//find leaves and sustaining blocks
					for (offX = -scanRange; offX <= scanRange; offX++)
					{
						for (offY = -scanRange; offY <= scanRange; offY++)
						{
							for (offZ = -scanRange; offZ <= scanRange; offZ++)
							{
								Block block = world.getBlock(xCoord + offX, yCoord + offY, zCoord + offZ);
								
								if (block != null && block.canSustainLeaves(world, xCoord + offX, yCoord + offY, zCoord + offZ))
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = 0;
								}
								else if (block != null && block.isLeaves(world, xCoord + offX, yCoord + offY, zCoord + offZ))
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = -2;
								}
								else
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = -1;
								}
							}
						}
					}
					
					//extend influence of sustaining blocks
					for (int i = 1; i <= 4; ++i)
					{
						for (offX = -scanRange; offX <= scanRange; ++offX)
						{
							for (offY = -scanRange; offY <= scanRange; ++offY)
							{
								for (offZ = -scanRange; offZ <= scanRange; ++offZ)
								{
									if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == i - 1)
									{
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod - 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod - 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod + 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod + 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod - 1) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod - 1) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod + 1) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod + 1) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + (offZ + arrayCenterMod - 1)] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + (offZ + arrayCenterMod - 1)] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod + 1] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod + 1] = i;
										}
									}
								}
							}
						}
					}
				}
				
				int sustainValueSelf = this.adjacentTreeBlocks[arrayCenterMod * xSequanceMod + arrayCenterMod * arraySize + arrayCenterMod];
				
				if (sustainValueSelf >= 0)
				{
					world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta & -9, 4);
				}
				else
				{
					this.removeLeaves(world, xCoord, yCoord, zCoord);
					flag = false;
				}
			}
			if (flag)
			{
				attemptGrow(world, xCoord, yCoord, zCoord, random);
			}
		}
	}
	
	@Override
	public void randomDisplayTick(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		if (world.canLightningStrikeAt(xCoord, yCoord + 1, zCoord) && !isSideSolid(world, xCoord, yCoord - 1, zCoord, UP) && random.nextInt(15) == 1)
		{
			double posX = (double)((float)xCoord + random.nextFloat());
			double posY = (double)yCoord - 0.05D;
			double posZ = (double)((float)zCoord + random.nextFloat());
			world.spawnParticle("dripWater", posX, posY, posZ, 0.0D, 0.0D, 0.0D);
		}
	}
	
	private void removeLeaves(World world, int xCoord, int yCoord, int zCoord)
	{
		this.dropBlockAsItem(world, xCoord, yCoord, zCoord, world.getBlockMetadata(xCoord, yCoord, zCoord), 0);
		world.setBlockToAir(xCoord, yCoord, zCoord);
	}
	
	/**
	 * this is called during updateTick. it attempts to progress the leaves growth stage.
	 */
	public void attemptGrow(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if ( (meta & 4) == 0 && random.nextDouble()*(growthChance) <= 1 &&
			 ((meta&3) + 1 ) < 4 && world.setBlock(xCoord, yCoord, zCoord, this, meta + 1, 2) )
		{
			tellClientOfChange(world, xCoord, yCoord, zCoord, this);
		}
	}
	
	/**
	 * Drop apple down thru the tree.  The wood and leaves IDs are passed by the class.
	 */
	public void dropAppleThruTree(World world, int xCoord, int yCoord, int zCoord, ItemStack itemStack)
	{
		dropAppleThruTree(world, xCoord, yCoord, zCoord, itemStack,
				new Block[]{Blocks.log, Blocks.log2,
						  LOE_Blocks.appleBloomLeaves});
	}
	
	protected void dropAppleThruTree(World world, int xCoord, int yCoord, int zCoord, ItemStack itemStack, Block blocks[])
	{
		Block block;
		while (true)
		{
			block = world.getBlock(xCoord, yCoord, zCoord);
			boolean flag = false;
			for (int i=0; i<blocks.length; i++)
			{
				if (Block.isEqualTo(blocks[i], block))
					flag=true;
			}
			if (flag)
				yCoord--;
			else
			{
				yCoord++;
				break;
			}
			if (!world.blockExists(xCoord, yCoord, zCoord))
			{
				yCoord++;
				break;
			}
		}
		if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops"))
		{
			float f = 0.7F;
			double d0 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
			double d1 = -0.2D;
			double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
			EntityItem entityitem = new EntityItem(world, (double)xCoord + d0, (double)yCoord + d1, (double)zCoord + d2, itemStack);
			entityitem.delayBeforeCanPickup = 10;
			world.spawnEntityInWorld(entityitem);
		}
	}
	
	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int xCoord, int yCoord, int zCoord)
	{
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if ((meta&3) >= bloomStage && !sheared && (meta&4)==0 )
		{
			return world.setBlock(xCoord, yCoord, zCoord, this, meta&12, 2);
		}
		return world.setBlockToAir(xCoord, yCoord, zCoord);
	}
	
	
	@Override
	public int quantityDropped(Random random)
	{
		return random.nextInt(20) == 0 ? 1 : 0;
	}
	
	@Override
	public Item getItemDropped(int meta, Random random, int fortune)
	{
		return Item.getItemFromBlock(LOE_Blocks.appleBloomSapling);
	}
	
	@Override
	public void dropBlockAsItemWithChance(World world, int xCoord, int yCoord, int zCoord, int meta, float dropChanceMod, int fortune)
	{
		if (!world.isRemote)
		{
			float chanceSapling = (dropChanceMod>0)? saplingDropRate/dropChanceMod: saplingDropRate;
			
			if (fortune > 0)
			{
				chanceSapling -= (int)(chanceSapling/10) << fortune;
				
				if (chanceSapling < ((dropChanceMod>0)? 10/dropChanceMod: 10))
				{
					chanceSapling = (dropChanceMod>0)? 10/dropChanceMod: 10;
				}
			}
			
			if (world.rand.nextFloat() < 1.0f/chanceSapling)
			{
				Item sapling = this.getItemDropped(meta, world.rand, fortune);
				this.dropBlockAsItem(world, xCoord, yCoord, zCoord, new ItemStack(sapling, 1, this.damageDropped(meta)));
			}
			
			if (!sheared)
			{
				if ((meta & 3) >= bloomStage)
				{
					int amount = world.rand.nextInt(fortune + 1)+1;
					
					this.dropAppleThruTree(world, xCoord, yCoord, zCoord, new ItemStack(apple, amount, appleType));
				}
			}
			//reset sheared
			sheared = false;
		}
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		//only return true if: both client side, and fancy graphics disabled
		return !graphicsLevel;
	}
	
	//altered vanilla code -- links the graphics level to the vanilla leaf
	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
	{
		Block testBlock = par1IBlockAccess.getBlock(par2, par3, par4);
		return !graphicsLevel && Block.isEqualTo(testBlock, this) ? false : par5 == 0 && this.minY > 0.0D ? true : (par5 == 1 && this.maxY < 1.0D ? true : (par5 == 2 && this.minZ > 0.0D ? true : (par5 == 3 && this.maxZ < 1.0D ? true : (par5 == 4 && this.minX > 0.0D ? true : (par5 == 5 && this.maxX < 1.0D ? true : !testBlock.isOpaqueCube())))));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		int IconByGraphicsLevel = graphicsLevel ? 0 : 1;
		if ((meta&3) < bloomStage)
			return icon[IconByGraphicsLevel];
		return icon[IconByGraphicsLevel+2];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		par3List.add(new ItemStack(par1, 1, 0));//Normal
		par3List.add(new ItemStack(par1, 1, bloomStage));//Blossomed
	}
	
	@Override
	protected ItemStack createStackedBlock(int meta)
	{
		return new ItemStack(this, 1, meta & 3);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon[0] = iconRegister.registerIcon("loecraftpack:trees/leaves");
		icon[1] = iconRegister.registerIcon("loecraftpack:trees/leaves_opaque");
		icon[2] = iconRegister.registerIcon("loecraftpack:trees/leaves_bloom");
		icon[3] = iconRegister.registerIcon("loecraftpack:trees/leaves_bloom_opaque");
	}
	
	@Override
	public boolean isShearable(ItemStack item, IBlockAccess world, int xCoord, int yCoord, int zCoord)
	{
		return true;
	}

	@Override
	public ArrayList<ItemStack> onSheared(ItemStack item, IBlockAccess world, int xCoord, int yCoord, int zCoord, int fortune)
	{
		sheared = true;
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		ret.add(new ItemStack(this, 1, world.getBlockMetadata(xCoord, yCoord, zCoord) & 3));
		return ret;
	}
	
	@Override
	public void beginLeavesDecay(World world, int xCoord, int yCoord, int zCoord)
	{
		world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, world.getBlockMetadata(xCoord, yCoord, zCoord) | 8, 4);
	}

	@Override
	public boolean isLeaves(IBlockAccess world, int xCoord, int yCoord, int zCoord)
	{
		return true;
	}
	
	/**
	 * used to tell the client that the block ID is changed
	 */
  	public void tellClientOfChange(World world, int xCoord, int yCoord, int zCoord, Block newBlock)
  	{
  		if (world != null && !world.isRemote)
  		{
  			LoECraftPack.packetHandler.sendToAll(new PacketAppleBloomUpdate().INIT(xCoord, yCoord, zCoord, newBlock));
  		}
  	}

	
}
