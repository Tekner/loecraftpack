package loecraftpack.content.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.LoECraftPack;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCustomLog extends Block 
{
	String iconNameSide;
	String iconNameTop;
	IIcon iconSide;
	IIcon iconTop;
	
	public BlockCustomLog(String iconNameSide, String iconNameTop)
	{
		super(Material.wood);
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.iconNameSide = iconNameSide;
		this.iconNameTop = iconNameTop;
	}
	
	@Override
	public int getRenderType()
	{
		return 31;
	}
	
	@Override
	public void breakBlock(World world, int xCoord, int yCoord, int zCoord, Block oldBlock, int oldMeta)
	{
		byte b0 = 4;
		int j1 = b0 + 1;

		if (world.checkChunksExist(xCoord - j1, yCoord - j1, zCoord - j1, xCoord + j1, yCoord + j1, zCoord + j1))
		{
			for (int k1 = -b0; k1 <= b0; ++k1)
			{
				for (int l1 = -b0; l1 <= b0; ++l1)
				{
					for (int i2 = -b0; i2 <= b0; ++i2)
					{
						Block adjBlock = world.getBlock(xCoord + k1, yCoord + l1, zCoord + i2);

						if (adjBlock != null)
						{
							adjBlock.beginLeavesDecay(world, xCoord + k1, yCoord + l1, zCoord + i2);
						}
					}
				}
			}
		}
	}
	
	/**used by ItemBlock **/
	@Override
	public int onBlockPlaced(World par1World, int xCoord, int yCoord, int zCoord, int direction, float vecOffsetX, float vecOffsetY, float vecOffsetZ, int itemMeta)
	{
		int metaPiece1 = itemMeta & 3;
		byte metaPiece2 = 0;

		switch (direction)
		{
			case 0:
			case 1:
				metaPiece2 = 0;
				break;
			case 2:
			case 3:
				metaPiece2 = 8;
				break;
			case 4:
			case 5:
				metaPiece2 = 4;
		}
		
		metaPiece2 += 2;//player placed log
		
		return metaPiece1 | metaPiece2;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		int k = meta & 12;
		//int l = meta & 3;
		return k == 0 && (side == 1 || side == 0) ? this.iconTop : (k == 4 && (side == 5 || side == 4) ? this.iconTop : (k == 8 && (side == 2 || side == 3) ? this.iconTop : this.iconSide));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.iconTop = iconRegister.registerIcon(iconNameTop);
		this.iconSide = iconRegister.registerIcon(iconNameSide);
	}
	
	@Override
	public boolean canSustainLeaves(IBlockAccess world, int x, int y, int z)
	{
		return true;
	}

	@Override
	public boolean isWood(IBlockAccess world, int x, int y, int z)
	{
		return true;
	}

}
