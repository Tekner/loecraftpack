package loecraftpack.content.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.Icon;

import loecraftpack.content.blocks.tile.TileColoredBed;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.logic.handlers.HandlerColoredBed;
import loecraftpack.referrance.enums.Dye;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBed;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockColoredBed extends BlockBed implements ITileEntityProvider {

	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedend;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedside;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedtop;
	
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairTopLeft;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairEndLeft;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairSideLeft;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairTopRight;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairEndRight;
	@SideOnly(Side.CLIENT)
	public List<IIcon[]> bedPairSideRight;
	
	public int renderID = 14;
	private int bedDropID = -1;//default is the null instance
	
	
	@Override
	public int getRenderType()
	{
		return renderID;
	}
	
	
	/**
	 * When this method is called, your block should register all the icons it needs with the given IconRegister. This
	 * is the only chance you get to register icons.
	 */
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.bedtop = new ArrayList<IIcon[]>();
		this.bedend = new ArrayList<IIcon[]>();
		this.bedside = new ArrayList<IIcon[]>();
		
		this.bedPairTopLeft = new ArrayList<IIcon[]>();
		this.bedPairEndLeft = new ArrayList<IIcon[]>();
		this.bedPairSideLeft = new ArrayList<IIcon[]>();
		this.bedPairTopRight = new ArrayList<IIcon[]>();
		this.bedPairEndRight = new ArrayList<IIcon[]>();
		this.bedPairSideRight = new ArrayList<IIcon[]>();
		
		for (String name : HandlerColoredBed.iconNames)
		{
			this.bedtop.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/"+name+"_feet_top"), iconRegister.registerIcon("loecraftpack:beds/"+name+"_head_top")} );
			this.bedend.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/"+name+"_feet_end"), iconRegister.registerIcon("loecraftpack:beds/"+name+"_head_end")} );
			this.bedside.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/"+name+"_feet_side"), iconRegister.registerIcon("loecraftpack:beds/"+name+"_head_side")} );
		}
		
		for (String name : HandlerColoredBed.bedPairs.keySet())
		{
			this.bedPairTopLeft.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_top_left"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_top_left")} );
			this.bedPairEndLeft.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_end_left"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_end_left")} );
			this.bedPairSideLeft.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_side_left"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_side_left")} );
			
			this.bedPairTopRight.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_top_right"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_top_right")} );
			this.bedPairEndRight.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_end_right"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_end_right")} );
			this.bedPairSideRight.add( new IIcon[] {iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_feet_side_right"), iconRegister.registerIcon("loecraftpack:beds/pair_"+name.replace(" ", "").toLowerCase()+"_head_side_right")} );
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return this.blockIcon;
	}
	
	/* bug fix : dropping only white beds */
	@Override
	public void breakBlock(World world, int x, int y, int z, Block oldBlock, int oldMeta)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile != null && tile instanceof TileColoredBed)
		{
			bedDropID = ((TileColoredBed)tile).id;
		}
		else
			bedDropID = -1;
		world.removeTileEntity(x, y, z);
		TileColoredBed.finishTileRemoval(world, x, y, z, oldMeta);
		
	}
	
	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		
		int damageValue=0;

		TileEntity tile = world.getTileEntity(x, y, z);
		
		if (tile != null && tile instanceof TileColoredBed)
		{
			damageValue = ((TileColoredBed)tile).id;
		}
		else
			damageValue = bedDropID;
		
		if (damageValue == -1)//null instance
			ret.add(new ItemStack(Blocks.bed, 1, 0));
		else
			ret.add(new ItemStack(getItemDropped(metadata, world.rand, fortune), 1, damageValue));
		return ret;
	}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block scource)
	{
		int meta = world.getBlockMetadata(x, y, z);
		int dir = getDirection(meta);
		boolean flag = false;
		
		if (isBlockHeadOfBed(meta))
		{
			if (world.getBlock(x - field_149981_a[dir][0], y, z - field_149981_a[dir][1]) != this)
			{
				world.setBlockToAir(x, y, z);
				flag=true;
			}
		}
		else if (world.getBlock(x + field_149981_a[dir][0], y, z + field_149981_a[dir][1]) != this)
		{
			world.setBlockToAir(x, y, z);
			flag=true;
			
			if (!world.isRemote)
			{
				this.dropBlockAsItem(world, x, y, z, meta, 0);
			}
		}
	}
	
	@Override
	public Item getItemDropped(int meta, Random random, int fortune)
	{
		return isBlockHeadOfBed(meta) ? null : LOE_Items.beds;
	}
	
	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof TileColoredBed)
			return new ItemStack(LOE_Items.beds, 1, ((TileColoredBed)tile).id);
		else
			return new ItemStack(LOE_Items.beds, 1, Dye.White.ordinal());
	}
	
	@Override
	public boolean isBed(IBlockAccess world, int x, int y, int z, EntityLivingBase player)
	{
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileColoredBed(0);
	}
}
