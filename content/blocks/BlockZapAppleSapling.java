package loecraftpack.content.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.content.worldgen.WorldGenCustomAppleTree;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.BlockSapling;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.IPlantable;

public class BlockZapAppleSapling extends BlockAppleBloomSapling
{

	public BlockZapAppleSapling()
	{
		super();
	}
	
	public void grow(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);

		if ((meta & 8) == 0)
		{
			world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta | 8, 4);
		}
		else
		{
			( new WorldGenCustomAppleTree(true, this, 
											 LOE_Blocks.zapAppleLog,
											 LOE_Blocks.zapAppleLeaves, 6 ) ).generate(world, random, xCoord, yCoord, zCoord);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("loecraftpack:trees/sapling_zapapple");
	}
}
