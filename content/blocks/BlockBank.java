package loecraftpack.content.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.LoECraftPack;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockBank extends Block
{
	protected IIcon icon;
	
	public BlockBank()
	{
		super(Material.circuits);
		setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.blockResistance = 2000;
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer playerEntity, int w, float px, float py, float pz)
	{
		if (playerEntity.getCurrentEquippedItem() == null || playerEntity.getCurrentEquippedItem().getItem() == LOE_Items.bits)
			playerEntity.openGui(LoECraftPack.instance, GuiIds.BANK.ordinal(), world, x, y, z);
		else if (playerEntity.getCurrentEquippedItem().getItem() == LOE_Items.bits)
			/*Deposit Bit Code*/;
		
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return icon;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("loecraftpack:blank");
	}
}
