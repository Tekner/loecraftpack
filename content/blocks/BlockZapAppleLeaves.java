package loecraftpack.content.blocks;

import java.util.Random;

import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockZapAppleLeaves extends BlockAppleBloomLeaves
{
	
	public BlockZapAppleLeaves()
	{
		super();
		this.setTickRandomly(true);
		this.setBlockName("leavesZap");
		this.apple = LOE_Items.zapApple;
		saplingDropRate = 600;//roughly 60 leaves on a tree
	}
	
	//no color change - inventory
	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderColor(int par1)
	{
		return 16777215;
	}
	
	//no color change - world
	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess blockAccess, int coordX, int coordY, int coordZ)
	{
		return 16777215;
	}
	
	@Override
	public void attemptGrow(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if ((meta&4) == 0)
		{
			//chance to grow
			if (random.nextInt(30) == 0)
			{
				if (meta == 3)
					zapGrow(world, xCoord, yCoord, zCoord);
				else
				{
					if (world.setBlock(xCoord, yCoord, zCoord, this, meta + 1, 2))
						tellClientOfChange(world, xCoord, yCoord, zCoord, this);
				}
			}
		}
	}
	
	
	/**
	 * Performs lightning animation and converts nearby valid leaves, into charged leaves.
	 */
	public void zapGrow(World world, int xCoord, int yCoord, int zCoord)
	{
		if (!world.isRaining())return;
		
		world.addWeatherEffect(new EntityLightningBolt(world, xCoord, yCoord, zCoord));
		if (world.setBlock(xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeavesCharged, 0, 2))
			tellClientOfChange(world, xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeavesCharged);
		
		for (int xMod = -3; xMod <4; xMod++)
		{
			for (int yMod = -3; yMod <4; yMod++)
			{
				for (int zMod = -3; zMod <4; zMod++)
				{
					if (world.blockExists(xCoord+xMod, yCoord+yMod, zCoord+zMod) 
							&& Block.isEqualTo(world.getBlock(xCoord+xMod, yCoord+yMod, zCoord+zMod), LOE_Blocks.zapAppleLeaves))
					{
						int meta = world.getBlockMetadata(xCoord+xMod, yCoord+yMod, zCoord+zMod);
						if ((meta&3) >= bloomStage && (meta & 4) == 0)
						{
							if (world.setBlock(xCoord+xMod, yCoord+yMod, zCoord+zMod, LOE_Blocks.zapAppleLeavesCharged, 0, 2))
								tellClientOfChange(world, xCoord+xMod, yCoord+yMod, zCoord+zMod, LOE_Blocks.zapAppleLeavesCharged);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void dropAppleThruTree(World world, int xCoord, int yCoord, int zCoord, ItemStack itemStack)
	{
		dropAppleThruTree(world, xCoord, yCoord, zCoord, itemStack,
				new Block[]{LOE_Blocks.zapAppleLog,
						  LOE_Blocks.zapAppleLeaves,
						  LOE_Blocks.zapAppleLeavesCharged});
	}
	
	@Override
	public Item getItemDropped(int meta, Random random, int fortune)
	{
		return Item.getItemFromBlock(LOE_Blocks.zapAppleSapling);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon[0] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple");
		icon[1] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple_opaque");
		icon[2] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple_bloom");
		icon[3] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple_bloom_opaque");
	}
}
