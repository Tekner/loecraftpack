package loecraftpack.content.blocks;

import java.util.List;
import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.items.ItemGemStones;
import loecraftpack.content.items.ItemPickaxeGem;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockHiddenOre extends Block
{
	IIcon[] hiddenIcons;
	protected ItemGemStones itemSet;
	protected String[] names;
	public int renderID = 0;
	
	public BlockHiddenOre() {
		super(Material.rock);
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.setHarvestLevel("pickaxe", 2);
	}
	
	public BlockHiddenOre setItemSet(ItemGemStones itemSet)
	{
		this.itemSet = itemSet;
		this.names = itemSet.getNames();
		hiddenIcons = new IIcon[names.length];
		
		return this;
	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getHiddenBlockTextureFromSideAndMetadata(int side, int meta)
	{
		return hiddenIcons[meta];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.blockIcon = iconRegister.registerIcon("stone");
		for(int i=0; i<hiddenIcons.length; i++)
		{
			hiddenIcons[i] = iconRegister.registerIcon("loecraftpack:ores/hiddenore"+i);
		}
	}
	
	@Override
	public int getRenderType()
	{
		return renderID;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		for(int i=0; i<hiddenIcons.length; i++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	public Item getItemDropped(int meta, Random random, int fortune)
	{
		return Item.getItemFromBlock(Blocks.cobblestone);
	}
	
	/**
	 * Called when the player destroys a block with an item that can harvest it.
	 */
	@Override
	public void harvestBlock(World world, EntityPlayer entityPlayer, int xCoord, int yCoord, int zCoord, int meta)
	{
		System.out.println("harvest");
		ItemStack results;
		
		if(entityPlayer.getHeldItem().getItem()== LOE_Items.pickaxeGem)
		{
			int fortune = EnchantmentHelper.getFortuneModifier(entityPlayer);
			if (fortune > 0)
			{
				fortune = world.rand.nextInt(fortune + 2);
				if (fortune < 1)
					fortune = 1;
			}
			else
				fortune = 1;
			results = new ItemStack(((Item)itemSet), fortune, meta);
		}
		else if (this.canSilkHarvest(world, entityPlayer, xCoord, yCoord, zCoord, meta) && EnchantmentHelper.getSilkTouchModifier(entityPlayer))
			results = new ItemStack(Blocks.stone, 1, 0);
		else
			results = new ItemStack(Blocks.cobblestone, 1, 0);
		
		this.dropBlockAsItem(world, xCoord, yCoord, zCoord, results);
	}
	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z)
	{
		return new ItemStack(Item.getItemFromBlock(Blocks.stone), 1, 0);
	}
	
	public String getName(int meta)
	{
		return names[meta];
	}
}