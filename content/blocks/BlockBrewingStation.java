package loecraftpack.content.blocks;

import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.logic.helpers.inventoryaccess.IInventoryBlockAccess;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBrewingStation extends BlockContainer implements IInventoryBlockAccess {

	private Random rand = new Random();
	
	protected IIcon icon;
	
	public int renderID;

	public BlockBrewingStation()
	{
		super(Material.iron);
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public int getRenderType()
	{
		return renderID;
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int i)
	{
		return new TileBrewingStation();
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int side, float vecOffsetX, float vecOffsetY, float vecOffsetZ)
	{
		if (world.isRemote)
		{
			return true;
		}
		else
		{
			TileBrewingStation tilePotionStation = (TileBrewingStation)world.getTileEntity(x, y, z);

			if (tilePotionStation != null)
			{
				entityPlayer.openGui(LoECraftPack.instance, GuiIds.BREWING_STATION.ordinal(), world, x, y, z);
			}

			return true;
		}
	}
	
	@Override
	public void onBlockAdded(World par1World, int par2, int par3, int par4)
	{
		super.onBlockAdded(par1World, par2, par3, par4);
		this.updateMetadata(par1World, par2, par3, par4);
	}
	
	@Override
	public void onBlockPlacedBy(World world, int xCoord, int yCoord, int zCoord, EntityLivingBase entityLivingBase, ItemStack itemStack)
	{
		if (itemStack.hasDisplayName())
		{
			((TileBrewingStation)world.getTileEntity(xCoord, yCoord, zCoord)).setCustomInvName(itemStack.getDisplayName());
		}
		int dir = MathHelper.floor_double((double)(entityLivingBase.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, Direction.directionToFacing[(dir+2)%4], 3);
	}
	
	@Override
	public void breakBlock(World par1World, int xCoord, int yCoord, int zCoord, Block par5, int par6)
	{
		TileEntity tileEntity = par1World.getTileEntity(xCoord, yCoord, zCoord);

		if (tileEntity instanceof TileBrewingStation)
		{
			ItemStack[] itemstacks = ((TileBrewingStation)tileEntity).getContentsOnBreak();

			for (int index = 0; index < itemstacks.length; ++index)
			{
				ItemStack itemstack = itemstacks[index];

				if (itemstack != null)
				{
					float xOffset = this.rand.nextFloat() * 0.8F + 0.1F;
					float yOffset = this.rand.nextFloat() * 0.8F + 0.1F;
					float zOffset = this.rand.nextFloat() * 0.8F + 0.1F;

					while (itemstack.stackSize > 0)
					{
						int pieceSize = this.rand.nextInt(21) + 10;

						if (pieceSize > itemstack.stackSize)
						{
							pieceSize = itemstack.stackSize;
						}

						itemstack.stackSize -= pieceSize;
						EntityItem entityitem = new EntityItem(par1World, (double)((float)xCoord + xOffset), (double)((float)yCoord + yOffset), (double)((float)zCoord + zOffset), new ItemStack(itemstack.getItem(), pieceSize, itemstack.getItemDamage()));
						float f3 = 0.05F;
						entityitem.motionX = (double)((float)this.rand.nextGaussian() * f3);
						entityitem.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
						entityitem.motionZ = (double)((float)this.rand.nextGaussian() * f3);
						par1World.spawnEntityInWorld(entityitem);
					}
				}
			}
		}

		super.breakBlock(par1World, xCoord, yCoord, zCoord, par5, par6);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
	{
		//Do: customize this
		TileEntity tile = par1World.getTileEntity(par2, par3, par4);
		if (tile instanceof TileBrewingStation && ((TileBrewingStation)tile).getProgress()>0)
		{
			double d0 = (double)((float)par2 + 0.4F + par5Random.nextFloat() * 0.2F);
			double d1 = (double)((float)par3 + 0.7F + par5Random.nextFloat() * 0.3F);
			double d2 = (double)((float)par4 + 0.4F + par5Random.nextFloat() * 0.2F);
			par1World.spawnParticle("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
		}
	}
	
	@Override
	public boolean hasComparatorInputOverride()
	{
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World par1World, int par2, int par3, int par4, int par5)
	{
		//Do: customize this
		return Container.calcRedstoneFromInventory((IInventory)par1World.getTileEntity(par2, par3, par4));
	}
	
	@Override
	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, Block par5)
	{
		this.updateMetadata(par1World, par2, par3, par4);
	}
	
	private void updateMetadata(World world, int par2, int par3, int par4)
	{
		int meta = world.getBlockMetadata(par2, par3, par4);
		
		boolean flag = world.isBlockIndirectlyGettingPowered(par2, par3, par4);
		boolean flag1 = (meta & 8) == 8;

		if (flag != flag1)
		{
			world.setBlockMetadataWithNotify(par2, par3, par4, (meta&7) | (flag ? 8 : 0), 4);
		}
	}

	@Override
	public IInventory getInventory(World world, int xCoord, int yCoord, int zCoord) 
	{
		TileEntity tile = world.getTileEntity(xCoord, yCoord, zCoord);
		if (tile instanceof IInventory)
			return (IInventory) tile;
		
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return icon;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("loecraftpack:blank");
	}
}
