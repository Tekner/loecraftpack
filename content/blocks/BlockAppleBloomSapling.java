package loecraftpack.content.blocks;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.LoECraftPack;
import loecraftpack.content.worldgen.WorldGenCustomAppleTree;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.BlockFlower;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockAppleBloomSapling extends BlockFlower{
	
	protected IIcon icon;

	public BlockAppleBloomSapling()
    {
        super(0);
        float f = 0.4F;
        this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
        this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
    }
	
	public void updateTick(World world, int xCoord, int yCoord, int zCoord, Random random)
    {
        if (!world.isRemote)
        {
            super.updateTick(world, xCoord, yCoord, zCoord, random);

            if (world.getBlockLightValue(xCoord, yCoord + 1, zCoord) >= 9 && random.nextInt(7) == 0)
            {
                this.grow(world, xCoord, yCoord, zCoord, random);
            }
        }
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int par1, int par2)
    {
        return this.icon;
    }
	
	public void grow(World world, int xCoord, int yCoord, int zCoord, Random random)
    {
        int l = world.getBlockMetadata(xCoord, yCoord, zCoord);

        if ((l & 8) == 0)
        {
            world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, l | 8, 4);
        }
        else
        {
        	( new WorldGenCustomAppleTree(true, this, 
                                             LOE_Blocks.appleBloomLog,
                                             LOE_Blocks.appleBloomLeaves, 6 ) ).generate(world, random, xCoord, yCoord, zCoord);
        }
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs tab, List list)
    {
		list.add(new ItemStack(item, 1, 0));
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("sapling_oak");
	}
}