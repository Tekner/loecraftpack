package loecraftpack.content.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.LoECraftPack;
import loecraftpack.content.registery.LOE_Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;

public class BlockHarmonyLeaves extends BlockLeavesBase implements IShearable {

	protected int saplingDropRate = 5;
	
	protected IIcon icon;
	protected int IconByGraphicsLevel;
	protected int[] adjacentTreeBlocks;
	
	public BlockHarmonyLeaves()
	{
		super(Material.leaves, false);
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
		this.setBlockName("leavesHarmony");
	}
	
	@Override
	public void breakBlock(World world, int xCoord, int yCoord, int zCoord, Block oldBlock, int oldMeta)
	{
		byte effectRange = 1;
		int requiredChunkRange = effectRange + 1;
		
		if (world.checkChunksExist(xCoord - requiredChunkRange, yCoord - requiredChunkRange, zCoord - requiredChunkRange, xCoord + requiredChunkRange, yCoord + requiredChunkRange, zCoord + requiredChunkRange))
		{
			for (int offX = -effectRange; offX <= effectRange; ++offX)
			{
				for (int offY = -effectRange; offY <= effectRange; ++offY)
				{
					for (int offZ = -effectRange; offZ <= effectRange; ++offZ)
					{
						Block block = world.getBlock(xCoord + offX, yCoord + offY, zCoord + offZ);
						
						if (block != null)
						{
							block.beginLeavesDecay(world, xCoord + offX, yCoord + offY, zCoord + offZ);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void updateTick(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		if (!world.isRemote)
		{
			int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
			boolean flag = true;
			
			if ((meta & 8) != 0 && (meta & 4) == 0)
			{
				byte scanRange = 4;
				int requiredChunkRange = scanRange + 1;
				byte arraySize = 32;
				int xSequanceMod = arraySize * arraySize;
				int arrayCenterMod = arraySize / 2;
				
				if (this.adjacentTreeBlocks == null)
				{
					this.adjacentTreeBlocks = new int[arraySize * arraySize * arraySize];
				}
				
				if (world.checkChunksExist(xCoord - requiredChunkRange, yCoord - requiredChunkRange, zCoord - requiredChunkRange, xCoord + requiredChunkRange, yCoord + requiredChunkRange, zCoord + requiredChunkRange))
				{
					int offX;
					int offY;
					int offZ;
					
					for (offX = -scanRange; offX <= scanRange; ++offX)
					{
						for (offY = -scanRange; offY <= scanRange; ++offY)
						{
							for (offZ = -scanRange; offZ <= scanRange; ++offZ)
							{
								Block block = world.getBlock(xCoord + offX, yCoord + offY, zCoord + offZ);
								
								if (block != null && block.canSustainLeaves(world, xCoord + offX, yCoord + offY, zCoord + offZ))
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = 0;
								}
								else if (block != null && block.isLeaves(world, xCoord + offX, yCoord + offY, zCoord + offZ))
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = -2;
								}
								else
								{
									this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = -1;
								}
							}
						}
					}
					
					for (int i = 1; i <= 4; ++i)
					{
						for (offX = -scanRange; offX <= scanRange; ++offX)
						{
							for (offY = -scanRange; offY <= scanRange; ++offY)
							{
								for (offZ = -scanRange; offZ <= scanRange; ++offZ)
								{
									if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == i - 1)
									{
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod - 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod - 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod + 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod + 1) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod - 1) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod - 1) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod + 1) * arraySize + offZ + arrayCenterMod] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod + 1) * arraySize + offZ + arrayCenterMod] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + (offZ + arrayCenterMod - 1)] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + (offZ + arrayCenterMod - 1)] = i;
										}
										
										if (this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod + 1] == -2)
										{
											this.adjacentTreeBlocks[(offX + arrayCenterMod) * xSequanceMod + (offY + arrayCenterMod) * arraySize + offZ + arrayCenterMod + 1] = i;
										}
									}
								}
							}
						}
					}
				}
				
				int sustainValueSelf = this.adjacentTreeBlocks[arrayCenterMod * xSequanceMod + arrayCenterMod * arraySize + arrayCenterMod];
				
				if (sustainValueSelf >= 0)
				{
					world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta & -9, 4);
				}
				else
				{
					this.removeLeaves(world, xCoord, yCoord, zCoord);
					flag = false;
				}
			}
		}
	}
	
	public void randomDisplayTick(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		if (world.canLightningStrikeAt(xCoord, yCoord + 1, zCoord) && !World.doesBlockHaveSolidTopSurface(world, xCoord, yCoord - 1, zCoord) && random.nextInt(15) == 1)
		{
			double d0 = (double)((float)xCoord + random.nextFloat());
			double d1 = (double)yCoord - 0.05D;
			double d2 = (double)((float)zCoord + random.nextFloat());
			world.spawnParticle("dripWater", d0, d1, d2, 0.0D, 0.0D, 0.0D);
		}
	}
	
	private void removeLeaves(World world, int xCoord, int yCoord, int zCoord)
	{
		this.dropBlockAsItem(world, xCoord, yCoord, zCoord, world.getBlockMetadata(xCoord, yCoord, zCoord), 0);
		world.setBlockToAir(xCoord, yCoord, zCoord);
	}
	
	@Override
	public int quantityDropped(Random random)
	{
		return random.nextInt(1) == 0 ? 1 : 0;
	}
	
	@Override
	public Item getItemDropped(int meta, Random random, int fortune)
	{
		return Item.getItemFromBlock(LOE_Blocks.harmonySapling);
	}
	
	@Override
	public void dropBlockAsItemWithChance(World world, int xCoord, int yCoord, int zCoord, int meta, float dropChanceMod, int fortune)
	{
		if (!world.isRemote)
		{
			int chance = saplingDropRate;
			
			if (fortune > 0)
			{
				chance -= (saplingDropRate/10) << fortune;
				
				if (chance < 10)
				{
					chance = 10;
				}
			}
			
			if (world.rand.nextInt(chance) == 0)
			{
				Item item = this.getItemDropped(meta, world.rand, fortune);
				this.dropBlockAsItem(world, xCoord, yCoord, zCoord, new ItemStack(item, 1, 0));
			}
		}
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
	{
		return null;
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public int getRenderType()
	{
		return 1;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return icon;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon = iconRegister.registerIcon("loecraftpack:trees/leaves_harmony");
	}

	@Override
	public boolean isShearable(ItemStack item, IBlockAccess world, int xCoord, int yCoord, int zCoord)
	{
		return true;
	}

	@Override
	public ArrayList<ItemStack> onSheared(ItemStack item, IBlockAccess world, int xCoord, int yCoord, int zCoord, int fortune)
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		ret.add(new ItemStack(this, 1, world.getBlockMetadata(xCoord, yCoord, zCoord) & 3));
		return ret;
	}

}
