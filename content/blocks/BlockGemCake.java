package loecraftpack.content.blocks;

import net.minecraft.block.BlockCake;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockGemCake extends BlockCake {

	public BlockGemCake() {
		super();
		this.textureName = "loecraftpack:food/gemcake";
		this.setCreativeTab(CreativeTabs.tabFood);
	}
	
	@Override
	public boolean onBlockActivated(World world, int xCoord, int yCoord, int zCoord, EntityPlayer entityPlayer, int par6, float par7, float par8, float par9)
	{
		this.eatCakeSlice(world, xCoord, yCoord, zCoord, entityPlayer);
		return true;
	}
	
	@Override
	public void onBlockClicked(World world, int xCoord, int yCoord, int zCoord, EntityPlayer entityPlayer)
	{
		this.eatCakeSlice(world, xCoord, yCoord, zCoord, entityPlayer);
	}
	
	protected void eatCakeSlice(World world, int xCoord, int yCoord, int zCoord, EntityPlayer entityPlayer)
	{
		if (entityPlayer.canEat(false))
		{
			entityPlayer.getFoodStats().addStats(2, 0.1F);
			entityPlayer.attackEntityFrom(DamageSource.starve, 4);
			entityPlayer.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 1200, 0));
			
			int l = world.getBlockMetadata(xCoord, yCoord, zCoord) + 1;

			if (l >= 6)
			{
				world.setBlockToAir(xCoord, yCoord, zCoord);
			}
			else
			{
				world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, l, 2);
			}
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z)
    {
        return new ItemStack(Item.getItemFromBlock(this), 1, 0);
    }

}
