package loecraftpack.content.blocks;

import java.util.List;
import java.util.Random;

import loecraftpack.content.entities.EntityElectricBlock;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockZapAppleLeavesCharged extends BlockZapAppleLeaves
{
	
	public BlockZapAppleLeavesCharged() {
		super();
		this.setLightLevel(0.5f);
		this.setBlockName("leavesZapCharged");
		this.appleType = 1;
		bloomStage = 4;//no bloom stage
		growthChance = 150/41;//average of one day(in-game) to reach stage 4
	}
	
	@Override
	public void dropBlockAsItemWithChance(World world, int xCoord, int yCoord, int zCoord, int meta, float dropChanceMod, int fortune)
	{
		if (!world.isRemote)
		{
			float chanceSapling = (dropChanceMod>0)? saplingDropRate/dropChanceMod: saplingDropRate;

			if (fortune > 0)
			{
				chanceSapling -= (int)(chanceSapling/10) << fortune;

				if (chanceSapling < ((dropChanceMod>0)? 10/dropChanceMod: 10))
				{
					chanceSapling = (dropChanceMod>0)? 10/dropChanceMod: 10;
				}
			}

			if (world.rand.nextFloat() < 1.0f/chanceSapling)
			{
				Item sapling = this.getItemDropped(meta, world.rand, fortune);
				this.dropBlockAsItem(world, xCoord, yCoord, zCoord, new ItemStack(sapling, 1, this.damageDropped(meta)));
			}
			
			if (!sheared)
			{
				int amount = world.rand.nextInt(fortune + 1)+1;
				
				this.dropAppleThruTree(world, xCoord, yCoord, zCoord, new ItemStack(apple, amount, appleType));
			}
			//reset sheared
			sheared = false;
		}
	}
	
	@Override
	public void attemptGrow(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if ((meta & 4) == 0)
		{
			//chance to weaken
			if (random.nextDouble()*(growthChance) <= 1)
			{
				if (meta == 3)
				{
					if (world.setBlock(xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeaves, LOE_Blocks.zapAppleLeaves.bloomStage, 2))
						tellClientOfChange(world, xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeaves);
				}
				else
				{
					if (world.setBlock(xCoord, yCoord, zCoord, this, meta + 1, 2))
						tellClientOfChange(world, xCoord, yCoord, zCoord, this);
				}
			}
		}
	}
	
	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int xCoord, int yCoord, int zCoord)
	{
		for (Object spark : world.getEntitiesWithinAABB(EntityElectricBlock.class, AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord+1, yCoord+1, zCoord+1)))
			((EntityElectricBlock)spark).setDead(); //Remove spark effect entities
		
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if (!sheared &&  (meta&4)==0)
		{
			return world.setBlock(xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeaves, meta&12, 2);
		}
		return world.setBlockToAir(xCoord, yCoord, zCoord);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int xCoord, int yCoord, int zCoord, Random random)
	{
		//charged apple effect
		if (random.nextInt(5) == 0 && world.getEntitiesWithinAABB(EntityElectricBlock.class, AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord+1, yCoord+1, zCoord+1)).size() == 0)
		{
			//spawn electric animation
			EntityElectricBlock eleField = new EntityElectricBlock(world);
			eleField.setPosition(xCoord+0.5, yCoord-0.1, zCoord+0.5);
			world.spawnEntityInWorld(eleField);
		}
				
		//water droplet code
		if (world.canLightningStrikeAt(xCoord, yCoord + 1, zCoord) && !World.doesBlockHaveSolidTopSurface(world, xCoord, yCoord - 1, zCoord) && random.nextInt(15) == 1)
		{
			double d0 = (double)((float)xCoord + random.nextFloat());
			double d1 = (double)yCoord - 0.05D;
			double d2 = (double)((float)zCoord + random.nextFloat());
			world.spawnParticle("dripWater", d0, d1, d2, 0.0D, 0.0D, 0.0D);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		list.add(new ItemStack(item, 1, 0));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icon[0] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple_charge");
		icon[1] = iconRegister.registerIcon("loecraftpack:trees/leaves_zapapple_charge_opaque");
	}
}