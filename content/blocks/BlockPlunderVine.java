package loecraftpack.content.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.LoECraftPack;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCactus;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import static net.minecraftforge.common.util.ForgeDirection.*;

public class BlockPlunderVine extends BlockCactus {

	@SideOnly(Side.CLIENT)
	protected IIcon TopIcon;
	@SideOnly(Side.CLIENT)
	protected IIcon BottomIcon;
	
	public BlockPlunderVine()
	{
		super();
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
	}
	
	@Override
	public boolean canBlockStay(World world, int xCoord, int yCoord, int zCoord)
	{
		Block targetBlock = world.getBlock(xCoord, yCoord - 1, zCoord);
		return targetBlock != null && (Block.isEqualTo(targetBlock, this) || targetBlock.canSustainPlant(world, xCoord, yCoord - 1, zCoord, UP, this));
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, int xCoord, int yCoord, int zCoord, Entity entity)
	{
		super.onEntityCollidedWithBlock(world, xCoord, yCoord, zCoord, entity);
		entity.setInWeb();
	}
	
	
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int xCoord, int yCoord, int zCoord)
	{
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return side == 1 ? TopIcon : (side == 0 ? BottomIcon : blockIcon);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		blockIcon = iconRegister.registerIcon("loecraftpack:trees/" + this.getTextureName() + "_side");
		TopIcon = iconRegister.registerIcon("loecraftpack:trees/" + this.getTextureName() + "_top");
		BottomIcon = iconRegister.registerIcon("loecraftpack:trees/" + this.getTextureName() + "_bottom");
	}
	
	@Override
	public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z)
	{
		//grows like a weed
		return EnumPlantType.Cave;
	}
}
