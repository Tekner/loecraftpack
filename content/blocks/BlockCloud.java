package loecraftpack.content.blocks;

import loecraftpack.LoECraftPack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class BlockCloud extends Block {

	public BlockCloud() {
		super(LoECraftPack.materialCloud);
		setHardness(0.5f);
		setStepSound(Block.soundTypeCloth);
		setCreativeTab(LoECraftPack.LoECraftTabBlock);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.blockIcon = iconRegister.registerIcon("loecraftpack:cloud");
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderBlockPass()
	{
		return 1;
	}
	
	//gather water?
	@Override
	public void fillWithRain(World par1World, int par2, int par3, int par4) {}
	
	//bind this to other cloud blocks from other mods
	@Override
	public boolean isAssociatedBlock(Block block)
	{
		return this == block;
	}
}