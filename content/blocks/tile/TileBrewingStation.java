package loecraftpack.content.blocks.tile;

import java.util.Random;

import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import loecraftpack.logic.helpers.inventoryaccess.InventoryAccessHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileBrewingStation extends TileEntity implements ISidedInventory
{
	private Random rand = new Random();
	
	public static final int[] slotRangeIngredients = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8};
	
	protected String invName;
	protected ItemStack[] inventory = new ItemStack[9];
	protected boolean invChange = true;
	protected int progressTime = -1;
	protected boolean autoBrew = false;
	CustomBrewingRecipe recipe;
	
	protected byte RollingID = 0;
	
	public static int renderPass = 0;
	
	
	  /*****************************************/
	 /**  ACCESSORS  **************************/
	/*****************************************/
	
	
	  /********************/
	 /**  GETTERS  *******/
	
	public int getProgress()
	{
		return progressTime;
	}
	
	public int getBrewTime()
	{
		return recipe == null? 0: recipe.brewingTime();
	}
	
	public boolean getAutoBrew()
	{
		return autoBrew;
	}
	
	public byte getRollingID()
	{
		return RollingID;
	}
	
	/**external test function**/
	public int getRecipeState(CustomBrewingRecipe recipe)
	{
		if (this.recipe != null && recipe.id == this.recipe.id)
			return 2;
		else if (recipe.isValidRecipe(inventory))
			return 1;
		else
			return 0;
	}
	
	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return slot >= 0 && slot < this.inventory.length ? this.inventory[slot] : null;
	}
	
	public ItemStack[] getContentsOnBreak()
	{
		return inventory;
	}
	
	@Override
	public int getSizeInventory()
	{
		return inventory.length;
	}
	
	@Override
	public String getInventoryName()
	{
		return this.hasCustomInventoryName() ? this.invName : "container.potionstation";
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return this.invName != null && this.invName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int slot)
	{
		return slot == 0? new int[0]: slotRangeIngredients;
	}
	
	
	  /********************/
	 /**  SETTERS  *******/
	
	public void setCustomInvName(String string)
	{
		this.invName = string;
	}
	
	public void selectRecipe(CustomBrewingRecipe recipe)
	{
		if (recipe != null)
		{
			if (this.recipe == null || this.recipe.id != recipe.id)
			{
				this.recipe = recipe;
				progressTime=0;
				markDirty(true);
			}
			else if (this.recipe != null)
			{
				this.recipe = null;
				progressTime=-1;
				markDirty(true);
			}
		}
	}
	
	public void setAutoBrew(boolean mode)
	{
		if (autoBrew != mode)
		{
			autoBrew = mode;
			markDirty(true);
		}
	}
	
	
	
	  /********************/
	 /**  SUPPORT  *******/
	
	@Override
	public void markDirty()
	{
		super.markDirty();
		invChange = true;
	}
	
	public boolean hasInventoryChanged()
	{
		return invChange;
	}
	
	public void resolvedAllChanges()
	{
		invChange = false;
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		if (slot >= 0 && slot < this.inventory.length)
		{
			ItemStack itemstack = this.inventory[slot];
			this.inventory[slot] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public ItemStack decrStackSize(int slot, int ammount)
	{
		if (this.inventory[slot] != null)
		{
			ItemStack itemstack;
			
			if (this.inventory[slot].stackSize <= ammount)
			{
				itemstack = this.inventory[slot];
				this.inventory[slot] = null;
				this.markDirty();
				return itemstack;
			}
			else
			{
				itemstack = this.inventory[slot].splitStack(ammount);
				
				if (this.inventory[slot].stackSize == 0)
				{
					this.inventory[slot] = null;
				}
				
				this.markDirty();
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public void setInventorySlotContents(int slot, ItemStack itemstack) {
		if (slot >= 0 && slot < this.inventory.length)
		{
			this.inventory[slot] = itemstack;
		}
		this.markDirty();
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : entityplayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
	}
	
	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}
	
	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemstack) 
	{
		if (slot < 0 || slot >= 9)
			return false;
		else
			return isIngredient(itemstack);
	}
	
	public boolean isIngredient(ItemStack itemstack)
	{
		return true;/*
		return itemstack.itemID == Item.potion.itemID ||
				itemstack.itemID == Item.dyePowder.itemID ||
				itemstack.itemID == Item.appleRed.itemID ||
				Item.itemsList[itemstack.itemID].isPotionIngredient() ||
				(itemstack.getItem() instanceof ICustomPotion) ||
				((itemstack.getItem() instanceof ICustomPotionIngredient) && ((ICustomPotionIngredient)itemstack.getItem()).validIngredient(itemstack));*/
	}
	
	
	@Override
	public boolean canInsertItem(int slot, ItemStack itemstack, int side)
	{
		return isItemValidForSlot(slot, itemstack);
	}
	
	@Override
	public boolean canExtractItem(int slot, ItemStack itemstack, int side)
	{
		//prevent hoppers from auto removing items from the bottom
		return side!=0;
	}
	
	@Override
	public boolean shouldRenderInPass(int pass)
	{
		renderPass = pass;
		return true;
	}
	
	
	
	  /*****************************************/
	 /**  NBT  METHODS  ***********************/
	/*****************************************/
	
	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		this.readNBT(compound);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		this.writeNBT(compound);
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound compound = new NBTTagCompound();
		
		this.writeNBT(compound);
		compound.setByte("RollingID", RollingID++);
		
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, compound);
    }
	
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet)
	{
		this.readNBT(packet.func_148857_g());
		RollingID = packet.func_148857_g().getByte("RollingID");
	}
	
	protected void writeNBT(NBTTagCompound compound)
	{
		NBTTagList nbttaglist = new NBTTagList();

		for (int slot = 0; slot < inventory.length; ++slot)
		{
			if (inventory[slot] != null)
			{
				NBTTagCompound compoundINV = new NBTTagCompound();
				compoundINV.setByte("Slot", (byte)slot);
				inventory[slot].writeToNBT(compoundINV);
				nbttaglist.appendTag(compoundINV);
			}
		}
		compound.setTag("Items", nbttaglist);
		
		compound.setInteger("RecipeID", recipe != null? recipe.id: -1);
		compound.setInteger("Progress", progressTime);
		compound.setBoolean("AutoBrew", autoBrew);
	}
	
	public void readNBT(NBTTagCompound compound)
	{
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		inventory = new ItemStack[this.getSizeInventory()];

		for (int index = 0; index < nbttaglist.tagCount(); ++index)
		{
			NBTTagCompound compoundINV = (NBTTagCompound)nbttaglist.getCompoundTagAt(index);
			int slot = compoundINV.getByte("Slot") & 255;

			if (slot >= 0 && slot < inventory.length)
			{
				inventory[slot] = ItemStack.loadItemStackFromNBT(compoundINV);
			}
		}
		
		recipe = CustomBrewingRecipe.getRecipe(compound.getInteger("RecipeID"));
		progressTime = compound.getInteger("Progress");
		autoBrew = compound.getBoolean("AutoBrew");
	}
	
	
	
	  /*****************************************/
	 /**  TICK LOGIC  *************************/
	/*****************************************/
	
	@Override
	public void updateEntity()
	{
		super.updateEntity();
		
		if (progressTime >= 0)
		{
			//check progress
			progressTime++;
			
			if (recipe != null)
			{
				
				if (!invChange || recipe.isValidRecipe(inventory))
				{
					invChange = false;
					
					if (progressTime >= recipe.brewingTime())
					{
						if (worldObj.isRemote)
						{
							endRecipe();
							return;
						}
						
						ItemStack[] products = recipe.processResult(inventory);
						this.cleanINV();
						if (products != null)
						{
							//move items to container below
							IInventory inventory = InventoryAccessHelper.findInventory(worldObj, xCoord, yCoord - 1, zCoord);
							if (inventory != null)
							{
								products = InventoryAccessHelper.dumpItemsIntoBlockInventory(inventory, products, 1);
							}
							
							// then try the appropriate slots of the station
							if (products != null)
							{
								products = InventoryAccessHelper.dumpItemsIntoInventoryNoReg(this, products);
							}
							
							//then eject them as entities if no room found
							if (products != null)
							{
								this.releaseItemsIntoWorld(products);
							}
						}
						
						if (canAutoBrew())
						{
							progressTime=0;
							markDirty(false);//Do: TilePotionStation - check if this needs to send a packet
						}
						else
							endRecipe();
					}
					else if (progressTime%20 == 0)
						markDirty(false);
				}
				else
					endRecipe();
			}
			else
				endRecipe();
		}
		else if (canAutoBrew())
		{
			progressTime=0;
			
			markDirty(true);
		}
	}
	
	protected void cleanINV()
	{
		for (int index=0; index <inventory.length; index++)
		{
			if (inventory[index] != null && inventory[index].stackSize<=0)
			{
				inventory[index] = null;
			}
		}
	}
	
	protected boolean canAutoBrew()
	{
		return autoBrew && (worldObj.getBlockMetadata(xCoord, yCoord, zCoord)&8)==0 && recipe!=null && recipe.isValidRecipe(inventory);
	}
	
	protected void endRecipe()
	{
		progressTime=-1;
		if (!autoBrew)
			recipe = null;
		
		markDirty(true);
	}
	
	protected void markDirty(boolean sendpacket)
	{
		if (this.worldObj != null && !this.worldObj.isRemote)
		{
			this.blockMetadata = this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord);
			this.worldObj.markTileEntityChunkModified(this.xCoord, this.yCoord, this.zCoord, this);
			if (sendpacket)
				this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
		}
	}
	
	
	
	  /*****************************************/
	 /**  Toss Items  *************************/
	/*****************************************/
	
	protected void releaseItemsIntoWorld(ItemStack[] itemstacks)
	{
		for (int index = 0; index < itemstacks.length; ++index)
		{
			ItemStack itemstack = itemstacks[index];

			if (itemstack != null)
			{
				float xOffset = this.rand.nextFloat() * 0.8F + 0.1F;
				float yOffset = this.rand.nextFloat() * 0.8F + 0.1F;
				float zOffset = this.rand.nextFloat() * 0.8F + 0.1F;

				while (itemstack.stackSize > 0)
				{
					int pieceSize = this.rand.nextInt(21) + 10;

					if (pieceSize > itemstack.stackSize)
					{
						pieceSize = itemstack.stackSize;
					}

					itemstack.stackSize -= pieceSize;
					EntityItem entityitem = new EntityItem(worldObj, (double)((float)xCoord + xOffset), (double)((float)yCoord + yOffset), (double)((float)zCoord + zOffset), new ItemStack(itemstack.getItem(), pieceSize, itemstack.getItemDamage()));
					float f3 = 0.05F;
					entityitem.motionX = (double)((float)this.rand.nextGaussian() * f3);
					entityitem.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
					entityitem.motionZ = (double)((float)this.rand.nextGaussian() * f3);
					worldObj.spawnEntityInWorld(entityitem);
				}
			}
		}
	}
}