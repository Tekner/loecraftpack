package loecraftpack.content.blocks.tile;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileProtectionMonolith extends TileEntity
{
	public int offsetX
			 , offsetZ
			 , width = 8
			 , length = 8
			 , offsetXInc = 0
			 , offsetZInc = 0
			 , widthInc = 0
			 , lengthInc = 0;
			 
	public Map<String, String> owners = new HashMap<String, String>();
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
    {
		super.readFromNBT(nbt);
		owners.clear();
        offsetX = nbt.getInteger("ox");
        offsetZ = nbt.getInteger("oz");
        width = nbt.getInteger("w");
        length = nbt.getInteger("l");
        NBTTagList nbttaglist = nbt.getTagList("owners", 8/*string*/);
        for (int i = 0; i < nbttaglist.tagCount()-1; i += 2)
        {
        	owners.put(nbttaglist.getStringTagAt(i), nbttaglist.getStringTagAt(i+1));
        }
    }
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
    {
		super.writeToNBT(nbt);
        nbt.setInteger("ox", offsetX);
        nbt.setInteger("oz", offsetZ);
        nbt.setInteger("w", width);
        nbt.setInteger("l", length);
        NBTTagList nbttaglist = new NBTTagList();
        for (Entry<String, String> entry: owners.entrySet())
        {
        	nbttaglist.appendTag(new NBTTagString(entry.getKey()));
        	nbttaglist.appendTag(new NBTTagString(entry.getValue()));
        }
    }
	
	@Override
	public Packet getDescriptionPacket()
    {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, nbt);
    }
	
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}
	
	@SideOnly(Side.CLIENT)
    public double getMaxRenderDistanceSquared()
    {
        return 65536.0D;
    }
	
	@SideOnly(Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox()
    {
        return INFINITE_EXTENT_AABB;
    }
	
	public boolean pointIsProtected(int x, int y, int z)
	{
		return (x >= (xCoord+offsetX-Math.ceil(width/2)) && x < (xCoord+offsetX+Math.floor(width/2))) &&
			   (z >= (zCoord+offsetZ-Math.ceil(length/2)) && z < (zCoord+offsetZ+Math.floor(length/2)));
	}
	
	public boolean isOwner(String id)
	{
		return owners.containsKey(id);
	}
}
