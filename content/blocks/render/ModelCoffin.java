package loecraftpack.content.blocks.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelCoffin extends ModelBase
{
	/** The chest lid in the chest's model. */
	public ModelRenderer chestLid = (new ModelRenderer(this, 0, 0)).setTextureSize(128, 128);

	/** The chest's knob in the chest model. */
	public ModelRenderer chestTrim;
	
	/** The model of the bottom of the chest. */
	public ModelRenderer chestBelow;

	/** The chest's knob in the chest model. */
	public ModelRenderer chestBase;
	
	public ModelCoffin()
	{
		chestLid = (new ModelRenderer(this, 0, 0)).setTextureSize(128, 128);
		chestLid.addBox(0.0F, -5.0F, -14.0F, 30, 3, 14, 0.0F);
		chestLid.rotationPointX = 1.0F;
		chestLid.rotationPointY = 6.0F;
		chestLid.rotationPointZ = 15.0F;
		chestTrim = (new ModelRenderer(this, 0, 17)).setTextureSize(128, 128);
		chestTrim.addBox(0.0F, -3.0F, -15.0F, 32, 3, 16, 0.0F);
		chestTrim.rotationPointX = 0.0F;
		chestTrim.rotationPointY = 6.0F;
		chestTrim.rotationPointZ = 15.0F;
		chestBelow = (new ModelRenderer(this, 0, 36)).setTextureSize(128, 128);
		chestBelow.addBox(0.0F, 0.0F, 0.0F, 30, 10, 14, 0.0F);
		chestBelow.rotationPointX = 1.0F;
		chestBelow.rotationPointY = 5.0F;
		chestBelow.rotationPointZ = 1.0F;
		chestBase = (new ModelRenderer(this, 0, 17)).setTextureSize(128, 128);
		chestBase.addBox(0.0F, 8.0F, -15.0F, 32, 3, 16, 0.0F);
		chestBase.rotationPointX = 0.0F;
		chestBase.rotationPointY = 5.0F;
		chestBase.rotationPointZ = 15.0F;
	}
	
	/**
	 * This method renders out all parts of the chest model.
	 */
	public void renderAll()
	{
		chestTrim.rotateAngleX = chestLid.rotateAngleX;
		chestLid.render(0.0625F);
		chestTrim.render(0.0625F);
		chestBelow.render(0.0625F);
		chestBase.render(0.0625F);
	}
}