package loecraftpack.content.blocks.render;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.blocks.BlockHiddenOre;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderWorldLastEvent;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class RenderHiddenOre implements ISimpleBlockRenderingHandler
{
	public int renderID;
	public static boolean phantomPass = false;
	
	public List<int[]> phantomBlocks = new ArrayList<int[]>();
	
	@Override
	public void renderInventoryBlock(Block blockBase, int metadata, int modelID, RenderBlocks renderer)
	{
		BlockHiddenOre block = (BlockHiddenOre)blockBase;
		
		Tessellator tessellator = Tessellator.instance;
		boolean flag = Block.isEqualTo(block, Blocks.grass);
		float brightness = 1.0f;//why isn't this passed to us?
		
		int renderColor;
		float red;
		float green;
		float blue;

		if (renderer.useInventoryTint)
		{
			renderColor = block.getRenderColor(metadata);

			if (flag)
			{
				renderColor = 16777215;
			}

			red = (float)(renderColor >> 16 & 255) / 255.0F;
			green = (float)(renderColor >> 8 & 255) / 255.0F;
			blue = (float)(renderColor & 255) / 255.0F;
			GL11.glColor4f(red * brightness, green * brightness, blue * brightness, 1.0F);
		}
		
		renderer.setRenderBoundsFromBlock(block);
		
		block.setBlockBoundsForItemRender();
		renderer.setRenderBoundsFromBlock(block);
		GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1.0F, 0.0F);
		renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(0, metadata));
		tessellator.draw();

		if (flag && renderer.useInventoryTint)
		{
			renderColor = block.getRenderColor(metadata);
			green = (float)(renderColor >> 16 & 255) / 255.0F;
			blue = (float)(renderColor >> 8 & 255) / 255.0F;
			float f7 = (float)(renderColor & 255) / 255.0F;
			GL11.glColor4f(green * brightness, blue * brightness, f7 * brightness, 1.0F);
		}

		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(1, metadata));
		tessellator.draw();

		if (flag && renderer.useInventoryTint)
		{
			GL11.glColor4f(brightness, brightness, brightness, 1.0F);
		}
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, -1.0F);
		renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(2, metadata));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, 1.0F);
		renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(3, metadata));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
		renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(4, metadata));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getHiddenBlockTextureFromSideAndMetadata(5, metadata));
		tessellator.draw();
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		renderer.renderStandardBlock(block, x, y, z);
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId)
	{
		return true;
	}

	@Override
	public int getRenderId()
	{
		return renderID;
	}
	
	public void drawBlockPhantomTexture(RenderWorldLastEvent event)
	{
		World world = event.context.theWorld;
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		
		phantomPass = true;
		
		boolean hold = event.context.renderBlocksRg.renderAllFaces;
		event.context.renderBlocksRg.renderAllFaces = true;
		
		Tessellator par1Tessellator = Tessellator.instance;
		EntityLivingBase par2EntityPlayer = event.context.mc.renderViewEntity;
		float partialTicks = event.partialTicks;
		
		
		double d0 = par2EntityPlayer.lastTickPosX + (par2EntityPlayer.posX - par2EntityPlayer.lastTickPosX) * (double)partialTicks;
		double d1 = par2EntityPlayer.lastTickPosY + (par2EntityPlayer.posY - par2EntityPlayer.lastTickPosY) * (double)partialTicks;
		double d2 = par2EntityPlayer.lastTickPosZ + (par2EntityPlayer.posZ - par2EntityPlayer.lastTickPosZ) * (double)partialTicks;
		
		if (!this.phantomBlocks.isEmpty())
		{
			GL11.glBlendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE_MINUS_SRC_COLOR);
			event.context.renderEngine.bindTexture(TextureMap.locationBlocksTexture);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
			GL11.glPushMatrix();
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glPolygonOffset(-3.0F, -3.0F);
			GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			par1Tessellator.startDrawingQuads();
			par1Tessellator.setTranslation(-d0, -d1, -d2);
			par1Tessellator.disableColor();
			
			int[][] locations = this.phantomBlocks.toArray(new int[phantomBlocks.size()][]);

			for (int i=0; i<locations.length; i++)
			{
				int[] location = locations[i];
				
				Block block = world.getBlock(location[0], location[1], location[2]);
				
				if (block != null)
					if (block instanceof BlockHiddenOre)
					{
						event.context.renderBlocksRg.renderBlockUsingTexture(block, location[0], location[1], location[2],
								 ((BlockHiddenOre)block).getHiddenBlockTextureFromSideAndMetadata(0, world.getBlockMetadata(location[0], location[1], location[2])));
					}
					else
					{
						event.context.renderBlocksRg.renderBlockUsingTexture(block, location[0], location[1], location[2],
								 block.getIcon(0, world.getBlockMetadata(location[0], location[1], location[2])));
					}
			}

			par1Tessellator.draw();
			par1Tessellator.setTranslation(0.0D, 0.0D, 0.0D);
			GL11.glDisable(GL11.GL_ALPHA_TEST);
			GL11.glPolygonOffset(0.0F, 0.0F);
			GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glDepthMask(true);
			GL11.glPopMatrix();
		}
		
		event.context.renderBlocksRg.renderAllFaces = hold;
		
		phantomPass = false;
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}
}
