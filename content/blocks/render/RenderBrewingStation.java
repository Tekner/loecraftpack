package loecraftpack.content.blocks.render;

import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class RenderBrewingStation extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler
{
	protected static final ResourceLocation RES_BREWING = new ResourceLocation("loecraftpack:entities/brewing.png");
	protected static final ResourceLocation RES_POTION = new ResourceLocation("loecraftpack:entities/potion.png");
	
	protected ModelBrewingStation modelBrew = new ModelBrewingStation();
	protected ModelPotion modelPotion = new ModelPotion(Items.potionitem.getColorFromDamage(0));
	
	protected TileBrewingStation tile = new TileBrewingStation();
	
	public int renderID;
	
	public void renderTileBrewingStation(TileBrewingStation tileEntity, double relativeX, double relativeY, double relativeZ, float par8)
	{
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)relativeX + 0.5f, (float)relativeY + 0.5f, (float)relativeZ + 0.5f);
		
		int metadata = tileEntity.blockMetadata;
		short AngleY = 0;
		boolean renderAlphaBackwards = false;
		
		if (metadata == 2)
		{
			AngleY = 180;
			renderAlphaBackwards = relativeX<-0.5f;
		}
		else if (metadata == 3)
		{
			AngleY = 0;
			renderAlphaBackwards = relativeX>-0.5f;
		}
		else if (metadata == 4)
		{
			AngleY = -90;
			renderAlphaBackwards = relativeZ>-0.5f;
		}
		else if (metadata == 5)
		{
			AngleY = 90;
			renderAlphaBackwards = relativeZ<-0.5f;
		}
		
		GL11.glPushMatrix();
		
		GL11.glRotatef((float)AngleY, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5f, -0.5f, -0.5f);
		
		if (tileEntity.renderPass == 0)
		{
			this.bindTexture(RES_BREWING);
			modelBrew.renderAll();
		}
		else if (tileEntity.renderPass == 1)
		{
			int numOfWater=0;
			for (int i=0; i<tileEntity.getSizeInventory(); i++)
			{
				ItemStack stack = tileEntity.getStackInSlot(i);
				if (stack != null && stack.getItem() == Items.potionitem && stack.getItemDamage() == 0)
					numOfWater++;
			}
			
			if (numOfWater > 0)
			{
				this.bindTexture(RES_POTION);
				if (renderAlphaBackwards)
				{
					//render in reverse order
					for (int i=Math.min(2, numOfWater-1); i>=0; i--)
					{
						modelPotion.renderAll(0.25f*(i+1), 0.1251f, 0.875f);
					}
				}
				else
				{
					for (int i=0; i<3 && i<numOfWater; i++)
					{
						modelPotion.renderAll(0.25f*(i+1), 0.1251f, 0.875f);
					}
				}
			}
		}
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double relativeX, double relativeY, double relativeZ, float par8)
	{
		((TileBrewingStation)tileentity).getBlockMetadata();
		this.renderTileBrewingStation((TileBrewingStation)tileentity, relativeX, relativeY, relativeZ, par8);
	}
	
	
	
	public void renderItem(Block block, int metadata, int modelID)
	{
		if (block == LOE_Blocks.brewingStation)
		{
			TileBrewingStation.renderPass = 0;
			renderTileBrewingStation((TileBrewingStation)this.tile, 0.0D, 0.0D, 0.0D, 0.0F);
		}
	}
	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
	{
		GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		this.renderItem(block, metadata, modelID);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
	}
	
	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		return false;
	}
	
	@Override
	public boolean shouldRender3DInInventory(int modelId)
	{
		return true;
	}
	
	@Override
	public int getRenderId()
	{
		return 0;
	}
}
