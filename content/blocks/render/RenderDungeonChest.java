package loecraftpack.content.blocks.render;

import java.util.Calendar;

import loecraftpack.content.blocks.BlockDungeonChest;
import loecraftpack.content.blocks.tile.TileDungeonChest;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelChest;
import net.minecraft.client.model.ModelLargeChest;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.common.FMLLog;

public class RenderDungeonChest extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler {
	
	protected static final ResourceLocation RES_BOSS_DOUBLE = new ResourceLocation("loecraftpack:entities/chest/boss_double.png");
	protected static final ResourceLocation RES_COFFIN_DOUBLE = new ResourceLocation("loecraftpack:entities/chest/coffin_double.png");
	protected static final ResourceLocation RES_HALLOWEEN_DOUBLE = new ResourceLocation("loecraftpack:entities/chest/halloween_double.png");
	protected static final ResourceLocation RES_BOSS_SINGLE = new ResourceLocation("loecraftpack:entities/chest/boss.png");
	protected static final ResourceLocation RES_COFFIN_SINGLE = new ResourceLocation("loecraftpack:entities/chest/coffin.png");
	protected static final ResourceLocation RES_HALLOWEEN_SINGLE = new ResourceLocation("loecraftpack:entities/chest/halloween.png");

	protected ModelChest chestModel = new ModelChest();//Do: create model small chest
	protected ModelChest largeChestModel = new ModelLargeChest();//Do: create model large chest
	
	protected ModelShrine shrineModel = new ModelShrine();//Do: create model small chest
	protected ModelCoffin coffinModel = new ModelCoffin();//Do: create model large chest

	/** If true, chests will be rendered with the Spooky textures. */
	protected boolean isHalloween;
	
	protected TileDungeonChest tile0 = new TileDungeonChest(0);
	protected TileDungeonChest tile1 = new TileDungeonChest(1);
	
	public int renderID;
	
	public RenderDungeonChest()
	{
		Calendar calendar = Calendar.getInstance();

		if ((calendar.get(Calendar.MONTH) + 1 == 10 && calendar.get(Calendar.DAY_OF_MONTH) >= 30 ) || (calendar.get(Calendar.MONTH) + 1 == 11 && calendar.get(Calendar.DAY_OF_MONTH) <= 1))
		{
			this.isHalloween = true;
		}
	}
	
	
	//Do: RenderDungeonChest - rebuild the following code (current is old vanilla code)
	
	/**
	 * Renders the TileEntity for the chest at a position.
	 */
	public void renderTileDungeonChestAt(TileDungeonChest tileEntityChest, double relativeX, double relativeY, double relativeZ, float par8)
	{
		int metadata;

		if (!tileEntityChest.hasWorldObj())
		{
			metadata = 0;
		}
		else
		{
			Block block = tileEntityChest.getBlockType();
			metadata = tileEntityChest.getBlockMetadata();
			
			if (block instanceof BlockDungeonChest && metadata == 0)
			{
				try
				{
					((BlockDungeonChest)block).unifyAdjacentChests(tileEntityChest.getWorldObj(), tileEntityChest.xCoord, tileEntityChest.yCoord, tileEntityChest.zCoord);
				}
				catch (ClassCastException e)
				{
					FMLLog.severe("Attempted to render a chest at %d,  %d, %d that was not a chest",
							tileEntityChest.xCoord, tileEntityChest.yCoord, tileEntityChest.zCoord);
				}
				metadata = tileEntityChest.getBlockMetadata();
			}

			tileEntityChest.checkForAdjacentChests();
		}

		if (tileEntityChest.adjacentChestZNeg == null && tileEntityChest.adjacentChestXNeg == null)
		{
			ModelBase model;

			if (tileEntityChest.adjacentChestXPos == null && tileEntityChest.adjacentChestZPosition == null)
			{
				model = this.chestModel;

				if (tileEntityChest.getChestType() == 1)
				{
					model = this.shrineModel;
					this.bindTexture(RES_COFFIN_SINGLE);
				}
				else if (this.isHalloween)
				{
					this.bindTexture(RES_HALLOWEEN_SINGLE);
				}
				else
				{
					this.bindTexture(RES_BOSS_SINGLE);
				}
			}
			else
			{
				model = this.largeChestModel;

				if (tileEntityChest.getChestType() == 1)
				{
					model = this.coffinModel;
					this.bindTexture(RES_COFFIN_DOUBLE);
				}
				else if (this.isHalloween)
				{
					this.bindTexture(RES_HALLOWEEN_DOUBLE);
				}
				else
				{
					this.bindTexture(RES_BOSS_DOUBLE);
				}
			}

			GL11.glPushMatrix();
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glTranslatef((float)relativeX, (float)relativeY + 1.0F, (float)relativeZ + 1.0F);
			GL11.glScalef(1.0F, -1.0F, -1.0F);
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			short AngleY = 0;

			if (metadata == 2)
			{
				AngleY = 180;
			}

			if (metadata == 3)
			{
				AngleY = 0;
			}

			if (metadata == 4)
			{
				AngleY = 90;
			}

			if (metadata == 5)
			{
				AngleY = -90;
			}

			if (metadata == 2 && tileEntityChest.adjacentChestXPos != null)
			{
				GL11.glTranslatef(1.0F, 0.0F, 0.0F);
			}

			if (metadata == 5 && tileEntityChest.adjacentChestZPosition != null)
			{
				GL11.glTranslatef(0.0F, 0.0F, -1.0F);
			}

			GL11.glRotatef((float)AngleY, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
			float angleXLid = tileEntityChest.prevLidAngle + (tileEntityChest.lidAngle - tileEntityChest.prevLidAngle) * par8;
			float angleXLidPartner;

			if (tileEntityChest.adjacentChestZNeg != null)
			{
				angleXLidPartner = tileEntityChest.adjacentChestZNeg.prevLidAngle + (tileEntityChest.adjacentChestZNeg.lidAngle - tileEntityChest.adjacentChestZNeg.prevLidAngle) * par8;

				if (angleXLidPartner > angleXLid)
				{
					angleXLid = angleXLidPartner;
				}
			}

			if (tileEntityChest.adjacentChestXNeg != null)
			{
				angleXLidPartner = tileEntityChest.adjacentChestXNeg.prevLidAngle + (tileEntityChest.adjacentChestXNeg.lidAngle - tileEntityChest.adjacentChestXNeg.prevLidAngle) * par8;

				if (angleXLidPartner > angleXLid)
				{
					angleXLid = angleXLidPartner;
				}
			}

			angleXLid = 1.0F - angleXLid;
			angleXLid = 1.0F - angleXLid * angleXLid * angleXLid;
			if (model instanceof ModelChest)
			{
				ModelChest modelchest = (ModelChest)model;
				modelchest.chestLid.rotateAngleX = -(angleXLid * (float)Math.PI / 2.0F);
				modelchest.renderAll();
			}
			else if (model instanceof ModelShrine)
			{
				ModelShrine modelshrine = (ModelShrine)model;
				modelshrine.chestLid.rotateAngleX = -(angleXLid * (float)Math.PI / 2.0F);
				modelshrine.renderAll();
			}
			else if (model instanceof ModelCoffin)
			{
				ModelCoffin modelcoffin = (ModelCoffin)model;
				modelcoffin.chestLid.rotateAngleX = -(angleXLid * (float)Math.PI / 2.0F);
				modelcoffin.renderAll();
			}
			
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
			GL11.glPopMatrix();
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
	
	@Override
	public void renderTileEntityAt(TileEntity par1TileEntity, double par2, double par4, double par6, float par8)
	{
		this.renderTileDungeonChestAt((TileDungeonChest)par1TileEntity, par2, par4, par6, par8);
	}
	
	
	
	public void renderChestItem(Block block, int metadata, int modelID)
	{
		if (Block.isEqualTo(block, LOE_Blocks.bossChest))
		{
			TileEntityRendererDispatcher.instance.renderTileEntityAt(this.tile0, 0.0D, 0.0D, 0.0D, 0.0F);
		}
		else if (Block.isEqualTo(block, LOE_Blocks.coffin))
		{
			TileEntityRendererDispatcher.instance.renderTileEntityAt(this.tile1, 0.0D, 0.0D, 0.0D, 0.0F);
		}
	}
	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
	{
		GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		this.renderChestItem(block, metadata, modelID);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
	}


	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		return false;
	}


	@Override
	public boolean shouldRender3DInInventory(int modelId)
	{
		return true;
	}


	@Override
	public int getRenderId()
	{
		return renderID;
	}

}
