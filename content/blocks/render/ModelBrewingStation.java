package loecraftpack.content.blocks.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelBrewingStation extends ModelBase {
	
	public ModelRenderer brewingStand;
	public ModelRenderer tankard;
	public ModelRenderer tankardLegs;
	
	ModelBrewingStation()
	{
		brewingStand = (new ModelRenderer(this, 0, 0)).setTextureSize(64, 64);
		brewingStand.addBox(0.0F, -2.0F, -16.0F, 16, 2, 16, 0.0F);
		brewingStand.rotateAngleX = (float)Math.PI;
		
		tankard = (new ModelRenderer(this, 0, 18)).setTextureSize(64, 64);
		tankard.addBox(2.0F, -9.5F, -7.0F, 12, 6, 6, 0.0F);
		tankard.rotateAngleX = (float)Math.PI;
		
		tankardLegs = (new ModelRenderer(this, 0, 18)).setTextureSize(64, 64);
		tankardLegs.addBox( 2.1F, -4.0F, -2.1F, 1, 2, 1, 0.0F);
		tankardLegs.addBox(12.9F, -4.0F, -2.1F, 1, 2, 1, 0.0F);
		tankardLegs.addBox( 2.1F, -4.0F, -6.9F, 1, 2, 1, 0.0F);
		tankardLegs.addBox(12.9F, -4.0F, -6.9F, 1, 2, 1, 0.0F);
		tankardLegs.rotateAngleX = (float)Math.PI;
	}
	
	public void renderAll()
	{
		brewingStand.render(0.0625F);
		tankard.render(0.0625F);
		tankardLegs.render(0.0625F);
	}

}
