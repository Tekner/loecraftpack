package loecraftpack.content.blocks.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelPotion extends ModelBase 
{
	public ModelRenderer plug;
	public ModelRenderer opening;
	public ModelRenderer nozzle;
	public ModelRenderer topBody;
	public ModelRenderer mainBody;
	public ModelRenderer bottomBody;
	public ModelRenderer contents;
	
	public int color;
	
	public ModelPotion(int color)
	{
		this.color = color;
		
		plug = (new ModelRenderer(this, 0, 0)).setTextureSize(64, 64);
		plug.addBox(-1.0F, -13.0F, -1.0F, 2, 2, 2, 0.0F);
		plug.rotateAngleX = (float)Math.PI;
		
		opening = (new ModelRenderer(this, 0, 4)).setTextureSize(64, 64);
		opening.addBox(-2.0F, -12.0F, -2.0F, 4, 2, 4, 0.0F);
		opening.rotateAngleX = (float)Math.PI;
		
		nozzle = (new ModelRenderer(this, 0, 10)).setTextureSize(64, 64);
		nozzle.addBox(-1.0F, -10.0F, -1.0F, 2, 2, 2, 0.0F);
		nozzle.rotateAngleX = (float)Math.PI;
		
		topBody = (new ModelRenderer(this, 0, 14)).setTextureSize(64, 64);
		topBody.addBox(-2.0F, -8.0F, -2.0F, 4, 1, 4, 0.0F);
		topBody.rotateAngleX = (float)Math.PI;
		
		mainBody = (new ModelRenderer(this, 0, 19)).setTextureSize(64, 64);
		mainBody.addBox(-3.0F, -7.0F, -3.0F, 6, 6, 6, 0.0F);
		mainBody.rotateAngleX = (float)Math.PI;
		
		bottomBody = (new ModelRenderer(this, 0, 31)).setTextureSize(64, 64);
		bottomBody.addBox(-2.0F, -1.0F, -2.0F, 4, 1, 4, 0.0F);
		bottomBody.rotateAngleX = (float)Math.PI;
		
		if (color>=0)
		{
			contents = (new ModelRenderer(this, 0, 36)).setTextureSize(64, 64);
			contents.addBox(-2.0F, -6.0F, -2.0F, 4, 4, 4, 0.0F);
			contents.rotateAngleX = (float)Math.PI;
		}
	}
	
	public void renderAll(float relativeX, float relativeY, float relativeZ)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(relativeX, relativeY, relativeZ);
		renderAll();
		GL11.glPopMatrix();
	}
	
	
	public void renderAll()
	{
		GL11.glPushMatrix();
		
		GL11.glScalef(0.5f, 0.5f, 0.5f);
		
		plug.render(0.0625F);
		if (color>=0)
		{
			float red = (float)(color >> 16 & 255) / 255.0F;
			float green = (float)(color >> 8 & 255) / 255.0F;
			float blue = (float)(color & 255) / 255.0F;
			GL11.glColor4f(red, green, blue, 1.0F);
			contents.render(0.0625F);
		}
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		bottomBody.render(0.0625F);
		mainBody.render(0.0625F);
		topBody.render(0.0625F);
		nozzle.render(0.0625F);
		opening.render(0.0625F);
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}

}
