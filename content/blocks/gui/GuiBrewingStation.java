package loecraftpack.content.blocks.gui;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.gui.buttons.GuiButtonCustomState;
import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import loecraftpack.network.packets.PacketUpdateBrewingMode;
import loecraftpack.network.packets.PacketUpdateBrewingSelection;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiBrewingStation extends GuiContainer {

	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/brewing_station.png");
	
	protected EntityPlayer player;
	protected TileBrewingStation tileEntity;
	protected GuiButtonCustomState autoButton;
	protected RecipeButton[] recipeButtons = new RecipeButton[5];
	protected final int startingId = 4;
	
	protected final int windowPosX = 71;
	protected final int windowPosY = 17;
	
	protected final int scrollbuttonSizeX = 12;
	protected final int scrollbuttonSizeY = 15;
	protected final int windowSizeX = 105;
	protected final int windowSizeY = 78;
	protected final int buttonSizeX = windowSizeX;
	protected final int buttonSizeY = 19;
	protected final int BrewSizeX = 48;//+1
	protected final int BrewSizeY = 26;
	
	protected final int brewU = 0;
	protected final int brewV = 192;
	protected final int buttonU = 49;
	protected final int buttonV = 192;
	protected final int scrollU = 232;
	protected final int scrollV = 241;
	
	protected int scrollMax = Math.max(0, CustomBrewingRecipe.size()*buttonSizeY-windowSizeY);
	protected int scroll = 0;
	protected int listIndex = 0;
	protected boolean isScrolling = false;
	
	protected int lastScroll = scroll;
	protected byte rollingTileID;
	
	public GuiBrewingStation(EntityPlayer player, TileBrewingStation tileEntity)
	{
		super(new ContainerPotionStation(player, tileEntity));
		this.player = player;
		this.tileEntity = tileEntity;
		xSize = 196;
		ySize = 192;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.buttonList.clear();
		
		this.buttonList.add(autoButton = new GuiButtonCustomState(startingId, mainImage, this.guiLeft+54, this.guiTop+70, 154, 192, 10, 26, null));
		for (int i=0; i< recipeButtons.length; i++)
			this.buttonList.add(recipeButtons[i] = new RecipeButton(startingId+1+i, this, mainImage, this.guiLeft+windowPosX, this.guiTop+windowPosY, this.guiLeft+windowPosX+windowSizeX, this.guiTop+windowPosY+windowSizeY, buttonU, buttonV, buttonSizeX, buttonSizeY));
		
		updateButtons();
		rollingTileID = tileEntity.getRollingID();
	}
	
	@Override
	public void drawScreen(int posXmouse, int posYmouse, float par3)
	{
		if (rollingTileID != tileEntity.getRollingID() || lastScroll != scroll || tileEntity.hasInventoryChanged())
		{
			updateButtons();
			rollingTileID = tileEntity.getRollingID();
			lastScroll = scroll;
			tileEntity.resolvedAllChanges();
		}
		
		super.drawScreen(posXmouse, posYmouse, par3);
		
		
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		RenderHelper.enableGUIStandardItemLighting();
		
		//tooltips
	  	renderPopouts(posXmouse, posYmouse);
		
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		RenderHelper.enableStandardItemLighting();
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int posXmouse, int posYmouse)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal("Brewing Station"), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 18, this.ySize - 96 + 2, 4210752);
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		//scroll bar
		this.mc.renderEngine.bindTexture(mainImage);
		int offsetY = (int) (((float)windowSizeY-(float)scrollbuttonSizeY)*((float)scroll/(float)scrollMax));
		this.drawTexturedModalRect(windowPosX+windowSizeX, windowPosY+offsetY, scrollU, scrollV, scrollbuttonSizeX, scrollbuttonSizeY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(mainImage);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
		
		//brew time
		this.drawTexturedModalRect(k+10, l+70, brewU, brewV, BrewSizeX, BrewSizeY);
		if (tileEntity.getBrewTime()>0)
			this.drawTexturedModalRect(k+10, l+70, brewU, brewV+BrewSizeY, (int)((float)(BrewSizeX+1)*((float)tileEntity.getProgress()/(float)tileEntity.getBrewTime())), BrewSizeY);
	}
	
	@Override
	public void actionPerformed(GuiButton button)
	{
		if (button.id == startingId)
			LoECraftPack.packetHandler.sendToServer(new PacketUpdateBrewingMode().INIT(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, tileEntity.getAutoBrew()));
		
		else if (button.id >= startingId+1 && button.id < startingId+1+recipeButtons.length )
		{
			CustomBrewingRecipe recipe = CustomBrewingRecipe.getRecipe((button.id-(startingId+1))+listIndex);
			if (recipe != null)
			{
				LoECraftPack.packetHandler.sendToServer(new PacketUpdateBrewingSelection().INIT(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, recipe.id));
			}
		}
	}
	
	@Override
	public void handleMouseInput()
	{
		super.handleMouseInput();
		int i = Mouse.getEventDWheel();

		if (i != 0)
		{

			if (i > 0)
			{
				i = 10;
			}

			if (i < 0)
			{
				i = -10;
			}
			
			setScroll(scroll - i);
		}
	}
	
	@Override
	protected void mouseClicked(int posX, int posY, int par3)
	{
		int scrollBarPosX = guiLeft+windowPosX+windowSizeX;
		int scrollBarPosY = guiTop+windowPosY;
		
		if (posX >= scrollBarPosX && posY >= scrollBarPosY && posX < scrollBarPosX + scrollbuttonSizeX && posY < scrollBarPosY + windowSizeY)
		{
			isScrolling = true;
		}
		else
			super.mouseClicked(posX, posY, par3);
	}
	
	@Override
	protected void mouseMovedOrUp(int par1, int par2, int par3)
	{
		super.mouseMovedOrUp(par1, par2, par3);
		isScrolling = false;
	}
	
	@Override
	protected void mouseClickMove(int posX, int posY, int par3, long time)
	{
		if (isScrolling)
		{
			int scrollBarPosY = guiTop+windowPosY + scrollbuttonSizeY/2;
			float target = Math.max(0.0F, Math.min((float)(posY - scrollBarPosY)/(float)(windowSizeY-scrollbuttonSizeY), 1.0f));
			
			this.setScroll((int)((float)target * scrollMax));
		}
	}
	
	protected void setScroll(int newValue)
	{
		scroll = Math.min(scrollMax, Math.max(0, newValue));
	}
	
	protected void renderPopouts(int posXmouse, int posYmouse)
	{
		for (RecipeButton target : recipeButtons)
		{
			if (target.getHoverDrawnButton(posXmouse, posYmouse))
			{
				target.renderPopout(mc, 0, 0, posXmouse, posYmouse);
			}
		}
		
	}
	
	protected void updateButtons()
	{
		autoButton.state = tileEntity.getAutoBrew()? 1: 0;
		
		listIndex = MathHelper.floor_float((float)scroll/(float)buttonSizeY);
		for (int i=0; i<recipeButtons.length; i++)
		{
			if (i+listIndex >= CustomBrewingRecipe.size())
			{
				recipeButtons[i].mute();
			}
			else
			{
				CustomBrewingRecipe recipe = CustomBrewingRecipe.getRecipe(i+listIndex);
				if (recipe == null)
				{
					recipeButtons[i].mute();
				}
				else
				{
					recipeButtons[i].updateButton(0, (i+listIndex)*buttonSizeY - scroll, tileEntity.getRecipeState(recipe), recipe);
				}
				
			}
		}
	}
}
