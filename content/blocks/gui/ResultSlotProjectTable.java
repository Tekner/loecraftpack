package loecraftpack.content.blocks.gui;

import loecraftpack.content.blocks.tile.TileProjectTable;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;

public class ResultSlotProjectTable extends Slot
{
    private final TileProjectTable craftMatrix;

    private EntityPlayer thePlayer;

    private int amountCrafted;

    public ResultSlotProjectTable(EntityPlayer par1EntityPlayer, TileProjectTable par2IInventory, IInventory par3IInventory, int par4, int par5, int par6)
    {
        super(par3IInventory, par4, par5, par6);
        this.thePlayer = par1EntityPlayer;
        this.craftMatrix = par2IInventory;
    }

    public boolean isItemValid(ItemStack par1ItemStack)
    {
        return false;
    }

    public ItemStack decrStackSize(int par1)
    {
        if (this.getHasStack())
        {
            this.amountCrafted += Math.min(par1, this.getStack().stackSize);
        }

        return super.decrStackSize(par1);
    }

    protected void onCrafting(ItemStack par1ItemStack, int par2)
    {
        this.amountCrafted += par2;
        this.onCrafting(par1ItemStack);
    }
    
    protected void onCrafting(ItemStack par1ItemStack)
    {
        par1ItemStack.onCrafting(this.thePlayer.worldObj, this.thePlayer, this.amountCrafted);
        this.amountCrafted = 0;

        if (par1ItemStack.getItem() == Item.getItemFromBlock(Blocks.crafting_table))
        {
            this.thePlayer.addStat(AchievementList.buildWorkBench, 1);
        }
        else if (par1ItemStack.getItem() == Items.wooden_pickaxe)
        {
            this.thePlayer.addStat(AchievementList.buildPickaxe, 1);
        }
        else if (par1ItemStack.getItem() == Item.getItemFromBlock(Blocks.furnace))
        {
            this.thePlayer.addStat(AchievementList.buildFurnace, 1);
        }
        else if (par1ItemStack.getItem() == Items.wooden_hoe)
        {
            this.thePlayer.addStat(AchievementList.buildHoe, 1);
        }
        else if (par1ItemStack.getItem() == Items.bread)
        {
            this.thePlayer.addStat(AchievementList.makeBread, 1);
        }
        else if (par1ItemStack.getItem() == Items.cake)
        {
            this.thePlayer.addStat(AchievementList.bakeCake, 1);
        }
        else if (par1ItemStack.getItem() == Items.stone_pickaxe)
        {
            this.thePlayer.addStat(AchievementList.buildBetterPickaxe, 1);
        }
        else if (par1ItemStack.getItem() == Items.wooden_sword)
        {
            this.thePlayer.addStat(AchievementList.buildSword, 1);
        }
        else if (par1ItemStack.getItem() == Item.getItemFromBlock(Blocks.enchanting_table))
        {
            this.thePlayer.addStat(AchievementList.enchantments, 1);
        }
        else if (par1ItemStack.getItem() == Item.getItemFromBlock(Blocks.bookshelf))
        {
            this.thePlayer.addStat(AchievementList.bookcase, 1);
        }
    }

    public void onPickupFromSlot(EntityPlayer par1EntityPlayer, ItemStack par2ItemStack)
    {
    	FMLCommonHandler.instance().firePlayerCraftingEvent(par1EntityPlayer, par2ItemStack, craftMatrix);
        this.onCrafting(par2ItemStack);
        
        Item[] reagentItem = new Item[9];
        int[] reagentMeta = new int[9];
        boolean[] reagentFlag = new boolean[9];
        
        for(int i = 0; i < 9; i++)
        {
        	reagentFlag[i] = false;
    		ItemStack itemStack = this.craftMatrix.getStackInSlot(i);
    		if (itemStack != null)
    		{
    			reagentItem[i] = itemStack.getItem();
    			reagentMeta[i] = itemStack.getItemDamage();
    		}
    		else
    		{
    			reagentItem[i] = null;
    			reagentMeta[i] = 0;
    		}
        }

        
    	for(int n = 0; n < 9; n++)
        {
    		for (int i = craftMatrix.getSizeInventory()-1; i >= 0; --i)
            {
            	ItemStack item = this.craftMatrix.getStackInSlot(i);
                if (item != null)
                {
                	if (item.getItem() == reagentItem[n] && item.getItemDamage() == reagentMeta[n])
            		{
                		if ((i < 9 && !reagentFlag[i]) || i >= 9)
                		{
                			craftMatrix.decrStackSize(i, 1);
	            			reagentItem[n] = null;
	            			reagentMeta[n] = -1;
	            			if (i < 9)
	            				reagentFlag[i] = true;
	            			i = -1;
                		}
            		}
                	
                	if (item.getItem().hasContainerItem(null))
                    {
                        ItemStack itemstack2 = item.getItem().getContainerItem(item);

                        if (itemstack2.isItemStackDamageable() && itemstack2.getItemDamage() > itemstack2.getMaxDamage())
                        {
                            MinecraftForge.EVENT_BUS.post(new PlayerDestroyItemEvent(thePlayer, itemstack2));
                            itemstack2 = null;
                        }

                        if (itemstack2 != null && (!item.getItem().doesContainerItemLeaveCraftingGrid(item) || !this.thePlayer.inventory.addItemStackToInventory(itemstack2)))
                        {
                            if (this.craftMatrix.getStackInSlot(i) == null)
                            {
                                this.craftMatrix.setInventorySlotContents(i, itemstack2);
                            }
                            else
                            {
                                this.thePlayer.dropPlayerItemWithRandomChoice(itemstack2, false);
                            }
                        }
                    }
                }
        	}
        }
        
        craftMatrix.eventHandler.onCraftMatrixChanged(craftMatrix);
    }
    
    public boolean hasReagents()
    {
    	return true;
    }
}
