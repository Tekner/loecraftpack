package loecraftpack.content.blocks.gui;

import loecraftpack.content.blocks.tile.TileBrewingStation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPotionStation extends Container {
	
	final static int slotRangeIngredients = 9;
	final static int slotRangeMain = 27+slotRangeIngredients;
	final static int slotRangeHotBar = 9+slotRangeMain;
	
	
	protected EntityPlayer player;
	protected TileBrewingStation tileEntity;
	
	public ContainerPotionStation(EntityPlayer player, TileBrewingStation tileEntity)
	{
		this.player = player;
		this.tileEntity = tileEntity;
		
		//ingredients
		for (int y = 0; y < 3; ++y)
		{
			for (int x = 0; x < 3; ++x)
			{
				this.addSlotToContainer(new SlotInventorySensitive(tileEntity, x + y * 3, 11 + x * 18, 17 + y * 18));
			}
		}
		
		for (int y = 0; y < 3; ++y)
		{
			for (int x = 0; x < 9; ++x)
			{
				this.addSlotToContainer(new Slot(player.inventory, x + y * 9 + 9, 18 + x * 18, 110 + y * 18));
			}
		}
		
		for (int x = 0; x < 9; ++x)
		{
			this.addSlotToContainer(new Slot(player.inventory, x, 18 + x * 18, 168));
		}
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex)
	{
		ItemStack stackCopy = null;
		Slot slot = (Slot)this.inventorySlots.get(slotIndex);

		if (slot != null && slot.getHasStack())
		{
			ItemStack stackInSlot = slot.getStack();
			stackCopy = stackInSlot.copy();

			if (slotIndex < slotRangeIngredients)//move items from station to player
			{
				if (!this.mergeItemStack(stackInSlot, slotRangeMain, slotRangeHotBar, true))
				{
					if (!this.mergeItemStack(stackInSlot, slotRangeIngredients, slotRangeMain, false))
					{
						return null;
					}
				}
			}
			else if (slotIndex < slotRangeHotBar)//move items from player, to either stand or other part of player inv
			{
				if (!tileEntity.isItemValidForSlot(0, stackInSlot) || !this.mergeItemStack(stackInSlot, 0, slotRangeIngredients, false))
				{
					if (slotIndex < slotRangeMain)
					{
						if (!this.mergeItemStack(stackInSlot, slotRangeMain, slotRangeHotBar, true))
						{
							return null;
						}
					}
					else
					{
						if (!this.mergeItemStack(stackInSlot, slotRangeIngredients, slotRangeMain, false))
						{
							return null;
						}
					}
				}
				
			}
			
			//moved entire stack - clear ItemStack
			if (stackInSlot.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (stackInSlot.stackSize == stackCopy.stackSize)
			{
				return null;//do nothing
			}

			slot.onPickupFromSlot(entityPlayer, stackInSlot);
		}

		return stackCopy;//return original stack state, as a reference
	}

}
