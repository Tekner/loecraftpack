package loecraftpack.content.blocks.gui;

import loecraftpack.content.gui.buttons.GuiScrollButtonOld;
import loecraftpack.content.gui.tooltip.TooltipHandler;
import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

public class RecipeButton extends GuiScrollButtonOld
{
	CustomBrewingRecipe recipe;

	public RecipeButton(int id, GuiScreen parent, ResourceLocation mainImage, int minX, int minY, int maxX, int maxY, int baseU, int baseV, int width, int height)
	{
		super(id, parent, mainImage, minX, minY, maxX, maxY, baseU, baseV, width, height);
		recipe = null;
	}
	
	public void updateButton(int subPosX, int subPosY, int state, CustomBrewingRecipe recipe)
	{
		super.updateButton(subPosX, subPosY, state);
		this.recipe = recipe;
	}
	
	@Override
	protected void renderContent(Minecraft mc, int posXmouse, int posYmouse)
	{
		FontRenderer fontRenderer = mc.fontRenderer;
		
		mc.getTextureManager().bindTexture(mainImage);
		this.drawTexturedModalRect(this.xPosition, this.yPosition, posU+offsetU, posV+offsetV+state*baseHeight, this.width, this.height);
		
		fontRenderer.drawString(StatCollector.translateToLocal(recipe.name), xPosition - offsetU + 4, yPosition - offsetV + 6, 16777215);
	}
	
	@Override
	protected void renderPopout(Minecraft mc, int guiLeft, int guiTop, int posXmouse, int posYmouse)
	{
		TooltipHandler.drawWithBounds(recipe.getTooltip(), guiLeft, guiTop, posXmouse, posYmouse, 0, 0, parent.width, parent.height);
	}
}
