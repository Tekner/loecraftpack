package loecraftpack.content.blocks.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotInventorySensitive extends Slot {

	public SlotInventorySensitive(IInventory inventory, int par2, int par3, int par4) {
		super(inventory, par2, par3, par4);
	}
	
	public boolean isItemValid(ItemStack itemstack)
	{
		return inventory.isItemValidForSlot(this.getSlotIndex(), itemstack);
	}
	
	public boolean canTakeStack(EntityPlayer par1EntityPlayer)
	{
		if (inventory instanceof ISidedInventory)
		{
			return ((ISidedInventory)inventory).canExtractItem(this.getSlotIndex(), null, -1);
		}
		else
			return true;
	}

}
