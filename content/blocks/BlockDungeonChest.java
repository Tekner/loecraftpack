package loecraftpack.content.blocks;

import static net.minecraftforge.common.util.ForgeDirection.DOWN;

import java.util.Iterator;
import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.tile.TileDungeonChest;
import loecraftpack.logic.helpers.inventoryaccess.IInventoryBlockAccess;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockDungeonChest extends BlockContainer implements IInventoryBlockAccess{
	
	private final Random random = new Random();
	
	/** 0: Boss Chests, 1: Coffins*/
	public final int chestType;
	
	public int renderID = 22;//default chest - gets replaced
	
	public BlockDungeonChest(int type)
	{
		super(Material.anvil);
		this.chestType = type;
		this.setCreativeTab(LoECraftPack.LoECraftTabBlock);
		switch (this.chestType)
		{
		case 0:
			this.setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
			break;
		case 1:
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.9375F, 1.0F);
			break;
		}
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block oldBlock, int oldMetaData)
	{
		spawnMobs(world, x, y, z);
		this.breakBlockChest(world, x, y, z, oldBlock, oldMetaData);
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int side, float vecOffsetX, float vecOffsetY, float vecOffsetZ)
	{
		spawnMobs(world, x, y, z);
		if (this.isBed(world, x, y, z, entityPlayer))
			return onUseAsBed(world, x, y, z, entityPlayer, side, vecOffsetX, vecOffsetY, vecOffsetZ);
		else
			return this.onBlockActivatedChest(world, x, y, z, entityPlayer, side, vecOffsetX, vecOffsetY, vecOffsetZ);
	}
	
	@Override
	public boolean isBed(IBlockAccess world, int x, int y, int z, EntityLivingBase player)
	{
		//Do: BlockDungeonChest - make this detect bed status.
		return false;
	}
	
	//Do: BlockDungeonChest - remove this if not implemented
	@Override
	public boolean canProvidePower()
	{
		return false;
	}
	
	@Override
	public TileEntity createNewTileEntity(World par1World, int metaData)
	{
		TileEntityChest tileentitychest = new TileDungeonChest();
		return tileentitychest;
	}
	
	
  /********************************************************************************************/
 /**  Custom Code ****************************************************************************/
/********************************************************************************************/
	
	protected void spawnMobs(World world, int x, int y, int z)
	{
		IInventory inventory = this.getInventory(world, x, y, z);
		TileDungeonChest tile = (TileDungeonChest)world.getTileEntity(x, y, z);
		for (int slot=0; slot<inventory.getSizeInventory(); slot++)
		{
			ItemStack stack = inventory.getStackInSlot(slot);
			if (stack!= null)
			{
				Item item = stack.getItem();
				if (item instanceof ItemMonsterPlacer)
				{
					System.out.println("SUPRISE!"+stack.stackSize);
					int eggs = stack.stackSize;
					for (int i=0; i<eggs;)
					{
						int xf = tile.spawnMinX + random.nextInt(1 + tile.spawnMaxX - tile.spawnMinX);
						int yf = tile.spawnMinY + random.nextInt(1 + tile.spawnMaxY - tile.spawnMinY);
						int zf = tile.spawnMinZ + random.nextInt(1 + tile.spawnMaxZ - tile.spawnMinZ);
						System.out.println("----"+tile.spawnMinX+" "+tile.spawnMinY+" "+tile.spawnMinZ);
						
						Entity entity = ItemMonsterPlacer.spawnCreature(world, stack.getItemDamage(), (double)(x+xf) + 0.5D, (double)(y+yf), (double)(z+zf) + 0.5D);
						
						if (entity != null)
						{
							inventory.decrStackSize(slot, 1);
							i++;
						}
						else
						{
							System.out.println("DEM!");
						}
					}
				}
			}
		}
	}
	
	
	
	
  /********************************************************************************************/
 /**  Added Bed Code (mostly vanilla code) *****************************************************/
/********************************************************************************************/
	
	protected boolean onUseAsBed(World world, int x, int y, int z, EntityPlayer entityPlayer, int side, float vecOffsetX, float vecOffsetY, float vecOffsetZ)
	{
		return true;
		/*
		if (world.isRemote)
		{
			return true;
		}
		else
		{
			int i1 = world.getBlockMetadata(x, y, z);
			
			if (!isBlockHeadOfBed(i1))
			{
				int j1 = getDirection(i1);
				x += footBlockToHeadBlockMap[j1][0];
				z += footBlockToHeadBlockMap[j1][1];
				
				if (world.getBlockId(x, y, z) != this.blockID)
				{
					return true;
				}
				
				i1 = world.getBlockMetadata(x, y, z);
			}
			
			if (world.provider.canRespawnHere() && world.getBiomeGenForCoords(x, z) != BiomeGenBase.hell)
			{
				if (isBedOccupied(i1))
				{
					EntityPlayer entityplayer1 = null;
					Iterator iterator = world.playerEntities.iterator();
					
					while (iterator.hasNext())
					{
						EntityPlayer entityplayer2 = (EntityPlayer)iterator.next();
						
						if (entityplayer2.isPlayerSleeping())
						{
							ChunkCoordinates chunkcoordinates = entityplayer2.playerLocation;
							
							if (chunkcoordinates.posX == x && chunkcoordinates.posY == y && chunkcoordinates.posZ == z)
							{
								entityplayer1 = entityplayer2;
							}
						}
					}
					
					if (entityplayer1 != null)
					{
						entityPlayer.addChatMessage("tile.bed.occupied");
						return true;
					}
					
					setBedOccupied(world, x, y, z, false);
				}
				
				EnumStatus enumstatus = entityPlayer.sleepInBedAt(x, y, z);
				
				if (enumstatus == EnumStatus.OK)
				{
					setBedOccupied(world, x, y, z, true);
					return true;
				}
				else
				{
					if (enumstatus == EnumStatus.NOT_POSSIBLE_NOW)
					{
						entityPlayer.addChatMessage("tile.bed.noSleep");
					}
					else if (enumstatus == EnumStatus.NOT_SAFE)
					{
						entityPlayer.addChatMessage("tile.bed.notSafe");
					}
					
					return true;
				}
			}
			else
			{
				double d0 = (double)x + 0.5D;
				double d1 = (double)y + 0.5D;
				double d2 = (double)z + 0.5D;
				world.setBlockToAir(x, y, z);
				int k1 = getDirection(i1);
				x += footBlockToHeadBlockMap[k1][0];
				z += footBlockToHeadBlockMap[k1][1];
				
				if (world.getBlockId(x, y, z) == this.blockID)
				{
					world.setBlockToAir(x, y, z);
					d0 = (d0 + (double)x + 0.5D) / 2.0D;
					d1 = (d1 + (double)y + 0.5D) / 2.0D;
					d2 = (d2 + (double)z + 0.5D) / 2.0D;
				}
				
				world.newExplosion((Entity)null, (double)((float)x + 0.5F), (double)((float)y + 0.5F), (double)((float)z + 0.5F), 5.0F, true, true);
				return true;
			}
		}*/
	}
	
	
	
	
  /********************************************************************************************/
 /**  Vanilla Chest Code  (methods removed/renamed as appropriate)   *************************/
/********************************************************************************************/
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public int getRenderType()
	{
		return renderID;
	}
	
	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess blockAccess, int xCoord, int yCoord, int zCoord)
	{
		switch (this.chestType)
		{
		case 0:
			if (Block.isEqualTo(blockAccess.getBlock(xCoord, yCoord, zCoord - 1), this))
			{
				this.setBlockBounds(0.0625F, 0.0F, 0.0F, 0.9375F, 0.875F, 0.9375F);
			}
			else if (Block.isEqualTo(blockAccess.getBlock(xCoord, yCoord, zCoord + 1), this))
			{
				this.setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 1.0F);
			}
			else if (Block.isEqualTo(blockAccess.getBlock(xCoord - 1, yCoord, zCoord), this))
			{
				this.setBlockBounds(0.0F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
			}
			else if (Block.isEqualTo(blockAccess.getBlock(xCoord + 1, yCoord, zCoord), this))
			{
				this.setBlockBounds(0.0625F, 0.0F, 0.0625F, 1.0F, 0.875F, 0.9375F);
			}
			else
			{
				this.setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F);
			}
			break;
		case 1:
			break;
		}
	}
	
	@Override
	public void onBlockAdded(World world, int xCoord, int yCoord, int zCoord)
	{
		super.onBlockAdded(world, xCoord, yCoord, zCoord);
		this.unifyAdjacentChests(world, xCoord, yCoord, zCoord);
		Block id1 = world.getBlock(xCoord, yCoord, zCoord - 1);
		Block id2 = world.getBlock(xCoord, yCoord, zCoord + 1);
		Block id3 = world.getBlock(xCoord - 1, yCoord, zCoord);
		Block id4 = world.getBlock(xCoord + 1, yCoord, zCoord);
		
		if (Block.isEqualTo(id1, this))
		{
			this.unifyAdjacentChests(world, xCoord, yCoord, zCoord - 1);
		}
		
		if (Block.isEqualTo(id2, this))
		{
			this.unifyAdjacentChests(world, xCoord, yCoord, zCoord + 1);
		}
		
		if (Block.isEqualTo(id3, this))
		{
			this.unifyAdjacentChests(world, xCoord - 1, yCoord, zCoord);
		}
		
		if (Block.isEqualTo(id4, this))
		{
			this.unifyAdjacentChests(world, xCoord + 1, yCoord, zCoord);
		}
	}
	
	@Override
	public void onBlockPlacedBy(World world, int xCoord, int yCoord, int zCoord, EntityLivingBase entityLivingBase, ItemStack itemStack)
	{
		Block id1 = world.getBlock(xCoord, yCoord, zCoord - 1);
		Block id2 = world.getBlock(xCoord, yCoord, zCoord + 1);
		Block id3 = world.getBlock(xCoord - 1, yCoord, zCoord);
		Block id4 = world.getBlock(xCoord + 1, yCoord, zCoord);
		byte meta = 0;
		int l1 = MathHelper.floor_double((double)(entityLivingBase.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		
		if (l1 == 0)
		{
			meta = 2;
		}
		
		if (l1 == 1)
		{
			meta = 5;
		}
		
		if (l1 == 2)
		{
			meta = 3;
		}
		
		if (l1 == 3)
		{
			meta = 4;
		}
		
		if (!Block.isEqualTo(id1, this) && !Block.isEqualTo(id2, this) && !Block.isEqualTo(id3, this) && !Block.isEqualTo(id4, this))
		{
			world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
		}
		else
		{
			if ((Block.isEqualTo(id1, this) || Block.isEqualTo(id2, this)) && (meta == 4 || meta == 5))
			{
				if (Block.isEqualTo(id1, this))
				{
					world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord - 1, meta, 3);
				}
				else
				{
					world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord + 1, meta, 3);
				}
				
				world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
			}
			
			if ((Block.isEqualTo(id3, this) || Block.isEqualTo(id4, this)) && (meta == 2 || meta == 3))
			{
				if (Block.isEqualTo(id3, this))
				{
					world.setBlockMetadataWithNotify(xCoord - 1, yCoord, zCoord, meta, 3);
				}
				else
				{
					world.setBlockMetadataWithNotify(xCoord + 1, yCoord, zCoord, meta, 3);
				}
				
				world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
			}
		}
		
		if (itemStack.hasDisplayName())
		{
			((TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord)).func_145976_a(itemStack.getDisplayName());
		}
	}
	
	/**
	 * Turns the adjacent chests to a double chest.
	 */
	public void unifyAdjacentChests(World world, int xCoord, int yCoord, int zCoord)
	{
		if (!world.isRemote)
		{
			Block id1 = world.getBlock(xCoord, yCoord, zCoord - 1);
			Block id2 = world.getBlock(xCoord, yCoord, zCoord + 1);
			Block id3 = world.getBlock(xCoord - 1, yCoord, zCoord);
			Block id4 = world.getBlock(xCoord + 1, yCoord, zCoord);
			Block id5;
			Block id6;
			boolean flag;
			byte meta;
			int neighborMeta;
			
			if (!Block.isEqualTo(id1, this) && !Block.isEqualTo(id2, this))
			{
				if (!Block.isEqualTo(id3, this) && !Block.isEqualTo(id4, this))
				{
					meta = 3;
					
					if (id1.isOpaqueCube() && !id2.isOpaqueCube())
					{
						meta = 3;
					}
					
					if (id2.isOpaqueCube() && !id1.isOpaqueCube())
					{
						meta = 2;
					}
					
					if (id3.isOpaqueCube() && !id4.isOpaqueCube())
					{
						meta = 5;
					}
					
					if (id4.isOpaqueCube() && !id3.isOpaqueCube())
					{
						meta = 4;
					}
				}
				else
				{
					id5 = world.getBlock(Block.isEqualTo(id3, this) ? xCoord - 1 : xCoord + 1, yCoord, zCoord - 1);
					id6 = world.getBlock(Block.isEqualTo(id3, this) ? xCoord - 1 : xCoord + 1, yCoord, zCoord + 1);
					meta = 3;
					flag = true;
					
					if (Block.isEqualTo(id3, this))
					{
						neighborMeta = world.getBlockMetadata(xCoord - 1, yCoord, zCoord);
					}
					else
					{
						neighborMeta = world.getBlockMetadata(xCoord + 1, yCoord, zCoord);
					}
					
					if (neighborMeta == 2)
					{
						meta = 2;
					}
					
					if ((id1.isOpaqueCube() || id5.isOpaqueCube()) && !id2.isOpaqueCube() && !id6.isOpaqueCube())
					{
						meta = 3;
					}
					
					if ((id2.isOpaqueCube() || id6.isOpaqueCube()) && !id1.isOpaqueCube() && !id5.isOpaqueCube())
					{
						meta = 2;
					}
				}
			}
			else
			{
				id5 = world.getBlock(xCoord - 1, yCoord, Block.isEqualTo(id1, this) ? zCoord - 1 : zCoord + 1);
				id6 = world.getBlock(xCoord + 1, yCoord, Block.isEqualTo(id1, this) ? zCoord - 1 : zCoord + 1);
				meta = 5;
				flag = true;
				
				if (Block.isEqualTo(id1, this))
				{
					neighborMeta = world.getBlockMetadata(xCoord, yCoord, zCoord - 1);
				}
				else
				{
					neighborMeta = world.getBlockMetadata(xCoord, yCoord, zCoord + 1);
				}
				
				if (neighborMeta == 4)
				{
					meta = 4;
				}
				
				if ((id3.isOpaqueCube() || id5.isOpaqueCube()) && !id4.isOpaqueCube() && !id6.isOpaqueCube())
				{
					meta = 5;
				}
				
				if ((id4.isOpaqueCube() || id6.isOpaqueCube()) && !id3.isOpaqueCube() && !id5.isOpaqueCube())
				{
					meta = 4;
				}
			}
			
			world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
		}
	}
	
	@Override
	public boolean canPlaceBlockAt(World world, int xCoord, int yCoord, int zCoord)
	{
		int count = 0;
		
		if (Block.isEqualTo(world.getBlock(xCoord - 1, yCoord, zCoord), this))
		{
			++count;
		}
		
		if (Block.isEqualTo(world.getBlock(xCoord + 1, yCoord, zCoord), this))
		{
			++count;
		}
		
		if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord - 1), this))
		{
			++count;
		}
		
		if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord + 1), this))
		{
			++count;
		}
		
		return count > 1 ? false : (this.isThereANeighborChest(world, xCoord - 1, yCoord, zCoord) ? false : (this.isThereANeighborChest(world, xCoord + 1, yCoord, zCoord) ? false : (this.isThereANeighborChest(world, xCoord, yCoord, zCoord - 1) ? false : !this.isThereANeighborChest(world, xCoord, yCoord, zCoord + 1))));
	}
	
	/**
	 * Checks the neighbor blocks to see if there is a chest there. Args: world, x, y, z
	 */
	private boolean isThereANeighborChest(World world, int xCoord, int yCoord, int zCoord)
	{
		return !Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord), this) ? false : (Block.isEqualTo(world.getBlock(xCoord - 1, yCoord, zCoord), this) ? true : (Block.isEqualTo(world.getBlock(xCoord + 1, yCoord, zCoord), this) ? true : (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord - 1), this) ? true : Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord + 1), this))));
	}
	
	@Override
	public void onNeighborBlockChange(World world, int xCoord, int yCoord, int zCoord, Block scource)
	{
		super.onNeighborBlockChange(world, xCoord, yCoord, zCoord, scource);
		TileEntityChest tileEntityChest = (TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord);
		
		if (tileEntityChest != null)
		{
			tileEntityChest.updateContainingBlockInfo();
		}
	}
	
	/**
	 * Called on server worlds only when the block has been replaced by a different block ID, or the same block with a
	 * different metadata value, but before the new metadata value is set. Args: World, x, y, z, old block ID, old
	 * metadata
	 */
	public void breakBlockChest(World world, int xCoord, int yCoord, int zCoord, Block oldBlock, int oldMeta)
	{
		TileEntityChest tileEntityChest = (TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord);
		
		if (tileEntityChest != null)
		{
			for (int index = 0; index < tileEntityChest.getSizeInventory(); ++index)
			{
				ItemStack itemStack = tileEntityChest.getStackInSlot(index);
				
				if (itemStack != null)
				{
					float xOffset = this.random.nextFloat() * 0.8F + 0.1F;
					float yOffset = this.random.nextFloat() * 0.8F + 0.1F;
					EntityItem entityItem;
					
					for (float zOffset = this.random.nextFloat() * 0.8F + 0.1F; itemStack.stackSize > 0; world.spawnEntityInWorld(entityItem))
					{
						int pieceSize = this.random.nextInt(21) + 10;
						
						if (pieceSize > itemStack.stackSize)
						{
							pieceSize = itemStack.stackSize;
						}
						
						itemStack.stackSize -= pieceSize;
						entityItem = new EntityItem(world, (double)((float)xCoord + xOffset), (double)((float)yCoord + yOffset), (double)((float)zCoord + zOffset), new ItemStack(itemStack.getItem(), pieceSize, itemStack.getItemDamage()));
						float speedCap = 0.05F;
						entityItem.motionX = (double)((float)this.random.nextGaussian() * speedCap);
						entityItem.motionY = (double)((float)this.random.nextGaussian() * speedCap + 0.2F);
						entityItem.motionZ = (double)((float)this.random.nextGaussian() * speedCap);
						
						if (itemStack.hasTagCompound())
						{
							entityItem.getEntityItem().setTagCompound((NBTTagCompound)itemStack.getTagCompound().copy());
						}
					}
				}
			}
			
			world.func_147453_f(xCoord, yCoord, zCoord, oldBlock);
		}
		
		super.breakBlock(world, xCoord, yCoord, zCoord, oldBlock, oldMeta);
	}
	
	/**
	 * Called upon block activation (right click on the block.)
	 */
	public boolean onBlockActivatedChest(World world, int xCoord, int yCoord, int zCoord, EntityPlayer entityPlayer, int side, float vecOffsetX, float vecOffsetY, float vecOffsetZ)
	{
		if (world.isRemote)
		{
			return true;
		}
		else
		{
			IInventory iinventory = this.getInventory(world, xCoord, yCoord, zCoord);
			
			if (iinventory != null)
			{
				entityPlayer.displayGUIChest(iinventory);
			}
			
			return true;
		}
	}
	
	/**
	 * Gets the inventory of the chest at the specified coords, accounting for blocks or ocelots on top of the chest,
	 * and double chests.
	 */
	public IInventory getInventory(World world, int xCoord, int yCoord, int zCoord)
	{
		Object object = (TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord);
		
		if (object == null)
		{
			return null;
		}
		else if (world.isSideSolid(xCoord, yCoord + 1, zCoord, DOWN))
		{
			return null;
		}
		else if (isOcelotBlockingChest(world, xCoord, yCoord, zCoord))
		{
			return null;
		}
		else if (Block.isEqualTo(world.getBlock(xCoord - 1, yCoord, zCoord), this) && (world.isSideSolid(xCoord - 1, yCoord + 1, zCoord, DOWN) || isOcelotBlockingChest(world, xCoord - 1, yCoord, zCoord)))
		{
			return null;
		}
		else if (Block.isEqualTo(world.getBlock(xCoord + 1, yCoord, zCoord), this) && (world.isSideSolid(xCoord + 1, yCoord + 1, zCoord, DOWN) || isOcelotBlockingChest(world, xCoord + 1, yCoord, zCoord)))
		{
			return null;
		}
		else if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord - 1), this) && (world.isSideSolid(xCoord, yCoord + 1, zCoord - 1, DOWN) || isOcelotBlockingChest(world, xCoord, yCoord, zCoord - 1)))
		{
			return null;
		}
		else if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord + 1), this) && (world.isSideSolid(xCoord, yCoord + 1, zCoord + 1, DOWN) || isOcelotBlockingChest(world, xCoord, yCoord, zCoord + 1)))
		{
			return null;
		}
		else
		{
			if (Block.isEqualTo(world.getBlock(xCoord - 1, yCoord, zCoord), this))
			{
				object = new InventoryLargeChest("container.chestDouble", (TileEntityChest)world.getTileEntity(xCoord - 1, yCoord, zCoord), (IInventory)object);
			}
			
			if (Block.isEqualTo(world.getBlock(xCoord + 1, yCoord, zCoord), this))
			{
				object = new InventoryLargeChest("container.chestDouble", (IInventory)object, (TileEntityChest)world.getTileEntity(xCoord + 1, yCoord, zCoord));
			}
			
			if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord - 1), this))
			{
				object = new InventoryLargeChest("container.chestDouble", (TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord - 1), (IInventory)object);
			}
			
			if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord + 1), this))
			{
				object = new InventoryLargeChest("container.chestDouble", (IInventory)object, (TileEntityChest)world.getTileEntity(xCoord, yCoord, zCoord + 1));
			}
			
			return (IInventory)object;
		}
	}
	
	@Override
	public int isProvidingWeakPower(IBlockAccess blockAccess, int xCoord, int yCoord, int zCoord, int direction)
	{
		if (!this.canProvidePower())
		{
			return 0;
		}
		else
		{
			int userCount = ((TileEntityChest)blockAccess.getTileEntity(xCoord, yCoord, zCoord)).numPlayersUsing;
			return MathHelper.clamp_int(userCount, 0, 15);
		}
	}
	
	@Override
	public int isProvidingStrongPower(IBlockAccess blockAccess, int xCoord, int yCoord, int zCoord, int direction)
	{
		return direction == 1 ? this.isProvidingWeakPower(blockAccess, xCoord, yCoord, zCoord, direction) : 0;
	}
	
	/**
	 * Looks for a sitting ocelot within certain bounds. Such an ocelot is considered to be blocking access to the
	 * chest.
	 */
	public static boolean isOcelotBlockingChest(World world, int xCoord, int yCoord, int zCoord)
	{
		Iterator iterator = world.getEntitiesWithinAABB(EntityOcelot.class, AxisAlignedBB.getAABBPool().getAABB((double)xCoord, (double)(yCoord + 1), (double)zCoord, (double)(xCoord + 1), (double)(yCoord + 2), (double)(zCoord + 1))).iterator();
		EntityOcelot entityocelot;
		
		do
		{
			if (!iterator.hasNext())
			{
				return false;
			}
			
			EntityOcelot entityocelot1 = (EntityOcelot)iterator.next();
			entityocelot = (EntityOcelot)entityocelot1;
		}
		while (!entityocelot.isSitting());
		
		return true;
	}
	
	@Override
	public boolean hasComparatorInputOverride()
	{
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World world, int xCoord, int yCoord, int zCoord, int direction)
	{
		return Container.calcRedstoneFromInventory(this.getInventory(world, xCoord, yCoord, zCoord));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.blockIcon = iconRegister.registerIcon("planks_oak");
	}
}
