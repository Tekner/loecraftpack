package loecraftpack.content.commands;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.referrance.enums.Race;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

public class CommandStat extends CommandBase {
	
	private List aliases;
	
	public CommandStat()
	{
		aliases = new ArrayList();
		aliases.add("stat");
	}

	@Override
	public int compareTo(Object obj) 
	{
		return this.getCommandName().compareTo(((ICommand)obj).getCommandName());
	}

	@Override
	public String getCommandName() 
	{
		return "stat";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "/stat <set/get> <stat name> <optional:username> <value>";
	}

	@Override
	public List getCommandAliases() 
	{
		return aliases;
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] string) 
	{
		if (!canCommandSenderUseCommand(icommandsender))
			throw new CommandException("You are not allowed to use that command.");
		
		if (string.length<2 || string.length>4)
		{
			throw new WrongUsageException("Invalid number of arguments", new Object[]{});
		}
		
		int mode;
		String modeS = string[0].toLowerCase();
		if (modeS.matches("get"))
		{
			if (string.length<2)
				throw new WrongUsageException("Invalid number of arguments", new Object[]{});
			mode = 0;
		}
		else if (modeS.matches("set"))
		{
			if (string.length<3)
				throw new WrongUsageException("Invalid number of arguments", new Object[]{});
			mode = 1;
		}
		else
		{
			throw new WrongUsageException("Invalid 2nd argument", new Object[]{});
		}
		
		String[] statStrings = new String[] {"1st Alignment", "2nd Alignment", "3rd Alignment", "Strength", "Magic", "Agility"};
		
		String username = "";
		Race race = null;
		EntityPlayer player = null;
		
		//get user
		if ((mode==1 && string.length==3) || (mode==0 && string.length==2))
			username = icommandsender.getCommandSenderName();
		else 
			username = string[2];
		
		Stats stats = (Stats)LoECraftPack.statHandler.stats.get(username);
		if (stats == null)
			throw new WrongUsageException("No stats data for " + username + ".");
		
		Stat stat = getStat(string[1], stats);
		if (stat == null)
			throw new WrongUsageException("Invalid stat name. Use: l (level), s (strength), m (magic), a (agility), o (offense), d (defense), or u (utility)", new Object[]{});
		
		player = LoECraftPack.getPlayerForUsername(username);
		if (player==null)
		{
			throw new WrongUsageException("Invalid username");
		}
		else if (mode == 0)
		{
			//get current race
			notifyAdmins(icommandsender, username+"'s "+stat.getName()+" is: "+stat.getValue());
		}
		else
		{
			try
			{
				int value = Integer.parseInt(string[string.length-1]);
				//set stat
				stat.setValue(value);
				stats.abilityData.fixRaceStats();
				
				//notifyAdmins(icommandsender, "commands.stat.race.set", new Object[]{username+"'s race is set to: "+LoECraftPack.statHandler.getRace(player)});
				notifyAdmins(icommandsender, username+"'s "+stat.getName()+" is set to: "+value, new Object[]{});
			}
			catch(Exception e)
			{
				throw new WrongUsageException("Invalid value. Must be an integer.");
			}
		}
	}
	
	private Stat getStat(String stat, Stats stats)
	{
		if ("level".startsWith(stat))
			return stats.ponyLevel;
		else if ("strength".startsWith(stat))
			return stats.strength;
		else if ("magic".startsWith(stat))
			return stats.magic;
		else if ("agility".startsWith(stat))
			return stats.agility;
		else if ("offense".startsWith(stat))
			return stats.offense;
		else if ("defense".startsWith(stat))
			return stats.defense;
		else if ("utility".startsWith(stat))
			return stats.utility;
		else
			return null;
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
		if (!LoECraftPack.isSinglePlayer() && (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER && !icommandsender.getCommandSenderName().equals("Server") && !LoECraftPack.isPlayerOP((EntityPlayer) icommandsender)))
			return false;
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender icommandsender,
			String[] string) 
	{
		List tab = new ArrayList();
		switch(string.length)
		{
		case 0:
			return null;
			
		case 1:
			String part = string[0].toLowerCase();
			if (part.matches(""))
			{
				tab.add("Set");
				tab.add("Get");
			}
			else if ("set".startsWith(part))
				tab.add("Set");
			else if ("get".startsWith(part))
				tab.add("Get");
			break;
			
		case 2:
			String function = string[0].toLowerCase();
			part = string[1].toLowerCase();
			if (function.matches("get") || function.matches("set"))
			{
				String[] users = MinecraftServer.getServer().getConfigurationManager().getAllUsernames();
				if (part.matches(""))
				{
					tab.add("level");
					tab.add("strength");
					tab.add("magic");
					tab.add("agility");
					tab.add("offense");
					tab.add("defense");
					tab.add("utility");
				}
				else
				{
					if (part.startsWith("l"))
						tab.add("level");
					else if (part.startsWith("s"))
						tab.add("strength");
					else if (part.startsWith("m"))
						tab.add("magic");
					else if (part.startsWith("a"))
						tab.add("agility");
					else if (part.startsWith("o"))
						tab.add("offense");
					else if (part.startsWith("d"))
						tab.add("defense");
					else if (part.startsWith("u"))
						tab.add("utility");
				}
			}
			break;
			
		default:
			return null;
		}
		if(tab.size()>0)
			return tab;
		else
			return null;
	}

	@Override
	public boolean isUsernameIndex(String[] string, int i) {
		if (i==1)
		{
			if (string[0].toLowerCase().matches("get"))
				return true;
			else if (string[0].toLowerCase().matches("set"))
			{
				if (string.length > 2)
					return true;
			}
		}
		return false;
	}

}
