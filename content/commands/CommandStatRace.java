package loecraftpack.content.commands;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.referrance.enums.Race;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

public class CommandStatRace extends CommandBase {
	
	private List aliases;
	
	public CommandStatRace()
	{
		aliases = new ArrayList();
		aliases.add("race");
	}

	@Override
	public int compareTo(Object obj) 
	{
		return this.getCommandName().compareTo(((ICommand)obj).getCommandName());
	}

	@Override
	public String getCommandName() 
	{
		return "race";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "/race <set/get> <username:optional> <race:set>";
	}

	@Override
	public List getCommandAliases() 
	{
		return aliases;
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] string) 
	{/*
		if (!canCommandSenderUseCommand(icommandsender))
			throw new CommandException("You are not allowed to use that command.");*/
		
		if (string.length<1 || string.length>3)
		{
			//throw new WrongUsageException("commands.stat.race.usage", new Object[]{"Invalid number of arguments"});
			throw new WrongUsageException("Invalid number of arguments", new Object[]{});
		}
		
		int mode;
		String modeS = string[0].toLowerCase();
		System.out.println(modeS);
		if (modeS.matches("get"))
			mode = 0;
		else if (modeS.matches("set"))
		{
			if (string.length<2)
			{
				//throw new WrongUsageException("commands.stat.race.usage", new Object[]{"Invalid number of arguments"});
				throw new WrongUsageException("Invalid number of arguments", new Object[]{});
			}
			mode = 1;
		}
		else
		{
			//throw new WrongUsageException("commands.stat.race.usage", new Object[]{"Invalid 2nd argument"});
			throw new WrongUsageException("Invalid 2nd argument", new Object[]{});
		}
		
		
		String username = "";
		Race race = null;
		EntityPlayer player = null;
		
		//get user
		if ( (mode==1 && string.length==2) || (mode==0 && string.length==1))
			username = icommandsender.getCommandSenderName();
		else 
			username = string[1];
		player = LoECraftPack.getPlayerForUsername(username);
		if (player==null)
		{
			//throw new WrongUsageException("commands.stat.race.usage", new Object[]{"Invalid number of arguments"});
			throw new WrongUsageException("Invalid username", new Object[]{});
		}
		else if (mode == 0)
		{
			//get current race
			//notifyAdmins(icommandsender, "commands.stat.race.get", new Object[]{username+"'s race is: "+LoECraftPack.statHandler.getRace(player)});
			notifyAdmins(icommandsender, username+"'s race is: "+LoECraftPack.statHandler.getRace(player), new Object[]{});
		}
		else
		{
			//set new race
			String goal = string[string.length-1].toLowerCase();
			if (goal.equals("n"))
				race = Race.NONE;
			else if (goal.equals("u"))
				race = Race.UNICORN;
			else if (goal.equals("e"))
				race = Race.EARTH;
			else if (goal.equals("p"))
				race = Race.PEGASUS;
			else if (goal.equals("a"))
				race = Race.ALICORN;
			else
			{
				float matchPer = 0;
				for(int ir = 0; ir < Race.values().length; ir++)
				{
					Race r = Race.values()[ir];
					float matches = 0, total = 0;
					String name = r.toString().toLowerCase();
					skip:
					for(int i = 0; i < name.length(); i++)
					{
						for(int ii = i + 1; ii <= name.length(); ii++)
						{
							total++;
							String test = name.substring(i, ii);
							if (goal.equals(test))
							{
								race = r;
								ir = Race.values().length;
								i = name.length();
								break skip;
							}
							else if (goal.contains(test))
								matches++;
						}
					}
					float _mp = matches / total;
					if (_mp > matchPer)
					{
						race = r;
						matchPer = _mp;
					}
				}
			}
			
			if (race==null)
			{
				//icommandsender.sendChatToPlayer("Invalid race type");
				//throw new WrongUsageException("commands.stat.race.usage", new Object[]{"Invalid race type"});
				throw new WrongUsageException("Invalid race type", new Object[]{});
			}
			else
			{
				//set race
				LoECraftPack.statHandler.setRace(player, race);
				//notifyAdmins(icommandsender, "commands.stat.race.set", new Object[]{username+"'s race is set to: "+LoECraftPack.statHandler.getRace(player)});
				notifyAdmins(icommandsender, username+"'s race is set to: "+LoECraftPack.statHandler.getRace(player), new Object[]{});
			}
		}
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
		if (!LoECraftPack.isSinglePlayer() && (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER && !icommandsender.getCommandSenderName().equals("Server") && !LoECraftPack.isPlayerOP((EntityPlayer) icommandsender)))
			return false;
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender icommandsender,
			String[] string) 
	{
		List tab = new ArrayList();
		switch(string.length)
		{
		case 0:
			return null;
		case 1:
			String part = string[0].toLowerCase();
			if (part.matches(""))
			{
				tab.add("Set");
				tab.add("Get");
			}
			else if ("set".startsWith(part))
				tab.add("Set");
			else if ("get".startsWith(part))
				tab.add("Get");
			break;
		case 2:
			String function = string[0].toLowerCase();
			part = string[1].toLowerCase();
			if (function.matches("get") || function.matches("set"))
			{
				String[] users = MinecraftServer.getServer().getConfigurationManager().getAllUsernames();
				if (part.matches(""))
				{
					if (function.matches("get"))
					{
						tab.add("");
					}
					else
					{
						tab.add("None");
						tab.add("Earth");
						tab.add("Pegasus");
						tab.add("Unicorn");
						tab.add("Alicorn");
					}
					for (int i=0; i<users.length; i++)
						tab.add(users[i]);
				}
				else
				{
					if (function.matches("set"))
					{
						if ("none".startsWith(part))
							tab.add("None");
						else if ("earth".startsWith(part))
							tab.add("Earth");
						else if ("pegasus".startsWith(part))
							tab.add("Pegasus");
						else if ("unicorn".startsWith(part))
							tab.add("Unicorn");
						else if ("alicorn".startsWith(part))
							tab.add("Alicorn");
					}
					for (int i=0; i<users.length; i++)
						if (users[i].toLowerCase().startsWith(part))
							tab.add(users[i]);
				}
			}
			break;
		case 3:
			function = string[0].toLowerCase();
			if (!function.matches("set"))
				return null;
			part = string[2].toLowerCase();
			if (part.matches(""))
			{
				tab.add("None");
				tab.add("Earth");
				tab.add("Pegasus");
				tab.add("Unicorn");
				tab.add("Alicorn");
			}
			else
			{
				if ("none".startsWith(part))
					tab.add("None");
				else if ("earth".startsWith(part))
					tab.add("Earth");
				else if ("pegasus".startsWith(part))
					tab.add("Pegasus");
				else if ("unicorn".startsWith(part))
					tab.add("Unicorn");
				else if ("alicorn".startsWith(part))
					tab.add("Alicorn");
			}
			break;
		default:
			return null;
		}
		if(tab.size()>0)
			return tab;
		else
			return null;
	}

	@Override
	public boolean isUsernameIndex(String[] string, int i) {
		if (i==1)
		{
			if (string[0].toLowerCase().matches("get"))
				return true;
			else if (string[0].toLowerCase().matches("set"))
			{
				if (string.length > 2)
					return true;
			}
		}
		return false;
	}

}
