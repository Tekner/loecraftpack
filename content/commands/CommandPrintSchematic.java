package loecraftpack.content.commands;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

public class CommandPrintSchematic extends CommandBase {
	
	private List aliases;
	
	public CommandPrintSchematic()
	{
		aliases = new ArrayList();
		aliases.add("Print_LOE");
	}

	@Override
	public int compareTo(Object obj) 
	{
		return this.getCommandName().compareTo(((ICommand)obj).getCommandName());
	}

	@Override
	public String getCommandName() 
	{
		return "Print_LOE";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "/Print_LOE [x] [y] [z] [filename] <-Air>";
	}

	@Override
	public List getCommandAliases() 
	{
		return aliases;
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] string) 
	{/*
		if (!canCommandSenderUseCommand(icommandsender))
			throw new CommandException("You are not allowed to use that command.");*/
		
		if (string.length >= 4)
		{
			int x = Integer.valueOf(string[0]);
			int y = Integer.valueOf(string[1]);
			int z = Integer.valueOf(string[2]);
			NBTSchematic build = new NBTSchematic(string[3]+".schematic");
			
			if (string.length >= 5)
			{
				if (string[4].matches("-Air"))
					build.setIgnoreAir(true);
			}
			
			build.printSchematicDEBUG();
			build.spawnNBTSchematic(icommandsender.getEntityWorld(), x, y, z);
		}
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
		if (!LoECraftPack.isSinglePlayer() && (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER && !icommandsender.getCommandSenderName().equals("Server") && !LoECraftPack.isPlayerOP((EntityPlayer) icommandsender)))
			return false;
		return true;
	}

}
