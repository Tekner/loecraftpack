package loecraftpack.content.potions;

import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;

public class PotionEnergyRegen extends Potion {
	
	/*bad: FALSE*/
	public PotionEnergyRegen(int potionID, boolean bad, int color)
	{
		super(potionID, bad, color);
	}
	
	@Override
	public void performEffect(EntityLivingBase entityLiving, int level)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)entityLiving;
			AbilityPlayerData abilityData = AbilityPlayerData.Get(player.getGameProfile().getId());
			
			if (player.worldObj.isRemote)
				abilityData.restoreOrDrainEnergy(0.3125f*(level+1));
			else
				abilityData.addEnergy(0.3125f*(level+1), false);
		}
	}
	
	@Override
	public boolean isUsable()
	{
		return true;
	}
	
	//restore every sec
	@Override
	public boolean isReady(int par1, int par2)
    {
        return par1 > 0 && par1 % 5 == 0;
    }
	
	@Override
	public PotionEnergyRegen setIconIndex(int par1, int par2)
	{
		return (PotionEnergyRegen)super.setIconIndex(par1, par2);
	}
}
