package loecraftpack.content.potions;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;

/** 
 * This effect causes players to randomly take tiny damage, unless it's raining and they are outside; 
 * to which they randomly get hit by lightning
 */
public class PotionCharged extends Potion {

	/*bad: TRUE*/
	public PotionCharged(int potionID, int color)
	{
		super(potionID, true, color);
	}
	
	@Override
	public void performEffect(EntityLivingBase entityLiving, int level)
	{
		World world = entityLiving.worldObj;
		if (!world.isRemote && world.rand.nextFloat() < ((float)(level+1)/20.0f))
			world.addWeatherEffect(new EntityLightningBolt(world, entityLiving.posX, entityLiving.posY, entityLiving.posZ));
	}
	
	@Override
	public boolean isReady(int par1, int par2)
	{
		int k;
		k = 40;
		return k > 0 ? par1 % k == 0 : true;
	}
	
	@Override
	public PotionCharged setIconIndex(int par1, int par2)
	{
		return (PotionCharged)super.setIconIndex(par1, par2);
	}

}