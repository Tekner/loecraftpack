package loecraftpack.content.potions;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;

public class PotionWellFed extends Potion
{
	public PotionWellFed(int potionID)
	{
		super(potionID, false, 16752762);
	}
	
	@Override
	public void performEffect(EntityLivingBase entityLiving, int level)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)entityLiving;
			player.getFoodStats().addStats(level+1, 1.2f);
		}
	}
	
	@Override
	public boolean isUsable()
	{
		return true;
	}
	
	@Override
	public boolean isReady(int par1, int par2)
    {
        return par1 > 0 && par1 % (int)(600f / (10 + par2)) == 0;
    }
	
	@Override
	public PotionWellFed setIconIndex(int par1, int par2)
	{
		return (PotionWellFed)super.setIconIndex(par1, par2);
	}
}
