package loecraftpack.content.potions;

import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.BaseAttributeMap;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;

public class PotionPetrified extends Potion {
	
	/**half Speed**/protected static final AttributeModifier petrifyWeak = new AttributeModifier(UUID.randomUUID(), "Petrified (Weak)", -0.5d, 2).setSaved(false);
	/**Zero Speed**/protected static final AttributeModifier petrifyStrong = new AttributeModifier(UUID.randomUUID(), "Petrified (Strong)", -1.0d, 2).setSaved(false);
	
	public PotionPetrified(int potionID, int color)
	{
		super(potionID, true, color);
	}
	
	@Override
	public void performEffect(EntityLivingBase entityLiving, int level)
	{
		IAttributeInstance moveStat = entityLiving.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
		if (level == 0)
		{
			if (moveStat.getModifier(petrifyStrong.getID())!=null)
				moveStat.removeModifier(petrifyStrong);
			
			if (moveStat.getModifier(petrifyWeak.getID())==null)
				moveStat.applyModifier(petrifyWeak);
		}
		else
		{
			if (moveStat.getModifier(petrifyWeak.getID())!=null)
				moveStat.removeModifier(petrifyWeak);
			
			if (moveStat.getModifier(petrifyStrong.getID())==null)
				moveStat.applyModifier(petrifyStrong);
		}
	}
	
	public void removeAttributesModifiersFromEntity(EntityLivingBase entityLiving, BaseAttributeMap par2BaseAttributeMap, int par3)
	{
		IAttributeInstance moveStat = entityLiving.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
		moveStat.removeModifier(petrifyWeak);
		moveStat.removeModifier(petrifyStrong);
	}
	
	@Override
	public boolean isReady(int par1, int par2)
	{
		int k = 5;
		return k > 0 ? par1 % k == 0 : true;
	}
	
	@Override
	public PotionPetrified setIconIndex(int par1, int par2)
	{
		return (PotionPetrified)super.setIconIndex(par1, par2);
	}
}
