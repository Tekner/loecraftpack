package loecraftpack.content.potions.custombrewing;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.gui.tooltip.ComponentBase;
import loecraftpack.content.gui.tooltip.ComponentString;
import loecraftpack.content.potions.custombrewing.list.CustomPotionRecipes;
import loecraftpack.content.potions.custombrewing.list.DrinkRecipes;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public abstract class CustomBrewingRecipe
{
	protected static List<CustomBrewingRecipe> masterBrewingList = new ArrayList<CustomBrewingRecipe>();
	protected static int nextID = 0;
	public final int id;
	public final String name;
	protected List<ComponentBase> tooltip;
	
	protected final int maxParallel;
	protected ItemStack[] products;
	protected Requirement[] ingredients;
	protected Requirement[] catalysts = new Requirement[0];
	
	public CustomBrewingRecipe(String name, int maxParallel)
	{
		id = nextID++;
		this.name = name;
		this.maxParallel = maxParallel;
		masterBrewingList.add(this);
		System.out.println("-----ADDED recipe "+id+" "+name);
	}
	
	protected void init(){}
	
	protected void initClient()
	{
		generateToolTip();
	}
	
	public static CustomBrewingRecipe getRecipe(int id)
	{
		if (id > masterBrewingList.size() || id <0)
			return null;
		return masterBrewingList.get(id);
	}
	
	public static CustomBrewingRecipe findRecipe(ItemStack[] ingredients)
	{
		return null;
	}
	
	public ItemStack[] getProducts()
	{
		return products.clone();
	}
	
	public Requirement[] getIngredients()
	{
		return ingredients.clone();
	}
	
	public Requirement[] getCatalysts()
	{
		return catalysts.clone();
	}
	
	
	
	public abstract int brewingTime();
	
	/**determines if the recipe can be used with the listed ingredients**/
	public boolean isValidRecipe(ItemStack[] ingredientsToTest)
	{
		//check ingredients
		for (int index=0; index<this.ingredients.length; index++)
		{
			if (this.ingredients[index] == null)
				continue;
			int count = 0;
			
			for (int slot=0; slot<ingredientsToTest.length; slot++)
			{
				if (ingredientsToTest[slot] == null)
					continue;
				if (this.ingredients[index].isValid(ingredientsToTest[slot]))
						count += ingredientsToTest[slot].stackSize;
			}
			if (count < this.ingredients[index].amount)
				return false;
		}
		
		//check catalysts
		for (int index=0; index<this.catalysts.length; index++)
		{
			if (this.catalysts[index] == null)
				continue;
			int count = 0;
			
			for (int slot=0; slot<ingredientsToTest.length; slot++)
			{
				if (ingredientsToTest[slot] == null)
					continue;
				if (this.catalysts[index].isValid(ingredientsToTest[slot]))
						count += ingredientsToTest[slot].stackSize;
			}
			if (count < this.catalysts[index].amount)
				return false;
		}
		
		return true;
	}
	
	/**create the potion from the ingredients**/
	public ItemStack[] processResult(ItemStack[] ingredientsToProcess)
	{
		int[] countI = new int[ingredients.length];
		int[] countC = new int[catalysts.length];
		int numberMade = maxParallel;
		int amount;
		
		//identify resource capacity
		for (int p=0; p<ingredientsToProcess.length; p++)
		{
			if (ingredientsToProcess[p]!=null)
			{
				for (int i=0; i<ingredients.length; i++)
				{
					if (ingredients[i]!=null)
					{
						if (ingredients[i].isValid(ingredientsToProcess[p]))
						{
							countI[i] = Math.min(maxParallel*ingredients[i].amount, countI[i] + ingredientsToProcess[p].stackSize);
						}
					}
				}
				
				for (int i=0; i<catalysts.length; i++)
				{
					if (catalysts[i]!=null)
					{
						if (catalysts[i].isValid(ingredientsToProcess[p]))
						{
							countC[i] = Math.min(maxParallel*catalysts[i].amount, countC[i] + ingredientsToProcess[p].stackSize);
						}
					}
				}
			}
		}
		
		//calculate amount
		for (int i=0; i<ingredients.length && numberMade>0; i++)
		{
			if (ingredients[i]!=null)
			{
				numberMade = Math.min(countI[i]/ingredients[i].amount, numberMade);
			}
		}
		for (int i=0; i<catalysts.length; i++)
		{
			if (catalysts[i]!=null)
			{
				numberMade = Math.min(countC[i]/catalysts[i].amount, numberMade);
			}
		}
		for (int i=0; i<ingredients.length; i++)
		{
			if (ingredients[i]!=null)
			{
				countI[i] = numberMade*ingredients[i].amount;
			}
		}
		
		if (numberMade > 0)
		{
			ItemStack[] result = null;
			
			//use up items
			for (int p=0; p<ingredientsToProcess.length; p++)
			{
				if (ingredientsToProcess[p]!=null)
				{
					for (int i=0; i<ingredients.length; i++)
					{
						if (ingredients[i]!=null && countI[i]>0)
						{
							if (ingredients[i].isValid(ingredientsToProcess[p]))
							{
								amount = Math.min(ingredientsToProcess[p].stackSize, countI[i]);
								countI[i] -= amount;
								ingredientsToProcess[p].stackSize -= amount;
							}
						}
					}
				}
			}
			
			//and create result
			result = new ItemStack[products.length];
			for (int i=0; i<products.length; i++)
			{
				result[i] = ItemStack.copyItemStack(products[i]);
				result[i].stackSize *= numberMade;
			}
			
			return result;
		}
		
		return null;
	}
	
	/**the tooltip that displayes the recipe information**/
	protected void generateToolTip()
	{
		Minecraft mc = Minecraft.getMinecraft();
		List<ComponentBase> components = new ArrayList<ComponentBase>();
		
		int stepX = 12;
		int stepY = 2;
		
		int sub = 10;
		
		components.add(new ComponentString(mc.fontRenderer, stepX,  stepY, EnumChatFormatting.BLUE+name));
		
		stepY += 1+mc.fontRenderer.FONT_HEIGHT;
		BrewingHelper.addStandardRecipeList(components, mc.fontRenderer, stepX,  stepY, products, ingredients, catalysts);
		
		//add border
		tooltip = BrewingHelper.standardTooltipBorder(components);
	}
	
	
	
	
	
	public List<ComponentBase> getTooltip()
	{
		return tooltip;
	}
	
	public static int size()
	{
		return masterBrewingList.size();
	}
	
	static
	{
		//load custom brewing recipes
		DrinkRecipes.load();
		CustomPotionRecipes.load();
		
		//init recipes
		for (CustomBrewingRecipe recipe : masterBrewingList)
		{
			recipe.init();
			
			if (LoECraftPack.proxy.isClient())
			{
				recipe.initClient();
			}
		}
	}
}
