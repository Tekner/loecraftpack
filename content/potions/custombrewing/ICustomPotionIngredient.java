package loecraftpack.content.potions.custombrewing;

import net.minecraft.item.ItemStack;

public interface ICustomPotionIngredient {
	public abstract boolean validIngredient(ItemStack stack);
}
