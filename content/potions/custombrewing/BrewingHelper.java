package loecraftpack.content.potions.custombrewing;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.gui.tooltip.ComponentBase;
import loecraftpack.content.gui.tooltip.ComponentBuffer;
import loecraftpack.content.gui.tooltip.ComponentCycle;
import loecraftpack.content.gui.tooltip.ComponentIcon;
import loecraftpack.content.gui.tooltip.ComponentSet;
import loecraftpack.content.gui.tooltip.ComponentString;
import loecraftpack.content.gui.tooltip.ComponentTexture;
import loecraftpack.content.gui.tooltip.TooltipHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

/**Big old fat list of relevant static methods**/
public class BrewingHelper {
	
	protected static final ResourceLocation brewStationImage = new ResourceLocation("loecraftpack:gui/brewing_station.png");
	
	protected static int iconSize = 12;
	protected static int iconSpacingX = 16;
	protected static int iconSpacingY;
	protected static int numOffset = 10;
	protected static int arrowSize = 8;
	
	/**build a basic recipe tooltip**/
	public static void addStandardRecipeList(List<ComponentBase> components, FontRenderer font, int posX, int posY, ItemStack[] products, Requirement[] Ingredients, Requirement[] catalysts)
	{
		iconSpacingY = numOffset+font.FONT_HEIGHT+2;
		int arrowPosX;
		int arrowPosY2 = posY+2+arrowSize;
		int catalystPosY3;
		
		arrowPosX = addStandardRecipeList(components, font, posX, posY, products)[0];
		
		components.add(new ComponentTexture(brewStationImage, 240, 161, 16, 16, 256, 256, arrowPosX, posY+2, arrowSize, arrowSize));
		
		catalystPosY3 = addStandardRecipeList(components, font, arrowPosX + arrowSize, posY, Ingredients)[1];
		
		if (catalysts!= null && catalysts.length>0)
		{
			components.add(new ComponentTexture(brewStationImage, 240, 177, 16, 16, 256, 256, arrowPosX, arrowPosY2, arrowSize, catalystPosY3-arrowPosY2));
			components.add(new ComponentTexture(brewStationImage, 240, 193, 16, 16, 256, 256, arrowPosX, catalystPosY3, arrowSize, arrowSize));
			
			addStandardRecipeList(components, font, arrowPosX+arrowSize, catalystPosY3, catalysts);
		}
	}
	
	/**sub method**/
	protected static int[] addStandardRecipeList(List<ComponentBase> components, FontRenderer font, int posX, int posY, ItemStack[] stacks)
	{	
		int[] result = new int[]{posX+iconSpacingX*stacks.length, posY+iconSpacingY};
		
		for (int i=0; i<stacks.length; i++)
		{
			Item item = stacks[i].getItem();
			int meta = stacks[i].getItemDamage();
			int quantity = stacks[i].stackSize;
			
			addTooltipRecipeItem(components, font, posX, posY, iconSize, numOffset, ""+EnumChatFormatting.GREEN+quantity, item, meta);
			
			posX += iconSpacingX;
		}
		return result;
	}
	
	/**sub method**/
	protected static int[] addStandardRecipeList(List<ComponentBase> components, FontRenderer font, int posX, int posY, Requirement[] stacks)
	{
		int[] result = new int[]{posX+iconSpacingX*stacks.length, posY+iconSpacingY};
		
		for (int i=0; i<stacks.length; i++)
		{
			Item item = stacks[i].item;
			int[] meta = stacks[i].metaRange;
			if (meta == null) meta = new int[]{0};
			int quantity = stacks[i].amount;
			
			addTooltipRecipeItem(components, font, posX, posY, iconSize, numOffset, ""+EnumChatFormatting.GREEN+quantity, item, meta);
			
			posX += iconSpacingX;
		}
		
		return result;
	}
	
	/**basic recipe tooltip piece**/
	public static void addTooltipRecipeItem(List<ComponentBase> components, FontRenderer font, 
									 int posX, int posY, int iconSize, int numOffset, String quantity,
									 Item item, int... meta)
	{
		if (meta.length > 1)
		{
			List<ComponentBase> sublist = new ArrayList<ComponentBase>();
			for (int metaID: meta)
			{
				if (item instanceof ItemPotion)
				{
					List<ComponentBase> sublist2 = new ArrayList<ComponentBase>();
					sublist2.add(new ComponentIcon(item.getIconFromDamageForRenderPass(metaID, 0), posX, posY, iconSize, iconSize, ((ItemPotion)item).getColorFromDamage(metaID)));
					sublist2.add(new ComponentIcon(item.getIconFromDamageForRenderPass(metaID, 1), posX, posY, iconSize, iconSize));
					sublist.add(new ComponentSet(sublist2));
				}/*
				else if (item instanceof ItemEnchantedBook)
				{
					List<ComponentBase> sublist2 = new ArrayList<ComponentBase>();
					sublist2.add(new ComponentIcon(item.getIconFromDamage(metaID), posX, posY, iconSize, iconSize));
					sublist.add(new ComponentGlint(sublist2));
				}*/
				else
					sublist.add(new ComponentIcon(item.getIconFromDamage(metaID), posX, posY, iconSize, iconSize));
			}
			components.add(new ComponentCycle(sublist, 1000));
		}
		else
		{
			if (item instanceof ItemPotion)
			{
				List<ComponentBase> sublist = new ArrayList<ComponentBase>();
				sublist.add(new ComponentIcon(item.getIconFromDamageForRenderPass(meta[0], 0), posX, posY, iconSize, iconSize, ((ItemPotion)item).getColorFromDamage(meta[0])));
				sublist.add(new ComponentIcon(item.getIconFromDamageForRenderPass(meta[0], 1), posX, posY, iconSize, iconSize));
				components.add(new ComponentSet(sublist));
			}/*
			else if (item instanceof ItemEnchantedBook)
			{
				List<ComponentBase> sublist = new ArrayList<ComponentBase>();
				sublist.add(new ComponentIcon(item.getIconFromDamage(meta[0]), posX, posY, iconSize, iconSize));
				components.add(new ComponentGlint(sublist));
			}*/
			else
				components.add(new ComponentIcon(item.getIconFromDamage(meta[0]), posX, posY, iconSize, iconSize));
		}
		components.add(new ComponentString(font, posX+numOffset,  posY+numOffset, quantity));
	}
	
	public static List<ComponentBase> standardTooltipBorder(List<ComponentBase> components)
	{
		//add spacing between border and contents
		int[] bounds = TooltipHandler.detectDimesions(components);
		if (bounds == null)
			return components;
		int posX = bounds[0]-1;
		int posY = bounds[1]-1;
		int width = (bounds[2]+3) - posX;
		int height = (bounds[3]+1) - posY;
		components.add(new ComponentBuffer(posX, posY, width, height));
		
		//add border
		return TooltipHandler.generateAndAddBorderAndBackgroundTexture(components, brewStationImage, 224, 209, 32, 32, 256, 256, 4, 4, 2, 2);
	}
	
	public static int[] readEnchanmentFromBook(ItemStack stack)
	{
		NBTTagList data = Items.enchanted_book.func_92110_g(stack);
		int[] result = new int[data.tagCount()*2];
		
		for (int i = 0; i < data.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound = (NBTTagCompound)data.getCompoundTagAt(i);
			result [i*2] = nbttagcompound.getShort("id");
			result [i*2+1] = nbttagcompound.getShort("lvl");
		}
		
		return result;
	}

}