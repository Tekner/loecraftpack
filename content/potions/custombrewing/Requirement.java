package loecraftpack.content.potions.custombrewing;


import java.util.ArrayList;
import java.util.List;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemEnchantedBook;
import net.minecraft.item.ItemStack;

public class Requirement {

	public final Item item;
	public final int amount;
	public final int[] metaRange;
	protected Enchantment[] enchant;
	protected int level;
	protected ItemStack[] stackform = null;

	public Requirement(Item item, int ammount)
	{
		this(item, ammount, null);
	}
	
	public Requirement(Item item, int ammount, int meta)
	{
		this(item, ammount, new int[]{meta});
	}
	
	public Requirement(Item item, int ammount, int[] meta)
	{
		this.item = item;
		this.amount = ammount;
		this.metaRange = meta;
	}
	
	public Requirement addEnchantment(int requiredLevel, Enchantment... enchant)
	{
		stackform = null;
		this.enchant = enchant;
		this.level = requiredLevel;
		return this;
	}

	public boolean isValid(ItemStack stack)
	{
		if (stack != null)
		{
			if (item != null)
			{
				if (stack.getItem() != item)
					return false;
				
				if (metaRange != null)
				{
					boolean notValid = true;
					for (int meta: metaRange)
						if (meta == stack.getItemDamage())
						{
							notValid = false;
							break;
						}
					if (notValid)
						return false;
				}
			}
			
			if (enchant != null)
			{
				boolean notValid = true;
				if (stack.getItem() instanceof ItemEnchantedBook)
				{
					int[] data = BrewingHelper.readEnchanmentFromBook(stack);
					for (int e=0; e<enchant.length; e++)
					{
						for (int i=0; i<data.length-1; i+=2)
						{
							if (data[i] == enchant[e].effectId && data[i+1] >= level)
							{
								notValid = false;
								break;
							}
						}
						if (!notValid) break;
					}
					
				}
				else
				{
					for (int e=0; e<enchant.length; e++)
					{
						int level = EnchantmentHelper.getEnchantmentLevel(enchant[e].effectId, stack);
						if (level >= this.level)
						{
							notValid = false;
							break;
						}
					}
				}
				if (notValid)
					return false;
			}
			return true;
		}
		return false;
	}
	
	public ItemStack[] toItemStack()
	{
		if (stackform != null)
			return stackform;
		
		if (metaRange == null)
		{
			if (item.getHasSubtypes())
			{
				//wild card meta value
				List<Item> temp = new ArrayList<Item>();
				item.getSubItems(item, null, temp);
				stackform = temp.toArray(new ItemStack[temp.size()]);
				for (ItemStack stack: stackform)
				{
					stack.stackSize = amount;
				}
			}
		}
		else
		{
			if (metaRange.length > 1)
			{
				//multi-specific meta value
				stackform = new ItemStack[metaRange.length];
				for (int i=0; i<metaRange.length; i++)
				{
					stackform[i] = new ItemStack(item, amount, metaRange[i]);
				}
			}
			else if (metaRange.length == 1)
			{
				//single meta value
				stackform = new ItemStack[1];
				stackform[0] = new ItemStack(item, amount, metaRange[0]);
			}
		}
		if (stackform == null)
		{
			//no meta value
			stackform = new ItemStack[1];
			stackform[0] = new ItemStack(item, amount, 0);
		}
		if (enchant != null)
		{
			int index;
			ItemStack[] unEnchanted = stackform.clone();
			stackform = new ItemStack[stackform.length * enchant.length];
			for (int i1=0; i1<enchant.length; i1++)
			{
				for (int i2=0; i2<unEnchanted.length; i2++)
				{
					index = i1*unEnchanted.length+i2;
					stackform[index] = unEnchanted[i2].copy();
					stackform[index].addEnchantment(enchant[i1], level);
				}
			}
		}
		
		return stackform;
	}
}
