package loecraftpack.content.potions.custombrewing.list;

import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import loecraftpack.content.potions.custombrewing.Requirement;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class DrinkRecipes extends CustomBrewingRecipe
{
	enum Type{
		DIET,
		APPLE,
		FUSION,
		MAX,
		CIDER,
		DRAGON
	}
	
	static String[] names = {"Zero-energy",
							 "Apple Power",
							 "Magic Fusion",
							 "Rainbow Max!!!",
							 "Sweet Apple Cider",
							 "Dragon Drought"};
	
	static int [] parallel = {3, 3, 3, 3, 1, 1};
	
	static int [] time = {400, 400, 600, 600, 1200, 1200};
	
	Type type;
	
	DrinkRecipes(Type type)
	{
		super(names[type.ordinal()], parallel[type.ordinal()]);
		this.type = type;
		switch(type)
		{
		case DIET:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 1, 0)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 1, 0), new Requirement(Items.dye, 1, 15)};
			break;
			
		case APPLE:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 1, 1)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 1, 0), new Requirement(Items.apple, 1), new Requirement(Items.sugar, 1)};
			break;
			
		case FUSION:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 1, 2)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 1, 0), new Requirement(LOE_Items.ingredients, 1, 3), new Requirement(Items.sugar, 1)};
			break;
			
		case MAX:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 1, 3)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 1, 0), new Requirement(LOE_Items.ingredients, 1, 4), new Requirement(Items.sugar, 3)};
			break;
			
		case CIDER:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 3, 4)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 3, 0), new Requirement(Items.apple, 3), new Requirement(Items.sugar, 2), new Requirement(Items.dye, 1, 15)};
			break;
			
		case DRAGON:
			products = new ItemStack[]{new ItemStack(LOE_Items.drink, 1, 8)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 1, 0), new Requirement(LOE_Items.ingredients, 2, 6), new Requirement(Items.dye, 1, 15)};
			break;
		}
	}

	@Override
	public int brewingTime() {
		return time[type.ordinal()];
	}
	
	public static void load()
	{
		for (Type type :Type.values())
			new DrinkRecipes(type);
	}
}
