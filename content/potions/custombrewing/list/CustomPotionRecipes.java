package loecraftpack.content.potions.custombrewing.list;

import loecraftpack.content.potions.custombrewing.CustomBrewingRecipe;
import loecraftpack.content.potions.custombrewing.Requirement;
import loecraftpack.content.registery.LOE_Enchantment;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CustomPotionRecipes extends CustomBrewingRecipe
{
	enum Type
	{
		ELECTRIC,
		FLAME,
		PETRIFY
	}
	
	static String[] names = {"Splash P. of Electricity",
							 "Splash P. of Flame",
							 "Splash P. of Petrification"};
	
	static int [] parallel = {1, 1, 1};
	
	Type type;
	
	public CustomPotionRecipes(Type type)
	{
		super(names[type.ordinal()], parallel[type.ordinal()]);
		
		this.type = type;
		
		ingredients = new Requirement[]{new Requirement(Items.potionitem, 3, 0),
				  new Requirement(LOE_Items.ingredients, 1, 3)};
		
		switch(type)
		{
		case ELECTRIC:
			products = new ItemStack[]{new ItemStack(LOE_Items.throwableDrink, 3, 0)};
			catalysts = new Requirement[]{(new Requirement(Items.enchanted_book, 1)).addEnchantment(1, LOE_Enchantment.enchantElectric)};
			break;
			
		case FLAME:
			products = new ItemStack[]{new ItemStack(LOE_Items.throwableDrink, 3, 1)};
			catalysts = new Requirement[]{(new Requirement(Items.enchanted_book, 1)).addEnchantment(1, Enchantment.fireAspect, Enchantment.flame)};
			break;
			
		case PETRIFY:
			products = new ItemStack[]{new ItemStack(LOE_Items.throwableDrink, 3, 2)};
			ingredients = new Requirement[]{new Requirement(Items.potionitem, 3, 0), new Requirement(LOE_Items.ingredients, 1, 10)/*Cockatrice Eye*/};
			break;
		}
	}

	@Override
	public int brewingTime()
	{
		return 400;
	}
	
	public static void load()
	{
		for (Type type :Type.values())
			new CustomPotionRecipes(type);
	}
}
