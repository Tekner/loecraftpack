package loecraftpack.content.potions;

import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;

public class PotionEnergy extends Potion {
	
	/*bad: FALSE*/
	public PotionEnergy(int potionID, boolean bad, int color) {
		super(potionID, bad, color);
	}
	
	@Override
	public void performEffect(EntityLivingBase entityLiving, int level)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)entityLiving;
			AbilityPlayerData abilityData = AbilityPlayerData.Get(player.getGameProfile().getId());
			
			if (player.worldObj.isRemote)
			{
				abilityData.restoreOrDrainEnergy(100*(level+1));
			}
			else
				abilityData.addEnergy(100*(level+1), false);
		}
	}
	
	@Override
	public boolean isInstant()
    {
        return true;
    }
	
	@Override
	public boolean isUsable()
	{
		return true;
	}
	
	@Override
	public boolean isReady(int par1, int par2)
    {
        return par1 >= 20;
    }
}
