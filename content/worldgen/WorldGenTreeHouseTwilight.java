package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class WorldGenTreeHouseTwilight extends WorldGenSchematicBase
{
	public WorldGenTreeHouseTwilight()
	{
		build = new NBTSchematic("LibraryTreeHouse.schematic");
		if (build != null)
		{
			build.setIgnoreAir(true);
			build.printSchematicDEBUG();
		}
		offsetX = -19;
		offsetY = -7;
		offsetZ = -17;
	}

	@Override
	protected boolean generate(World world, Random random)
	{
		int[] xz;
		
		//Debug: twilight worldgen
		System.out.println("-----[[[TWILIGHT]]]-----X:"+coordX+"  Y:"+coordY+"  Z:"+coordZ);
		
		rotation = random.nextInt(4);
		
		//clear main structure space
		clearSpaceSphere(world, 6, 8, 5, 34, 39, 33,
								20, 13, 19, 15, 10, 15);
		
		//clear back structure space
		clearSpaceSphere(world, 17, 8, -2, 32, 39, 10,
								24.5f, 10, 5, 8, 7, 9);
		
		//clear bottom structure space
		clearSpace(world, 16, 1, 10, 23, 7, 11);
		clearSpace(world, 13, 1, 12, 24, 6, 13);
		clearSpace(world, 12, 1, 14, 25, 5, 17);
		clearSpace(world, 13, 1, 18, 24, 5, 19);
		clearSpace(world, 14, 1, 20, 23, 5, 21);
		clearSpace(world, 17, 1, 22, 20, 5, 22);
		
		
		printBuild(world);
		
		//place bed
		xz = getWorldCoordFromSchematicCoord(16, 14);
		placeBed(world, xz[0], coordY+13, xz[1], (rotation+2)&3, 12/*twilight*/);
		
		////grass-fy
		// 6, 0, 5 : 34, 8, 33
		xz = getWorldBBFromSchematicBB(6, 5, 34, 33);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
				for (int y2 = coordY; y2 <= coordY+8; y2++)
				{
					if (Block.isEqualTo(world.getBlock(x2, y2, z2), Blocks.dirt) && world.isAirBlock(x2, y2+1, z2))
						world.setBlock(x2, y2, z2, Blocks.grass, 0, 2/*do everything but update neighbors.*/);
				}
		// 17, 0, -2 : 32, 5, 10
		xz = getWorldBBFromSchematicBB(17, -2, 32, 10);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
				for (int y2 = coordY; y2 <= coordY+5; y2++)
				{
					if (Block.isEqualTo(world.getBlock(x2, y2, z2), Blocks.dirt) && world.isAirBlock(x2, y2+1, z2))
						world.setBlock(x2, y2, z2, Blocks.grass, 0, 2/*do everything but update neighbors.*/);
				}
		
		
		return true;
	}
}
