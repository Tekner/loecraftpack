package loecraftpack.content.worldgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Settings;
import loecraftpack.content.registery.LOE_WorldGen;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.ChunkProviderGenerate;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.terraingen.ChunkProviderEvent;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class HandlerTerrain {
	
	//vanilla reference
	public static List biomelistVanilla = Arrays.asList(new BiomeGenBase[] {BiomeGenBase.desert, BiomeGenBase.desertHills, BiomeGenBase.jungle, BiomeGenBase.jungleHills, BiomeGenBase.swampland});
	protected static int maxDistanceBetweenScatteredFeaturesV = 32;
	protected static int minDistanceBetweenScatteredFeaturesV = 8;
	
	//altered vanilla scattered feature variables
	protected static int maxDistanceBetweenScatteredFeatures = 32;
	protected static int minDistanceBetweenScatteredFeatures = 8;
	protected List scatteredFeatureSpawnList;//entities
	
	public static Random chunkSeedRandCarryOver = null;
	public final static long randStartType = 987234911L;//worldGen seed
	
	public final static long randStartPlunder = 453331324L;//area seed
	public final static long randStartPlunderSub = 774444466L;//worldGen seed
	
	//zone size is 7x7
	static int plunderZoneChance = 20;// 1/20 zones might be plunder-able
	static float plunderChunkChance = 0.8f;
	static int plunderDensity = 20;//attempts to add plunder vines
	
	//doesn't work for pocket dimensions like "THE END" ... yet
	protected class SpawnData
	{
		int dimensionID;
		int chunkX, chunkZ;
		int spawnX, spawnY, spawnZ;
		Random chunkSeedRand;
		WorldGenType type;
		
		SpawnData (int dimensionID, int chunkX, int chunkZ, int spawnX, int spawnY, int spawnZ, Random seed, WorldGenType type)
		{
			this.dimensionID = dimensionID;
			this.chunkX = chunkX;
			this.chunkZ = chunkZ;
			this.spawnX = spawnX;
			this.spawnY = spawnY;
			this.spawnZ = spawnZ;
			this.chunkSeedRand = seed;
			this.type = type;
		}
		
		public boolean isChunk (int dimensionID, int chunkX, int chunkZ)
		{
			return this.dimensionID == dimensionID && this.chunkX == chunkX && this.chunkZ == chunkZ;
		}
	}
	
	List<SpawnData> structuresToSpawn = new ArrayList<SpawnData>();
	
	
	public HandlerTerrain()
	{
		if (BiomeGenBase.getBiomeGenArray().length > 50)//AKA to many biomes
		{
			//up the density of spawning structures, so as to prevent them from being too rare
			maxDistanceBetweenScatteredFeatures = 16;
			minDistanceBetweenScatteredFeatures = 6;
		}
	}
	
	@SubscribeEvent
	public void findSurfaceAndCreateSpawnProfile(ChunkProviderEvent.ReplaceBiomeBlocks event)
	{
		if (!LOE_Settings.Enabled_Structures)
			return;
		
		if (event.chunkProvider instanceof ChunkProviderGenerate)
		{
			ChunkProviderGenerate cpg = (ChunkProviderGenerate)event.chunkProvider;
			
			if (canSpawnStructureAtCoords(cpg.worldObj, event.chunkX, event.chunkZ))
			{
				int dimensionID = cpg.worldObj.provider.dimensionId;
				int spawnX;
				int spawnZ;
				int spawnY;
				
				//also sets the chunkSeedRandCarryOver
				WorldGenType type = getWorldGenType(cpg.worldObj, event.chunkX, event.chunkZ);
				
				//Debug: worldgen spawn data
				if (type == null)
				{
					System.out.println("Invalid gen type");
				}
				else
				{
					System.out.println("structure spawn "+type);
				}
				
				spawnX = 4+chunkSeedRandCarryOver.nextInt(8);
				spawnZ = 4+chunkSeedRandCarryOver.nextInt(8);
				spawnY = getYat(spawnX, spawnZ, event.blockArray);
				spawnY += getYat(spawnX-4, spawnZ-4, event.blockArray);
				spawnY += getYat(spawnX-4, spawnZ+4, event.blockArray);
				spawnY += getYat(spawnX+4, spawnZ-4, event.blockArray);
				spawnY += getYat(spawnX+4, spawnZ+4, event.blockArray);
				spawnY /= 5;
				
				structuresToSpawn.add(new SpawnData(dimensionID, event.chunkX, event.chunkZ, spawnX, spawnY, spawnZ, chunkSeedRandCarryOver, type));
			}
		}
	}
	
	protected int getYat(int x, int z, Block[] blockArray)
	{
		int heightMax = blockArray.length/256;
		int y;
		Block block;
		for (y=heightMax-1; y>=0; y--)
		{
			block = blockArray[(x * 16 + z) * 128 + y];
			if (block != null && block.getMaterial() != Material.air)
			{
				if (y>100) System.out.println("floating land block "+block+"   at: "+y);
				break;
			}
		}
		return y;
	}
	
	
	@SubscribeEvent
	public void onPreWorldGen(PopulateChunkEvent.Pre event)
	{
		if (!LOE_Settings.Enabled_Structures)
			return;
		
		int x = event.chunkX << 4;//*16
		int z = event.chunkZ << 4;
		
		int dimensionID = event.world.provider.dimensionId;
		
		SpawnData spawnData = null;
		for (SpawnData data : structuresToSpawn)
		{
			if (data.isChunk (dimensionID, event.chunkX, event.chunkZ))
			{
				structuresToSpawn.remove(data);
				spawnData = data;
				break;
			}
		}
		
		if (spawnData!=null)
		{//TODO: add landmark generator
			
			switch(spawnData.type)
			{
			case ZECORA:
				LOE_WorldGen.worldGeneratorZecoraHouse.generate(event.world, spawnData.chunkSeedRand, x+spawnData.spawnX, spawnData.spawnY, z+spawnData.spawnZ);
				break;
				
			case SMALL_RUIN:
				LOE_WorldGen.worldGeneratorSmallRuin.generate(event.world, spawnData.chunkSeedRand, x+spawnData.spawnX, spawnData.spawnY, z+spawnData.spawnZ);
				break;
				
			case TWILIGHT:
				LOE_WorldGen.worldGeneratorTwilightHouse.generate(event.world, spawnData.chunkSeedRand, x+spawnData.spawnX, spawnData.spawnY, z+spawnData.spawnZ);
				break;
				
			case CATHEDRAL:
				LOE_WorldGen.worldGeneratorCathedral.generate(event.world, spawnData.chunkSeedRand, x+spawnData.spawnX, spawnData.spawnY, z+spawnData.spawnZ);
				break;
			}
		}
		else
		{
			//Debug: worldgen spawn data  --- have yet to see it occur, but created this as a way to warn us if it does happen
			if (canSpawnStructureAtCoords(event.world, event.chunkX, event.chunkZ))
			{
				System.out.print("----[ERROR]--- X:"+event.chunkX+" Z:"+event.chunkZ);
				switch(getWorldGenType(event.world, event.chunkX, event.chunkZ))
				{
				case ZECORA:
					System.out.println(" Failed to find spawnData for zercora hut");
					break;
					
				case SMALL_RUIN:
					System.out.println(" Failed to find spawnData for small ruin");
					break;
				
				case TWILIGHT:
					System.out.println(" Failed to find spawnData for library tree house");
					break;
					
				case CATHEDRAL:
					System.out.println(" Failed to find spawnData for cathedral");
					break;
					
				default:
					System.out.println(" invalid gen type");
				}
			}
		}
	}
	
	
	@SubscribeEvent
	public void onPostWorldGen(PopulateChunkEvent.Post event)
	{
		if (!LOE_Settings.Enabled_Structures)
			return;
		
		int x = event.chunkX << 4;//*16
		int z = event.chunkZ << 4;
		
		/** PLUNDER VINES **/
		if (isChunkPlundered(event.world, event.chunkX, event.chunkZ))
		{
			int x2;
			int y2;
			int z2;
			int density;
			if (event.world.getBiomeGenForCoords(x+8, z+8).biomeID == LOE_WorldGen.biomeGeneratorEverFreeForest.biomeID)
				density = plunderDensity;
			else
				density = plunderDensity/2;
			
			for (int i = 0; i < density; ++i)
			{
				x2 = x + this.chunkSeedRandCarryOver.nextInt(16) + 8;
				z2 = z + this.chunkSeedRandCarryOver.nextInt(16) + 8;
				y2 = event.world.getHeightValue(x2, z2);
				Block block = event.world.getBlock(x2, y2, z2);
				while (y2 >= 0 && (block == null || block.isAir(event.world, x2, y2, z2) || block.isLeaves(event.world, x2, y2, z2) || block.isWood(event.world, x2, y2, z2) || Block.isEqualTo(block, Blocks.tallgrass)))
				{
					y2--;
					block = event.world.getBlock(x2, y2, z2);
				}
				if (y2<0) continue;
				y2++;
				
				WorldGenerator worldgenerator = LOE_WorldGen.worldGeneratorPlunderVine;
				worldgenerator.setScale(1.0D, 1.0D, 1.0D);
				worldgenerator.generate(event.world, this.chunkSeedRandCarryOver, x2, y2, z2);
			}
		}
	}
	
	public static boolean isChunkPlundered(World world, int xChunk, int zChunk)
	{
		int x = xChunk << 4;
		int z = zChunk << 4;
		
		//find offset from grid
		int offX = xChunk<0? 6+((xChunk+1)%7): xChunk%7;
		int offZ = zChunk<0? 6+((zChunk+1)%7): zChunk%7;
		
		//location seed
		chunkSeedRandCarryOver = getRandomWithSeed(randStartPlunder, world, xChunk-offX, zChunk-offZ);
		BiomeGenBase biomeApparent = world.getBiomeGenForCoords(x+8, z+8);
		int everfreeID = LOE_WorldGen.biomeGeneratorEverFreeForest.biomeID;
		
		//is possible seed location
		if (chunkSeedRandCarryOver.nextInt(plunderZoneChance)==0)
		{
			//ignore corners
			if ( !(((offX == 0 || offX == 6) && (offZ < 2 || offZ > 4)) || ((offZ == 0 || offZ == 6) && (offX < 2 || offX > 4))) )
			{
				//worldGen seed
				chunkSeedRandCarryOver = getRandomWithSeed(randStartPlunderSub, world, xChunk, zChunk);
				if (chunkSeedRandCarryOver.nextFloat()<plunderChunkChance)
				{
					//is everfree biome nearby
					for (int x2 = x - 40; x2 <= x + 56; x2 += 16)
					{
						for (int z2 = z - 40; z2 <= z + 56; z2 += 16)
						{
							if (world.getBiomeGenForCoords(x2, z2).biomeID == everfreeID)
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public static WorldGenType getWorldGenType(World world, int xChunk, int zChunk)
	{
		chunkSeedRandCarryOver = getRandomWithSeed(randStartType, world, xChunk, zChunk);
		
		BiomeGenBase biomegenbase = world.getWorldChunkManager().getBiomeGenAt(xChunk * 16 + 8, zChunk * 16 + 8);
		
		if (biomegenbase == LOE_WorldGen.biomeGeneratorEverFreeForest)
		{
			if (chunkSeedRandCarryOver.nextFloat() < 0.5f)
				return WorldGenType.ZECORA;
			else
				return WorldGenType.SMALL_RUIN;
		}
		else if (BiomeDictionary.isBiomeOfType(biomegenbase, BiomeDictionary.Type.PLAINS))
		{
			if (chunkSeedRandCarryOver.nextFloat() < 0.7f)
				return WorldGenType.TWILIGHT;
			else
				return WorldGenType.CATHEDRAL;
		}
		return null;
	}
	
	public static Random getRandomWithSeed(long par1, World world, int xChunk, int zChunk)
	{
		return new Random(world.getSeed() + (long)(xChunk * xChunk * 4987142) + (long)(xChunk * 5947611) + (long)(zChunk * zChunk) * 4392871L + (long)(zChunk * 389711) ^ par1);
	}
	
	
	//vanilla scattered feature code
	
	
	public static boolean canSpawnStructureAtCoords(World world, int xChunk, int zChunk)
	{
		int k = xChunk;
		int l = zChunk;
		
		if (zoneHasVanillaScatteredStructure(world, xChunk, zChunk))
			return false;//vanilla scattered feature present
		
		if (xChunk < 0)
		{
			xChunk -= maxDistanceBetweenScatteredFeatures - 1;
		}
		
		if (zChunk < 0)
		{
			zChunk -= maxDistanceBetweenScatteredFeatures - 1;
		}
		
		int cornerX = xChunk / maxDistanceBetweenScatteredFeatures;
		int cornerZ = zChunk / maxDistanceBetweenScatteredFeatures;
		Random random = world.setRandomSeed(cornerX, cornerZ, 14357617);
		cornerX *= maxDistanceBetweenScatteredFeatures;
		cornerZ *= maxDistanceBetweenScatteredFeatures;
		
		for (int attempt = 0; attempt < 10; attempt++)
		{
			int locationX = cornerX + random.nextInt(maxDistanceBetweenScatteredFeatures - minDistanceBetweenScatteredFeatures);
			int locationZ = cornerZ + random.nextInt(maxDistanceBetweenScatteredFeatures - minDistanceBetweenScatteredFeatures);
			
			BiomeGenBase biomegenbase = world.getWorldChunkManager().getBiomeGenAt(k * 16 + 8, l * 16 + 8);
			if (k == locationX && l == locationZ)
			{
				//Debug: worldgen spawn data
				//System.out.println("-------spawn zone? "+k+" "+l+" "+biomegenbase.biomeName);
			}
			
			if ((attempt < 4) && BiomeDictionary.isBiomeOfType(biomegenbase, BiomeDictionary.Type.PLAINS))
				return (k == locationX && l == locationZ);
			
			if (biomegenbase == LOE_WorldGen.biomeGeneratorEverFreeForest)
				return (k == locationX && l == locationZ);
		}

		return false;
	}
	
	protected static boolean zoneHasVanillaScatteredStructure(World world, int xChunk, int zChunk)
	{
		int k = xChunk;
		int l = zChunk;
		
		if (xChunk < 0)
		{
			xChunk -= maxDistanceBetweenScatteredFeaturesV - 1;
		}
		
		if (zChunk < 0)
		{
			zChunk -= maxDistanceBetweenScatteredFeaturesV - 1;
		}
		
		int i1 = xChunk / maxDistanceBetweenScatteredFeaturesV;
		int j1 = zChunk / maxDistanceBetweenScatteredFeaturesV;
		Random random = world.setRandomSeed(i1, j1, 14357617);
		i1 *= maxDistanceBetweenScatteredFeaturesV;
		j1 *= maxDistanceBetweenScatteredFeaturesV;
		i1 += random.nextInt(maxDistanceBetweenScatteredFeaturesV - minDistanceBetweenScatteredFeaturesV);
		j1 += random.nextInt(maxDistanceBetweenScatteredFeaturesV - minDistanceBetweenScatteredFeaturesV);
		
		BiomeGenBase biomegenbase = world.getWorldChunkManager().getBiomeGenAt(i1 * 16 + 8, j1 * 16 + 8);
		Iterator iteratorV = biomelistVanilla.iterator();
		
		while (iteratorV.hasNext())
		{
			BiomeGenBase biomegenbase1 = (BiomeGenBase)iteratorV.next();
			
			if (biomegenbase == biomegenbase1)
			{
				//Debug: worldgen spawn data
				/*
				if (k == i1 && l == j1)
					System.out.println("-------vanilla spawn zone "+k+" "+l+" "+biomegenbase.biomeName);*/
				return true;
			}
		}

		return false;
	}
	
	@SubscribeEvent
	public void onDecorateWorldPostSurface(DecorateBiomeEvent.Post event)
	{
		BiomeGenBase biome = event.world.getBiomeGenForCoords(event.chunkX, event.chunkZ);
		
		if(biome.biomeID != BiomeGenBase.hell.biomeID)
		{
			//random apple blooms
			if (biome.biomeID != 0 && biome.biomeID != BiomeGenBase.desert.biomeID && biome.biomeID != BiomeGenBase.frozenOcean.biomeID)
			{
				if (event.rand.nextInt(20)==0)
				{
					int k = event.chunkX + event.rand.nextInt(16) + 8;
					int l = event.chunkZ + event.rand.nextInt(16) + 8;
					WorldGenerator worldgenerator = LOE_WorldGen.worldGeneratorAppleBloom;
					worldgenerator.setScale(1.0D, 1.0D, 1.0D);
					worldgenerator.generate(event.world, event.rand, k, event.world.getHeightValue(k, l), l);
				}
			}
		}
	}
	
	
	
  /**********************************************************************************************/
 /**Hidden Ores*********************************************************************************/
/**********************************************************************************************/
	
	/*
	 * NOTE (WorldGenMinable):
	 * the value number is actually 2 greater than the amount that can be generated in a cluster
	 * so compensate for that.  (unconfirmed)
	 */
	
	@SubscribeEvent
	public void onDecorateWorldPreORE(DecorateBiomeEvent.Pre event)
	{
		BiomeGenBase biome = event.world.getBiomeGenForCoords(event.chunkX, event.chunkZ);
		
		//TODO: LOE_Settings.Mode_GemOreDistribution
		
		if(biome.biomeID != BiomeGenBase.hell.biomeID)
		{
			//rare - lower elevation
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 10, 3, Blocks.stone), 17,  40);//Laughter
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 11, 3, Blocks.stone), 17,  40);//Generosity
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 12, 3, Blocks.stone), 17,  40);//Kindness
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 13, 3, Blocks.stone), 17,  40);//Magic
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 14, 3, Blocks.stone), 17,  40);//Loyalty
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 15, 3, Blocks.stone), 17,  40);//Honesty
			
			//rare - higher elevation
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 10, 3, Blocks.stone), 41, 128);//Laughter
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 11, 3, Blocks.stone), 41, 128);//Generosity
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 12, 3, Blocks.stone), 41, 128);//Kindness
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 13, 3, Blocks.stone), 41, 128);//Magic
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 14, 3, Blocks.stone), 41, 128);//Loyalty
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre, 15, 3, Blocks.stone), 41, 128);//Honesty
			
			//rare - general
			generateOre(3, event, new WorldGenMinable(LOE_Blocks.gemOre,  9, 3, Blocks.stone), 17, 128);//Heart
			
		}
	}
	
	@SubscribeEvent
	public void onDecorateWorldPostORE(DecorateBiomeEvent.Post event)
	{
		BiomeGenBase biome = event.world.getBiomeGenForCoords(event.chunkX, event.chunkZ);
		
		//TODO: LOE_Settings.Mode_GemOreDistribution
		
		if(biome.biomeID != BiomeGenBase.hell.biomeID)
		{
			//Common
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  0, 6, Blocks.stone),  5, 128);//Sapphire
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  1, 4, Blocks.stone),  5, 128);//Fire Ruby
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  2, 4, Blocks.stone),  5, 128);
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  3, 4, Blocks.stone),  5, 128);
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  4, 4, Blocks.stone),  5, 128);
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  5, 4, Blocks.stone),  5, 128);
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  6, 4, Blocks.stone),  5, 128);
			generateOre(7, event, new WorldGenMinable(LOE_Blocks.gemOre,  7, 6, Blocks.stone),  5, 128);//Onyx
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.gemOre,  8, 5, Blocks.stone),  5, 128);
			
			//Tom
			generateOre(1, event, new WorldGenMinable(LOE_Blocks.tomOre,  0, 4, Blocks.stone),  5,  16);//Tom
		}
	}
	
		  /****************************/
		 /**Common Generate Ore Vein**/
		/****************************/
	protected void generateOre(int sets, DecorateBiomeEvent event, WorldGenerator worldGenerator, int minHeight, int maxHeight)
	{
		for(int i=0; i<sets; i++)
		{
			int x = event.chunkX + event.rand.nextInt(16);
			int y = event.rand.nextInt(maxHeight - minHeight) + minHeight;
			int z = event.chunkZ + event.rand.nextInt(16);
			worldGenerator.generate(event.world, event.rand, x, y, z);
		}
	}
}