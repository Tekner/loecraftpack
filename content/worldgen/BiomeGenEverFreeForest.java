package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.entities.EntityTimberWolf;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BiomeGenEverFreeForest extends BiomeGenBase {

	WorldGenCustomForest customForest;
	
	public BiomeGenEverFreeForest(int par1) {
		super(par1);
		this.spawnableMonsterList.remove(3);//the forest will be bad enough as is... it won't need creepers
		this.spawnableMonsterList.add(new SpawnListEntry(EntityTimberWolf.class, 12, 4, 6));
		//this.spawnableMonsterList.add(new SpawnListEntry(EntityCockatrice.class, 3, 2, 4));
		this.theBiomeDecorator = new BiomeDecoratorEverFree();
		this.theBiomeDecorator.treesPerChunk = 30;
		this.theBiomeDecorator.grassPerChunk = 20;
		this.theBiomeDecorator.mushroomsPerChunk = 8;
		
		this.customForest = new WorldGenCustomForest(false, Blocks.sapling, 0, Blocks.log, 0, Blocks.leaves, 0, true);
	}
	
	@Override
	public BiomeDecorator createBiomeDecorator()
	{
		return getModdedBiomeDecorator(new BiomeDecoratorEverFree());
	}
	
	//getRandomWorldGenForTrees
	@Override
	public WorldGenAbstractTree func_150567_a(Random random)
	{
		//return (WorldGenerator)(this.customForest);
		
		return (WorldGenAbstractTree)(random.nextFloat() < 0.25F ?
									this.customForest :
									(random.nextFloat() < 1.0F ?
										this.worldGeneratorBigTree :
										this.worldGeneratorTrees));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getBiomeGrassColor(int xCoord, int yCoord, int zCoord)
	{
		return getModdedBiomeFoliageColor(2113536);//25600;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getBiomeFoliageColor(int xCoord, int yCoord, int zCoord)
	{
		return getModdedBiomeFoliageColor(2578733);
	}
}
