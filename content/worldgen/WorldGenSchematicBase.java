package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.blocks.tile.TileColoredBed;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.ChestGenHooks;

public abstract class WorldGenSchematicBase extends WorldGenerator {
	
	protected NBTSchematic build;
	
	//Central Coords passed by parameter
	protected int coordX;
	protected int coordY;
	protected int coordZ;
	//Static positioning of schematic 0,0,0 from the Central Coords (ignoring rotation)
	protected int offsetX;
	protected int offsetY;
	protected int offsetZ;
	//rotation around Central Coords's Y-axis (0-3)
	protected int rotation;
	
	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		coordX = x;
		coordY = y;
		coordZ = z;
		
		if (!build.isvalid())
		{
			//Debug: invalid builds
			System.out.println("[[ERROR]] attepmted to print invalid build:"+build.getFileLocation()+"     X:"+coordX+"  Y:"+coordY+"  Z:"+coordZ);
			//temp pillar
			for (int y2 = coordY; y2 < coordY+30; y2++)
			{
				world.setBlock(coordX, y2, coordZ, Blocks.bedrock, 0, 3);
			}
			return false;
		}
		
		return generate(world, random);
	}
	
	protected abstract boolean generate(World world, Random random);
	
	protected void printBuild(World world)
	{
		int oldRotation = build.getRotation();
		if (oldRotation != rotation)
		{
			build.setToDefault();
			build.rotate(rotation);
		}
		
		//find corner of schematic
		int[] xz = new int[2];
		switch (rotation)
		{
		case 1:
			xz[0] = coordX - (offsetZ + build.getZSizeDefault());
			xz[1] = coordZ + (offsetX);
			break;
			
		case 2:
			xz[0] = coordX - (offsetX + build.getXSizeDefault());
			xz[1] = coordZ - (offsetZ + build.getZSizeDefault());
			break;
			
		case 3:
			xz[0] = coordX + (offsetZ);
			xz[1] = coordZ - (offsetX + build.getXSizeDefault());
			break;
			
		default:
			xz[0] = coordX + (offsetX);
			xz[1] = coordZ + (offsetZ);
		}
		
		build.spawnNBTSchematic(world, xz[0], coordY+offsetY, xz[1]);
	}
	
	protected int[] getWorldCoordFromSchematicCoord(int targetX, int targetZ)
	{
		switch (rotation)
		{
		case 1:
			return new int[] {coordX - (offsetZ + targetZ) -1, coordZ + (offsetX + targetX)   };
			
		case 2:
			return new int[] {coordX - (offsetX + targetX) -1, coordZ - (offsetZ + targetZ) -1};
			
		case 3:
			return new int[] {coordX + (offsetZ + targetZ)   , coordZ - (offsetX + targetX) -1};
		}
		return new int[] {coordX + (offsetX + targetX)   , coordZ + (offsetZ + targetZ)   };
	}
	
	protected int[] getWorldBBFromSchematicBB(int targetXmin, int targetZmin, int targetXmax, int targetZmax)
	{
		switch (rotation)
		{
		case 1:
			return new int[] {coordX - (offsetZ + targetZmax) -1, coordZ + (offsetX + targetXmin),
							  coordX - (offsetZ + targetZmin) -1, coordZ + (offsetX + targetXmax)   };
			
		case 2:
			return new int[] {coordX - (offsetX + targetXmax) -1, coordZ - (offsetZ + targetZmax) -1,
							  coordX - (offsetX + targetXmin) -1, coordZ - (offsetZ + targetZmin) -1};
			
		case 3:
			return new int[] {coordX + (offsetZ + targetZmin)   , coordZ - (offsetX + targetXmax) -1,
							  coordX + (offsetZ + targetZmax)   , coordZ - (offsetX + targetXmin) -1};
		}
		return new int[] {coordX + (offsetX + targetXmin)   , coordZ + (offsetZ + targetZmin),
						  coordX + (offsetX + targetXmax)   , coordZ + (offsetZ + targetZmax)};
	}
	
	protected int[] getTranslationRotateBBFloating(int targetXmin, int targetZmin, int targetXmax, int targetZmax)
	{
		switch (rotation)
		{
		case 1:
			return new int[] {(-targetZmax) -1, (targetXmin),
							  (-targetZmin) -1, (targetXmax)   };
			
		case 2:
			return new int[] {(-targetXmax) -1, (-targetZmax) -1,
							  (-targetXmin) -1, (-targetZmin) -1};
			
		case 3:
			return new int[] {(targetZmin)   , (targetXmax) -1,
							  (targetZmax)   , (targetXmin) -1};
		}
		return new int[] {(targetXmin)   , (targetZmin),
						  (targetXmax)   , (targetZmax)};
	}
	
	/**clear out a defined space for the schematic
	 * 
	 * @param world
	 * <br>--Schematic Coords:
	 * @param xPosMin
	 * @param yPosMin
	 * @param zPosMin
	 * @param xPosMax
	 * @param yPosMax
	 * @param zPosMax
	 */
	protected void clearSpace(World world, int xPosMin, int yPosMin, int zPosMin, int xPosMax, int yPosMax, int zPosMax)
	{
		int[] xz = getWorldBBFromSchematicBB(xPosMin, zPosMin, xPosMax, zPosMax);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
				for (int y2 = coordY+offsetY+yPosMin; y2 <= coordY+offsetY+yPosMax; y2++)
					world.setBlockToAir(x2, y2, z2);
	}
	
	protected void fillSpace(World world, Block block, int metaData, int xPosMin, int yPosMin, int zPosMin, int xPosMax, int yPosMax, int zPosMax)
	{
		int[] xz = getWorldBBFromSchematicBB(xPosMin, zPosMin, xPosMax, zPosMax);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
				for (int y2 = coordY+offsetY+yPosMin; y2 <= coordY+offsetY+yPosMax; y2++)
					world.setBlock(x2, y2, z2, block, metaData, 3);
	}
	
	protected void raiseTerrain(World world, Block block, int metaData, int xPosMin, int yPosMin, int zPosMin, int xPosMax, int yPosMax, int zPosMax)
	{
		int[] xz = getWorldBBFromSchematicBB(xPosMin, zPosMin, xPosMax, zPosMax);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
			{
				int y2 = coordY+offsetY+yPosMin;
				int yM = coordY+offsetY+yPosMax;
				int yH = world.getHeightValue(x2, z2);
				if (yH>yM)continue;
				if (yH<y2)continue;
				
				for (y2 = yH; y2 <= yM; y2++)
					world.setBlock(x2, y2, z2, block, metaData, 3);
			}
	}
	
	protected void clearSpaceSphere(World world, int xPosMin, int yPosMin, int zPosMin, int xPosMax, int yPosMax, int zPosMax,
										float xCenter, float yCenter, float zCenter, float xRadius, float yRadius, float zRadius)
	{
		float[] xzF;
		switch (rotation)
		{
		case 1:
			xzF = new float[] {coordX - (offsetZ + zCenter) -1, coordZ + (offsetX + xCenter)   };
			break;
			
		case 2:
			xzF = new float[] {coordX - (offsetX + xCenter) -1, coordZ - (offsetZ + zCenter) -1};
			break;
			
		case 3:
			xzF = new float[] {coordX + (offsetZ + zCenter)   , coordZ - (offsetX + xCenter) -1};
			break;
			
		default:
			xzF = new float[] {coordX + (offsetX + xCenter)   , coordZ + (offsetZ + zCenter)   };
			
		}
		xCenter = xzF[0];
		zCenter = xzF[1];
		
		switch (rotation)
		{
		case 1:
		case 3:
			xzF = new float[] {zRadius, xRadius};
			xRadius = xzF[0];
			zRadius = xzF[1];
			break;
		}
		
		int[] xz = getWorldBBFromSchematicBB(xPosMin, zPosMin, xPosMax, zPosMax);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
				for (int y2 = coordY+offsetY+yPosMin; y2 <= coordY+offsetY+yPosMax; y2++)
					if (withinSphere(x2, y2, z2, xCenter, coordY+offsetY+yCenter, zCenter, xRadius, yRadius, zRadius))
						world.setBlockToAir(x2, y2, z2);//world.setBlock(x2, y2, z2, Block.glass.blockID, 0, 3);
	}
	
	protected boolean withinSphere(int xPosMin, int yPosMin, int zPosMin, float xCenter, float yCenter, float zCenter, float xRadius, float yRadius, float zRadius)
	{
		float x = ((float)xPosMin - xCenter) / xRadius;
		float y = ((float)yPosMin - yCenter) / yRadius;
		float z = ((float)zPosMin - zCenter) / zRadius;
		return (x*x+y*y+z*z)<=1.0f;
	}
	
	public boolean placeBed(World world, int xCoord, int yCoord, int zCoord, int direction, int id)
	{
		byte b0 = 0;
		byte b1 = 0;

		if (direction == 0)
		{
			b1 = 1;
		}

		if (direction == 1)
		{
			b0 = -1;
		}

		if (direction == 2)
		{
			b1 = -1;
		}

		if (direction == 3)
		{
			b0 = 1;
		}

		if (world.isAirBlock(xCoord, yCoord, zCoord) && world.isAirBlock(xCoord + b0, yCoord, zCoord + b1))
		{
			//place foot
			world.setBlock(xCoord, yCoord, zCoord, LOE_Blocks.bed, direction, 3);
			world.setTileEntity(xCoord, yCoord, zCoord, new TileColoredBed(id));
			//place head
			world.setBlock(xCoord + b0, yCoord, zCoord + b1, LOE_Blocks.bed, direction + 8, 3);
			world.setTileEntity(xCoord + b0, yCoord, zCoord + b1, new TileColoredBed(id));
			//Perform final block update
			TileColoredBed.finishTileCreation(world, xCoord, yCoord, zCoord, xCoord + b0, yCoord, zCoord + b1);

			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void placeCoffin(World world, int x, int y, int z, int direction/*0:south, 1:west, 2:north, 3:east*/, boolean clearTop)
	{
		direction += 2;/*2:south, 3:west, 4:north, 5:east*/
		
		int x2 = x;
		int y2 = y;
		int z2 = z;
		switch (direction)
		{
		case 2:
			x2++;//place one east, open south
			break;
		case 3:
			z2++;//place one south, open west
			break;
		case 4:
			x2--;//place one west, open north
			break;
		case 5:
			z2--;//place one north, open east
			break;
		}
		
		world.setBlock(x, y, z, LOE_Blocks.coffin, direction, 3);
		world.setBlock(x2, y2, z2, LOE_Blocks.coffin, direction, 3);
		if (clearTop)
		{
			world.setBlockToAir(x, y+1, z);
			world.setBlockToAir(x2, y2+1, z2);
		}
	}
	
	protected void placeBones(IInventory inventory, boolean backwards)
	{
		if (backwards)
		{
			inventory.setInventorySlotContents(5, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(10, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(11, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(12, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(15, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(22, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(23, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(24, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(25, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(28, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(29, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(30, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(33, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(41, new ItemStack(Items.bone, 1, 0));
		}
		else
		{
			inventory.setInventorySlotContents(3, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(11, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(14, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(15, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(16, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(19, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(20, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(21, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(22, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(29, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(32, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(33, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(34, new ItemStack(Items.bone, 1, 0));
			inventory.setInventorySlotContents(39, new ItemStack(Items.bone, 1, 0));
		}
	}
	
	protected void addChestLoot(IInventory inventory, Random rand, int units, WeightedRandomChestContent[] listOfItems)
	{
		int invSize = inventory.getSizeInventory();
		for (int i=0; i<units; i++)
		{
			WeightedRandomChestContent weightedrandomchestcontent = (WeightedRandomChestContent)WeightedRandom.getRandomItem(rand, listOfItems);
			//Debug: loot
			System.out.println("------loot- weight:"+weightedrandomchestcontent.itemWeight+"  ID:"+weightedrandomchestcontent.theItemId+"  min:"+weightedrandomchestcontent.theMinimumChanceToGenerateItem+" max:"+weightedrandomchestcontent.theMaximumChanceToGenerateItem);
			ItemStack[] stacks = ChestGenHooks.generateStacks(rand, weightedrandomchestcontent.theItemId, weightedrandomchestcontent.theMinimumChanceToGenerateItem, weightedrandomchestcontent.theMaximumChanceToGenerateItem);
			for (ItemStack stack : stacks)
			{
				int start = rand.nextInt(invSize);
				for (int i2=0; i2<invSize; i2++)
				{
					int pos = (i2 + start)%invSize;
					if (inventory.getStackInSlot(pos)==null)
					{
						inventory.setInventorySlotContents(pos, stack);
						break;
					}
				}
			}
		}
	}

}
