package loecraftpack.content.worldgen;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenerator;
import static net.minecraftforge.common.util.ForgeDirection.*;
import net.minecraftforge.common.IPlantable;

public class WorldGenCustomForest extends WorldGenAbstractTree {

	Block saplingType;
	int metaSapling;
	Block logType;
	int metaLog;
	Block leafType;
	int metaLeaf;
	boolean dense;
	
	public WorldGenCustomForest(boolean notWorldGen, Block saplingType, Block log, Block leaf) {
		super(notWorldGen);
		this.saplingType = saplingType;
		this.metaSapling = 0;
		this.logType = log;
		this.metaLog = 0;
		this.leafType = leaf;
		this.metaLeaf = 0;
		this.dense = false;
	}
	
	public WorldGenCustomForest(boolean notWorldGen, Block saplingType, int metaSapling, Block log, int metaLog, Block leaf, int metaLeaf, boolean dense) {
		super(notWorldGen);
		this.saplingType = saplingType;
		this.metaSapling = metaSapling;
		this.logType = log;
		this.metaLog = metaLog;
		this.leafType = leaf;
		this.metaLeaf = metaLeaf;
		this.dense = dense;
	}
	
	@Override
	public boolean generate(World world, Random random, int xBase, int yBase, int zBase)
	{
		int targetHeight;
		if (dense)
			targetHeight = random.nextInt(3) + 3;
		else
			targetHeight = random.nextInt(3) + 5;
		
		boolean flag = true;

		if (yBase >= 1 && yBase + targetHeight + 1 <= 256)
		{
			int xTarget;
			int yTarget;
			int zTarget;

			if (!dense)
			{
				for (yTarget = yBase; yTarget <= yBase + 1 + targetHeight; ++yTarget)
				{
					byte radius = 1;
	
					if (yTarget == yBase)
					{
						radius = 0;
					}
	
					if (yTarget >= yBase + 1 + targetHeight - 2)
					{
						radius = 2;
					}
	
					for (xTarget = xBase - radius; xTarget <= xBase + radius && flag; ++xTarget)
					{
						for (zTarget = zBase - radius; zTarget <= zBase + radius && flag; ++zTarget)
						{
							if (yTarget >= 0 && yTarget < 256)
							{
								Block block = world.getBlock(xTarget, yTarget, zTarget);
	
								if (block != null && !block.isLeaves(world, xTarget,  yTarget, zTarget))
								{
									if (dense)
									{
										if (!block.isWood(world, xTarget,  yTarget, zTarget))
											flag = false;
									}
									else
										flag = false;
								}
							}
							else
							{
								flag = false;
							}
						}
					}
				}
			}
			if (!flag)
			{
				return false;
			}
			else
			{
				Block soil = world.getBlock(xBase, yBase - 1, zBase);
				boolean isValidSoil = soil != null && soil.canSustainPlant(world, xBase, yBase - 1, zBase, UP, (IPlantable)saplingType);

				if (isValidSoil && yBase < 256 - targetHeight - 1)
				{
					soil.onPlantGrow(world, xBase, yBase - 1, zBase, xBase, yBase, zBase);
					

					for (yTarget = yBase - 3 + targetHeight; yTarget <= yBase + targetHeight; ++yTarget)
					{
						int k33333 = yTarget - (yBase + targetHeight);
						int k44444 = 1 - k33333 / 2;

						for (xTarget = xBase - k44444; xTarget <= xBase + k44444; ++xTarget)
						{
							int j2 = xTarget - xBase;

							for (zTarget = zBase - k44444; zTarget <= zBase + k44444; ++zTarget)
							{
								int l2 = zTarget - zBase;

								if (Math.abs(j2) != k44444 || Math.abs(l2) != k44444 || random.nextInt(2) != 0 && k33333 != 0)
								{
									Block block = world.getBlock(xTarget, yTarget, zTarget);

									if (block == null || block.isAir(world, xTarget, yTarget, zTarget) || block.canBeReplacedByLeaves(world, xTarget, yTarget, zTarget))
									{
										this.setBlockAndNotifyAdequately(world, xTarget, yTarget, zTarget, leafType, metaLeaf);
									}
								}
							}
						}
					}

					for (yTarget = 0; yTarget < targetHeight; ++yTarget)
					{
						Block block = world.getBlock(xBase, yBase + yTarget, zBase);

						if (block == null || block.isAir(world, xBase, yBase + yTarget, zBase) || block.isLeaves(world, xBase, yBase + yTarget, zBase) || Block.isEqualTo(block, Blocks.tallgrass))
						{
							this.setBlockAndNotifyAdequately(world, xBase, yBase + yTarget, zBase, logType, metaLog);
						}
					}

					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}

}
