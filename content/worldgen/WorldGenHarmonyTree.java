package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.blocks.BlockHarmonySapling;
import loecraftpack.content.entities.EntityHarmonyTree;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import static net.minecraftforge.common.util.ForgeDirection.*;

public class WorldGenHarmonyTree extends WorldGenCustomAppleTree {

	public WorldGenHarmonyTree(boolean sapling, int minTreeHeight)
	{
		super(sapling, LOE_Blocks.harmonySapling, LOE_Blocks.harmonyLog, LOE_Blocks.harmonyLeaves, minTreeHeight);
	}
	
	@Override
	public boolean generate(World world, Random par2Random, int xCoord, int yCoord, int zCoord)
	{
		int l = par2Random.nextInt(1) + this.minTreeHeight;
		boolean flag = true;

		Block grass = Blocks.grass;
		Block tallGrass = Blocks.tallgrass;
		Block tallGrass2 = Blocks.double_plant;
		Block dirt = Blocks.dirt;
		
		if (yCoord >= 1 && yCoord + l + 1 <= 256)
		{
			int i1;
			byte b0;
			int j1;
			int k1;

			for (i1 = yCoord; i1 <= yCoord + l; ++i1)
			{
				b0 = 1;
				
				if (i1 == yCoord)
				{
					b0 = 0;
				}

				if (i1 >= yCoord + l - 2)
				{
					b0 = 2;
				}

				for (int l1 = xCoord - b0; l1 <= xCoord + b0 && flag; ++l1)
				{
					for (j1 = zCoord - b0; j1 <= zCoord + b0 && flag; ++j1)
					{
						if (i1 >= 0 && i1 < 256)
						{
							Block targetBlock = world.getBlock(l1, i1, j1);
							
							if (!Block.isEqualTo(targetBlock, saplingType) &&
								!targetBlock.isAir(world, l1, i1, j1) &&
								!targetBlock.isLeaves(world, l1, i1, j1) &&
								!Block.isEqualTo(targetBlock, grass) &&
								!Block.isEqualTo(targetBlock, tallGrass) &&
								!Block.isEqualTo(targetBlock, tallGrass2) &&
								!Block.isEqualTo(targetBlock, dirt) &&
								!targetBlock.isWood(world, l1, i1, j1) )
							{
								flag = false;
							}
						}
						else
						{
							flag = false;
						}
					}
				}
			}
			if (!flag)
			{
				return false;
			}
			else
			{
				Block soil = world.getBlock(xCoord, yCoord - 1, zCoord);
				boolean isSoil = (soil != null && soil.canSustainPlant(world, xCoord, yCoord - 1, zCoord, UP, (BlockHarmonySapling)saplingType ));
				if (isSoil && yCoord < 256 - l - 1)
				{
					soil.onPlantGrow(world, xCoord, yCoord - 1, zCoord, xCoord, yCoord, zCoord);
					
					int heightReached = -1;
					for (j1 = 0; j1 < l; ++j1)
					{
						Block targetBlock =  world.getBlock(xCoord, yCoord + j1, zCoord);

						if (targetBlock == null || targetBlock.isAir(world, xCoord, yCoord + j1, zCoord) || Block.isEqualTo(targetBlock, saplingType) || Block.isEqualTo(targetBlock, tallGrass) || Block.isEqualTo(targetBlock, tallGrass2) || targetBlock.isLeaves(world, xCoord, yCoord + j1, zCoord))
						{
							heightReached++;
							if (j1 != l-1)
								this.setBlockAndNotifyAdequately(world, xCoord, yCoord + j1, zCoord, logType, 0);
							else
								this.setBlockAndNotifyAdequately(world, xCoord, yCoord + j1, zCoord, logType, 12);
						}
						else if (j1 != 0)
							this.setBlockAndNotifyAdequately(world, xCoord, yCoord + j1-1, zCoord, logType, 12);
							
					}
					
					int x = xCoord;
					int y = yCoord+heightReached;
					int z = zCoord;
					
					placeLeaf(world, x+1, y, z);
					placeLeaf(world, x, y, z+1);
					placeLeaf(world, x-1, y, z);
					placeLeaf(world, x, y, z-1);
					
					for (int d=0; d<4; d++)
					{
						x = xCoord;
						y = yCoord+heightReached-3;
						z = zCoord;
						for (int i=0; i<2; i++)
						{
							y++;
							switch(d)
							{
							case 0:
								x++;
								break;
								
							case 1:
								z++;
								break;
								
							case 2:
								x--;
								break;
								
							case 3:
								z--;
								break;
							}
							
							Block targetBlock =  world.getBlock(x, y, z);

							if (targetBlock == null || targetBlock.isAir(world, x, y, z) || Block.isEqualTo(targetBlock, tallGrass) || Block.isEqualTo(targetBlock, tallGrass2) || targetBlock.isLeaves(world, x, y, z))
							{
								this.setBlockAndNotifyAdequately(world,x, y, z, logType, 12);
								
								if (i==1)
								{
									placeLeaf(world, x, y-1, z);
									if (d!=0)placeLeaf(world, x-1, y, z);
									if (d!=1)placeLeaf(world, x, y, z-1);
									if (d!=2)placeLeaf(world, x+1, y, z);
									if (d!=3)placeLeaf(world, x, y, z+1);
								}
							}
							else
								break;
						}
					}
					
					world.spawnEntityInWorld(new EntityHarmonyTree(world, xCoord+0.5f, yCoord, zCoord+0.5f));
					
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	
	
	public boolean placeLeaf(World world, int x, int y, int z)
	{
		Block targetBlock =  world.getBlock(x, y, z);
		
		if (targetBlock == null || targetBlock.isAir(world, x, y, z)  || targetBlock.canBeReplacedByLeaves(world, x, y, z))
		{
			this.setBlockAndNotifyAdequately(world, x, y, z, leafType, 0);
			return true;
		}
		return false;
	}
}
