package loecraftpack.content.worldgen;

import static net.minecraftforge.event.terraingen.DecorateBiomeEvent.Decorate.EventType.*;

import java.util.Random;

import loecraftpack.content.registery.LOE_WorldGen;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.feature.WorldGenDeadBush;
import net.minecraft.world.gen.feature.WorldGenLiquids;
import net.minecraft.world.gen.feature.WorldGenPumpkin;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.TerrainGen;

public class BiomeDecoratorEverFree extends BiomeDecorator {
	
	public BiomeDecoratorEverFree(){}
	
	/**
	 * Determines if the chunk is deep enough within the Everfree Biome, and then randomly tests for a success.
	 */
	public static boolean growZapApples(World world, Random random, int xChunk, int zChunk)
	{
		return	validZapApple(world, xChunk-3*16, zChunk-3*16) &&
				validZapApple(world, xChunk+3*16, zChunk-3*16) &&
				validZapApple(world, xChunk-3*16, zChunk+3*16) &&
				validZapApple(world, xChunk+3*16, zChunk+3*16) &&
				validZapApple(world, xChunk, zChunk) &&
				random.nextInt(5) == 0;
	}
	
	private static boolean validZapApple(World world, int xChunk, int zChunk)
	{
		return (world.getBiomeGenForCoords(xChunk, zChunk).biomeID == LOE_WorldGen.biomeGeneratorEverFreeForest.biomeID);
	}

	
	/**
	 * @Override It has been modified to create Zap-Apple trees within the Biome.
	 */
	@Override
	protected void genDecorations(BiomeGenBase bioneGen)
	{
		MinecraftForge.EVENT_BUS.post(new DecorateBiomeEvent.Pre(currentWorld, randomGenerator, chunk_X, chunk_Z));
		
		////detect special structures////
		
		boolean isZecoraHut = false;
		int xchu = chunk_X>>4;
		int zchu = chunk_Z>>4;
		for (int x = (xchu); x <= (xchu) +1; x++)
		{
			for (int z = (zchu); z <= (zchu) +1; z++)
			{
				if (HandlerTerrain.canSpawnStructureAtCoords(currentWorld, x, z) && HandlerTerrain.getWorldGenType(currentWorld, x, z)==WorldGenType.ZECORA)
				{
					isZecoraHut = true;
				}
			}
		}
		
		///normal code///
		
		this.generateOres();
		int i;
		int j;
		int k;

		boolean doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, SAND);
		for (i = 0; doGen && i < this.sandPerChunk2; ++i)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.sandGen.generate(this.currentWorld, this.randomGenerator, j, this.currentWorld.getTopSolidOrLiquidBlock(j, k), k);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, CLAY);
		for (i = 0; doGen && i < this.clayPerChunk; ++i)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.clayGen.generate(this.currentWorld, this.randomGenerator, j, this.currentWorld.getTopSolidOrLiquidBlock(j, k), k);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, SAND_PASS2);
		for (i = 0; doGen && i < this.sandPerChunk; ++i)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.sandGen.generate(this.currentWorld, this.randomGenerator, j, this.currentWorld.getTopSolidOrLiquidBlock(j, k), k);
		}

		i = this.treesPerChunk;

		if (this.randomGenerator.nextInt(10) == 0)
		{
			++i;
		}

		int l;

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, TREE);
		if (doGen && !isZecoraHut)
		{
			boolean zap = growZapApples(currentWorld, randomGenerator, chunk_X, chunk_Z);
			for (j = 0; doGen && j < i; ++j)
			{
				k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
				l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
				WorldGenerator worldgenerator;
				if(zap)
					worldgenerator = LOE_WorldGen.worldGeneratorZapAppleForest;
				else
					worldgenerator = bioneGen.func_150567_a(this.randomGenerator);
				worldgenerator.setScale(1.0D, 1.0D, 1.0D);
				worldgenerator.generate(this.currentWorld, this.randomGenerator, k, this.currentWorld.getHeightValue(k, l), l);
			}
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, BIG_SHROOM);
		for (j = 0; doGen && j < this.bigMushroomsPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.bigMushroomGen.generate(this.currentWorld, this.randomGenerator, k, this.currentWorld.getHeightValue(k, l), l);
		}

		int i1;

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, FLOWERS);
		for (j = 0; doGen && j < this.flowersPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.randomGenerator.nextInt(128);
			i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.yellowFlowerGen.generate(this.currentWorld, this.randomGenerator, k, l, i1);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, GRASS);
		for (j = 0; doGen && j < this.grassPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.randomGenerator.nextInt(128);
			i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			WorldGenerator worldgenerator1 = bioneGen.getRandomWorldGenForGrass(this.randomGenerator);
			worldgenerator1.generate(this.currentWorld, this.randomGenerator, k, l, i1);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, DEAD_BUSH);
		for (j = 0; doGen && j < this.deadBushPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.randomGenerator.nextInt(128);
			i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			(new WorldGenDeadBush(Blocks.deadbush)).generate(this.currentWorld, this.randomGenerator, k, l, i1);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, LILYPAD);
		for (j = 0; doGen && j < this.waterlilyPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;

			for (i1 = this.randomGenerator.nextInt(128); i1 > 0 && this.currentWorld.isAirBlock(k, i1 - 1, l); --i1)
			{
				;
			}

			this.waterlilyGen.generate(this.currentWorld, this.randomGenerator, k, i1, l);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, SHROOM);
		for (j = 0; doGen && j < this.mushroomsPerChunk; ++j)
		{
			if (this.randomGenerator.nextInt(4) == 0)
			{
				k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
				l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
				i1 = this.currentWorld.getHeightValue(k, l);
				this.mushroomBrownGen.generate(this.currentWorld, this.randomGenerator, k, i1, l);
			}

			if (this.randomGenerator.nextInt(8) == 0)
			{
				k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
				l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
				i1 = this.randomGenerator.nextInt(128);
				this.mushroomRedGen.generate(this.currentWorld, this.randomGenerator, k, i1, l);
			}
		}

		if (doGen && this.randomGenerator.nextInt(4) == 0)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.randomGenerator.nextInt(128);
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.mushroomBrownGen.generate(this.currentWorld, this.randomGenerator, j, k, l);
		}

		if (doGen && this.randomGenerator.nextInt(8) == 0)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.randomGenerator.nextInt(128);
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.mushroomRedGen.generate(this.currentWorld, this.randomGenerator, j, k, l);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, REED);
		for (j = 0; doGen && j < this.reedsPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			i1 = this.randomGenerator.nextInt(128);
			this.reedGen.generate(this.currentWorld, this.randomGenerator, k, i1, l);
		}

		for (j = 0; doGen && j < 10; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.randomGenerator.nextInt(128);
			i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.reedGen.generate(this.currentWorld, this.randomGenerator, k, l, i1);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, PUMPKIN);
		if (doGen && this.randomGenerator.nextInt(32) == 0)
		{
			j = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			k = this.randomGenerator.nextInt(128);
			l = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			(new WorldGenPumpkin()).generate(this.currentWorld, this.randomGenerator, j, k, l);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, CACTUS);
		for (j = 0; doGen && j < this.cactiPerChunk; ++j)
		{
			k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
			l = this.randomGenerator.nextInt(128);
			i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
			this.cactusGen.generate(this.currentWorld, this.randomGenerator, k, l, i1);
		}

		doGen = TerrainGen.decorate(currentWorld, randomGenerator, chunk_X, chunk_Z, LAKE);///This is actually generate fountain....
		if (doGen && this.generateLakes)
		{
			for (j = 0; j < 50; ++j)
			{
				k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
				l = this.randomGenerator.nextInt(this.randomGenerator.nextInt(120) + 8);
				i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
				(new WorldGenLiquids(Blocks.water)).generate(this.currentWorld, this.randomGenerator, k, l, i1);
			}

			for (j = 0; j < 20; ++j)
			{
				k = this.chunk_X + this.randomGenerator.nextInt(16) + 8;
				l = this.randomGenerator.nextInt(this.randomGenerator.nextInt(this.randomGenerator.nextInt(112) + 8) + 8);
				i1 = this.chunk_Z + this.randomGenerator.nextInt(16) + 8;
				(new WorldGenLiquids(Blocks.lava)).generate(this.currentWorld, this.randomGenerator, k, l, i1);
			}
		}

		MinecraftForge.EVENT_BUS.post(new DecorateBiomeEvent.Post(currentWorld, randomGenerator, chunk_X, chunk_Z));
	}
}
