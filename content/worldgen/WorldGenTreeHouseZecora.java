package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.entities.EntityItemMask;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.world.World;

public class WorldGenTreeHouseZecora extends WorldGenSchematicBase
{
	public WorldGenTreeHouseZecora()
	{
		build = new NBTSchematic("ZecoraHut.schematic");
		if (build != null)
		{
			build.setIgnoreAir(true);
			build.printSchematicDEBUG();
		}
		offsetX = -12;
		offsetY = -5;
		offsetZ = -12;
	}

	@Override
	public boolean generate(World world, Random random)
	{
		int[] xz;
		
		//Debug: zecora worldgen
		System.out.println("-----[[[ZECORA]]]-----X:"+coordX+"  Y:"+coordY+"  Z:"+coordZ);
		
		rotation = random.nextInt(4);
		
		//clear the area a bit for the interior
		for (int x2 = 8; x2 < 17; x2++)
		{
			for (int z2 = 8; z2 < 17; z2++)
			{
				if ((x2 == 8 || x2 == 16) && ( z2 == 8 || z2 == 16))
					continue;//cut corners
				xz = getWorldCoordFromSchematicCoord(x2, z2);
				for (int y2 = coordY; y2 < coordY+6; y2++)
					world.setBlockToAir(xz[0], y2, xz[1]);
			}
		}
		clearSpace(world, 17, 5, 11, 19, 7, 13);
		
		//print build
		printBuild(world);
		
		//add bed
		xz = getWorldCoordFromSchematicCoord(19, 12);
		placeBed(world, xz[0], coordY, xz[1], (2+rotation)&3, 32);
		
		//add masks
		EntityItemMask hang;
		
		//Skeley
		xz = getWorldCoordFromSchematicCoord(13, 17);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+2, xz[1], (2+rotation)&3);
		hang.setMaskType(0);
		world.spawnEntityInWorld(hang);
		
		xz = getWorldCoordFromSchematicCoord(8, 9);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+2, xz[1], (3+rotation)&3);
		hang.setMaskType(0);
		world.spawnEntityInWorld(hang);
		
		//Creeper
		xz = getWorldCoordFromSchematicCoord(13, 7);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+3, xz[1], (0+rotation)&3);
		hang.setMaskType(1);
		world.spawnEntityInWorld(hang);
		
		//Zombie
		xz = getWorldCoordFromSchematicCoord(16, 15);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+3, xz[1], (1+rotation)&3);
		hang.setMaskType(2);
		world.spawnEntityInWorld(hang);
		
		xz = getWorldCoordFromSchematicCoord(8, 15);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+3, xz[1], (3+rotation)&3);
		hang.setMaskType(2);
		world.spawnEntityInWorld(hang);
		
		//Steve
		xz = getWorldCoordFromSchematicCoord(16, 10);
		hang = (EntityItemMask)LOE_Items.mask.createHangingEntity(world, xz[0], coordY+2, xz[1], (1+rotation)&3);
		hang.setMaskType(3);
		world.spawnEntityInWorld(hang);
		
		return true;
	}
}
