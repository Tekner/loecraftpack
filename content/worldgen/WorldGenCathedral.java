package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.blocks.BlockDungeonChest;
import loecraftpack.content.blocks.tile.TileDungeonChest;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;

public class WorldGenCathedral extends WorldGenSchematicBase
{
	public static final WeightedRandomChestContent[] crapToGenerateInCathedral =
			new WeightedRandomChestContent[] {new WeightedRandomChestContent(Items.bone, 0, 4, 6, 20),
											  new WeightedRandomChestContent(Items.rotten_flesh, 0, 3, 7, 16)};
	public static final WeightedRandomChestContent[] lootToGenerateInCathedral =
			new WeightedRandomChestContent[] {new WeightedRandomChestContent(Items.diamond, 0, 1, 3, 3),
											  new WeightedRandomChestContent(Items.gold_ingot, 0, 2, 7, 15),
											  new WeightedRandomChestContent(Items.emerald, 0, 2, 5, 5),
											  new WeightedRandomChestContent(Items.golden_helmet, 0, 1, 1, 2),
											  new WeightedRandomChestContent(Items.golden_chestplate, 0, 1, 1, 2),
											  new WeightedRandomChestContent(Items.golden_helmet, 0, 1, 1, 2),
											  new WeightedRandomChestContent(Items.golden_boots, 0, 1, 1, 2)};
	
	public WorldGenCathedral()
	{
		build = new NBTSchematic("Cathedral.schematic");
		if (build != null)
		{
			build.setIgnoreAir(true);
			build.printSchematicDEBUG();
		}
		offsetX = -15;
		offsetY = -6;
		offsetZ = -12;
	}
	
	@Override
	public boolean generate(World world, Random random)
	{
		int[] xz;
		
		//Debug: cathedral worldgen
		System.out.println("-----[[[CATHERDRAL]]]-----X:"+coordX+"  Y:"+coordY+"  Z:"+coordZ);
		
		rotation = random.nextInt(2)*2;
		
		////Raise Terrain
		
		//pillar 1
		fillSpace(world, Blocks.stonebrick, 0, 6, -20, 1, 7, -1, 1);
		fillSpace(world, Blocks.stonebrick, 0, 7, -20, 0, 7, -1, 0);
		
		//pillar 2
		fillSpace(world, Blocks.stonebrick, 0, 13, -20, 0, 13, -1, 1);
		
		//pillar 3
		fillSpace(world, Blocks.stonebrick, 0, 19, -20, 0, 19, -1, 1);
		
		//pillar 4
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 1, 26, -1, 1);
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 0, 25, -1, 0);
		
		//pillar 5
		fillSpace(world, Blocks.stonebrick, 0, 6, -20, 8, 7, -1, 8);
		
		//pillar 6
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 8, 26, -1, 8);
		
		//pillar 7
		fillSpace(world, Blocks.stonebrick, 0, 6, -20, 16, 7, -1, 16);
		
		//pillar 8
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 16, 26, -1, 16);
		
		//pillar 9
		fillSpace(world, Blocks.stonebrick, 0, 6, -20, 23, 7, -1, 23);
		fillSpace(world, Blocks.stonebrick, 0, 7, -20, 24, 7, -1, 24);
		
		//pillar 10
		fillSpace(world, Blocks.stonebrick, 0, 13, -20, 23, 13, -1, 24);
		
		//pillar 11
		fillSpace(world, Blocks.stonebrick, 0, 19, -20, 23, 19, -1, 24);
		
		//pillar 12
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 23, 26, -1, 23);
		fillSpace(world, Blocks.stonebrick, 0, 25, -20, 24, 25, -1, 24);
		
		//dirt
		raiseTerrain(world, Blocks.dirt, 0, 7, -3, 1, 26, 6, 23);
		
		
		////clear the area a bit
		clearSpace(world, -1, 7, -2, 31, 21, 26);
		
		////clear the basement
		
		//stairs
		for (int i = 0; i < 4; i++)
		{
			for (int y2 = coordY-4+i; y2 <= coordY; y2++)
			{
				for (int z2 = 11; z2 <= 13; z2++)
				{
					xz = getWorldCoordFromSchematicCoord(19+i, z2);
					world.setBlockToAir(xz[0], y2, xz[1]);
				}
			}
		}
		clearSpace(world, 17, 1, 11, 18, 5, 13);
		
		//main room
		clearSpace(world, 4, 1, 9, 16, 5, 15);
		
		//end piece
		clearSpace(world, 1, 1, 11, 3, 5, 13);
		
		///add coffins
		BlockDungeonChest coffinBlock = LOE_Blocks.coffin;
		
		//trap
		xz = getWorldCoordFromSchematicCoord(3, 12);
		placeCoffin(world, xz[0], coordY+(-4), xz[1], (rotation)&3, true);
		IInventory inventory = coffinBlock.getInventory(world, xz[0], coordY+(-4), xz[1]);
		inventory.setInventorySlotContents(0, new ItemStack(Items.spawn_egg, 5, 51));
		addChestLoot(inventory, random, 3, crapToGenerateInCathedral);
		addChestLoot(inventory, random, 2, lootToGenerateInCathedral);
		TileDungeonChest tile = (TileDungeonChest)world.getTileEntity(xz[0], coordY+(-4), xz[1]);
		xz = getTranslationRotateBBFloating(4, -3, 12, 3);
		tile.spawnMinX=xz[0];
		tile.spawnMinY=-1;
		tile.spawnMinZ=xz[1];
		tile.spawnMaxX=xz[2];
		tile.spawnMaxY=-1;
		tile.spawnMaxZ=xz[3];
		xz = getWorldCoordFromSchematicCoord(4, 12);
		tile = (TileDungeonChest)world.getTileEntity(xz[0], coordY+(-4), xz[1]);
		xz = getTranslationRotateBBFloating(3, -3, 11, 3);
		tile.spawnMinX=xz[0];
		tile.spawnMinY=-1;
		tile.spawnMinZ=xz[1];
		tile.spawnMaxX=xz[2];
		tile.spawnMaxY=-1;
		tile.spawnMaxZ=xz[3];
		
		//north wall
		xz = getWorldCoordFromSchematicCoord(7, 8);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), false);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), false);
		
		xz = getWorldCoordFromSchematicCoord(10, 8);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), false);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), false);
		
		xz = getWorldCoordFromSchematicCoord(13, 8);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), false);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), false);
		
		//south wall
		xz = getWorldCoordFromSchematicCoord(8, 16);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), true);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), true);
		
		xz = getWorldCoordFromSchematicCoord(11, 16);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), true);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), true);
		
		xz = getWorldCoordFromSchematicCoord(14, 16);
		placeCoffin(world, xz[0], coordY+(-5), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-5), xz[1]), true);
		
		placeCoffin(world, xz[0], coordY+(-3), xz[1], (2+rotation)&3, true);
		placeBones(coffinBlock.getInventory(world, xz[0], coordY+(-3), xz[1]), true);
		
		//print build
		printBuild(world);
		
		//grass-fy
		// -1, 0, -2 : 31, 0, 26
		xz = getWorldBBFromSchematicBB(-1, -2, 31, 26);
		for (int x2 = xz[0]; x2 <= xz[2]; x2++)
			for (int z2 = xz[1]; z2 <= xz[3]; z2++)
			{
				if (Block.isEqualTo(world.getBlock(x2, coordY, z2), Blocks.dirt))
					world.setBlock(x2, coordY, z2, Blocks.grass, 0, 2/*do everything but update neighbors.*/);
			}
		
		return true;
	}
	
	
}
