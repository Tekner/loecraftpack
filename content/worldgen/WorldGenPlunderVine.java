package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.blocks.BlockAppleBloomSapling;
import loecraftpack.content.registery.LOE_Blocks;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import static net.minecraftforge.common.util.ForgeDirection.*;
import net.minecraftforge.common.IPlantable;

public class WorldGenPlunderVine extends WorldGenerator {
	
	int minHeight, maxHeight;
	Block block = LOE_Blocks.plunderVine;
	
	public WorldGenPlunderVine(int minHeight, int maxHeight)
	{
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
	}

	@Override
	public boolean generate(World world, Random random, int xCoord, int yCoord, int zCoord) {
		int height = random.nextInt(maxHeight - minHeight) + minHeight;
		if (yCoord >= 1 && yCoord + height <= 256)
		{
			
			Block block2;
			for(int i=0; i<height; i++)
			{
				block2 = world.getBlock(xCoord, yCoord + i, zCoord);
				
				if (block2 != null &&
					!block2.isAir(world, xCoord, yCoord + i, zCoord) &&
					!block2.isLeaves(world, xCoord, yCoord + i, zCoord) &&
					!Block.isEqualTo(block2, block) &&
					!Block.isEqualTo(block2, Blocks.grass) &&
					!Block.isEqualTo(block2, Blocks.tallgrass) &&
					!Block.isEqualTo(block2, Blocks.double_plant) &&
					!Block.isEqualTo(block2, Blocks.dirt) &&
					!block2.isWood(world, xCoord, yCoord + i, zCoord) &&
					!Block.isEqualTo(block2, Blocks.stone) &&
					!Block.isEqualTo(block2, Blocks.gravel) &&
					!Block.isEqualTo(block2, Blocks.sand))
				{
					return false;
				}
			}
			Block soil = world.getBlock(xCoord, yCoord - 1, zCoord);
			if (soil != null && soil.canSustainPlant(world, xCoord, yCoord - 1, zCoord, UP, (IPlantable)block))
			{
				soil.onPlantGrow(world, xCoord, yCoord - 1, zCoord, xCoord, yCoord, zCoord);
				for (int i = 0; i < height; i++)
				{
					block2 = world.getBlock(xCoord, yCoord + i, zCoord);

					if (block2 == null || block2.isAir(world, xCoord, yCoord + i, zCoord) || block2.isLeaves(world, xCoord, yCoord + i, zCoord) || Block.isEqualTo(block2, block) || Block.isEqualTo(block2, Blocks.tallgrass) || Block.isEqualTo(block2, Blocks.double_plant))
					{
						this.setBlockAndNotifyAdequately(world, xCoord, yCoord + i, zCoord, block, 0);
					}
				}
			}
		}
		return false;
	}

}
