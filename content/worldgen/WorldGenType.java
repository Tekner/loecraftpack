package loecraftpack.content.worldgen;

public enum WorldGenType
{
	ZECORA,
	CATHEDRAL,
	TWILIGHT,
	SMALL_RUIN
}
