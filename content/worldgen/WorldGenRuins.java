package loecraftpack.content.worldgen;

import java.util.Random;

import loecraftpack.content.registery.LOE_Items;
import loecraftpack.content.schematic.NBTSchematic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.DungeonHooks;

public class WorldGenRuins extends WorldGenerator
{
	public static final int[] mobIDs = {};
	public static final WeightedRandomChestContent[] lootToGenerateInRuin =
			new WeightedRandomChestContent[] {new WeightedRandomChestContent(Items.golden_apple, 0, 1, 2, 3),
											  new WeightedRandomChestContent(Items.iron_ingot, 0, 2, 7, 20),
											  new WeightedRandomChestContent(Items.gold_ingot, 0, 2, 5, 15),
											  new WeightedRandomChestContent(Items.emerald, 0, 2, 4, 3),
											  new WeightedRandomChestContent(Items.iron_helmet, 0, 1, 1, 5),
											  new WeightedRandomChestContent(Items.iron_chestplate, 0, 1, 1, 5),
											  new WeightedRandomChestContent(Items.iron_leggings, 0, 1, 1, 5),
											  new WeightedRandomChestContent(Items.iron_boots, 0, 1, 1, 5),
											  new WeightedRandomChestContent(Items.iron_sword, 0, 1, 1, 10),
											  new WeightedRandomChestContent(LOE_Items.zapApple, 0, 1, 4, 5),
											  new WeightedRandomChestContent(LOE_Items.pickaxeGem, 0, 1, 1, 3)};
	
	WorldGenSchematicBase componentBasic;
	WorldGenSchematicBase componentBasement;
	
	public WorldGenRuins()
	{
		componentBasic = new WorldGenRuinsBasic();
		componentBasement = new WorldGenRuinsBasement();
	}
	
	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		//Debug: ruin worldgen
		System.out.println("-----[[[RUIN]]]-----X:"+x+"  Y:"+y+"  Z:"+z);
		
		boolean result = true;
		
		int rotation = random.nextInt(4);
		componentBasic.rotation = rotation;
		componentBasement.rotation = rotation;
		
		if (random.nextFloat() < 0.5f)
		{
			if (!componentBasic.generate(world, random, x, y, z))
				result = false;
			if (!componentBasement.generate(world, random, x, y, z))
				result = false;
		}
		else
		{
			if (!componentBasic.generate(world, random, x, y, z))
				result = false;
		}
		
		return result;//this is false if a schematic is missing.
	}
	
	
	
	
	public class WorldGenRuinsBasic extends WorldGenSchematicBase
	{
		
		WorldGenRuinsBasic()
		{
			build = new NBTSchematic("SmallRuinsBasic.schematic");
			if (build != null)
			{
				build.setIgnoreAir(true);
				build.printSchematicDEBUG();
			}
			
			offsetX = -5;
			offsetY = 0;
			offsetZ = -5;
		}

		@Override
		protected boolean generate(World world, Random random)
		{
			int[] xz;
			
			clearSpace(world, 2, 0, 2, 8, 5, 8);
			clearSpace(world, 3, 0, 1, 7, 5, 1);
			clearSpace(world, 3, 0, 9, 7, 5, 9);
			clearSpace(world, 1, 0, 3, 1, 5, 7);
			clearSpace(world, 9, 0, 3, 9, 5, 7);
			
			printBuild(world);
			
			BlockChest chestBlock = Blocks.chest;
			xz = getWorldCoordFromSchematicCoord(9, 5);
			IInventory inventory = chestBlock.func_149951_m(world, xz[0], coordY+(2), xz[1]);
			if (inventory != null)
				addChestLoot(inventory, random, 5, lootToGenerateInRuin);
			
			return true;
		}
	}
	
	
	
	
	public class WorldGenRuinsBasement extends WorldGenSchematicBase
	{
		
		WorldGenRuinsBasement()
		{
			build = new NBTSchematic("SmallRuinsBasement.schematic");
			if (build != null)
			{
				build.setIgnoreAir(true);
				build.printSchematicDEBUG();
			}
			
			offsetX = -5;
			offsetY = -5;
			offsetZ = -5;
		}
		
		@Override
		protected boolean generate(World world, Random random)
		{
			int[] xz;
			
			clearSpace(world, 2, 1, 2, 8, 4, 8);
			clearSpace(world, 3, 1, 1, 7, 4, 1);
			clearSpace(world, 3, 1, 9, 7, 4, 9);
			clearSpace(world, 1, 1, 3, 1, 4, 7);
			clearSpace(world, 9, 1, 3, 9, 4, 7);
			
			printBuild(world);
			
			xz = getWorldCoordFromSchematicCoord(5, 5);
			world.setBlock(xz[0], coordY+offsetY, xz[1], Blocks.mob_spawner, 0, 3);
			TileEntityMobSpawner tileentitymobspawner = (TileEntityMobSpawner)world.getTileEntity(xz[0], coordY+offsetY, xz[1]);
			if (tileentitymobspawner != null)
			{
				tileentitymobspawner.func_145881_a().setEntityName(DungeonHooks.getRandomDungeonMob(random));
			}
			
			return true;
		}
	}
}
