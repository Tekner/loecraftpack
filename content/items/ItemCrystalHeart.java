package loecraftpack.content.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;

public class ItemCrystalHeart extends Item {

	public ItemCrystalHeart() {
		this.maxStackSize = 1;
		this.setMaxDamage(0);
        this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
    
    @Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
    	itemIcon = iconRegister.registerIcon("loecraftpack:tools/crystalheart");
	}
}