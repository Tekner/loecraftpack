package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.EntityItemMask;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.IIcon;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;
import net.minecraftforge.common.ChestGenHooks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMask extends ItemCustomHangingEntity{
	
	protected static final String rescource_location = "loecraftpack:masks/";
	protected List<IIcon> icons;
	protected String[] names;
	
	public ItemMask(Class entityClass) {
		super(entityClass);
		this.setHasSubtypes(true);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		
		names = new String[]{/*0*/"death"     , /*1*/"happy"     , /*2*/"hungry"     , /*3*/"steve"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	@Override
	public boolean isValidArmor(ItemStack stack, int armorType, Entity entity)
	{
		if (armorType == 0)//helmet
		{
			return true;
		}
		return false;
	}
	
	@Override
	protected EntityHanging createHangingEntity(ItemStack itemStack, EntityPlayer entityPlayer, World world, int xPos, int yPos, int zPos, int direction3D, float par8, float par9, float par10)
	{
		int direction = Direction.facingToDirection[direction3D];
		EntityItemMask hang = (EntityItemMask)createHangingEntity(world, xPos, yPos, zPos, direction);
		hang.setMaskType(itemStack.getItemDamage());
		return hang; 
	}
	
	public WeightedRandomChestContent getChestGenBase(ChestGenHooks chest, Random rand, WeightedRandomChestContent original)
	{
		if (original.theItemId.getItemDamage()==1111)
		{
			int meta = rand.nextInt(names.length);
			return new WeightedRandomChestContent(this, meta, original.theMinimumChanceToGenerateItem, original.theMaximumChanceToGenerateItem, original.itemWeight);
		}
		return original;
	}
}
