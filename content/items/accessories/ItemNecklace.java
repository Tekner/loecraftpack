package loecraftpack.content.items.accessories;

import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Plain necklace
 */
public class ItemNecklace extends ItemAccessory {

	public ItemNecklace() {
		super();
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/necklace");
	}

}
