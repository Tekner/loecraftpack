package loecraftpack.content.items.accessories;

import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Plain ring
 */
public class ItemRing extends ItemAccessory {
	
	public ItemRing() {
		super();
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/ring");
	}
}