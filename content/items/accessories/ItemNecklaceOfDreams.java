package loecraftpack.content.items.accessories;

import loecraftpack.content.ponies.inventory.InventoryCustom;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemNecklaceOfDreams extends ItemNecklace {

	public ItemNecklaceOfDreams() {
		super();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/necklace_dream");
	}
	
	@Override
	public void onSleep(PlayerSleepInBedEvent event, EntityPlayer player, InventoryCustom inv, int slot, ItemStack itemStack)
	{
		System.out.println("ZZZZZ"+player.getGameProfile().getId());
		
		//Teleport to dreamWorld
	}

}
