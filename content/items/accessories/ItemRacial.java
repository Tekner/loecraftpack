package loecraftpack.content.items.accessories;

import loecraftpack.content.items.IRacialItem;

public abstract class ItemRacial extends ItemAccessory implements IRacialItem{
	
	public ItemRacial() {
		super();
	}
	
}
