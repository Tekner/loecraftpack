package loecraftpack.content.items.accessories;

import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemAlicornAmulet extends ItemNecklace
{

	public ItemAlicornAmulet() {
		super();
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/necklace_alicorn_amulet");
	}
	
	@Override
	public int getStatBoost(int meta, Race race, Stat stat)
	{
		if (stat.isType(Stats.Strength) || stat.isType(Stats.Magic) || stat.isType(Stats.Agility))
			return 3;
		else if (stat.isType(Stats.Offense) || stat.isType(Stats.Defense) || stat.isType(Stats.Utility))
			return 1;
		else
			return 0;
	}
	
	@Override
	protected void getStatBoostData(ItemStack itemStack, EntityPlayer player, List text, boolean bool)
	{
		addLine(text, "&5Strength&7 - 3");
		addLine(text, "&5Magic&7 - 3");
		addLine(text, "&5Agility&7 - 3");
		addLine(text, "&5Offense&7 - 1");
		addLine(text, "&5Defense&7 - 1");
		addLine(text, "&5Utility&7 - 1");
	}
}
