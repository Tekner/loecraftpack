package loecraftpack.content.items.accessories;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.inventory.InventoryCustom;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityFireworkRocket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2APacketParticles;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemNecklaceOfBling extends ItemNecklace {

	public ItemNecklaceOfBling() {
		super();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/necklace_bling");
	}
	
	@Override
	public void applyWornEffect(EntityPlayer player, InventoryCustom inv, int slot, ItemStack itemStack)
	{
		((EntityPlayerMP)player).playerNetServerHandler.sendPacket(new S2APacketParticles("happyVillager",
				(float)player.posX-player.width/2, (float)player.posY, (float)player.posZ-player.width/2,
				player.width, player.height, player.width,
				0.0f, 5));
	}
	
	//Flashy Entrance
	@Override
	public void onPlayerJoin(EntityPlayer player, InventoryCustom inv, int slot, ItemStack itemStack)
	{
		ChatComponentText message = (new ChatComponentText("Behold the Great and Powerful "+player.getGameProfile().getName()));
		MinecraftServer.getServer().getConfigurationManager().sendChatMsg(message);
		
		if (itemStack.getTagCompound() != null && itemStack.getTagCompound().getCompoundTag("Fireworks") != null)
		{
			EntityFireworkRocket entityfireworkrocket = new EntityFireworkRocket(player.worldObj, player.posX, player.posY, player.posZ, itemStack);
			player.worldObj.spawnEntityInWorld(entityfireworkrocket);
		}
		
		//EntityFireworkRocket
		//EntityFireworkStarterFX
	}
}
