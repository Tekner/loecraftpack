package loecraftpack.content.items.accessories;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemRingOf extends ItemRing
{
	protected static final String rescource_location = "loecraftpack:tools/ring_of_";
	List<IIcon> icons = null;
	protected String[] names;
	
	public ItemRingOf()
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		
		names = new String[]{/*0*/"power", /*1*/"astral", /*2*/"agility", /*3*/"violence", /*4*/"endurance", /*5*/"utility"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	@Override
	public int getStatBoost(int meta, Race race, Stat stat)
	{
		if ((meta == 0 && stat.isType(Stats.Strength)) || (meta == 1 && stat.isType(Stats.Magic)) || (meta == 2 && stat.isType(Stats.Agility)))
			return 3;
		else if ((meta == 3 && stat.isType(Stats.Offense)) || (meta == 4 && stat.isType(Stats.Defense)) || (meta == 5 && stat.isType(Stats.Utility)))
			return 1;
		else
			return 0;
	}
	
	@Override
	protected void getStatBoostData(ItemStack itemStack, EntityPlayer player, List text, boolean bool)
	{
		switch (itemStack.getItemDamage())
		{
		case 0:
			addLine(text, "&5Strength&7 - 3");
			break;
			
		case 1:
			addLine(text, "&5Magic&7 - 3");
			break;
			
		case 2:
			addLine(text, "&5Agility&7 - 3");
			break;
			
		case 3:
			addLine(text, "&5Attack&7 - 1");
			break;
			
		case 4:
			addLine(text, "&5Defense&7 - 1");
			break;
			
		case 5:
			addLine(text, "&5Utility&7 - 1");
			break;
		}
	}
}
