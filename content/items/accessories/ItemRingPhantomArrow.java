package loecraftpack.content.items.accessories;

import java.util.Random;

import loecraftpack.content.entities.arrow.EntityPhantomArrow;
import loecraftpack.content.ponies.inventory.InventoryCustom;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemRingPhantomArrow extends ItemRing {

	public ItemRingPhantomArrow() {
		super();
		this.setMaxDamage(192);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/ring_phantomArrow");
	}
	
	public void onArrowLoose(ArrowLooseEvent event, EntityPlayer player, InventoryCustom inv, int slot, ItemStack itemStack)
	{
		ItemStack itemStackBow = event.bow;
		World world = player.worldObj;
		
		int shoots = MathHelper.clamp_int((event.charge-20)/20, 0, 3);
		if (shoots>0)
		{
			Random rand = new Random();
			int charge = event.charge;
			
			boolean flag = player.capabilities.isCreativeMode || EnchantmentHelper.getEnchantmentLevel(Enchantment.infinity.effectId, itemStackBow) > 0;
			
			if (flag || player.inventory.hasItem(Items.arrow))
			{
				float chargeLevel = (float)charge / 20.0F;
				chargeLevel = (chargeLevel * chargeLevel + chargeLevel * 2.0F) / 3.0F;
				
				if ((double)chargeLevel < 0.1D)
				{
					return;
				}
				
				if (chargeLevel > 1.0F)
				{
					chargeLevel = 1.0F;
				}
				
				float angle = (rand.nextFloat()-0.5f)*(float)Math.PI;
				
				for(int i=0; i<shoots; i++)
				{
					EntityPhantomArrow entityarrow = new EntityPhantomArrow(world, player, chargeLevel * 2.0F, rand, angle);
					if(shoots>1)angle += (2.0f-(float)(1/shoots)- (rand.nextFloat() * (1.0f+2*(0.5f-(float)(1/shoots))) ) )*(float)Math.PI/(float)(shoots-1);
					
					
					if (chargeLevel == 1.0F)
					{
						entityarrow.setIsCritical(true);
					}
					
					int enchPowerLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, itemStackBow);
					
					if (enchPowerLevel > 0)
					{
						entityarrow.setDamage(entityarrow.getDamage() + (double)enchPowerLevel * 0.5D + 0.5D);
					}
					
					int enchKnockbackLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, itemStackBow);
					
					if (enchKnockbackLevel > 0)
					{
						entityarrow.setKnockbackStrength(enchKnockbackLevel);
					}
					
					if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, itemStackBow) > 0)
					{
						entityarrow.setFire(100);
					}
					
					itemStackBow.damageItem(1, player);
					world.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + chargeLevel * 0.5F);
					
					if (flag)
					{
						entityarrow.canBePickedUp = 2;
					}
					else
					{
						//damage ring
						itemStack.damageItem(1, player);
					}
					
					if (!world.isRemote)
					{
						world.spawnEntityInWorld(entityarrow);
					}
				}
			}
		}
	}

}
