package loecraftpack.content.items;

import loecraftpack.referrance.enums.Race;

public interface IRacialItem
{
	/**
	 * Determines which races can use this Item
	 */
	public abstract boolean canBeUsedBy(Race race);
}