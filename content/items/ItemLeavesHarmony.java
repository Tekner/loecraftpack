package loecraftpack.content.items;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.BlockAppleBloomLeaves;
import loecraftpack.content.blocks.BlockHarmonyLeaves;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLeavesHarmony extends ItemBlock
{
	protected int bloomStage = 2;
	protected BlockHarmonyLeaves leaf;
	
	//Preffered constructor, to allow more than 1 blockID
	public ItemLeavesHarmony(Block leaf)
    {
        super(leaf);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
        this.leaf = (BlockHarmonyLeaves)leaf;
        System.out.println("leaf passed: "+ this.leaf);
    }
	
    public int getMetadata(int par1)
    {
        return par1 | 4;
    }

    @SideOnly(Side.CLIENT)

    /**
     * Gets an icon index based on an item's damage value
     */
    public IIcon getIconFromDamage(int meta)
    {
    	return leaf.getIcon(0, meta);
    }

    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
        int meta = par1ItemStack.getItemDamage();
        return leaf.getRenderColor(meta);
    }
}
