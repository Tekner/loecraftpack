package loecraftpack.content.items;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import loecraftpack.content.entities.arrow.EntityCustomArrow;

public interface ICustomArrow {

	public abstract EntityCustomArrow createEntity(World world, EntityLivingBase entityLiving, float speed);
}
