package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemZapApple extends ItemFood
{
	protected static final String rescource_location = "loecraftpack:food/zapapple_";
	public static List<IIcon> icons = new ArrayList<IIcon>();
	protected String[] names;
	
	public ItemZapApple(int heal, float saturation, boolean wolf)
	{
		super(heal, saturation, wolf);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		
		names = new String[]{/*0*/"unripe", /*1*/"charged", /*2*/"sickly", /*3*/"cooked"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return null;
	}

	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack itemStack)
	{
		return itemStack.getItemDamage() % 2 == 1;
	}
	

	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack itemStack)
	{
		int meta = itemStack.getItemDamage();
		if (meta == 0)
			return EnumRarity.uncommon;
		else if (meta == 1)
			return EnumRarity.epic;
		else if (meta == 2)
			return EnumRarity.common;
		else
			return EnumRarity.rare;
	}

	protected void onFoodEaten(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (!world.isRemote)
		{
			//Do: ZapApple - Define Potion Effects
			int meta = itemStack.getItemDamage();
			if (meta == 0)
			{
				entityPlayer.addPotionEffect(new PotionEffect(LoECraftPack.potionCharged.id, 1000, 0));
			}
			else if (meta == 1)
			{
				entityPlayer.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 3000, 0));
				entityPlayer.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 3000, 0));
			}
			else if (meta == 2)
			{
				entityPlayer.addPotionEffect(new PotionEffect(Potion.confusion.id, 200, 0));
				entityPlayer.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 200, 1));
				entityPlayer.addPotionEffect(new PotionEffect(Potion.poison.id, 100, 0));
			}
			else
			{
				entityPlayer.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 1500, 0));
				entityPlayer.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 1500, 0));
			}
			
		}
	}
}