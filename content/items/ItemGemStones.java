package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.LoECraftPack;
import loecraftpack.content.potions.custombrewing.ICustomPotionIngredient;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemGemStones extends Item implements ICustomPotionIngredient
{
	protected static final String rescource_location = "loecraftpack:gems/";
	protected List<IIcon> icons;
	protected String[] names;
	
	protected static String[] gemDataNames = {/*0*/"sapphire", /*1*/"fireruby" , /*2*/"citrine", "3", /*4*/"phantomamethyst" , "5", "6", /*7*/"onyx", /*8*/"temp",
		/*9*/"shardheart", /*10*/"shardlaughter", /*11*/"shardgenerosity", /*12*/"shardkindness", /*13*/"shardmagic", /*14*/"shardloyalty", /*15*/"shardhonesty"};
	
	public ItemGemStones()
	{
		this(gemDataNames);
	}
	
	public ItemGemStones(String[] gemDataNames)
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		
		names = gemDataNames;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	public WeightedRandomChestContent getChestGenBase(ChestGenHooks chest, Random rand, WeightedRandomChestContent original)
	{
		if (original.theItemId.getItemDamage()==1111)
		{
			int meta;
			if (rand.nextFloat()<0.2f)
				meta = rand.nextInt(6)+10;
			else
				meta = rand.nextInt(10);
			
			int min = original.theMinimumChanceToGenerateItem;
			if (min < 0)
				min = meta<=8? 4: 1;
			
			int max = original.theMaximumChanceToGenerateItem;
			if (max < 0)
				max = meta<=8? 9: 3;
			
			return new WeightedRandomChestContent(this, meta, min, max, original.itemWeight);
		}
		return original;
	}

	@Override
	public boolean validIngredient(ItemStack stack)
	{
		int meta = stack.getItemDamage();
		return meta >= 10 && meta <= 15;
	}
	
	public String[] getNames()
	{
		return names;
	}
}
