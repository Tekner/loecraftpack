package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.stats.Stats;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemDrink extends ItemFood
{
	protected static final String rescource_location = "loecraftpack:food/drink/";
	protected List<IIcon> icons;
	protected String[]names;
	
	public ItemDrink() {
		super(5, false);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.maxStackSize = 6;
		setAlwaysEdible();
		
		names = new String[]{/*0*/"diet", /*1*/"apple", /*2*/"fusion", /*3*/"max", /*4*/"cider", /*5*/null, /*6*/null, /*7*/null, /*8*/"dragon"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	@Override
	public ItemStack onEaten(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		ItemStack emptyBottle = new ItemStack(Items.glass_bottle);
		super.onEaten(itemStack, world, entityPlayer);
		if (!entityPlayer.inventory.addItemStackToInventory(emptyBottle) && !world.isRemote)
			world.spawnEntityInWorld(new EntityItem(world, entityPlayer.posX, entityPlayer.posY, entityPlayer.posZ, emptyBottle));
	
		return itemStack;
	}
	
	@Override
	public void onFoodEaten(ItemStack itemStack, World world, EntityPlayer player)
	{
		int i = itemStack.getItemDamage();
		if (!world.isRemote)
			switch (i)
			{
			//energy drinks//
				/**DIET**/
			case 0:
				if (world.rand.nextFloat() < 0.95f)
				{
					player.addPotionEffect(new PotionEffect(LoECraftPack.potionEnergy.id, 1 * 20, 0));
				}
				break;
				
				/**APPLE**/
			case 1:
				player.addPotionEffect(new PotionEffect(LoECraftPack.potionEnergy.id, 1 * 20, 1));
				break;
				
				/**FUSION**/
			case 2:
				//player.addPotionEffect(new PotionEffect(LoECraftPack.potionEnergy.id, 1 * 20, 2));
				player.addExperience(30 + world.rand.nextInt(50) + world.rand.nextInt(50));
				break;
				
				/**MAX**/
			case 3:
				//player.addPotionEffect(new PotionEffect(LoECraftPack.potionEnergy.id, 1 * 20, 5));
				player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 60 * 20, 0));
				player.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 60 * 20, 0));
				player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 60 * 20, 0));
				player.addPotionEffect(new PotionEffect(Potion.jump.id, 60 * 20, 0));
				break;
				
			//recreation drinks//
				/**CIDER**/
			case 4:
				PotionEffect effectFilling = player.getActivePotionEffect(LoECraftPack.potionWellFed);
				if (effectFilling!=null)
				{
					if (effectFilling.getAmplifier() == 0)
						effectFilling.duration += 15 * 20;
					else
						effectFilling.duration += (15 * 20) / (effectFilling.getAmplifier()+1);
				}
				else
					player.addPotionEffect(new PotionEffect(LoECraftPack.potionWellFed.id, 15 * 20, 0));
				
				PotionEffect effectDrunk = player.getActivePotionEffect(Potion.confusion);
				if (effectDrunk==null)
					player.addPotionEffect(new PotionEffect(Potion.confusion.id, 60 * 20, 0));
				else
					effectDrunk.duration += 60 * 20;
				break;
				
			//special drinks//
			case 8:
				Stats stats = AbilityPlayerData.Get(player.getGameProfile().getId()).getPlayerStats();
				stats.dragonFuel.setValue(Math.min(stats.dragonFuel.getValue()+3, 5));
				break;
			}
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
	{
		switch(itemStack.getItemDamage())
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 8:
			player.setItemInUse(itemStack, this.getMaxItemUseDuration(itemStack));
			break;
			
		default:
			super.onItemRightClick(itemStack, world, player);
		}
		
		return itemStack;
	}
}
