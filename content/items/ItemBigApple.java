package loecraftpack.content.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class ItemBigApple extends ItemFood
{
	public ItemBigApple()
	{
		super(0, 0, false);
	}
	
	@Override
	public void onFoodEaten(ItemStack itemStack, World world, EntityPlayer player)
    {
		player.addPotionEffect(new PotionEffect(LoECraftPack.potionWellFed.id, 600, 0));
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
	    itemIcon = iconRegister.registerIcon("loecraftpack:food/apple");
	}
}
