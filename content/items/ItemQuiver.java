package loecraftpack.content.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.LoECraftPack;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class ItemQuiver extends Item
{
	public ItemQuiver()
	{
		super();
		this.maxStackSize = 1;
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player)
	{
		if (!world.isRemote)
		{
			player.openGui(LoECraftPack.instance,
							GuiIds.QUIVER.ordinal(),
							MinecraftServer.getServer().worldServerForDimension(player.dimension),
							(int)player.posX,
							(int)player.posY,
							(int)player.posZ);
		}
		return itemstack;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return itemIcon;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/quiver");
	}
}
