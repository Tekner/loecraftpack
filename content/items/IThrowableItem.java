package loecraftpack.content.items;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public interface IThrowableItem {
	
	public List<PotionEffect> getPotionEffects(int meta);
	
	public boolean hasNonPotionEffects(int meta);
	
	public void applyNonPotionEffects(Entity entity, int meta, double distanceInEdge);
	
	public void enviromentEffect(World world, double posX, double posY, double posZ, int meta);
}
