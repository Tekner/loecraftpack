package loecraftpack.content.items;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.arrow.EntityCustomArrow;
import loecraftpack.content.entities.arrow.EntityIronArrow;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemIronArrow extends Item implements ICustomArrow
{
	public ItemIronArrow() {
		super();
		
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/IronArrow");
	}

	@Override
	public EntityCustomArrow createEntity(World world, EntityLivingBase entityLiving, float speed)
	{
		return new EntityIronArrow(world, entityLiving, speed);
	}
	
}
