package loecraftpack.content.items;

import loecraftpack.content.registery.LOE_Items;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemZapAppleJam extends ItemFood
{
	IIcon icons[] = new IIcon[4];
	
	public ItemZapAppleJam(int heal, float saturation, boolean wolf)
    {
        super(heal, saturation, wolf);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
    }
	
	@SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack itemStack)
    {
        return true;
    }
	
	@SideOnly(Side.CLIENT)
	@Override
    public EnumRarity getRarity(ItemStack itemStack)
    {
		return EnumRarity.epic;
    }
	
	@Override
	public ItemStack onEaten(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		super.onEaten(itemStack, world, entityPlayer);
		ItemStack emptyBottle = new ItemStack(LOE_Items.emptyBottle);
		if (!entityPlayer.inventory.addItemStackToInventory(emptyBottle) && !world.isRemote)
			world.spawnEntityInWorld(new EntityItem(world, entityPlayer.posX, entityPlayer.posY, entityPlayer.posZ, emptyBottle));
	
		return itemStack;
	}
	
	@Override
	protected void onFoodEaten(ItemStack itemStack, World world, EntityPlayer entityPlayer)
    {
		if (!world.isRemote)
        {
			//Do: ZapAppleJam - define potion effects
        	entityPlayer.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 6000, 1));
        	entityPlayer.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 6000, 1));
        	entityPlayer.addPotionEffect(new PotionEffect(Potion.jump.id, 6000, 0));
        }
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
	    itemIcon = iconRegister.registerIcon("loecraftpack:food/zapapple_jam");
	}

}
