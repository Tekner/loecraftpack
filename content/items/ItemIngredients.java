package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemIngredients extends Item
{
	protected static final String rescource_location = "loecraftpack:ingredients/";
	protected List<IIcon> icons;
	protected String[] names;
	
	public ItemIngredients()
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		
		names = new String[]{/*0*/"dust_laughter"   , /*1*/"dust_generosity"   , /*2*/"dust_kindness"   , /*3*/"dust_magic"   , /*4*/"dust_loyalty"   , /*5*/"dust_honesty"   , /*6*/"dust_harmony"   , /*7*/"dust_diamond", /*8*/"dust_astral", /*9*/"astral_diamond", /*10*/"cockatrice_eye", /*11*/"tom"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	  /***************************************/
	 /*****Special Effects*******************/
	/***************************************/
	//if any
	
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (itemStack.getItemDamage()==8/*astral dust*/)
		{
			entityPlayer.setItemInUse(itemStack, this.getMaxItemUseDuration(itemStack));
		}
		
		return itemStack;
	}
	
	@Override
	public int getMaxItemUseDuration(ItemStack itemStack)
	{
		return 32;
	}
	
	public EnumAction getItemUseAction(ItemStack itemStack)
	{
		return EnumAction.drink;
	}
	
	@Override
	public ItemStack onEaten(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		--itemStack.stackSize;
		world.playSoundAtEntity(entityPlayer, "random.burp", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
		entityPlayer.addPotionEffect(new PotionEffect(Potion.confusion.id, 600, 0));
		entityPlayer.addExperience(500);
		return itemStack;
	}
}
