package loecraftpack.content.items;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.world.World;

@SuppressWarnings("rawtypes")
public class ItemCustomHangingEntity extends Item {
	
	protected final Class hangingEntityClass;
	protected final Constructor hangingEntityConstructor;
	
	public ItemCustomHangingEntity(Class entityClass)
	{
		Constructor tempC = null;
		if (EntityHanging.class.isAssignableFrom(entityClass))
		{
			hangingEntityClass = entityClass;
			try {
				tempC = hangingEntityClass.getConstructor(World.class, int.class, int.class, int.class, int.class);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		else
			hangingEntityClass = null;
		hangingEntityConstructor = tempC;
	}
	
	public boolean onItemUse(ItemStack itemStack, EntityPlayer entityPlayer, World world, int xPos, int yPos, int zPos, int direction3D, float par8, float par9, float par10)
	{
		if (direction3D == 0 && cannotUseBottomSurface())
		{
			return false;
		}
		else if (direction3D == 1 && cannotUseTopSurface())
		{
			return false;
		}
		else if (!entityPlayer.canPlayerEdit(xPos, yPos, zPos, direction3D, itemStack))
		{
			return false;
		}
		else
		{
			EntityHanging entityhanging = createHangingEntity(itemStack, entityPlayer, world, xPos, yPos, zPos, direction3D, par8, par9, par10);
			if (entityhanging != null && entityhanging.onValidSurface())
			{
				if (!world.isRemote)
				{
					world.spawnEntityInWorld(entityhanging);
				}
				
				--itemStack.stackSize;
			}
			
			return true;
		}
	}
	
	protected boolean cannotUseTopSurface()
	{
		return true;
	}
	
	protected boolean cannotUseBottomSurface()
	{
		return true;
	}
	
	/** override this to add additional properties to the entity**/
	protected EntityHanging createHangingEntity(ItemStack itemStack, EntityPlayer entityPlayer, World world, int xPos, int yPos, int zPos, int direction3D, float par8, float par9, float par10)
	{
		int direction = Direction.facingToDirection[direction3D];
		return createHangingEntity(world, xPos, yPos, zPos, direction);
	}
	
	public EntityHanging createHangingEntity(World world, int xPos, int yPos, int zPos, int direction)
	{
		if (hangingEntityConstructor == null)
			return null;
		else
		{
			try {
				return (EntityHanging)(hangingEntityConstructor.newInstance(world, xPos, yPos, zPos, direction));
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
