package loecraftpack.content.items;

import loecraftpack.content.blocks.BlockAppleBloomLeaves;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLeavesAppleBloom extends ItemBlock
{
	protected int bloomStage = 2;
	protected BlockAppleBloomLeaves leaf;
	
	//Preffered constructor, to allow more than 1 blockID
	public ItemLeavesAppleBloom(Block leaf)
    {
        super(leaf);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
        this.leaf = (BlockAppleBloomLeaves)leaf;
        System.out.println("leaf passed: "+ this.leaf);
    }
	
    public int getMetadata(int par1)
    {
        return par1 | 4;
    }

    @SideOnly(Side.CLIENT)

    /**
     * Gets an icon index based on an item's damage value
     */
    public IIcon getIconFromDamage(int meta)
    {
    	return leaf.getIcon(0, meta);
    }

    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
        int meta = par1ItemStack.getItemDamage();
        return leaf.getRenderColor(meta);
    }
    
    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        int meta = par1ItemStack.getItemDamage() & 3;
        
        if (Item.getItemFromBlock(LOE_Blocks.zapAppleLeavesCharged) == this)
        	return super.getUnlocalizedName();
        else if (meta < bloomStage)
        	return super.getUnlocalizedName()+".normal";
        else return super.getUnlocalizedName()+".blooming";
    }
}