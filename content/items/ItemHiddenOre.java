package loecraftpack.content.items;

import loecraftpack.content.blocks.BlockAppleBloomLeaves;
import loecraftpack.content.blocks.BlockHiddenOre;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHiddenOre extends ItemBlock
{
	protected BlockHiddenOre ore;
	
	public ItemHiddenOre(Block ore)
    {
        super(ore);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
        this.ore = (BlockHiddenOre)ore;
        System.out.println("ore passed: "+ this.ore);
    }
	
	@Override
    public int getMetadata(int par1)
    {
        return par1;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int meta)
    {
    	return ore.getHiddenBlockTextureFromSideAndMetadata(2, meta);
    }
    
    @Override
    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        return super.getUnlocalizedName()+"."+ore.getName(par1ItemStack.getItemDamage());
    }
}
