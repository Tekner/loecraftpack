package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import loecraftpack.LoECraftPack;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBits extends Item
{
	protected static final String rescource_location = "loecraftpack:bits/";
	@SideOnly(Side.CLIENT)
	List<IIcon> icons;
	String[] names;
	
	public ItemBits()
	{
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		this.setUnlocalizedName("itemBits");
		
		names = new String[]{/*0*/"1Bit" , /*1*/"5Bits" , /*2*/"10Bits" , /*3*/"25Bits" , /*4*/"100Bits"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	private static ItemStack[] GetItemstack(int amount)
	{
		ArrayList<ItemStack> bits = new ArrayList<ItemStack>();
		
		int count = (int)(amount / 100);
		if (count > 0)
		{
			bits.add(new ItemStack(LOE_Items.bits, count, 4));
			amount -= count * 100;
		}
		
		count = (int)(amount / 25);
		if (count > 0)
		{
			bits.add(new ItemStack(LOE_Items.bits, count, 3));
			amount -= count * 25;
		}
		
		count = (int)(amount / 10);
		if (count > 0)
		{
			bits.add(new ItemStack(LOE_Items.bits, count, 2));
			amount -= count * 10;
		}
		
		count = (int)(amount / 5);
		if (count > 0)
		{
			bits.add(new ItemStack(LOE_Items.bits, count, 1));
			amount -= count * 5;
		}
		
		if (amount > 0)
			bits.add(new ItemStack(LOE_Items.bits, amount, 0));
		
		return bits.toArray(new ItemStack[0]);
	}
	
	public static void DropBits(int amount, Entity entity)
	{
		for(ItemStack item : ItemBits.GetItemstack(amount))
			entity.entityDropItem(item, 0);
	}
	
	public static void DropBits(int amount, World world, Vec3 pos)
	{
		for(ItemStack item : ItemBits.GetItemstack(amount))
			world.spawnEntityInWorld(new EntityItem(world, pos.xCoord, pos.yCoord, pos.zCoord, item));
	}
}
