package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.EntityThrowableItem;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemThrowableDrink extends Item implements IThrowableItem
{
	protected static final String rescource_location = "loecraftpack:throwable/";
	protected List<IIcon> icons;
	protected String[] names;
	
	public ItemThrowableDrink() {
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		
		names = new String[]{/*0*/"electric", /*1*/"flame", /*2*/"petrify"};
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
				list.add(new ItemStack(item, 1, i));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return icons.get(index);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new ArrayList<IIcon>();
		
		for (int i=0; i< names.length; i++)
		{
			if (names[i]!=null)
			{
				icons.add(iconRegister.registerIcon(rescource_location+names[i]));
			}
			else
				icons.add(null);
		}
	}
	
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		String subname = names[par1ItemStack.getItemDamage()];
		if (subname != null)
			return super.getUnlocalizedName()+"."+subname;
		return super.getUnlocalizedName();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getColorFromItemStack(ItemStack stack, int pass)
	{
		if (pass>=0)
			return super.getColorFromItemStack(stack, pass);
		
		switch(stack.getItemDamage())
		{
		case 0:
			return LoECraftPack.potionCharged.getLiquidColor();
		case 1:
			return 16711680/*RED*/;//flame
		case 2:
			return LoECraftPack.potionPetrified.getLiquidColor();
		}
		
		return 0;
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (!entityPlayer.capabilities.isCreativeMode)
			itemStack.stackSize--;

		world.playSoundAtEntity(entityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

		if (!world.isRemote)
			world.spawnEntityInWorld(new EntityThrowableItem(world, entityPlayer, this, itemStack.getItemDamage()));

		return itemStack;
	}
	
	public List<PotionEffect> getPotionEffects(int meta)
	{
		List<PotionEffect> result = new ArrayList<PotionEffect>();
		switch(meta)
		{
		//electric
		case 0:
			result.add(new PotionEffect(LoECraftPack.potionCharged.id, 60 * 20, 0));
			return result;
			
		case 2:
			result.add(new PotionEffect(LoECraftPack.potionPetrified.id, 60 * 20, 0));
			return result;
		}
		return null;
	}
	
	public boolean hasNonPotionEffects(int meta)
	{
		switch(meta)
		{
		case 1:
			return true;
		}
		return false;
	}
	
	public void applyNonPotionEffects(Entity entity, int meta, double distanceInEdge)
	{
		switch(meta)
		{
		case 1:
			entity.setFire((int) (distanceInEdge*10));
			break;
		}
	}
	
	public void enviromentEffect(World world, double posX, double posY, double posZ, int meta)
	{
		switch(meta)
		{
		case 1:
			setBlockOnFire(world, (int)posX, (int)posY, (int)posZ);
			for (int i=0; i<9; i++)
			{
				int x = (int)posX + world.rand.nextInt(3)-1;
				int y = (int)posY + world.rand.nextInt(3)-1;
				int z = (int)posZ + world.rand.nextInt(3)-1;
				setBlockOnFire(world, x, y, z);
			}
			break;
		}
	}
	
	public void setBlockOnFire(World world, int x, int y, int z)
	{
		if (world.isAirBlock(x, y, z))
		{
			if (!world.isAirBlock(x-1, y, z) ||
				!world.isAirBlock(x+1, y, z) ||
				!world.isAirBlock(x, y-1, z) ||
				!world.isAirBlock(x, y+1, z) ||
				!world.isAirBlock(x, y, z-1) ||
				!world.isAirBlock(x, y, z+1))
					world.setBlock(x, y, z, Blocks.fire);
		}
	}
}
