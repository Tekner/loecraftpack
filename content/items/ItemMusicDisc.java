package loecraftpack.content.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import loecraftpack.LoECraftPack;
import net.minecraft.block.Block;
import net.minecraft.block.BlockJukebox;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMusicDisc extends ItemRecord
{
	private static final Map<String, ItemMusicDisc> discMap = new HashMap<String, ItemMusicDisc>();

	private String composer;
	
	public ItemMusicDisc(String composer, String recordName)
	{
		super(recordName);
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
		this.setUnlocalizedName("record");
		this.composer = composer;
		
		discMap.put(recordName, this);
	}
	
	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int xCoord, int yCoord, int zCoord, int side, float par8, float par9, float par10)
	{
		if (Block.isEqualTo(world.getBlock(xCoord, yCoord, zCoord), Blocks.jukebox) && world.getBlockMetadata(xCoord, yCoord, zCoord) == 0)
		{
			if (world.isRemote)
				return true;
			else
			{
				((BlockJukebox)Blocks.jukebox).func_149926_b(world, xCoord, yCoord, zCoord, itemStack);
				world.playAuxSFXAtEntity((EntityPlayer)null, 1005, xCoord, yCoord, zCoord, Item.getIdFromItem(this));
				itemStack.stackSize--;
				return true;
			}
		} 
		else
			return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
	{
		par3List.add(this.getRecordNameLocal());
		par3List.add("Composer: "+composer);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getRecordNameLocal()
	{
		return StatCollector.translateToLocal("tooltip.record."+recordName+ ".desc");
	}
	
	@Override
	public EnumRarity getRarity(ItemStack itemStack)
	{
		return EnumRarity.rare;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		this.itemIcon = iconRegister.registerIcon("loecraftpack:records/" + recordName);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack iconNamestack)
	{
		return super.getUnlocalizedName() + "." + recordName;
	}
	
	public static ItemMusicDisc getRecord(String par0Str)
	{
	return discMap.get(par0Str);
	}
	
	@Override
	public ResourceLocation getRecordResource(String name)
	{
	return new ResourceLocation("loecraftpack:" + name);
	}
}
