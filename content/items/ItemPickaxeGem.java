package loecraftpack.content.items;

import java.util.Set;

import com.google.common.collect.Sets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.IIcon;

public class ItemPickaxeGem extends ItemTool {
	
	public static final Set blocksEffectiveAgainst = Sets.newHashSet(new Block[] {LOE_Blocks.gemOre});
	
	public ItemPickaxeGem()
	{
		this(2, ToolMaterial.IRON);
		
		this.setHarvestLevel("pickaxe", 2);
	}
	
	public ItemPickaxeGem(float damageModifier, ToolMaterial par2EnumToolMaterial)
	{
		this(damageModifier, par2EnumToolMaterial, blocksEffectiveAgainst);
	}
	
	public ItemPickaxeGem(float damageModifier, ToolMaterial par2EnumToolMaterial, Set blocksEffectiveAgainst) {
		super(damageModifier, par2EnumToolMaterial, blocksEffectiveAgainst);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		return itemIcon;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/gempickaxe");
	}
	
	/**getStrVsBlock; super checks against: blocksEffectiveAgainst**/
	@Override
	public float func_150893_a(ItemStack par1ItemStack, Block par2Block)
	{
		return par2Block != null && (par2Block.getMaterial() == Material.iron || par2Block.getMaterial() == Material.anvil || par2Block.getMaterial() == Material.rock) ? this.efficiencyOnProperMaterial : super.func_150893_a(par1ItemStack, par2Block);
	}
}
