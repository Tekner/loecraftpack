package loecraftpack.content.items;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.arrow.EntityCustomArrow;
import loecraftpack.content.entities.arrow.EntityIceArrow;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemIceArrow extends Item implements ICustomArrow
{
	public ItemIceArrow()
	{
		super();
		this.setCreativeTab(LoECraftPack.LoECraftTabItem);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon("loecraftpack:tools/IceArrow");
	}
	
	@Override
	public EntityCustomArrow createEntity(World world, EntityLivingBase entityLiving, float speed)
	{
		return new EntityIceArrow(world, entityLiving, speed);
	}
}
