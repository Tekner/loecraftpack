package loecraftpack.content.items;

import loecraftpack.LoECraftPack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemAppleFritter extends ItemFood {
	
	public ItemAppleFritter()
	{
		super(1, 1.6f, false);
	}
	
	@Override
	public void onFoodEaten(ItemStack itemStack, World world, EntityPlayer player)
    {
		player.addPotionEffect(new PotionEffect(LoECraftPack.potionWellFed.id, 300, 0));
		player.addPotionEffect(new PotionEffect(Potion.jump.id, 600, 0));
		player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 600, 0));
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
	    itemIcon = iconRegister.registerIcon("loecraftpack:food/appleFritter");
	}

}
