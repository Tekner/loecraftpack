package loecraftpack.content.ponies.inventory.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotStatic extends Slot {
	
	public ItemStack staticItem;

	public SlotStatic(ItemStack staticItem, int par2, int par3, int par4) {
		super(null, par2, par3, par4);
		this.staticItem = staticItem;
	}
	
    public ItemStack getStack()
    {
        return staticItem.copy();
    }
    
    public boolean getHasStack()
    {
        return this.staticItem != null;
    }
    
    public void putStack(ItemStack par1ItemStack) {}
    
    public void onSlotChanged() {}
    
    public int getSlotStackLimit()
    {
    	return staticItem.getItem().getItemStackLimit();
    }
    
    public ItemStack decrStackSize(int par1)
    {
        return staticItem.copy();
    }
    
    public boolean isSlotInInventory(IInventory par1IInventory, int par2)
    {
        return false;
    }
    
    public boolean canTakeStack(EntityPlayer par1EntityPlayer)
    {
        return true;
    }
    
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        return false;
    }

}
