package loecraftpack.content.ponies.inventory.gui;

import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiEarthPonyInventory extends InventoryEffectRenderer {
	
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/earthInventory.png");
	
	public GuiEarthPonyInventory(EntityPlayer entityPlayer)
    {
        super(new ContainerEarthInventory(entityPlayer));
        this.allowUserInput = true;
    }
	
	public void initGui()
    {
        this.buttonList.clear();
        super.initGui();
    }
	
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        this.fontRendererObj.drawString(StatCollector.translateToLocal("Extend Inventory"), 8, 8, 4210752);
        this.fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, this.ySize - 96 + 4, 4210752);
    }

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(mainImage);
        int k = this.guiLeft;
        int l = this.guiTop;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
    }
}
