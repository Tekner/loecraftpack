package loecraftpack.content.ponies.inventory.gui;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.inventory.gui.tabs.MasterTabs;
import loecraftpack.network.packets.PacketCustomInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TabManager {
	
	public static boolean inventoryOpen = false;
	public static boolean tabsDisplayed = true;
	
	public static final int xOffsetByHotbar = -90;
	
	
	
	public static TabButton[] tabButtons = new TabButton[MasterTabs.pageSize];
	static
	{
		for (int i=0; i<tabButtons.length; i++)
		{
			tabButtons[i] = new TabButton(TabButton.startingID+i);
		}
	}
	public static TabPageButton tabPageButtonBackward = new TabPageButton(3, 0, "B");
	public static TabPageButton tabPageButtonForward = new TabPageButton(4, 1, "F");
	
	
	protected static GuiScreen lastCheckedScreen = null;
	public static int currentTabID = 0;
	public static int currentPage = 0;
	
	
	public static void applyTabs(GuiScreen currentGui)
	{
		boolean change = MasterTabs.compileUsableTabIndex();
		if (currentGui != lastCheckedScreen)
		{
			inventoryOpen = false;
			
			lastCheckedScreen = currentGui;
			
			if (currentGui != null)
			{
				if (currentGui instanceof GuiContainer)
					for (Class c : MasterTabs.inventoryGuis)
					{
						if (c.isInstance(currentGui))
						{
							inventoryOpen = true;
							change = true;
							break;
						}
					}
			}
		}
		if (currentGui == null)
			return;
		if (inventoryOpen && tabsDisplayed && !currentGui.buttonList.contains(tabButtons[0]))
		{
			currentGui.buttonList.add(tabPageButtonBackward);
			currentGui.buttonList.add(tabPageButtonForward);
			for (TabButton tabB : tabButtons)
			{
				currentGui.buttonList.add(tabB);
			}
			updateTabs();
		}
		else if (change)
		{
			updateTabs();
		}
		if (!tabsDisplayed && currentGui.buttonList.contains(tabButtons[0]))
		{
			currentGui.buttonList.remove(tabPageButtonBackward);
			currentGui.buttonList.remove(tabPageButtonForward);
			for (TabButton tabB : tabButtons)
			{
				currentGui.buttonList.remove(tabB);
			}
		}
	}
	
	public static void changePage(Boolean nextPage)
	{
		if (nextPage)
		{
			int max = MasterTabs.totalPagesUsable()-1;
			if (currentPage >= max )
				currentPage = max;
			else
				currentPage++;
		}
		else
		{
			if (currentPage <= 0 )
				currentPage = 0;
			else
				currentPage--;
		}
		
		updateTabs();
	}
	
	public static void switchTab(MasterTabs tab)
	{
		if (tab != null && tab.getTabID() != currentTabID && tab.isCurrentlyVisable())
		{
			currentTabID = tab.getTabID();
			LoECraftPack.packetHandler.sendToServer(new PacketCustomInventory().INIT(tab.getGuiId().ordinal()));
		}
	}
	
	public static void updateTabs()
	{
		int max = MasterTabs.totalPagesUsable()-1;
		if (currentPage>max)
			currentPage = max;
		
		MasterTabs[] visableTabs = MasterTabs.getPageOfVisableTabs(currentPage);
		
		for (int i = 0; i<visableTabs.length; i++)
		{
			tabButtons[i].assignTab(visableTabs[i]);
		}
		GuiScreen gui = Minecraft.getMinecraft().currentScreen;
		tabPageButtonBackward.xPosition = gui.width/2 - (TabButton.sizeX + TabPageButton.sizeX)*2 + xOffsetByHotbar;
		tabPageButtonBackward.yPosition = gui.height - TabPageButton.sizeY;
		tabPageButtonBackward.enabled = currentPage>0;
		tabPageButtonForward.xPosition = gui.width/2 - TabPageButton.sizeX + xOffsetByHotbar;
		tabPageButtonForward.yPosition = gui.height - TabPageButton.sizeY;
		tabPageButtonForward.enabled = currentPage<max;
		
	}
}
