package loecraftpack.content.ponies.inventory.gui.tabs;

import loecraftpack.content.ponies.inventory.gui.GuiStats;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class StatsTab extends MasterTabs
{
	private static final ResourceLocation iconImage = new ResourceLocation("loecraftpack:gui/tabs/stats.png");
	
	StatsTab()
    {
        super("statPage", GuiStats.class, GuiIds.STATS);
    }
	
	@Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getTabIconImageResource()
    {
        return iconImage;
    }
}