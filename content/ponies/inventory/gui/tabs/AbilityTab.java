package loecraftpack.content.ponies.inventory.gui.tabs;

import loecraftpack.content.ponies.inventory.gui.GuiAbilities;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class AbilityTab extends MasterTabs
{
	private static final ResourceLocation iconImage = new ResourceLocation("loecraftpack:gui/tabs/ability.png");
	
	AbilityTab()
    {
        super("abilityInventory", GuiAbilities.class, GuiIds.ABILITY_INV);
    }
	
	@Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getTabIconImageResource()
    {
        return iconImage;
    }
}
