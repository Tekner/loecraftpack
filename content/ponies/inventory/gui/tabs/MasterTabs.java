package loecraftpack.content.ponies.inventory.gui.tabs;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.util.StringTranslate;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class MasterTabs
{
	//used by tab manager to comfirm if a gui should have the Tabs applied to it
	public static List<Class> inventoryGuis = new ArrayList<Class>();
	
	public static MasterTabs[] masterTabArray = new MasterTabs[5];
	/////tabs init at bottom
	
	public static boolean[] usableStateArray = new boolean[5];
	public static int usableTabsTotal = 0;
	
	public final static int pageSize = 4;
	
	private final String tabLabel;
	public final GuiIds guiID;
	private final int tabID;
	protected int usableTabIndex;
    
    /** Texture to use. */
    private String backgroundImageName = "list_items.png";
    
	public MasterTabs(String label, Class gui, GuiIds guiID)
    {
    	int id = getNextID();
        if (id >= masterTabArray.length)
        {
        	MasterTabs[] tmp = new MasterTabs[id + 1];
            for (int x = 0; x < masterTabArray.length; x++)
            {
                tmp[x] = masterTabArray[x];
            }
            masterTabArray = tmp;
        }
        if (id >= usableStateArray.length)
        	usableStateArray = new boolean[id + 1];
        inventoryGuis.add(gui);
        this.guiID = guiID;
        this.tabID = id;
        this.tabLabel = label;
        masterTabArray[id] = this;
    }
    
    public static boolean compileUsableTabIndex()
    {
    	boolean change = false;
    	int index = 0;
    	usableTabsTotal = 0;
    	for (int i = 0; i < masterTabArray.length; i++)
    	{
    		MasterTabs tab = masterTabArray[i];
    		if (tab != null)
    		{
    			if (tab.isCurrentlyVisable())
    			{
	    			tab.usableTabIndex = index++;
	    			usableTabsTotal++;
    			}
    			else
    				tab.usableTabIndex = -1;
    			
    			if ((tab.usableTabIndex != -1) != usableStateArray[i])
    			{
    				change = true;
    				usableStateArray[i] = tab.usableTabIndex != -1;
    			}
    		}
    	}
    	return change;
    }
    
    //note: remember to copy any usability checks to HandlerExtendedInventoryCommon
    public boolean isCurrentlyVisable()
    {
    	return true;
    }
    
    //get ID
    public int getTabID()
    {
        return this.tabID;
    }

    public MasterTabs setBackgroundImageName(String par1Str)
    {
        this.backgroundImageName = par1Str;
        return this;
    }
    
    public String getTabLabel()
    {
        return this.tabLabel;
    }
    
    public ResourceLocation getTabIconImageResource()
    {
        return null;
    }
    
    public int getTabYOffset()
    {
        return 14;
    }
    
    /**
     * Gets the translated Label.
     */
    public String getTranslatedTabLabel()
    {
        return StatCollector.translateToLocal("itemGroup." + this.getTabLabel());
    }
    
    public String getBackgroundImageName()
    {
        return this.backgroundImageName;
    }
    
    ///get display position if on page
    public int getTabRow()
    {
        return this.usableTabIndex % pageSize;
    }
    
    //get display page
    public int getTabPage()
    {
        return this.usableTabIndex / pageSize;
    }
    
    protected static int getNextID()
    {
        return masterTabArray.length;
    }
    
    public static boolean isUseable(int id)
    {
    	return masterTabArray[id].usableTabIndex != -1;
    }
    
    public static int totalPagesUsable()
    {
    	return (usableTabsTotal-1) / pageSize + 1;
    }
    
    public static MasterTabs[] getPageOfVisableTabs(int currentPage)
    {
    	MasterTabs[] hold = new MasterTabs[pageSize];
    	int size = 0;
    	
    	for (MasterTabs tab:masterTabArray)
    	{
    		if (tab != null && tab.isCurrentlyVisable() && tab.getTabPage() == currentPage)
    		{
    			hold[size++] = tab;
    			if (size == pageSize)break;
    		}
    	}
    	
    	return hold;
    }
    
    public GuiIds getGuiId()
    {
    	return guiID;
    }
    
    //tabs init
    static
	{
		new MainTab();
		new EquipmentTab();
		new AbilityTab();
		new EarthTab();
		new StatsTab();
	}
}
