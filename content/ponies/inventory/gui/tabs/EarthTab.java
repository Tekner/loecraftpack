package loecraftpack.content.ponies.inventory.gui.tabs;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.inventory.gui.GuiEarthPonyInventory;
import loecraftpack.referrance.enums.GuiIds;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class EarthTab extends MasterTabs
{
	private static final ResourceLocation iconImage = new ResourceLocation("loecraftpack:gui/tabs/earth.png");
	
	EarthTab()
    {
        super("earthInventory", GuiEarthPonyInventory.class, GuiIds.EARTH_INV);
    }
	
	@Override
	public boolean isCurrentlyVisable()
    {
    	return LoECraftPack.statHandler.isRace(Minecraft.getMinecraft().thePlayer, Race.EARTH);
    }

	@Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getTabIconImageResource()
    {
        return iconImage;
    }
}
