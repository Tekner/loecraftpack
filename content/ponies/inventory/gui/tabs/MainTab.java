package loecraftpack.content.ponies.inventory.gui.tabs;

import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class MainTab extends MasterTabs
{
	private static final ResourceLocation iconImage = new ResourceLocation("loecraftpack:gui/tabs/main.png");
	
	MainTab()
    {
        super("mainInventory", GuiInventory.class, GuiIds.MAIN_INV);
        inventoryGuis.add(GuiContainerCreative.class);
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isCurrentlyVisable()
    {
    	return !((Minecraft.getMinecraft().currentScreen instanceof GuiInventory) || 
    			 (Minecraft.getMinecraft().currentScreen instanceof GuiContainerCreative));
    }

	@Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getTabIconImageResource()
    {
        return iconImage;
    }
}
