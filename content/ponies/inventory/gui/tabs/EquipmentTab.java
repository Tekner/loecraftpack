package loecraftpack.content.ponies.inventory.gui.tabs;

import loecraftpack.content.ponies.inventory.gui.GuiSpecialEquipment;
import loecraftpack.referrance.enums.GuiIds;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class EquipmentTab extends MasterTabs
{
	private static final ResourceLocation iconImage = new ResourceLocation("loecraftpack:gui/tabs/equipment.png");
	
	EquipmentTab()
    {
        super("specialInventory", GuiSpecialEquipment.class, GuiIds.EQUIPMENT_INV);
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isCurrentlyVisable()
    {
    	return ((Minecraft.getMinecraft().currentScreen instanceof GuiInventory) || 
    			(Minecraft.getMinecraft().currentScreen instanceof GuiContainerCreative));
    }

	@Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getTabIconImageResource()
    {
        return iconImage;
    }
}
