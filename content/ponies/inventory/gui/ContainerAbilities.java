package loecraftpack.content.ponies.inventory.gui;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityBase;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.ItemActiveAbility;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ContainerAbilities extends Container {
	
	protected static List<ItemStack> AlicornAbilities = new ArrayList<ItemStack>();
	protected static List<ItemStack> EarthAbilities = new ArrayList<ItemStack>();
	protected static List<ItemStack> UnicornAbilities = new ArrayList<ItemStack>();
	protected static List<ItemStack> PegasusAbilities = new ArrayList<ItemStack>();
	protected static List<ItemStack> UniversalAbilities = new ArrayList<ItemStack>();
	protected static List<ItemStack> DragonAbilities = new ArrayList<ItemStack>();
	
	private static Item[] items = new Item[] {LOE_Items.activeAbility, LOE_Items.passiveAbility};
	
	public static void registerAbilities()
	{
		ActiveAbility[] activeAbilities = ActiveAbility.NewAbilityArray();
		PassiveAbility[] passiveAbilities = PassiveAbility.NewAbilityArray();
		AbilityBase[] abilities =  new AbilityBase[activeAbilities.length + passiveAbilities.length];
		for(int i = 0; i < abilities.length; i++)
		{
			abilities[i] = i < activeAbilities.length ? activeAbilities[i] : passiveAbilities[i - activeAbilities.length];
		}
		
		for (AbilityBase ability : abilities)
		{
			if (ability == null || !ability.ShowInAbilityList())
				continue;
			int idIndex = ability instanceof ActiveAbility ? 0 : 1, id = ability.getId();
			switch (ability.GetRace())
			{
			case ALICORN:
				AlicornAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			case EARTH:
				EarthAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			case UNICORN:
				UnicornAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			case PEGASUS:
				PegasusAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			case NONE:
				UniversalAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			case DRAGON:
				DragonAbilities.add(new ItemStack(items[idIndex], 1, id));
				break;
			default:
				break;
			}
		}
	}

    public ContainerAbilities(EntityPlayer player)
    {
        InventoryPlayer inventoryplayer = player.inventory;
        
        int i;
        
        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(inventoryplayer, i, 9 + i * 18, 114));
        }
        
        int base = 0;
        int size = 0;
        
        size += UniversalAbilities.size();
        for (i = 0; i < size; i++)
        {
        	this.addSlotToContainer(new SlotStatic(UniversalAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
        }
        base += UniversalAbilities.size();
        
        if (LoECraftPack.statHandler.isRace(player, Race.EARTH))
        {
	        size += EarthAbilities.size();
	        for (; i < size; i++)
	        {
	        	this.addSlotToContainer(new SlotStatic(EarthAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
	        }
	        base += EarthAbilities.size();
        }
        
        if (LoECraftPack.statHandler.isRace(player, Race.UNICORN))
        {
	        size += UnicornAbilities.size();
	        for (; i < size; i++)
	        {
	        	this.addSlotToContainer(new SlotStatic(UnicornAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
	        }
	        base += UnicornAbilities.size();
        }
        
        if (LoECraftPack.statHandler.isRace(player, Race.PEGASUS))
        {
	        size += PegasusAbilities.size();
	        for (; i < size; i++)
	        {
	        	this.addSlotToContainer(new SlotStatic(PegasusAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
	        }
	        base += PegasusAbilities.size();
        }
        
        if (LoECraftPack.statHandler.isRace(player, Race.ALICORN))
        {
	        size += AlicornAbilities.size();
	        for (; i < size; i++)
	        {
	        	this.addSlotToContainer(new SlotStatic(AlicornAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
	        }
	        base += AlicornAbilities.size();
        }
        
        if (LoECraftPack.statHandler.isRace(player, Race.DRAGON))
        {
	        size += DragonAbilities.size();
	        for (; i < size; i++)
	        {
	        	this.addSlotToContainer(new SlotStatic(DragonAbilities.get(i - base), i, 9 + (i%9) * 18, 20 + (i/9) * 18));
	        }
	        base += DragonAbilities.size();
        }
        
        this.scrollTo(0.0F);
    }
    
    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return true;
    }
    
    /**
     * Updates the gui slots ItemStack's based on scroll position.
     */
    public void scrollTo(float par1)
    {
    	
    }

    protected void retrySlotClick(int par1, int par2, boolean par3, EntityPlayer par4EntityPlayer) {}

    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
    {
    	Slot slot = (Slot)this.inventorySlots.get(par2);
        if (par2 < 9 /*hotbar*/)
        {
            
            if (slot != null && slot.getHasStack() && slot.getStack().getItem() instanceof ItemActiveAbility)
            {
                slot.putStack((ItemStack)null);
            }
        }
        else
        {
        	if (slot != null && slot.getHasStack())
        	{
        		this.mergeItemStack(slot.getStack(), 0, 9/*hotbar*/, true);
        	}
        }
        
        //doesn't matter since function is empty
        return null;
        
    }

    public boolean func_94530_a(ItemStack par1ItemStack, Slot par2Slot)
    {
        return par2Slot.yDisplayPosition > 90;
    }

    /**
     * Returns true if the player can "drag-spilt" items into this slot,. returns true by default. Called to check if
     * the slot can be added to a list of Slots to split the held ItemStack across.
     */
    public boolean canDragIntoSlot(Slot par1Slot)
    {
        return false;
    }
    
    public void detectAndSendChanges()
    {
        for (int i = 0; i < 9/*hotbar*/; ++i)
        {
            ItemStack itemstack = ((Slot)this.inventorySlots.get(i)).getStack();
            ItemStack itemstack1 = (ItemStack)this.inventoryItemStacks.get(i);

            if (!ItemStack.areItemStacksEqual(itemstack1, itemstack))
            {
                itemstack1 = itemstack == null ? null : itemstack.copy();
                this.inventoryItemStacks.set(i, itemstack1);

                for (int j = 0; j < this.crafters.size(); ++j)
                {
                    ((ICrafting)this.crafters.get(j)).sendSlotContents(this, i, itemstack1);
                }
            }
        }
    }
    
    public ItemStack slotClick(int par1, int par2, int par3, EntityPlayer par4EntityPlayer)
    {
    	if (par1 >= 9 /*not hotbar*/)
    	{
	    	InventoryPlayer inventoryplayer = par4EntityPlayer.inventory;
	    	ItemStack itemstack1 = inventoryplayer.getItemStack();
	    	if (itemstack1 != null && itemstack1.getItem() instanceof ItemActiveAbility)
	    			inventoryplayer.setItemStack((ItemStack)null);
    	}
    	
    	return super.slotClick(par1, par2, par3, par4EntityPlayer);
    }

}