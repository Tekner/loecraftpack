package loecraftpack.content.ponies.inventory.gui;

import loecraftpack.LoECraftPack;
import loecraftpack.content.gui.scroll.GuiScrollBar;
import loecraftpack.content.gui.scroll.GuiScrollButton;
import loecraftpack.content.gui.scroll.GuiScrollPage;
import loecraftpack.content.gui.scroll.GuiScrollString;
import loecraftpack.content.gui.scroll.GuiScrollTexture;
import loecraftpack.content.gui.scroll.GuiScrollWithEffectRender;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.network.packets.PacketUpgradeStat;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiStats extends GuiScrollWithEffectRender
{
	EntityPlayer entityPlayer;
	Stats stats;
	
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/statsPage.png");
	private static final ResourceLocation scrollBackgroundImage = new ResourceLocation("loecraftpack:gui/statsPage2.png");
	private static final ResourceLocation componentImage = new ResourceLocation("loecraftpack:gui/components.png");
	
	public GuiScrollButton upgradeSTR;
	public GuiScrollButton upgradeMAG;
	public GuiScrollButton upgradeAGL;
	public GuiScrollButton upgradeAL1;
	public GuiScrollButton upgradeAL2;
	public GuiScrollButton upgradeAL3;
	
	public GuiScrollPage scrollPage;
	
	protected static int xPonyLevel = 18, yPonyLevel = 5;
	public GuiScrollString sPlayerPonyLevel;
	
	protected static int xDragonPower = 126, yDragonPower = 5;
	public GuiScrollString sPlayerDragonPower;
	
	protected static int xExperiance = 18, yExperiance = 18;
	public GuiScrollString sPlayerExperiance;
	public GuiScrollString sPlayerExperianceToLevel;
	
	protected static int xHealth = 18, yHealth = 31;
	public GuiScrollString sPlayerHealth;
	public GuiScrollString sPlayerHealthMax;
	
	protected static int xEnergy = 18, yEnergy = 44;
	public GuiScrollString sPlayerEnergy;
	public GuiScrollString sPlayerEnergyMax;
	
	protected static int xEnergyRegen = 18, yEnergyRegen = 57;
	public GuiScrollString sPlayerEnergyRegen;
	
	protected static int xAttack = 126, yAttack = 31;
	public GuiScrollString sPlayerAttack;
	
	protected static int xEvasion = 126, yEvasion = 44;
	public GuiScrollString sPlayerEvasion;
	
	protected static int xMoveSpeed = 126, yMoveSpeed = 57;
	public GuiScrollString sPlayerMoveSpeed;
	public GuiScrollString sPlayerMoveSpeedEffective;
	
	protected static int xPonyPoints = 18, yPonyPoints = 69;
	public GuiScrollString sPlayerPonyPoints;
	
	protected static int xAlignmentPoints = 126, yAlignmentPoints = 69;
	public GuiScrollString sPlayerAlignmentPoints;
	
	protected static int xStrength = 18, yStrength = 82;
	public GuiScrollString sPlayerStrength;
	public GuiScrollString sPlayerStrengthMod;
	
	protected static int xMagic = 18, yMagic = 101;
	public GuiScrollString sPlayerMagic;
	public GuiScrollString sPlayerMagicMod;
	
	protected static int xAgility = 18, yAgility = 120;
	public GuiScrollString sPlayerAgility;
	public GuiScrollString sPlayerAgilityMod;
	
	protected static int xOffense = 126, yOffense = 82;
	public GuiScrollString sPlayerOffense;
	public GuiScrollString sPlayerOffenseMod;
	
	protected static int xDefense = 126, yDefense = 101;
	public GuiScrollString sPlayerDefense;
	public GuiScrollString sPlayerDefenseMod;
	
	protected static int xUtility = 126, yUtility = 120;
	public GuiScrollString sPlayerUtility;
	public GuiScrollString sPlayerUtilityMod;
	
	public GuiStats(EntityPlayer entityPlayer)
	{
		super(new ContainerEmpty());
		this.entityPlayer = entityPlayer;
		this.stats = (Stats)LoECraftPack.statHandler.stats.get(entityPlayer.getGameProfile().getId());
		this.allowUserInput = true;
		this.xSize=256;
		this.ySize=165;
	}
	
	@Override
	public void initGui()
	{
		this.buttonList.clear();
		super.initGui();
		
		this.pages.add(scrollPage = new GuiScrollPage(this, 10, 33, 236, 125, 236, 256));
		scrollPage.backgroundImage = scrollBackgroundImage;
		
		new GuiScrollBar(scrollPage, 250, 30, 11, 130, true);
		
		if (stats != null)
		{
			new GuiScrollTexture(scrollPage, componentImage, xStrength-3, yStrength,   120,  0, 56, 18).inBackground();
			new GuiScrollTexture(scrollPage, componentImage,    xMagic-3,    yMagic,   120, 18, 56, 18).inBackground();
			new GuiScrollTexture(scrollPage, componentImage,  xAgility-3,  yAgility,   120, 36, 56, 18).inBackground();
			new GuiScrollTexture(scrollPage, componentImage,  xOffense-3,  yOffense,   176,  0, 56, 18).inBackground();
			new GuiScrollTexture(scrollPage, componentImage,  xDefense-3,  yDefense,   176, 18, 56, 18).inBackground();
			new GuiScrollTexture(scrollPage, componentImage,  xUtility-3,  yUtility,   176, 36, 56, 18).inBackground();
			
			/*****************/
			
			upgradeSTR = new GuiScrollButton(10, scrollPage, componentImage, xStrength+51, yStrength,   0, 0, 20, 18);
			this.buttonList.add(upgradeSTR);
			upgradeMAG = new GuiScrollButton(11, scrollPage, componentImage,    xMagic+51,    yMagic,  20, 0, 20, 18);
			this.buttonList.add(upgradeMAG);
			upgradeAGL = new GuiScrollButton(12, scrollPage, componentImage,  xAgility+51,  yAgility,  40, 0, 20, 18);
			this.buttonList.add(upgradeAGL);
			
			upgradeAL1 = new GuiScrollButton(13, scrollPage, componentImage,  xOffense+51,  yOffense,  60, 0, 20, 18);
			this.buttonList.add(upgradeAL1);
			upgradeAL2 = new GuiScrollButton(14, scrollPage, componentImage,  xDefense+51,  yDefense,  80, 0, 20, 18);
			this.buttonList.add(upgradeAL2);
			upgradeAL3 = new GuiScrollButton(15, scrollPage, componentImage,  xUtility+51,  yUtility, 100, 0, 20, 18);
			this.buttonList.add(upgradeAL3);
			
			/****************/
			
			new GuiScrollString(scrollPage, fontRendererObj, xPonyLevel, yPonyLevel, StatCollector.translateToLocal("Pony Level")).setColor(0);
			sPlayerPonyLevel = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xDragonPower, yDragonPower, StatCollector.translateToLocal("Dragon Power")).setColor(0);
			sPlayerDragonPower = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			
			
			new GuiScrollString(scrollPage, fontRendererObj, xExperiance, yExperiance, StatCollector.translateToLocal("Exp to Level:")).setColor(0);
			sPlayerExperiance = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerExperianceToLevel = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			
			
			new GuiScrollString(scrollPage, fontRendererObj, xHealth, yHealth, StatCollector.translateToLocal("HP")).setColor(0);
			sPlayerHealth = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerHealthMax = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xEnergy, yEnergy, StatCollector.translateToLocal("ENE")).setColor(0);
			sPlayerEnergy = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerEnergyMax = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xEnergyRegen, yEnergyRegen, StatCollector.translateToLocal("REG")).setColor(0);
			sPlayerEnergyRegen = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			
			
			new GuiScrollString(scrollPage, fontRendererObj, xAttack, yAttack, StatCollector.translateToLocal("ATK")).setColor(0);
			sPlayerAttack = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xEvasion, yEvasion, StatCollector.translateToLocal("EVA")).setColor(0);
			sPlayerEvasion = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xMoveSpeed, yMoveSpeed, StatCollector.translateToLocal("SPD")).setColor(0);
			sPlayerMoveSpeed = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerMoveSpeedEffective = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			
			
			new GuiScrollString(scrollPage, fontRendererObj, xPonyPoints, yPonyPoints, StatCollector.translateToLocal("Pony Points")).setColor(0);
			sPlayerPonyPoints = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			new GuiScrollString(scrollPage, fontRendererObj, xAlignmentPoints, yAlignmentPoints, StatCollector.translateToLocal("Align. Points")).setColor(0);
			sPlayerAlignmentPoints = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			
			
			
			new GuiScrollString(scrollPage, fontRendererObj, xStrength, yStrength+7, StatCollector.translateToLocal("STR")).setColor(0);
			sPlayerStrength = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerStrengthMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			new GuiScrollString(scrollPage, fontRendererObj, xMagic, yMagic+7, StatCollector.translateToLocal("MAG")).setColor(0);
			sPlayerMagic = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerMagicMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			new GuiScrollString(scrollPage, fontRendererObj, xAgility, yAgility+7, StatCollector.translateToLocal("AGL")).setColor(0);
			sPlayerAgility = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerAgilityMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			new GuiScrollString(scrollPage, fontRendererObj, xOffense, yOffense+7, StatCollector.translateToLocal(stats.getAlignmentText(0))).setColor(0);
			sPlayerOffense = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerOffenseMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			new GuiScrollString(scrollPage, fontRendererObj, xDefense, yDefense+7, StatCollector.translateToLocal(stats.getAlignmentText(1))).setColor(0);
			sPlayerDefense = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerDefenseMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
			
			new GuiScrollString(scrollPage, fontRendererObj, xUtility, yUtility+7, StatCollector.translateToLocal(stats.getAlignmentText(2))).setColor(0);
			sPlayerUtility = new GuiScrollString(scrollPage, fontRendererObj).setColor(0);
			sPlayerUtilityMod = new GuiScrollString(scrollPage, fontRendererObj).setColor(13056);
		}
		else
			new GuiScrollString(scrollPage, fontRendererObj, 10, 10, "STATS ARE MISSING").setColor(0);
		
		initGuiScroll();
	}
	
	@Override
	public void updateScrollComponents()
	{
		if (stats != null)
		{
			int stateP = stats.getPonyPoints() > 0? 1: 0;
			int stateA = stats.getAlignmentPoints() > 0? 1: 0;
			upgradeSTR.updateState(stateP);
			upgradeMAG.updateState(stateP);
			upgradeAGL.updateState(stateP);
			upgradeAL1.updateState(stateA);
			upgradeAL2.updateState(stateA);
			upgradeAL3.updateState(stateA);
			
			String display;
			String display2;
			int d1;
			int d2;
			int mod = 0;
			
			
			
			display = ""+stats.getPonyLevel();
			sPlayerPonyLevel.updateStringValue(display).updateStringPos(xPonyLevel+78-display.length()*6, yPonyLevel);
			
			display = ""+stats.dragonFuel.getValue();
			sPlayerDragonPower.updateStringValue(display).updateStringPos(xDragonPower+78-display.length()*6, yDragonPower);
			
			
			display = stats.experiance.getValue()+"/"+stats.getLevelupCost();
			display2 = "";
			sPlayerExperiance.updateStringValue(display).updateStringPos(xExperiance+78, yExperiance);
			sPlayerExperianceToLevel.updateStringValue(display2).updateStringPos(xExperiance, yExperiance);
			
			
			
			display = decimalLimit(""+entityPlayer.getHealth(), 0, true)+"/";
			d1=this.fontRendererObj.getStringWidth(display);
			display2 = decimalLimit(""+entityPlayer.getMaxHealth(), 0, true);
			sPlayerHealth.updateStringValue(display).updateStringPos(xHealth+66-d1, yHealth);
			sPlayerHealthMax.updateStringValue(display2).updateStringPos(xHealth+66, yHealth);
			
			display = decimalLimit(""+Math.round(AbilityPlayerData.clientData.energy), 0, true)+"/";
			d1=this.fontRendererObj.getStringWidth(display);
			display2 = decimalLimit(""+AbilityPlayerData.clientData.energyMax, 0, true);
			sPlayerEnergy.updateStringValue(display).updateStringPos(xEnergy+66-d1, yEnergy);
			sPlayerEnergyMax.updateStringValue(display2).updateStringPos(xEnergy+66, yEnergy);
			
			display = decimalLimit(""+(stats.getEnergyRegen()), 2, true)+"/sec";
			d1=this.fontRendererObj.getStringWidth(display);
			sPlayerEnergyRegen.updateStringValue(display).updateStringPos(xEnergyRegen+84-d1, yEnergyRegen);
			
			
			
			display = "x "+decimalLimit(""+(stats.getMeleeMultiplier()), 2, true);
			d1=this.fontRendererObj.getStringWidth(display);
			sPlayerAttack.updateStringValue(display).updateStringPos(xAttack+66-d1, yAttack);
			
			display = decimalLimit(""+((stats.getEvasion()+AbilityPlayerData.clientData.GetSharedFloat("evasion"))*100), 2, true)+"%";
			d1=this.fontRendererObj.getStringWidth(display);
			sPlayerEvasion.updateStringValue(display).updateStringPos(xEvasion+72-d1 , yEvasion);
			
			display = decimalLimit(""+(stats.getMoveSpeed()*1000), 2, true)+"%";
			d1=this.fontRendererObj.getStringWidth(display);
			display2 = decimalLimit(""+(entityPlayer.getAIMoveSpeed()*1000), 2, true)+"%";
			sPlayerMoveSpeed.updateStringValue(display).updateStringPos(xMoveSpeed+72-d1, yMoveSpeed);
			if (!display.contentEquals(display2))
				sPlayerMoveSpeedEffective.updateStringValue("("+display2+")").updateStringPos(xMoveSpeed+74, yMoveSpeed);
			else
				sPlayerMoveSpeedEffective.updateStringValue("");
			
			
			
			display = ""+stats.getPonyPoints();
			sPlayerPonyPoints.updateStringValue(display).updateStringPos(xPonyPoints+78-display.length()*6, yPonyPoints);
			
			display = ""+stats.getAlignmentPoints();
			sPlayerAlignmentPoints.updateStringValue(display).updateStringPos(xAlignmentPoints+78-display.length()*6, yAlignmentPoints);
			
			
			
			display = ""+stats.strength.getValue();
			sPlayerStrength.updateStringValue(display).updateStringPos(xStrength+52-display.length()*6, yStrength+7);
			mod = stats.getStatModifier(stats.strength);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerStrengthMod.updateStringValue(display2).updateStringPos(xStrength+72, yStrength+7);
			}
			
			display = ""+stats.magic.getValue();
			sPlayerMagic.updateStringValue(display).updateStringPos(xMagic+52-display.length()*6, yMagic+7);
			mod = stats.getStatModifier(stats.magic);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerMagicMod.updateStringValue(display2).updateStringPos(xMagic+72, yMagic+7);
			}
			
			display = ""+stats.agility.getValue();
			sPlayerAgility.updateStringValue(display).updateStringPos(xAgility+52-display.length()*6, yAgility+7);
			mod = stats.getStatModifier(stats.agility);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerAgilityMod.updateStringValue(display2).updateStringPos(xAgility+72, yAgility+7);
			}
			
			display = ""+stats.offense.getValue();
			sPlayerOffense.updateStringValue(display).updateStringPos(xOffense+52-display.length()*6, yOffense+7);
			mod = stats.getStatModifier(stats.offense);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerOffenseMod.updateStringValue(display2).updateStringPos(xOffense+72, yOffense+7);
			}
			
			display = ""+stats.defense.getValue();
			sPlayerDefense.updateStringValue(display).updateStringPos(xDefense+52-display.length()*6, yDefense+7);
			mod = stats.getStatModifier(stats.defense);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerDefenseMod.updateStringValue(display2).updateStringPos(xDefense+72, yDefense+7);
			}
			
			display = ""+stats.utility.getValue();
			sPlayerUtility.updateStringValue(display).updateStringPos(xUtility+52-display.length()*6, yUtility+7);
			mod = stats.getStatModifier(stats.utility);
			if (mod != 0)
			{
				display2 = ""+mod;
				if (mod > 0)
					display2 = "+"+display2;
				sPlayerUtilityMod.updateStringValue(display2).updateStringPos(xUtility+72, yUtility+7);
			}
		}
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int posXmouse, int posYmouse)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal("Stats Page"), 100, 10, 0);
		
		scrollPage.draw(posXmouse, posYmouse);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(mainImage);
		int k = this.guiLeft;
		int l = this.guiTop;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
		
		scrollPage.drawBackground();
	}
	
	public void actionPerformed(GuiButton button)
	{
		switch(button.id)
		{
			case 10:
				upgradeStat(stats.strength);
				break;
			case 11:
				upgradeStat(stats.magic);
				break;
			case 12:
				upgradeStat(stats.agility);
				break;
				
			case 13:
				upgradeStat(stats.offense);
				break;
			case 14:
				upgradeStat(stats.defense);
				break;
			case 15:
				upgradeStat(stats.utility);
				break;
		}
	}
	
	protected void upgradeStat(Stat stat)
	{
		System.out.println("upgrade stat "+stat.getID());
		LoECraftPack.packetHandler.sendToServer(new PacketUpgradeStat().INIT(stat.getID()));
	}
	
	public String decimalLimit(String string, int size, boolean decimal)
	{
		if (string.contains("."))
		{
			for (int i=0; i<string.length(); i++)
			{
				if (string.charAt(i)=='.')
				{
					if (size<=0)
					{
						return string.substring(0, i);
					}
					if ((i+size)<string.length())
					{
						if (!decimal)
						{
							return string.substring(0, i) + string.substring(i+1, i+size+1);
						}
						return string.substring(0, i+size+1);
					}
					String result;
					String decimals = string.substring(i+1, string.length());
					if (!decimal)
					{
						result = string.substring(0, i) + decimals;
					}
					else
						result = string;
					
					if (decimals.length()<size)
					{
						String extraZeros = "";
						for (int i2=0; i2<size-decimals.length();i2++)
						{
							extraZeros += "0";
						}
						result += extraZeros;
					}
					
					return result;
				}
			}
		}
		return string;
	}
}
