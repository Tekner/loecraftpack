package loecraftpack.content.ponies.inventory.gui;

import loecraftpack.content.gui.buttons.GuiButtonCustomBase;
import loecraftpack.content.ponies.inventory.gui.tabs.MasterTabs;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TabButton extends GuiButtonCustomBase {

	protected static final ResourceLocation mouseclick= new ResourceLocation("gui.button.press");
	protected static final ResourceLocation backGroundImage = new ResourceLocation("loecraftpack:gui/tabs/components.png");
	protected MasterTabs thisTab;
	protected static final int sizeX = 12;
	protected static final int sizeY = 12;
	protected static final int startingID = 5;
    
    public TabButton(int id)
    {
    	super(id, 0, 0, sizeX, sizeY, "HI");
    }
    
    public void assignTab(MasterTabs tab)
    {
    	//Columns 2
    	//Rows 2
    	GuiScreen gui = Minecraft.getMinecraft().currentScreen;
    	xPosition = gui.width/2 - (TabButton.sizeX*2 + TabPageButton.sizeX) + ((id-startingID)/2)*sizeX + TabManager.xOffsetByHotbar;
		yPosition = gui.height - sizeY*(2-(id-startingID)%2);
		thisTab = tab;
		this.enabled = (tab != null);
		/*
    	if (gui instanceof GuiContainer)
    	{
    		GuiContainer guiCon = (GuiContainer)gui;
    		xPosition = guiCon.guiLeft + guiCon.xSize;
    		yPosition = guiCon.guiTop + (guiCon.ySize - sizeY*MasterTabs.pageSize)/2 + (id-startingID)*sizeY;
    		
    	}*/
    	visible = true;
    }
    
    @Override
    public void drawButton(Minecraft par1Minecraft, int posX, int posY)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = par1Minecraft.fontRenderer;
            par1Minecraft.getTextureManager().bindTexture(backGroundImage);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            int state = this.getHoverStateOfButton(posX, posY);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 10, (((id-startingID)%2)>0? 12: 0)+state*24, this.width, this.height);
            if (thisTab != null && thisTab.getTabIconImageResource() != null)
            {
            	par1Minecraft.getTextureManager().bindTexture(thisTab.getTabIconImageResource());
            	this.drawIconTiny(this.xPosition + 1, this.yPosition + 2, 0, 0, 8, 8);
            }
            this.mouseDragged(par1Minecraft, posX, posY);
        }
    }
    
    @Override
    public boolean mousePressed(Minecraft par1Minecraft, int par2, int par3)
    {
    	//prevent clicking while the cursor is holding an item.
    	if (par1Minecraft.thePlayer.inventory.getItemStack() == null ? super.mousePressed(par1Minecraft, par2, par3): false)
    	{
    		par1Minecraft.getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(mouseclick, 1.0F));
    		TabManager.switchTab(thisTab);
    	}
    	return false;
    }
    /*
    public void drawIcon(int par1, int par2, int par3, int par4, int par5, int par6)
    {
    	float f = 0.0625F;
		float f1 = 0.0625F;
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(par1 + 0), (double)(par2 + par6), (double)this.zLevel, (double)((float)(par3 + 0) * f), (double)((float)(par4 + par6) * f1));
		tessellator.addVertexWithUV((double)(par1 + par5), (double)(par2 + par6), (double)this.zLevel, (double)((float)(par3 + par5) * f), (double)((float)(par4 + par6) * f1));
		tessellator.addVertexWithUV((double)(par1 + par5), (double)(par2 + 0), (double)this.zLevel, (double)((float)(par3 + par5) * f), (double)((float)(par4 + 0) * f1));
		tessellator.addVertexWithUV((double)(par1 + 0), (double)(par2 + 0), (double)this.zLevel, (double)((float)(par3 + 0) * f), (double)((float)(par4 + 0) * f1));
		tessellator.draw();
    }*/
    
    public void drawIconTiny(int par1, int par2, int par3, int par4, int par5, int par6)
    {
    	float f = 0.125F;
		float f1 = 0.125F;
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV((double)(par1 + 0), (double)(par2 + par6), (double)this.zLevel, (double)((float)(par3 + 0) * f), (double)((float)(par4 + par6) * f1));
		tessellator.addVertexWithUV((double)(par1 + par5), (double)(par2 + par6), (double)this.zLevel, (double)((float)(par3 + par5) * f), (double)((float)(par4 + par6) * f1));
		tessellator.addVertexWithUV((double)(par1 + par5), (double)(par2 + 0), (double)this.zLevel, (double)((float)(par3 + par5) * f), (double)((float)(par4 + 0) * f1));
		tessellator.addVertexWithUV((double)(par1 + 0), (double)(par2 + 0), (double)this.zLevel, (double)((float)(par3 + 0) * f), (double)((float)(par4 + 0) * f1));
		tessellator.draw();
    }
}
