package loecraftpack.content.ponies.inventory.gui;

import loecraftpack.content.gui.buttons.GuiButtonCustomBase;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TabPageButton extends GuiButtonCustomBase {

	protected static final ResourceLocation mouseclick= new ResourceLocation("gui.button.press");
	protected static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:gui/tabs/components.png");
	protected static final int sizeX = 10;
	protected static final int sizeY = 24;
	
	public int meta;
    
    public TabPageButton(int id, int meta, String text)
    {
    	super(id, 0, 0, sizeX, sizeY, text);
    	this.meta = meta;
    }

    @Override
    public void drawButton(Minecraft par1Minecraft, int par2, int par3)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = par1Minecraft.fontRenderer;
            par1Minecraft.getTextureManager().bindTexture(mainImage);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            int state = this.getHoverStateOfButton(par2, par3);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, meta == 0? 0: 22, state*24, this.width, this.height);
            this.mouseDragged(par1Minecraft, par2, par3);
        }
    }
    
    @Override
    public boolean mousePressed(Minecraft par1Minecraft, int par2, int par3)
    {
    	if (super.mousePressed(par1Minecraft, par2, par3))
    	{
    		par1Minecraft.getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(mouseclick, 1.0F));
    		TabManager.changePage(meta==1);
    	}
    	return false;
    }
}
