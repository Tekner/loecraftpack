package loecraftpack.content.ponies.inventory;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class LoePlayerInventory implements IExtendedEntityProperties {
	
	EntityPlayer player;
	InventoryEquipment equipmentInv;
	InventoryEarth earthInv;

	@Override
	public void saveNBTData(NBTTagCompound compound)
	{
		NBTTagCompound loeCompound = new NBTTagCompound();
		
		if (equipmentInv!= null)
		{
			equipmentInv.writeToNBT(loeCompound);
		}
		
		if (earthInv!=null)
		{
			earthInv.writeToNBT(loeCompound);
		}
		
		compound.setTag("LoePlayerInv", loeCompound);
	}

	@Override
	public void loadNBTData(NBTTagCompound compound)
	{
		NBTTagCompound loeCompound = compound.getCompoundTag("LoePlayerInv");
		
		equipmentInv = new InventoryEquipment(loeCompound);
		earthInv = new InventoryEarth(loeCompound);

	}

	@Override
	public void init(Entity entity, World world)
	{
		player = (EntityPlayer) entity;
		if (player != null && player.getGameProfile() != null && world != null)
			equipmentInv = new InventoryEquipment(player.getGameProfile().getId(), world.isRemote);
		earthInv = new InventoryEarth();
	}
	
	public InventoryEquipment getInvEquipment()
	{
		if (equipmentInv == null)
			equipmentInv = new InventoryEquipment(player.getGameProfile().getId(), player.worldObj.isRemote);
		return equipmentInv;
	}
	
	public InventoryEarth getInvEarth()
	{
		if (earthInv == null)
			earthInv = new InventoryEarth();
		return earthInv;
	}

}
