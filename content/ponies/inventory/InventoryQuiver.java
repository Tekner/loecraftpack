package loecraftpack.content.ponies.inventory;

import java.util.UUID;

import loecraftpack.content.registery.LOE_Items;
import loecraftpack.logic.helpers.UUIDHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryQuiver extends InventoryCustom
{
	protected ItemStack[] inventory = new ItemStack[4];
	protected ItemStack container;
	protected UUID itemUUID;
	protected boolean client;
	
	public InventoryQuiver(ItemStack container, boolean client)
	{
		this.client = client;
		if (container != null)
		{
			this.container = container;
			
			if (container.hasTagCompound())
				readFromNBT(container.getTagCompound());
			
			/**used to solve memory address change issue*/
			itemUUID = UUIDHelper.applyUUIDtoStack(container);
		}
	}
	
	@Override
	public int getSizeInventory()
	{
		return inventory.length;
	}
	
	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return inventory[slot];
	}
	
	@Override
	public ItemStack decrStackSize(int slot, int ammount)
	{
		if (this.inventory[slot] != null)
		{
			ItemStack itemstack;
			
			if (this.inventory[slot].stackSize <= ammount)
			{
				itemstack = this.inventory[slot];
				this.inventory[slot] = null;
				
				this.markDirty();
				return itemstack;
			}
			else
			{
				itemstack = this.inventory[slot].splitStack(ammount);
				
				if (this.inventory[slot].stackSize == 0)
				{
					this.inventory[slot] = null;
				}
				
				this.markDirty();
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}
	
	public void damageItemInSlot(int slot, int ammount, EntityLivingBase entityLivingBase)
	{
		ItemStack stack = inventory[slot];
		if (stack.isItemStackDamageable())
			stack.damageItem(ammount, entityLivingBase);
		else if (stack.getMaxStackSize() > 0)
			stack.stackSize--;
		
		if (stack.stackSize<1)
			inventory[slot] = null;
		if (entityLivingBase instanceof EntityPlayer)
			relocateTrueItemStackAndSaveChanges((EntityPlayer)entityLivingBase);
		this.markDirty();
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		if (this.inventory[slot] != null)
		{
			ItemStack itemstack = this.inventory[slot];
			this.inventory[slot] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public void setInventorySlotContents(int slot, ItemStack itemstack)
	{
		this.inventory[slot] = itemstack;
		
		if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit())
		{
			itemstack.stackSize = this.getInventoryStackLimit();
		}
		
		this.markDirty();
	}
	
	@Override
	public String getInventoryName()
	{
		return "Quiver";
	}
	
	@Override
	public boolean hasCustomInventoryName()
	{
		return false;
	}
	
	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemstack)
	{
		Item item = itemstack.getItem();
		return item == Items.arrow || item == LOE_Items.iceArrow;
	}
	
	@Override
	protected void readFromNBT(NBTTagCompound nbt)
	{
		NBTTagList nbttaglist = nbt.getTagList("QuiverInv", 10);
		inventory = new ItemStack[getSizeInventory()];
		
		for (int i = 0; i < nbttaglist.tagCount() && i < inventory.length; ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;
			
			if (j >= 0 && j < this.inventory.length)
			{
				this.inventory[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}
	
	@Override
	protected void writeToNBT(NBTTagCompound nbt)
	{
		NBTTagList nbttaglist = new NBTTagList();
		
		for (int i = 0; i < this.inventory.length; ++i)
		{
			if (this.inventory[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte)i);
				this.inventory[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		
		nbt.setTag("QuiverInv", nbttaglist);
	}
	
	@Override
	public void markDirty()
	{
		super.markDirty();
	}
	
	/**container item, contents do not drop*/
	@Override
	public void dropAllItems(EntityPlayer player) {}
	
	/**used to solve memory address change issue*/
	public void relocateTrueItemStackAndSaveChanges(EntityPlayer player)
	{
		boolean found = false;
		if (itemUUID != null)
		{
			ItemStack newStack = UUIDHelper.findUniqueItemStack(itemUUID, player);
			if (newStack != null)
				container = newStack;
		}
		
		this.writeToNBT(container.getTagCompound());
	}
}
