package loecraftpack.content.ponies.inventory;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class InventoryCustom implements IInventory {
	
	public Boolean dirty = false;
	
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}
	
	protected abstract void readFromNBT(NBTTagCompound nbt);
	
	protected abstract void writeToNBT(NBTTagCompound nbt);
	
	public abstract void dropAllItems(EntityPlayer player);
	
	public void informClientOfChanges(EntityPlayer player){}
	
	@Override
	public void markDirty() {dirty = true;}

}
