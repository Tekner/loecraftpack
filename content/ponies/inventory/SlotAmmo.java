package loecraftpack.content.ponies.inventory;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class SlotAmmo extends Slot
{
	public static IIcon slotIcon;
	
	public SlotAmmo(IInventory inventory, int index, int xPos, int yPos)
	{
		super(inventory, index, xPos, yPos);
	}
	
	public boolean isItemValid(ItemStack itemStack)
    {
		//is ammo?
		return true;
    }
	
	@SideOnly(Side.CLIENT)
    public IIcon getBackgroundIconIndex()
    {
		return slotIcon;
    }
	
	public static void registerSlotIcons(IIconRegister iconRegister)
	{
		slotIcon = iconRegister.registerIcon("loecraftpack:tools/slotAmmo");
	}
}
