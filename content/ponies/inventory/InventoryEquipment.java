package loecraftpack.content.ponies.inventory;

import java.util.HashMap;
import java.util.Map;

import loecraftpack.LoECraftPack;
import loecraftpack.network.packets.PacketUpdateInventory;
import loecraftpack.referrance.enums.PlayerInventoryId;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryEquipment extends InventoryCustom
{
	protected ItemStack[] inventory = new ItemStack[8];
	protected ItemStack[] inventoryOLD = new ItemStack[8];
	String player;
	boolean client;
	
	public InventoryEquipment()
	{
		super();
	}
	
	public InventoryEquipment(String player, boolean client)
	{
		super();
		this.player = player;
		this.client = client;
	}

	public InventoryEquipment(NBTTagCompound nbt)
	{
		super();
		readFromNBT(nbt);
	}

	@Override
	public int getSizeInventory() {
		return inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return inventory[i];
	}

	@Override
	public ItemStack decrStackSize(int par1, int par2)
	{
		if (this.inventory[par1] != null)
		{
			ItemStack itemstack;

			if (this.inventory[par1].stackSize <= par2)
			{
				itemstack = this.inventory[par1];
				this.inventory[par1] = null;
				
				this.markDirty();
				return itemstack;
			}
			else
			{
				itemstack = this.inventory[par1].splitStack(par2);

				if (this.inventory[par1].stackSize == 0)
				{
					this.inventory[par1] = null;
				}
				
				this.markDirty();
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		if (this.inventory[i] != null)
		{
			ItemStack itemstack = this.inventory[i];
			this.inventory[i] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		this.inventory[i] = itemstack;

		if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit())
		{
			itemstack.stackSize = this.getInventoryStackLimit();
		}
		
		markDirty();
	}

	@Override
	public String getInventoryName() {
		return "Additional Equipment";
	}
	
	@Override
	public boolean hasCustomInventoryName() {
		return false;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemStack)
	{
		return true;
	}
	
	protected void readFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		NBTTagList nbttaglist = par1NBTTagCompound.getTagList("SpecialInv", 10);
		
		inventory = new ItemStack[getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;

			if (j >= 0 && j < this.inventory.length)
			{
				this.inventory[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
		markDirty();
	}
	
	protected void writeToNBT(NBTTagCompound par1NBTTagCompound)
	{
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.inventory.length; ++i)
		{
			if (this.inventory[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte)i);
				this.inventory[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		par1NBTTagCompound.setTag("SpecialInv", nbttaglist);
	}

	@Override
	public void dropAllItems(EntityPlayer player)
	{
		int i;

		for (i = 0; i < inventory.length; ++i)
		{
			if (inventory[i] != null)
			{
				player.dropPlayerItemWithRandomChoice(inventory[i], true);
				inventory[i] = null;
			}
		}
		markDirty();
	}
	
	@Override
	public void informClientOfChanges(EntityPlayer player)
	{
		System.out.println("inform "+player.getGameProfile().getId()+" of inv. change");
		
		Map<Integer, ItemStack> changeArray = new HashMap<Integer, ItemStack>();
		
		//scan for changes
		for (int i=0; i<inventory.length; i++)
		{
			if (!(  (inventory[i] == null && inventoryOLD[i] == null) ||
					(inventory[i] != null && inventoryOLD[i] != null && inventory[i].isItemEqual(inventoryOLD[i])) ))
			{
				changeArray.put(Integer.valueOf(i), inventory[i]);
				
				if (inventory[i] != null)
					inventoryOLD[i] = inventory[i].copy();
				else
					inventoryOLD[i] = null;
			}
		}
		LoECraftPack.packetHandler.sendTo(new PacketUpdateInventory().INIT(PlayerInventoryId.EQUIPMENT, changeArray), (EntityPlayerMP) player);
	}

}
