package loecraftpack.content.ponies.abilities;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import loecraftpack.LoECraftPack;
import loecraftpack.logic.handlers.event.HandlerKey;
import loecraftpack.network.packets.PacketCastAbility;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ActiveAbility extends AbilityBase
{
	public static Class[] abilityClasses = new Class[0];
	private static HashMap abilityIdByName = new HashMap<String, Integer>();
	
	public TargetType targetType = TargetType.GENERIC;
	
	protected boolean toggled = false;
	public boolean isToggleable = false;
	private float cycle = 0; // Used while ability is toggled to make cooldown animation continually cycle
	
	protected int energyCost = 0;
	protected float CooldownGlobal = 1.5f;
	public float CooldownNormal = 0;
	protected float Cooldown = 0;
	protected float cooldown = 0;
	public float Casttime = 0;
	protected float casttime = 0;
	
	private boolean held;
	private boolean heldChanged;
	private long time;
	private long lastTime;

	public ActiveAbility(String name, Race race, int cost)
	{
		super(name, race);
		CooldownNormal = 1;
		energyCost = cost;
		isToggleable = true;
	}

	public ActiveAbility(String name, Race race, int cost, float cooldown)
	{
		super(name, race);
		energyCost = cost;
		CooldownNormal = cooldown;
	}

	public ActiveAbility(String name, Race race, int cost, float cooldown, float casttime)
	{
		super(name, race);
		energyCost = cost;
		CooldownNormal = cooldown;
		Casttime = casttime;
	}

	public static void RegisterAbilities(Class... classes)
	{
		System.out.println("Registering New Active Abilities...");
		abilityClasses = classes;
		ItemActiveAbility.num = Math.max(0, abilityClasses.length - 1);
		
		for(ActiveAbility ability : NewAbilityArray())
		{
			abilityIdByName.put(ability.name, ability.ID);
			System.out.println("Successfully registered \"" + ability.name + "\" - ID " + ability.getId());
		}
	}
	
	public static ActiveAbility[] NewAbilityArray()
	{
		ArrayList<ActiveAbility> abilityList = new ArrayList<ActiveAbility>();
		for (int i = 0; i < abilityClasses.length; i++)
		{
			Class c = abilityClasses[i];
			try
			{
				ActiveAbility ability = (ActiveAbility) c.getConstructor().newInstance();
				abilityList.add(ability);
				ability.ID = i;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return abilityList.toArray(new ActiveAbility[0]);
	}

	public void castAbility(float castTimeAdded, boolean syncWithServer)
	{
		System.out.println("Casting Spell: \"" + this.name + "\" - " + isClient());
		
		if (syncWithServer)
			LoECraftPack.packetHandler.sendToServer(new PacketCastAbility().INIT(getId()));
		
		held = true;
		time = System.currentTimeMillis();
		if (isUsable() && cooldown <= 0 && (toggled || playerData.player.capabilities.isCreativeMode || getEnergyCost() <= playerData.energy))
		{
			if (casttime >= getCasttime())
			{
				if ((isClient() && CastSpellClient()) || (!isClient() && CastSpellServer()))
				{
					playerData.setCharge(0, 100);
					if (!toggled)
						playerData.applyGlobalCooldown();
					cooldown = (Cooldown = getMaxCooldown());
				}

				if (isToggleable)
				{
					toggled = !toggled;
					if (!toggled)
					{
						cycle = 0;
						
						if (isClient())
							CastSpellUntoggledClient();
						else
							CastSpellUntoggledServer();
					}
				}
				
				casttime = 0;
			}
			else
			{
				casttime += castTimeAdded;
				playerData.setCharge(casttime, getCasttime());
			}
		}
	}

	public void onUpdate(EntityPlayer player)
	{
		if (cooldown > 0)
			cooldown -= 0.05f;

		if (toggled)
		{
			if (!player.capabilities.isCreativeMode)
				playerData.addEnergy(-getEnergyCostToggled(), isClient());

			if (player.capabilities.isCreativeMode || playerData.energy > getEnergyCostToggled())
				toggled = isClient() ? CastSpellToggledClient() : CastSpellToggledServer();
			else
				toggled = false;

			if (!toggled)
			{
				cycle = 0;
				
				if (isClient())
					CastSpellUntoggledClient();
				else
					CastSpellUntoggledServer();
			}
		}
		
		if (casttime > 0)
		{
			if (player.motionY > 0)
				player.motionY -= 0.1d;
			
			player.motionX = player.motionZ = 0;
		}

		if (time != lastTime || System.currentTimeMillis() - lastTime > 400)
		{
			lastTime = time;
			if (held)
			{
				heldChanged = true;
			}
			else
			{
				if (heldChanged)
				{
					casttime = 0;
					playerData.setCharge(0, 100);
					if (!isClient() && cooldown == 0)
					{
						LoECraftPack.packetHandler.sendTo(new PacketUseAbility().INIT(ID, this.playerData.energy), (EntityPlayerMP) player);
					}
				}

				heldChanged = false;
			}
			held = false;
		}
	}
	
	

	public boolean isToggled()
	{
		return toggled;
	}
	
	public boolean isToggleable()
	{
		return isToggleable;
	}
	
	@SideOnly(Side.CLIENT)
	public static boolean isToggled(int metadata)
	{
		return AbilityPlayerData.clientData.activeAbilities[metadata].isToggled();
	}
	
	public void applyGlobalCooldown()
	{
		if (cooldown < CooldownGlobal)
			cooldown = (Cooldown = CooldownGlobal);
	}

	public float getCooldown()
	{
		if (!toggled)
		{
			if (Cooldown == 0)
				return 0;

			return cooldown / Cooldown;
		}
		else
			return cycle;
	}
	
	@SideOnly(Side.CLIENT)
	public static boolean isUsable(int metadata)
	{
		return AbilityPlayerData.clientData.activeAbilities[metadata].isUsable();
	}
	
	public float getMaxCooldown()
	{
		return CooldownNormal;
	}
	
	public float getCasttime()
	{
		return Casttime;
	}

	public static float getCooldown(int metadata)
	{
		return AbilityPlayerData.clientData.activeAbilities[metadata].getCooldown();
	}
	
	/**
	 * activation cost
	 */
	public float getEnergyCost()
	{
		return energyCost;
	}
	
	/**
	 * activation cost tooltip
	 */
	public String getEnergyCostString()
	{
		return energyCost + " Energy";
	}
	
	/**
	 * sustain cost
	 */
	public float getEnergyCostToggled()
	{
		return 0;
	}
	
	/**
	 * packet triggered Ability logic - please override the method: castSpellServerPacket(Player player, int attemptID, DataInputStream data) throws IOException
	 */
	public void castSpellServerByHandler(EntityPlayer player, PacketBuffer data)
	{
		//Debug: server casting availability check info
		
		if (cooldown <= 0 && casttime >= getCasttime()-0.1f)
		{
			if (castSpellServerPacket(player, data))
			{
				cooldown = (Cooldown = getMaxCooldown());
				casttime = 0;
				return;
			}
		}
		LoECraftPack.packetHandler.sendTo(new PacketUseAbility().INIT(ID, this.playerData.energy), (EntityPlayerMP) player);
	}
	
	/**
	 * packet triggered Ability logic
	 */
	protected boolean castSpellServerPacket(EntityPlayer player, PacketBuffer data){return false;};
	
	/**
	 * Used to perform client-only ability logic and setup.
	 * Called once when the ability is fully cast.
	 */
	protected boolean CastSpellClient()
	{
		if (!playerData.player.capabilities.isCreativeMode)
		{
			if (isUsable() && playerData.energy >= getEnergyCost())
			{
				AbilityPlayerData.clientData.addEnergy(-energyCost, true);
				AbilityPlayerData.clientData.addAfterImage(energyCost);
				return true;
			}
			
			return false;
		}
		return isUsable();
	}
	
	/**
	 * Used to perform server-only ability logic and setup.
	 * Called once when the ability is fully cast.
	 */
	protected boolean CastSpellServer()
	{
		if (!playerData.player.capabilities.isCreativeMode)
		{
			if (isUsable() && playerData.energy >= getEnergyCost())
			{
				playerData.addEnergy(-energyCost, false);
				return true;
			}
			
			return false;
		}
		return isUsable();
	}
	
	/**
	 * Used to perform constant client-only operations and checks.
	 * Called every tick the ability is toggled.
	 */
	protected boolean CastSpellToggledClient(){return true;}

	/**
	 * Used to perform constant server-only operations and checks.
	 * Called every tick the ability is toggled.
	 */
	protected boolean CastSpellToggledServer(){return true;}

	/**
	 * Used to perform cleanup on client-only operations.
	 * Called once when the ability is untoggled.
	 */
	protected void CastSpellUntoggledClient() {}

	/**
	 * Used to perform cleanup on server-only operations.
	 * Called once when the ability is untoggled.
	 */
	protected void CastSpellUntoggledServer() {}
	
	public static int getIdByName(String abilityName)
	{
		return abilityIdByName.get(abilityName) == null ? -1 : (Integer)abilityIdByName.get(abilityName);
	}
}
