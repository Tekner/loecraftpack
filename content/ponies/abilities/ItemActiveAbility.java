package loecraftpack.content.ponies.abilities;

import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.logic.LogicDialog;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemActiveAbility extends Item
{
	public static int num = 0;
	private static IIcon[] icons;
	
	public ItemActiveAbility()
	{
		super();
		this.setHasSubtypes(true);
		this.setUnlocalizedName("itemActiveAbility");
		this.setCreativeTab(LoECraftPack.LoECraftTabAbility);
		this.maxStackSize = 1;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		if (icons == null || icons.length == 0)
			return null;
		return icons[MathHelper.clamp_int(index, 0, num)];
	}
	
	@Override
	public String getUnlocalizedName(ItemStack iconNamestack)
	{
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			if (AbilityPlayerData.clientData.activeAbilities == null || AbilityPlayerData.clientData.activeAbilities.length == 0)
				return "";
			return super.getUnlocalizedName() + "." + AbilityPlayerData.clientData.activeAbilities[MathHelper.clamp_int(iconNamestack.getItemDamage(), 0, num)].icon;
		}
		else
			return super.getUnlocalizedName();
	}
	
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for (int j = 0; j < num+1; ++j)
		{
			list.add(new ItemStack(item, 1, j));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		ActiveAbility[] abilities = AbilityPlayerData.clientData.activeAbilities;
		icons = new IIcon[abilities.length];
			
		for (int i = 0; i < abilities.length; i++)
		{
			if (abilities[i] == null)
				continue;
			
			icons[i] = iconRegister.registerIcon("loecraftpack:abilities/" + abilities[i].icon);
			itemIcon = icons[i];
		}
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
	{
		if (!world.isRemote && !AbilityPlayerData.HasPlayer(player.getGameProfile().getId()))
			return itemStack;
		
		ActiveAbility ability;
		if (world.isRemote)
			ability = AbilityPlayerData.clientData.activeAbilities[itemStack.getItemDamage()];
		else
			ability = AbilityPlayerData.Get(player.getGameProfile().getId()).activeAbilities[itemStack.getItemDamage()];
		
		if (!LoECraftPack.statHandler.isRace(player, ability.race))
		{
			itemStack.stackSize = 0;
			return itemStack;
		}
		ability.castAbility(0.25f, false);
		return itemStack;
	}
	
	private void addLine(List text, String line)
	{
		text.add(LogicDialog.TranslateChatColor(line));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer player, List text, boolean bool)
	{
		if (AbilityPlayerData.clientData.activeAbilities.length > 0)
		{
			ActiveAbility ability = AbilityPlayerData.clientData.activeAbilities[itemStack.getItemDamage()];
			String race = "Unknown Race";
			if (ability.race == Race.EARTH)
				race = "&cEarth Pony&7";
			else if (ability.race == Race.PEGASUS)
				race = "&ePegasus&7";
			else if (ability.race == Race.UNICORN)
				race = "&9Unicorn&7";
			else if (ability.race == Race.ALICORN)
				race = "&fAlicorn&7";
			else if (ability.race == Race.DRAGON)
				race = "&aDragon&7";
			if (ability.isToggleable)
			{
				addLine(text, "&5Toggle&7 - " + race);
				addLine(text, "Cost: " + ability.getEnergyCostString() + " + " + ability.getEnergyCostToggled() + "/s");
				addLine(text, "Cooldown: " + ability.getMaxCooldown() + "s");
			}
			else
			{
				addLine(text, "&5Active&7 - " + race);
				addLine(text, "Cost: " + ability.getEnergyCostString());
				addLine(text, "Cooldown: " + ability.getMaxCooldown() + "s");
				addLine(text, "Cast-Time: " + (ability.Casttime>0?ability.getCasttime()+"s":"Instant"));
			}
			
			addLine(text, "&fInfo:");
			for(String line : ability.GetDescription().split("\n"))
				addLine(text, "&f" + line);
		}
	}
}