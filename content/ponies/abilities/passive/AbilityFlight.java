package loecraftpack.content.ponies.abilities.passive;

import ibxm.Player;
import io.netty.buffer.Unpooled;
import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.logic.handlers.event.HandlerKey;
import loecraftpack.network.packets.PacketFlightStatus;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class AbilityFlight extends PassiveAbility
{
	private int keyPressTime = 0; //Used as a grounded timer on the server
	private int wetTime = 0;
	private boolean stunned = false;
	private int cooldown = 0;
	public boolean flying = false;
	private double xAcceleration = 0;
	private double yAcceleration = 0;
	private double zAcceleration = 0;
	private float maxSpeed = 5; //in meters per second
	private final double dtr = Math.PI/180d; //Degree To Radian
	private final double halfAngle = Math.sqrt(2)/2d; //Sine-Cosine of 45 Degrees
	
	public AbilityFlight()
	{
		super("Flight", Race.PEGASUS, 1);
	}
	
	private float getEnergyCost()
	{
		return (playerStats.getEnergyRegen() + (100 / Stats.GetAlignmentCurve(getAlignmentPoints(), 79.12f, 40.88f, 60))) / 20f; //Drains energy equal to a curve so that flying lasts approx 60s at 0 Flight skill and 180s at 99 Flight skill.
	}
	
	private int getMaxHeight()
	{
		return 100 + (int)((getAlignmentPoints() / 99f) * 156);
	}
	
	@Override
	public void onUpdateServer(EntityPlayer player)
	{
		if (flying)
		{
			if (player.onGround)
				keyPressTime ++;
			else
				keyPressTime = 0;
			
			if (keyPressTime < 10)
			{
				player.motionY = applyFriction(player.motionY, 0.2d) - (player.isWet()?0.015d:0.005d);
				playerData.addEnergy(-getEnergyCost(), false);
			}
			else
			{
				flying = false;
				keyPressTime = 0;
			}
		}
	}
	
	@Override
	public void onUpdateClient(EntityPlayer player)
	{
		boolean noScreen = Minecraft.getMinecraft().currentScreen == null;
		boolean petrified = player.isPotionActive(LoECraftPack.potionPetrified);
		
		double modifier = 1.0d;
		if (player.isPotionActive(LoECraftPack.potionPetrified))
		{
			if (player.getActivePotionEffect(LoECraftPack.potionPetrified).getAmplifier()==0)
				modifier *= 0.5;
			else
				return;
		}
		
		if (HandlerKey.getKeyDown(HandlerKey.jump) && noScreen && player.onGround)
		{
			player.motionY = 0.5*modifier;
		}
		if (HandlerKey.getKeyHeldDown(HandlerKey.jump) && player.motionY > 0 && player.motionY < 0.3f)
			player.motionY *= 1.0d+(0.25d*modifier);
		
		if (HandlerKey.getKeyDown(HandlerKey.jump) && noScreen && !player.capabilities.isCreativeMode)
		{
			boolean _flying = !flying;
			
			if (_flying && cooldown == 0 && wetTime == 0 && !player.isBurning() && !stunned && !petrified)
			{
				if (playerData.energy >= 20)
				{
					player.setSprinting(false);
					cooldown = 20;
					yAcceleration = player.motionY + 0.5d;
					xAcceleration = player.motionX;
					zAcceleration = player.motionZ;
				}
				else
				{
					player.addChatMessage(new ChatComponentText("You don't have enough energy to start flying yet!"));
					_flying = false;
				}
			}
			else
			{
				_flying = false;
				if (petrified)
					player.addChatMessage(new ChatComponentText("You can't fly while Pertrified!"));
				else if (stunned)
					player.addChatMessage(new ChatComponentText("You can't fly while you're stunned!"));
				else if (wetTime > 0)
					player.addChatMessage(new ChatComponentText("You can't fly while your wings are wet!"));
				else if (player.isBurning())
					player.addChatMessage(new ChatComponentText("You can't fly while your wings are on fire!"));
			}
			
			SetFlying(_flying);
		}
		if (cooldown > 0)
			cooldown--;
		if (wetTime > 0)
			wetTime--;
		
		if (player.isInWater() || player.isBurning() || player.onGround || player.capabilities.isCreativeMode)
		{
			if (player.isInWater() && wetTime < 99)
				wetTime += 2;
			
			stunned = false;
			if (player.onGround)
			{
				flying = false;
				SetSharedData("flying", false);
			}
			else
				SetFlying(false);
		}
		else if (flying)
		{
			if (noScreen)
			{
				double multiplier = player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getAttributeValue() / (double)player.capabilities.getWalkSpeed();
				maxSpeed = playerStats.getMoveSpeed() * 50;
				
				if (HandlerKey.getKeyHeldDown(HandlerKey.forward))
				{
					if (HandlerKey.getKeyHeldDown(HandlerKey.left) != HandlerKey.getKeyHeldDown(HandlerKey.right))
					{
						double tempYAcc = Math.sin(player.rotationPitch*dtr) * maxSpeed/80d * halfAngle;
						if (tempYAcc > 0 || (tempYAcc < 0 && player.posY < getMaxHeight()))
							yAcceleration -= tempYAcc * multiplier;
						xAcceleration -= Math.cos(player.rotationPitch*dtr) * Math.sin(player.rotationYaw*dtr) * maxSpeed/80d * halfAngle * multiplier;
						zAcceleration += Math.cos(player.rotationPitch*dtr) * Math.cos(player.rotationYaw*dtr) * maxSpeed/80d * halfAngle * multiplier;
					}
					else
					{
						double tempYAcc = Math.sin(player.rotationPitch*dtr) * maxSpeed/80d;
						if (tempYAcc > 0 || (tempYAcc < 0 && player.posY < getMaxHeight()))
							yAcceleration -= tempYAcc * multiplier;
						xAcceleration -= Math.cos(player.rotationPitch*dtr) * Math.sin(player.rotationYaw*dtr) * maxSpeed/80d * multiplier;
						zAcceleration += Math.cos(player.rotationPitch*dtr) * Math.cos(player.rotationYaw*dtr) * maxSpeed/80d * multiplier;
					}
				}
				
				if (HandlerKey.getKeyHeldDown(HandlerKey.back))
				{
					if (HandlerKey.getKeyHeldDown(HandlerKey.left) != HandlerKey.getKeyHeldDown(HandlerKey.right))
					{
						double tempYAcc = Math.sin(player.rotationPitch*dtr) * maxSpeed/80d * halfAngle;
						if (tempYAcc < 0 || (tempYAcc > 0 && player.posY < getMaxHeight()))
							yAcceleration += tempYAcc * multiplier;
						xAcceleration += Math.cos(player.rotationPitch*dtr) * Math.sin(player.rotationYaw*dtr) * maxSpeed/80d * halfAngle * multiplier;
						zAcceleration -= Math.cos(player.rotationPitch*dtr) * Math.cos(player.rotationYaw*dtr) * maxSpeed/80d * halfAngle * multiplier;
					}
					else
					{
						double tempYAcc = Math.sin(player.rotationPitch*dtr) * maxSpeed/80d;
						if (tempYAcc < 0 || (tempYAcc > 0 && player.posY < getMaxHeight()))
							yAcceleration += tempYAcc * multiplier;
						xAcceleration += Math.cos(player.rotationPitch*dtr) * Math.sin(player.rotationYaw*dtr) * maxSpeed/80d * multiplier;
						zAcceleration -= Math.cos(player.rotationPitch*dtr) * Math.cos(player.rotationYaw*dtr) * maxSpeed/80d * multiplier;
					}
				}
				
				if (HandlerKey.getKeyHeldDown(HandlerKey.left))
				{
					if (HandlerKey.getKeyHeldDown(HandlerKey.forward) != HandlerKey.getKeyHeldDown(HandlerKey.back))
					{
						xAcceleration -= Math.sin((player.rotationYaw-90)*dtr) * maxSpeed/80d * halfAngle * multiplier;
						zAcceleration += Math.cos((player.rotationYaw-90)*dtr) * maxSpeed/80d * halfAngle * multiplier;
					}
					else
					{
						xAcceleration -= Math.sin((player.rotationYaw-90)*dtr) * maxSpeed/80d * multiplier;
						zAcceleration += Math.cos((player.rotationYaw-90)*dtr) * maxSpeed/80d * multiplier;
					}
				}
				
				if (HandlerKey.getKeyHeldDown(HandlerKey.right))
				{
					if (HandlerKey.getKeyHeldDown(HandlerKey.forward) != HandlerKey.getKeyHeldDown(HandlerKey.back))
					{
						xAcceleration -= Math.sin((player.rotationYaw+90)*dtr) * maxSpeed/80d * halfAngle * multiplier;
						zAcceleration += Math.cos((player.rotationYaw+90)*dtr) * maxSpeed/80d * halfAngle * multiplier;
					}
					else
					{
						xAcceleration -= Math.sin((player.rotationYaw+90)*dtr) * maxSpeed/80d * multiplier;
						zAcceleration += Math.cos((player.rotationYaw+90)*dtr) * maxSpeed/80d * multiplier;
					}
				}
				
				//Handle up/down
				if (HandlerKey.getKeyHeldDown(HandlerKey.jump) && player.posY < getMaxHeight())
					yAcceleration += maxSpeed/80d  * multiplier;
				if (HandlerKey.getKeyHeldDown(HandlerKey.sneak))
					yAcceleration -= maxSpeed/80d * multiplier;
			}
			
			//Apply friction
			yAcceleration = applyFriction(yAcceleration, 0.2d) - ((HandlerKey.getKeyHeldDown(HandlerKey.sneak) && noScreen)?0.0d:player.isWet()?0.03d:0.01d);
			xAcceleration = applyFriction(xAcceleration, 0.2d);
			zAcceleration = applyFriction(zAcceleration, 0.2d);
			
			//Apply acceleration
			player.motionY = yAcceleration;
			player.motionX = xAcceleration;
			player.motionZ = zAcceleration;
			
			playerData.restoreOrDrainEnergy(-getEnergyCost());
			if (playerData.energy == 0)
			{
				SetFlying(false);
				if (isClient())
				{
					stunned = true;
					playerData.player.addChatMessage(new ChatComponentText("You've run out of energy!"));
				}
			}
		}
	}
	
	private void UpdateServer()
	{
		LoECraftPack.packetHandler.sendToServer(new PacketFlightStatus().INIT(flying));
	}
	
	public void SetFlying(boolean flying)
	{
		if (this.flying != flying)
		{
			this.flying = flying;
			if (flying)
			{
				playerData.addEnergy(-5, isClient());
				SetSharedData("evasion", 0f); //Turn off Wind Dance
			}
			else
				playerData.player.fallDistance = 0;
			SetSharedData("flying", flying);
			if (isClient())
				UpdateServer();
			else if (!flying)
				LoECraftPack.packetHandler.sendTo(new PacketFlightStatus().INIT(flying), (EntityPlayerMP) playerData.player);
		}
	}
	
	private double applyFriction(double input, double multiplier)
	{
		if (input > 0)
			input -= Math.max(0.005d, input*multiplier);
		else if (input < 0)
			input -= Math.min(-0.005d, input*multiplier);
		
		return input;
	}
	
	@Override
	public void FallEvent(LivingFallEvent event)
	{
		if (flying)
			event.setCanceled(true);
		else
			event.distance *= 0.75f;
	}
	
	@Override
	public void HurtEvent(LivingHurtEvent event)
	{
		if (flying)
		{
			if (event.source instanceof EntityDamageSource)
			{
				playerData.addEnergy(-40, false);
				playerData.sendChangingPlayerStatPacket();
			}
			else if (event.source == DamageSource.lava || event.source == DamageSource.inFire || event.source == DamageSource.onFire)
			{
				SetFlying(false);
			}
		}
	}
	
	@Override
	public String GetDescription()
	{
		return "Double tap jump to fly.\nBe careful not to get hit while flying or get your wings wet.\nFlight drains your energy while active.";
	}
}
