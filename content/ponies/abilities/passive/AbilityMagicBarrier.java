package loecraftpack.content.ponies.abilities.passive;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class AbilityMagicBarrier extends PassiveAbility
{
	private int timer = 1;
	public AbilityMagicBarrier()
	{
		super("Magic Barrier", Race.UNICORN, 20);
	}
	
	@Override
	public void onUpdateServer(EntityPlayer player)
	{
		if (player.getAbsorptionAmount() > 0 && player.isBurning())
			player.extinguish();
		
		int period = 10;
		if (timer++ == 0 && player.getAbsorptionAmount() < getMaxShieldAmount())
			player.setAbsorptionAmount(Math.min(getMaxShieldAmount(), player.getAbsorptionAmount() + (getMaxShieldAmount() / 10)));
		
		if (timer >= period)
			timer = 0;
	}
	
	private int getMaxShieldAmount()
	{
		return getMaxShieldAmount(getAlignmentPoints());
	}
	
	public static int getMaxShieldAmount(int alignment)
	{
		return (int)(10 + (alignment / 99) * 90);
	}
	
	@Override
	protected void HurtEvent(LivingHurtEvent event)
	{
		if (playerData.player.getAbsorptionAmount() > 0)
		{
			if (event.source == DamageSource.onFire)
				event.setCanceled(true);
			else if (event.source.isMagicDamage())
				event.ammount *= 0.5f;
			else if (event.source == DamageSource.lava)
				event.ammount *= 0.1f;
		}
	}
	
	@Override
	public String GetDescription()
	{
		return "You surround yourself with a magical shield.\nThis shield is hit before your armor and does a better job\ndeflecting magic and fire damage than physical damage.";
	}
}
