package loecraftpack.content.ponies.abilities.passive;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.logic.handlers.event.HandlerKey;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingFallEvent;

public class AbilityLeapingBound extends PassiveAbility
{
	private int jumping = 0;
	
	public AbilityLeapingBound()
	{
		super("Leaping Bound", Race.EARTH, 1);
	}
	
	@Override
	public void onUpdateClient(EntityPlayer player)
	{
		boolean noScreen = Minecraft.getMinecraft().currentScreen == null;
		
		double modifier = 1.0d;
		if (player.isPotionActive(LoECraftPack.potionPetrified))
		{
			if (player.getActivePotionEffect(LoECraftPack.potionPetrified).getAmplifier()==0)
				modifier *= 0.5;
			else
				return;
		}
		
		if (player.onGround)
			jumping = 0;
		if (HandlerKey.jump.isPressed() && noScreen && jumping < 2)
		{
			if (jumping == 0 && player.motionY < 0)
				jumping++;
			jumping++;
			player.motionY = 0.5*modifier;
		}
		if (HandlerKey.getKeyDown(HandlerKey.jump) && player.motionY > 0 && player.motionY < 0.3f)
			player.motionY *= 1.0d+(0.25d*modifier);
	}
	
	@Override
	public void FallEvent(LivingFallEvent event)
	{
		event.distance -= 4f;
	}
	
	@Override
	public String GetDescription()
	{
		return "Your jump height has been doubled and you can double jump.\nYou also take slightly less fall damage.";
	}
}
