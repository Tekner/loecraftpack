package loecraftpack.content.ponies.abilities.passive;

import java.util.Random;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class AbilityToughHide extends PassiveAbility
{
	private Random rand = new Random();
	
	public AbilityToughHide()
	{
		super("Tough Hide", Race.EARTH);
		alignment = Willpower;
	}
	
	@Override
	protected void HurtEvent(LivingHurtEvent event)
	{
		if (event.source instanceof EntityDamageSource)
		{
			float damageReduc = 0.2f;
			event.ammount *= Math.max(0.01f, (event.source instanceof EntityDamageSourceIndirect ? (1-damageReduc*2.5f) : (1-damageReduc)) - GetSharedFloat("dmgreduc"));
			playerData.addEnergy(25, false);
			playerData.sendChangingPlayerStatPacket();
		}
	}
	
	@Override
	public String GetDescription()
	{
		return "Protective Hide reduces enemy damage taken by 20%\n(50% for projectiles.)";
	}
}
