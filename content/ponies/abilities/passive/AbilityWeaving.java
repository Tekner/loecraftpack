package loecraftpack.content.ponies.abilities.passive;

import java.util.Random;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.abilities.mechanics.PonyDodgedEvent;
import loecraftpack.referrance.enums.Race;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class AbilityWeaving extends PassiveAbility
{
	private Random rand = new Random();
	
	public AbilityWeaving()
	{
		super("Weaving", Race.PEGASUS);
		alignment = Reflexes;
	}
	
	@Override
	protected void DodgedEvent(PonyDodgedEvent event)
	{
		playerData.player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 60, (int)(getAlignmentPoints() / 19.8f)));
	}
	
	@Override
	public String GetDescription()
	{
		return "You gain a small movement speed buff\nwhenever you evade an enemy attack.";
	}
}
