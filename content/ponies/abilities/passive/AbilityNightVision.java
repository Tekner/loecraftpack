package loecraftpack.content.ponies.abilities.passive;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class AbilityNightVision extends PassiveAbility
{
	int timer = 0;
	PotionEffect nightVision = new PotionEffect(Potion.nightVision.id, 320, 0);
	public AbilityNightVision()
	{
		super("Night Vision", Race.EARTH, 20);
	}
	
	@Override
	public void onUpdateServer(EntityPlayer player)
	{
		if (isDark(player))
		{
			if (timer < 1)
				timer++;
			else
				ApplyNightVision(player, true);
		}
		else
		{
			if (timer > 0)
				timer--;
			else
				ApplyNightVision(player, false);
		}
	}
	
	private boolean isDark(EntityPlayer player)
	{
		return player.worldObj.getBlockLightValue((int)player.posX, (int)player.posY + 1, (int)player.posZ) < 7;
	}
	
	private void ApplyNightVision(EntityPlayer player, boolean enabled)
	{
		if (enabled)
		{
			nightVision.duration = 320;
			player.addPotionEffect(nightVision);
		}
		else
			player.removePotionEffect(Potion.nightVision.id);
	}
	
	@Override
	public String GetDescription()
	{
		return "After being in the dark for a few seconds, your eyes\nautomatically adapt and let you see in the dark.";
	}
}
