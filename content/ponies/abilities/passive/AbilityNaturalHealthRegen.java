package loecraftpack.content.ponies.abilities.passive;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;

public class AbilityNaturalHealthRegen extends PassiveAbility
{
	int foodTimer = 0;
	int timer = 0;
	
	public AbilityNaturalHealthRegen()
	{
		super("Natural Health Regeneration", Race.NONE, 20);
		HideInAbilityList();
	}
	
	private int counter = 0;
	private float factor = 1, healthDif = 0, healAmount = 0;
	@Override
	public void onUpdateServer(EntityPlayer plr)
	{
		plr.getFoodStats().foodTimer = 0; //Disable Minecraft's health regeneration logic
		if (++timer >= 4 && plr.getFoodStats().getFoodLevel() > 6 && plr.shouldHeal())
		{
			factor = (plr.getFoodStats().getFoodLevel() + 6) / 26f; //1-0.5 from 20-7 food level
			healthDif = plr.getMaxHealth() - plr.getHealth();
			healAmount = factor * Stats.GetStatLinear(playerStats.getPonyLevel(), 1, 5); //1-5 health restored
			plr.heal(healAmount); //1-5 hp (x1.5 for EP) regenerated every 4 seconds from STR 0-999
            plr.addExhaustion(factor * (healthDif < healAmount ? healthDif/healAmount : 1) *
            					Stats.GetStatLinear(playerStats.getPonyLevel(), 3, 1.5f)); //3-1.5 exhaustion per regeneration tick
            timer = 0;
		}
	}
}
