package loecraftpack.content.ponies.abilities.passive;

import java.util.Random;

import loecraftpack.content.ponies.abilities.PassiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;

public class AbilityWindDancePassive extends PassiveAbility
{
	private Random rand = new Random();
	
	public AbilityWindDancePassive()
	{
		super("Wind Dance Passive", Race.PEGASUS);
		HideInAbilityList();
	}
	
	@Override
	public void onUpdateClient(EntityPlayer player)
	{
		if (playerData.energy == 0)
			SetSharedData("evasion", 0);
	}
	
	@Override
	public void onUpdateServer(EntityPlayer player)
	{
		if (playerData.energy == 0)
			SetSharedData("evasion", 0);
	}
}
