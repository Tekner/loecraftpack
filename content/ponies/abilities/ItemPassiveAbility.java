package loecraftpack.content.ponies.abilities;

import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.logic.LogicDialog;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemPassiveAbility extends Item
{
	public static int num = 0;
	private static IIcon[] icons;
	
	public ItemPassiveAbility()
	{
		super();
		this.setHasSubtypes(true);
		this.setUnlocalizedName("itemPassiveAbility");
        this.setCreativeTab(LoECraftPack.LoECraftTabAbility);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int index)
	{
		if (icons == null || icons.length == 0)
			return null;
		return icons[MathHelper.clamp_int(index, 0, num)];
	}
	
	@Override
	public String getUnlocalizedName(ItemStack iconNamestack)
	{
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			if (AbilityPlayerData.clientData.passiveAbilities == null || AbilityPlayerData.clientData.passiveAbilities.length == 0)
				return "";
			return super.getUnlocalizedName() + "." + AbilityPlayerData.clientData.passiveAbilities[MathHelper.clamp_int(iconNamestack.getItemDamage(), 0, num)].icon;
		}
		else
			return super.getUnlocalizedName();
	}
	
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for (int j = 0; j < num+1; ++j)
    	{
    		list.add(new ItemStack(item, 1, j));
    	}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister)
	{
		PassiveAbility[] abilities = AbilityPlayerData.clientData.passiveAbilities;
	    icons = new IIcon[abilities.length];
	        
		for (int i = 0; i < abilities.length; i++)
		{
			if (abilities[i] == null)
				continue;
			
	    	icons[i] = iconRegister.registerIcon("loecraftpack:abilities/" + abilities[i].icon);
	    	itemIcon = icons[i];
		}
	}
	
	private void addLine(List text, String line)
	{
		text.add(LogicDialog.TranslateChatColor(line));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer player, List text, boolean bool)
	{
		if (AbilityPlayerData.clientData.passiveAbilities.length > 0)
		{
			PassiveAbility ability = AbilityPlayerData.clientData.passiveAbilities[itemStack.getItemDamage()];
			String race = "Unknown Race";
			if (ability.race == Race.EARTH)
				race = "&cEarth Pony&7";
			else if (ability.race == Race.PEGASUS)
				race = "&ePegasus&7";
			else if (ability.race == Race.UNICORN)
				race = "&9Unicorn&7";
			else if (ability.race == Race.ALICORN)
				race = "&fAlicorn&7";
			else if (ability.race == Race.DRAGON)
				race = "&aDragon&7";
			addLine(text, "&5Passive&7 - " + race);
			
	
			addLine(text, "&fInfo:");
			for(String line : ability.GetDescription().split("\n"))
				addLine(text, "&f" + line);
		}
	}
}
