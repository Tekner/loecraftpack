package loecraftpack.content.ponies.abilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import loecraftpack.LoECraftPack;
import loecraftpack.referrance.enums.Race;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.content.ponies.stats.components.StatFlags;
import net.minecraft.nbt.NBTTagCompound;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

public abstract class AbilityBase
{
	private static final Map<String, List<Class>> abilityNbtData = new HashMap<String, List<Class>>();
	protected final List<INBTData> nbtData = new ArrayList<INBTData>();
	
	public String name;
	public String icon;
	protected Race race = Race.NONE;
	private boolean isClient = false;
	
	protected static final int Storm    = 0, Elemental   = 0, Combat    = 0,
							   Reflexes = 1, Restoration = 1, Willpower = 1,
							   Flight   = 2, Arcane      = 2, Nature    = 2;
	protected int alignment = -1;
	private Stat[] alignments;
	protected int minAlignmentNeeded = 0; //Amount of alignment needed to be able to use ability
	
	protected String playerName = "";
	protected AbilityPlayerData playerData = null;
	protected Stats playerStats = null;
	protected int ID = -1;
	private boolean showInAbilityList = true;
	
	public AbilityBase(String name, Race race)
	{
		this.isClient = (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT);
		this.name = name;
		this.icon = name.toLowerCase().replace(" ", "").replace(":", "");
		this.race = race;
		
		List<Class> list = abilityNbtData.get(name);
		if (list != null)
			for(Class c : list)
			{
				try
				{
					nbtData.add((INBTData)c.getConstructor().newInstance());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
	}
	
	public static void AddNBTDataExtension(String abilityName, Class clazz)
	{
		for (Class interfaces: clazz.getInterfaces())
		{
			if (interfaces == INBTData.class)
			{
				if (abilityNbtData.containsKey(abilityName))
					abilityNbtData.get(abilityName).add(clazz);
				else
				{
					List<Class>list = new ArrayList<Class>();
					list.add(clazz);
					abilityNbtData.put(abilityName, list);
				}
				break;
			}
		}
	}
	
	public void SetPlayer(String player, AbilityPlayerData data)
	{
		playerName = player;
		playerData = data;
		playerStats = (Stats)LoECraftPack.statHandler.stats.get(player);
		alignments = new Stat[] { playerStats.offense, playerStats.defense, playerStats.utility };
	}
	
	//Returns the value of the ability's alignment, or the sum of the player's alignments if the ability did not set its alignment (universal race ability)
	protected int getAlignmentPoints()
	{
		if (playerStats != null)
		{
			if (alignment == -1)
				return playerStats.getStatTotal(StatFlags.Alignment);
			else if (alignment < 3)
				return playerStats.getStatValueWithModifiers(alignments[alignment]);
		}
		
		return 0;
	}
	
	public boolean isUsable()
	{
		return getAlignmentPoints() >= minAlignmentNeeded;
	}
	
	public boolean isClient()
	{
		return isClient;
	}
	
	public Race GetRace()
	{
		return race;
	}
	
	public int getId()
	{
		return ID;
	}
	
	public boolean ShowInAbilityList()
	{
		return showInAbilityList;
	}
	
	protected void HideInAbilityList()
	{
		showInAbilityList = false;
	}
	
	public String GetDescription()
	{
		return "There is no description at this time.";
	}
	
	protected void SetSharedData(String name, Object data)
	{
		playerData.SetSharedData(name, data);
	}
	
	protected Boolean GetSharedFlag(String name)
	{
		return playerData.GetSharedFlag(name);
	}
	
	protected Integer GetSharedInt(String name)
	{
		return playerData.GetSharedInt(name);
	}
	
	protected Float GetSharedFloat(String name)
	{
		return playerData.GetSharedFloat(name);
	}
	
	public final void loadFromNBT(NBTTagCompound nbt)
	{
		readFromNBT(nbt);
		
		//extended data
		for(INBTData data : nbtData)
			data.readFromNBT(nbt);
	}
	
	public final void SaveToNBT(NBTTagCompound nbt)
	{
		writeToNBT(nbt);
		
		//extended data
		for(INBTData data : nbtData)
			data.writeToNBT(nbt);
	}
	
	protected void readFromNBT(NBTTagCompound nbt)
	{
		
	}
	
	protected void writeToNBT(NBTTagCompound nbt)
	{
		
	}
}
