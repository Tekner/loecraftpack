package loecraftpack.content.ponies.abilities;

import net.minecraft.nbt.NBTTagCompound;

public interface INBTData
{
	void readFromNBT(NBTTagCompound nbt);
	
	void writeToNBT(NBTTagCompound nbt);
	
}
