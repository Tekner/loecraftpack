package loecraftpack.content.ponies.abilities.projectiles;

import loecraftpack.content.ponies.abilities.mechanics.MechanicExplosion;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.world.World;

public class Fireball extends EntityLargeFireball
{
	public static void SpawnFireballOnPlayer(EntityPlayer player, float size, float force, float damage, boolean mobFlamey, boolean blockFlamey)
	{
		Fireball fireball = new Fireball(player.worldObj, player, player.getLookVec().xCoord/10d, player.getLookVec().yCoord/10d, player.getLookVec().zCoord/10d);
		fireball.size = size;
		fireball.force = force;
		fireball.damage = damage;
		fireball.mobFlamey = mobFlamey;
		fireball.blockFlamey = blockFlamey;
		player.worldObj.spawnEntityInWorld(fireball);
	}
	
	public static void SpawnFireballOnPlayer(EntityPlayer player, float size, float force, float damage, boolean mobFlamey, boolean blockFlamey, boolean destroyBlocks, boolean dropBlocks, float chanceToDrop)
	{
		Fireball fireball = new Fireball(player.worldObj, player, player.getLookVec().xCoord/10d, player.getLookVec().yCoord/10d, player.getLookVec().zCoord/10d);
		fireball.size = size;
		fireball.force = force;
		fireball.damage = damage;
		fireball.mobFlamey = mobFlamey;
		fireball.blockFlamey = blockFlamey;
		fireball.destroyBlocks = destroyBlocks;
		fireball.dropBlocks = dropBlocks;
		fireball.chanceToDrop = chanceToDrop;
		player.worldObj.spawnEntityInWorld(fireball);
	}
	
	public static void SpawnFireballOnPlayer(EntityPlayer player, float size, float force, float damage, boolean mobFlamey, boolean blockFlamey, boolean destroyBlocks, boolean dropBlocks, float chanceToDrop, boolean smokey, boolean explodey, PotionEffect[] potionEffects)
	{
		Fireball fireball = new Fireball(player.worldObj, player, player.getLookVec().xCoord/10d, player.getLookVec().yCoord/10d, player.getLookVec().zCoord/10d);
		fireball.size = size;
		fireball.force = force;
		fireball.damage = damage;
		fireball.mobFlamey = mobFlamey;
		fireball.blockFlamey = blockFlamey;
		fireball.destroyBlocks = destroyBlocks;
		fireball.dropBlocks = dropBlocks;
		fireball.chanceToDrop = chanceToDrop;
		fireball.smokey = smokey;
		fireball.explodey = explodey;
		fireball.potionEffects = potionEffects;
		player.worldObj.spawnEntityInWorld(fireball);
	}
	
	private float size = 1, force = 0, damage = 1, chanceToDrop = 0;
	private boolean mobFlamey = false, blockFlamey, destroyBlocks = false, dropBlocks = false, smokey = true, explodey = true;
	private PotionEffect[] potionEffects = new PotionEffect[0];
	
	public Fireball(World world, EntityLivingBase player, double xMotion, double yMotion, double zMotion)
	{
		super(world, player, xMotion, yMotion, zMotion);
		this.accelerationX = xMotion;
        this.accelerationY = yMotion;
        this.accelerationZ = zMotion;
        this.posY ++;
	}
	
	@Override
	protected void onImpact(MovingObjectPosition target)
    {
        if (!this.worldObj.isRemote)
        {
        	if (target.typeOfHit == MovingObjectType.BLOCK)
        	{
        		double x = (int)target.hitVec.xCoord;
    			double y = (int)target.hitVec.yCoord;
    			double z = (int)target.hitVec.zCoord;
    			switch(target.sideHit)
				{
					case 0: y += 0.5d; break;
					case 1: y -= 0.5d; break;
					case 2: z += 0.5d; break;
					case 3: z -= 0.5d; break;
					case 4: x += 0.5d; break;
					case 5: x -= 0.5d; break;
				}
        		if (this.worldObj.isAirBlock((int)x, (int)y, (int)z) ||
        				this.worldObj.getBlock((int)x, (int)y, (int)z).getCollisionBoundingBoxFromPool(worldObj, (int)x, (int)y, (int)z) == null)
        			return;
        	}
        	MechanicExplosion.ExplodeCustomDamage(this.worldObj, this.shootingEntity, this.posX, this.posY, this.posZ, size, damage, mobFlamey, blockFlamey, smokey, explodey, destroyBlocks, dropBlocks, force, chanceToDrop, potionEffects);
        	
            if (target.entityHit != null)
            	target.entityHit.attackEntityFrom(DamageSource.causeFireballDamage(this, this.shootingEntity), 6);
            
            this.setDead();
        }
    }
}
