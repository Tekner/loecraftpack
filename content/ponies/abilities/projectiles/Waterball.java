package loecraftpack.content.ponies.abilities.projectiles;

import java.util.List;

import loecraftpack.content.ponies.abilities.mechanics.MechanicExplosion;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Waterball extends Entity
{
	public static void SpawnWaterballOnPlayer(EntityPlayer player, float size, float force, float damage, PotionEffect[] potionEffects)
	{
		Waterball waterball = new Waterball(player.worldObj, player, player.getLookVec().xCoord/10d, player.getLookVec().yCoord/10d, player.getLookVec().zCoord/10d);
		waterball.size = size;
		waterball.force = force;
		waterball.damage = damage;
		waterball.potionEffects = potionEffects;
		player.worldObj.spawnEntityInWorld(waterball);
	}
	
	public static void SpawnWaterballOnPlayer(EntityPlayer player, float size, float force, float damage)
	{
		Waterball waterball = new Waterball(player.worldObj, player, player.getLookVec().xCoord/10d, player.getLookVec().yCoord/10d, player.getLookVec().zCoord/10d);
		waterball.size = size;
		waterball.force = force;
		waterball.damage = damage;
		waterball.potionEffects = new PotionEffect[0];
		player.worldObj.spawnEntityInWorld(waterball);
	}
	
	private float size = 1, force = 0, damage = 1;
	private PotionEffect[] potionEffects = new PotionEffect[0];
	private int xTile = -1;
    private int yTile = -1;
    private int zTile = -1;
    private Block tileLodged;
    private boolean inGround;
    public EntityLivingBase shootingEntity;
    private int ticksAlive;
    private int ticksInAir;
    public double accelerationX;
    public double accelerationY;
    public double accelerationZ;
	
	public Waterball(World world, EntityLivingBase entity, double xMotion, double yMotion, double zMotion)
	{
		super(world);
        this.shootingEntity = entity;
        this.setSize(1.0F, 1.0F);
        this.setLocationAndAngles(entity.posX, entity.posY, entity.posZ, entity.rotationYaw, entity.rotationPitch);
        this.setPosition(this.posX, this.posY, this.posZ);
        this.yOffset = 0.0F;
        this.motionX = this.motionY = this.motionZ = 0.0D;
        xMotion += this.rand.nextGaussian() * 0.4D;
        yMotion += this.rand.nextGaussian() * 0.4D;
        zMotion += this.rand.nextGaussian() * 0.4D;
        double d3 = (double)MathHelper.sqrt_double(xMotion * xMotion + yMotion * yMotion + zMotion * zMotion);
        this.accelerationX = xMotion / d3 * 0.1D;
        this.accelerationY = yMotion / d3 * 0.1D;
        this.accelerationZ = zMotion / d3 * 0.1D;
	}
	
	/**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        if (!this.worldObj.isRemote && (this.shootingEntity != null && this.shootingEntity.isDead || !this.worldObj.blockExists((int)this.posX, (int)this.posY, (int)this.posZ)))
        {
            this.setDead();
        }
        else
        {
            super.onUpdate();
            this.setFire(1);

            if (this.inGround)
            {
                Block block = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);

                if (block == this.tileLodged)
                {
                    ++this.ticksAlive;

                    if (this.ticksAlive == 600)
                    {
                        this.setDead();
                    }

                    return;
                }

                this.inGround = false;
                this.motionX *= (double)(this.rand.nextFloat() * 0.2F);
                this.motionY *= (double)(this.rand.nextFloat() * 0.2F);
                this.motionZ *= (double)(this.rand.nextFloat() * 0.2F);
                this.ticksAlive = 0;
                this.ticksInAir = 0;
            }
            else
            {
                ++this.ticksInAir;
            }

            Vec3 vecPos = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY, this.posZ);
            Vec3 vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
            MovingObjectPosition interceptioningObject = this.worldObj.rayTraceBlocks(vecPos, vecMove);
            vecPos = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY, this.posZ);
            vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

            if (interceptioningObject != null)
            {
                vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(interceptioningObject.hitVec.xCoord, interceptioningObject.hitVec.yCoord, interceptioningObject.hitVec.zCoord);
            }

            Entity interceptingEntity = null;
            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
            double d0 = 0.0D;

            for (int j = 0; j < list.size(); ++j)
            {
                Entity entity1 = (Entity)list.get(j);

                if (entity1.canBeCollidedWith() && (!entity1.isEntityEqual(this.shootingEntity) || this.ticksInAir >= 25))
                {
                    float f = 0.3F;
                    AxisAlignedBB axisalignedbb = entity1.boundingBox.expand((double)f, (double)f, (double)f);
                    MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vecPos, vecMove);

                    if (movingobjectposition1 != null)
                    {
                        double d1 = vecPos.distanceTo(movingobjectposition1.hitVec);

                        if (d1 < d0 || d0 == 0.0D)
                        {
                            interceptingEntity = entity1;
                            d0 = d1;
                        }
                    }
                }
            }

            if (interceptingEntity != null)
            {
                interceptioningObject = new MovingObjectPosition(interceptingEntity);
            }

            if (interceptioningObject != null)
            {
                this.onImpact(interceptioningObject);
            }

            this.posX += this.motionX;
            this.posY += this.motionY;
            this.posZ += this.motionZ;
            float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            this.rotationYaw = (float)(Math.atan2(this.motionZ, this.motionX) * 180.0D / Math.PI) + 90.0F;

            for (this.rotationPitch = (float)(Math.atan2((double)f1, this.motionY) * 180.0D / Math.PI) - 90.0F; this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F)
            {
                ;
            }

            while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
            {
                this.prevRotationPitch += 360.0F;
            }

            while (this.rotationYaw - this.prevRotationYaw < -180.0F)
            {
                this.prevRotationYaw -= 360.0F;
            }

            while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
            {
                this.prevRotationYaw += 360.0F;
            }

            this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
            this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
            float f2 = this.getMotionFactor();

            if (this.isInWater())
            {
                for (int k = 0; k < 4; ++k)
                {
                    float f3 = 0.25F;
                    this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double)f3, this.posY - this.motionY * (double)f3, this.posZ - this.motionZ * (double)f3, this.motionX, this.motionY, this.motionZ);
                }

                f2 = 0.8F;
            }

            this.motionX += this.accelerationX;
            this.motionY += this.accelerationY;
            this.motionZ += this.accelerationZ;
            this.motionX *= (double)f2;
            this.motionY *= (double)f2;
            this.motionZ *= (double)f2;
            this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
            this.setPosition(this.posX, this.posY, this.posZ);
        }
    }

    /**
     * Return the motion factor for this projectile. The factor is multiplied by the original motion.
     */
    protected float getMotionFactor()
    {
        return 0.95F;
    }
	
	protected void onImpact(MovingObjectPosition target)
    {
        if (!this.worldObj.isRemote)
        {
        	if (target.typeOfHit == MovingObjectType.BLOCK)
        	{
        		double x = (int)target.hitVec.xCoord;
    			double y = (int)target.hitVec.yCoord;
    			double z = (int)target.hitVec.zCoord;
    			switch(target.sideHit)
				{
					case 0: y += 0.5d; break;
					case 1: y -= 0.5d; break;
					case 2: z += 0.5d; break;
					case 3: z -= 0.5d; break;
					case 4: x += 0.5d; break;
					case 5: x -= 0.5d; break;
				}
    			Block block = this.worldObj.getBlock((int)x, (int)y, (int)z);
    			boolean isAir = this.worldObj.isAirBlock((int)x, (int)y, (int)z);
        		if (isAir || block.getCollisionBoundingBoxFromPool(worldObj, (int)x, (int)y, (int)z) == null)
        			return;
        	}
        	MechanicExplosion.ExplodeCustomDamage(this.worldObj, this.shootingEntity, this.posX, this.posY, this.posZ, size, damage, false, false, false, false, false, false, force, 0, potionEffects);
        	
            if (target.entityHit != null)
            	target.entityHit.attackEntityFrom(DamageSource.causeIndirectMagicDamage(this, this.shootingEntity), 6);
            
            this.setDead();
        }
    }
	/**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        par1NBTTagCompound.setShort("xTile", (short)this.xTile);
        par1NBTTagCompound.setShort("yTile", (short)this.yTile);
        par1NBTTagCompound.setShort("zTile", (short)this.zTile);
        par1NBTTagCompound.setString("tileLodged", Block.blockRegistry.getNameForObject(tileLodged));
        par1NBTTagCompound.setByte("inGround", (byte)(this.inGround ? 1 : 0));
        par1NBTTagCompound.setTag("direction", this.newDoubleNBTList(new double[] {this.motionX, this.motionY, this.motionZ}));
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        this.xTile = par1NBTTagCompound.getShort("xTile");
        this.yTile = par1NBTTagCompound.getShort("yTile");
        this.zTile = par1NBTTagCompound.getShort("zTile");
        this.tileLodged = Block.getBlockFromName(par1NBTTagCompound.getString("tileLodged"));
        this.inGround = par1NBTTagCompound.getByte("inGround") == 1;

        if (par1NBTTagCompound.hasKey("direction"))
        {
            NBTTagList nbttaglist = par1NBTTagCompound.getTagList("direction", 10);
            this.motionX = nbttaglist.func_150309_d(0);
            this.motionY = nbttaglist.func_150309_d(1);
            this.motionZ = nbttaglist.func_150309_d(2);
        }
        else
        {
            this.setDead();
        }
    }

    /**
     * Returns true if other Entities should be prevented from moving through this Entity.
     */
    public boolean canBeCollidedWith()
    {
        return true;
    }

    public float getCollisionBorderSize()
    {
        return 1.0F;
    }

    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
        if (this.isEntityInvulnerable())
        {
            return false;
        }
        else
        {
            this.setBeenAttacked();

            if (par1DamageSource.getEntity() != null)
            {
                Vec3 vec3 = par1DamageSource.getEntity().getLookVec();

                if (vec3 != null)
                {
                    this.motionX = vec3.xCoord;
                    this.motionY = vec3.yCoord;
                    this.motionZ = vec3.zCoord;
                    this.accelerationX = this.motionX * 0.1D;
                    this.accelerationY = this.motionY * 0.1D;
                    this.accelerationZ = this.motionZ * 0.1D;
                }

                if (par1DamageSource.getEntity() instanceof EntityLivingBase)
                {
                    this.shootingEntity = (EntityLivingBase)par1DamageSource.getEntity();
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }

    @SideOnly(Side.CLIENT)
    public float getShadowSize()
    {
        return 0.0F;
    }

    /**
     * Gets how bright this entity is.
     */
    public float getBrightness(float par1)
    {
        return 1.0F;
    }

    @SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float par1)
    {
        return 15728880;
    }

	@Override
	protected void entityInit() {}
}
