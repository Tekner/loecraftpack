package loecraftpack.content.ponies.abilities.projectiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import loecraftpack.logic.helpers.inventoryaccess.InventoryAccessHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntitySmallFireball;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityCustomBalls extends Entity
{
	private final float dtr = (float)Math.PI/180f; //Degree To Radian
	
	public enum Type
	{
		FLAME,//fire damage
		WIND,//knockback  (might become a pegasus ability)
		WATER,//water blast
		HOLY,//like a healing potion
		ENDER;//like spikes fire breath.
		
		public final ResourceLocation image;
		
		Type()
		{
			image = new ResourceLocation("loecraftpack:entities/ball/"+this.name().toLowerCase()+".png");
		}
	}
	
	public static void SpawnSprayOnPlayer(EntityPlayer player, float range, float force, float damage, Type type)
	{
		double speed = 0.1d;
		double headingX = player.getLookVec().xCoord;
		double headingY = player.getLookVec().yCoord;
		double headingZ = player.getLookVec().zCoord;
		
		double hX;
		double hY;
		double hZ;
		
		Random rand = new Random();
		
		for(int i=0; i<3; i++)
		{
			hX = (headingX + (rand.nextGaussian()-0.5d)*0.2d)*speed;
			hY = (headingY + (rand.nextGaussian()-0.5d)*0.2d)*speed;
			hZ = (headingZ + (rand.nextGaussian()-0.5d)*0.2d)*speed;
			
			EntityCustomBalls fireSprayPart = new EntityCustomBalls(player.worldObj, player, hX, hY, hZ);
			fireSprayPart.range = range;
			fireSprayPart.force = force;
			fireSprayPart.damage = damage;
			fireSprayPart.setType(type);
			player.worldObj.spawnEntityInWorld(fireSprayPart);
		}
	}
	
	public static void SpawnBallOnPlayer(EntityPlayer player, float range, float force, float damage, Type type)
	{
		double speed = 0.1d;
		double headingX = player.getLookVec().xCoord;
		double headingY = player.getLookVec().yCoord;
		double headingZ = player.getLookVec().zCoord;
		
		double hX = headingX*speed;
		double hY = headingY*speed;
		double hZ = headingZ*speed;
		
		EntityCustomBalls ball = new EntityCustomBalls(player.worldObj, player, hX, hY, hZ);
		ball.range = range;
		ball.force = force;
		ball.damage = damage;
		ball.setType(type);
		player.worldObj.spawnEntityInWorld(ball);
	}
	
	protected float range = 0, force = 0, damage = 1, chanceToDrop = 0;
	protected PotionEffect[] potionEffects = new PotionEffect[0];
	protected double startX, startY, startZ;
	
	protected int xTile = -1;
	protected int yTile = -1;
	protected int zTile = -1;
	protected Block tileLodged;
	protected boolean inGround;
	public EntityLivingBase shootingEntity;
	protected int ticksStuck;
	protected int ticksInAir;
	public double accelerationX;
	public double accelerationY;
	public double accelerationZ;
	
	public EntityCustomBalls(World world)
	{
		super(world);
		this.setSize(0.3125F, 0.3125F);
	}
	
	public EntityCustomBalls(World world, EntityLivingBase player, double xMotion, double yMotion, double zMotion)
	{
		super(world);
		this.shootingEntity = player;
		this.setSize(1.0F, 1.0F);
		this.setLocationAndAngles(player.posX, player.posY + player.getEyeHeight()-0.3d, player.posZ, player.rotationYaw, player.rotationPitch);
		this.yOffset = 0.0F;
		this.motionX = this.motionY = this.motionZ = 0.0D;
		
		this.setSize(0.3125F, 0.3125F);
		this.accelerationX = xMotion;
		this.accelerationY = yMotion;
		this.accelerationZ = zMotion;
		
		startX = posX;
		startY = posY;
		startZ = posZ;
	}
	
	@Override
	protected void entityInit()
	{
		this.getDataWatcher().addObject(3, Byte.valueOf((byte)0));
	}
	
	public void setType(Type type)
	{
		this.getDataWatcher().updateObject(3, Byte.valueOf((byte)type.ordinal()));
	}
	
	public Type getType()
	{
		return Type.values()[this.getDataWatcher().getWatchableObjectByte(3)];
	}
	
	@SideOnly(Side.CLIENT)
	public ResourceLocation getTexture()
	{
		return getType().image;
	}
	
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRenderDist(double par1)
	{
		double d1 = this.boundingBox.getAverageEdgeLength() * 4.0D;
		d1 *= 64.0D;
		return par1 < d1 * d1;
	}
	
	public void setHeading(double xMotion, double yMotion, double zMotion)
	{
		this.accelerationX = xMotion;
		this.accelerationY = yMotion;
		this.accelerationZ = zMotion;
	}
	
	@Override
	public void onUpdate()
	{
		if(!worldObj.isRemote)
		{
			double dx = this.posX-startX;
			double dy = this.posY-startY;
			double dz = this.posZ-startZ;
			if(dx*dx+dy*dy+dz*dz > range*range)
			{
				this.setDead();
				return;
			}
		}
		
		if (!this.worldObj.isRemote && (this.shootingEntity != null && this.shootingEntity.isDead || !this.worldObj.blockExists((int)this.posX, (int)this.posY, (int)this.posZ)))
		{
			this.setDead();
		}
		else
		{
			super.onUpdate();
			if (getType() == Type.FLAME)
				this.setFire(1);
			
			if (this.inGround)
			{
				Block block = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
				
				if (block == tileLodged)
				{
					++this.ticksStuck;
					
					if (this.ticksStuck == 600)
					{
						this.setDead();
					}
					
					return;
				}
				
				this.inGround = false;
				this.motionX *= (double)(this.rand.nextFloat() * 0.2F);
				this.motionY *= (double)(this.rand.nextFloat() * 0.2F);
				this.motionZ *= (double)(this.rand.nextFloat() * 0.2F);
				this.ticksStuck = 0;
				this.ticksInAir = 0;
			}
			else
			{
				++this.ticksInAir;
			}
			
			Vec3 vecPos = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY, this.posZ);
			Vec3 vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
			MovingObjectPosition interceptioningObject = this.worldObj.rayTraceBlocks(vecPos, vecMove);
			vecPos = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY, this.posZ);
			if (interceptioningObject != null)
				vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(interceptioningObject.hitVec.xCoord, interceptioningObject.hitVec.yCoord, interceptioningObject.hitVec.zCoord);
			else
				vecMove = this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
			
			Entity interceptingEntity = null;
			List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
			double closestDistance = 0.0D;
			
			for (int j = 0; j < list.size(); ++j)
			{
				Entity entity1 = (Entity)list.get(j);
				
				if ((entity1.canBeCollidedWith() || (entity1 instanceof EntityItem)) && (!entity1.isEntityEqual(this.shootingEntity) || this.ticksInAir >= 25))
				{
					float f = 0.3F;
					AxisAlignedBB axisalignedbb = entity1.boundingBox.expand((double)f, (double)f, (double)f);
					MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vecPos, vecMove);
					
					if (movingobjectposition1 != null)
					{
						double d1 = vecPos.distanceTo(movingobjectposition1.hitVec);
						
						if (d1 < closestDistance || closestDistance == 0.0D)
						{
							interceptingEntity = entity1;
							closestDistance = d1;
						}
					}
				}
			}
			
			if (interceptingEntity != null)
			{
				interceptioningObject = new MovingObjectPosition(interceptingEntity);
			}
			
			if (interceptioningObject != null)
			{
				this.onImpact(interceptioningObject);
			}
			
			this.posX += this.motionX;
			this.posY += this.motionY;
			this.posZ += this.motionZ;
			float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.rotationYaw = (float)(Math.atan2(this.motionZ, this.motionX) * 180.0D / Math.PI) + 90.0F;
			
			for (this.rotationPitch = (float)(Math.atan2((double)f1, this.motionY) * 180.0D / Math.PI) - 90.0F; this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F)
			{
				;
			}
			
			while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
			{
				this.prevRotationPitch += 360.0F;
			}
			
			while (this.rotationYaw - this.prevRotationYaw < -180.0F)
			{
				this.prevRotationYaw -= 360.0F;
			}
			
			while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
			{
				this.prevRotationYaw += 360.0F;
			}
			
			this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
			this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
			float f2 = this.getMotionFactor();
			
			if (this.isInWater())
			{
				for (int k = 0; k < 4; ++k)
				{
					float f3 = 0.25F;
					this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double)f3, this.posY - this.motionY * (double)f3, this.posZ - this.motionZ * (double)f3, this.motionX, this.motionY, this.motionZ);
				}
				
				f2 = 0.8F;
			}
			
			this.motionX += this.accelerationX;
			this.motionY += this.accelerationY;
			this.motionZ += this.accelerationZ;
			this.motionX *= (double)f2;
			this.motionY *= (double)f2;
			this.motionZ *= (double)f2;
			if (getType() == Type.FLAME || getType() == Type.ENDER)
				this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
			this.setPosition(this.posX, this.posY, this.posZ);
		}
	}
	
	protected float getMotionFactor()
	{
		return 0.95F;
	}
	
	protected void onImpact(MovingObjectPosition target)
	{
		if (!this.worldObj.isRemote)
		{
			switch (getType())
			{
			case FLAME:
				if (target.entityHit != null)
				{
					target.entityHit.attackEntityFrom(causeFireballDamage(this, this.shootingEntity), damage);
					target.entityHit.setFire(5);
				}
				else
				{
					int x = target.blockX;
					int y = target.blockY;
					int z = target.blockZ;
					
					switch (target.sideHit)
					{
						case 0: --y; break;
						case 1: ++y; break;
						case 2: --z; break;
						case 3: ++z; break;
						case 4: --x; break;
						case 5: ++x; break;
					}
					
					if (this.worldObj.isAirBlock(x, y, z))
					{
						this.worldObj.setBlock(x, y, z, Blocks.fire);
					}
				}
				break;
				
			case WIND:
				if (target.entityHit != null)
				{
					if (!(target.entityHit instanceof EntityItem))
						target.entityHit.attackEntityFrom(DamageSource.causeIndirectMagicDamage(this, this.shootingEntity), damage);
					float pitch = this.rotationPitch*dtr, yaw = this.rotationYaw*dtr;
					target.entityHit.motionX += force * Math.cos(pitch) * Math.sin(yaw);
					target.entityHit.motionY += force * (0.2d - Math.sin(pitch));
					target.entityHit.motionZ -= force * Math.cos(pitch) * Math.cos(yaw);
				}
				break;
				
			case HOLY:
				if (target.entityHit != null && target.entityHit instanceof EntityLivingBase)
				{
					if (((EntityLivingBase)target.entityHit).isEntityUndead())
						target.entityHit.attackEntityFrom(DamageSource.causeIndirectMagicDamage(this, this.shootingEntity), damage*2);
					else
						((EntityLivingBase)target.entityHit).heal(damage);
				}
				break;
				
			case ENDER:
				if (target.entityHit != null)
				{
					if (target.entityHit instanceof EntityItem)
					{
						System.out.println("ITEM");
						if (this.shootingEntity instanceof EntityPlayer)
						{
							//stick in ender chest
							InventoryEnderChest inv = ((EntityPlayer)this.shootingEntity).getInventoryEnderChest();
							ItemStack[] stack = new ItemStack[1];
							stack[0] = ((EntityItem)target.entityHit).getEntityItem();
							ItemStack[] remaining = InventoryAccessHelper.dumpItemsIntoInventoryNoReg(inv, stack);
							if (remaining != null)
								((EntityItem)target.entityHit).setEntityItemStack(remaining[0]);
							else
								target.entityHit.worldObj.removeEntity(target.entityHit);
						}
					}
					else if (target.entityHit instanceof EntityPlayer)
					{
						//send home
						teleportHome((EntityPlayer)target.entityHit, target.entityHit.worldObj);
					}
					else if (target.entityHit instanceof EntityLiving)
					{
						if (!(target.entityHit instanceof IBossDisplayData || ((EntityLiving)target.entityHit).hasCustomNameTag()))
						{
							target.entityHit.worldObj.removeEntity(target.entityHit);
						}
					}
				}
				break;
			}
			
			
			if (target.typeOfHit == MovingObjectType.BLOCK)
				this.setDead();
		}
	}
	
	public static void teleportHome(EntityPlayer player, World world)
	{
		ChunkCoordinates currentDimBedCoords = player.getBedLocation(world.provider.dimensionId);
		ChunkCoordinates defaultDimBedCoords = player.getBedLocation(0);
		if (currentDimBedCoords != null)
		{
			player.setPositionAndUpdate(currentDimBedCoords.posX, currentDimBedCoords.posY, currentDimBedCoords.posZ);
		}
		else if (defaultDimBedCoords != null)
		{
			MinecraftServer.getServer().getConfigurationManager().transferPlayerToDimension((EntityPlayerMP)player, 0);
			player.setPositionAndUpdate(defaultDimBedCoords.posX, defaultDimBedCoords.posY, defaultDimBedCoords.posZ);
		}
		else if (world.getSpawnPoint() != null)
		{
			player.setPositionAndUpdate(world.getSpawnPoint().posX, world.getSpawnPoint().posY, world.getSpawnPoint().posZ);
			player.addChatMessage(new ChatComponentText("Your bed could not be found. We know ...we checked. Instead we sent you to the spawn area."));
		}
		else
			player.addChatMessage(new ChatComponentText("You were pulled through the dimensions in search of a place to teleport to safely, but you failed. The Doctor had to bring you back. Go sleep in a bed or go to a world with a proper spawn point and don't bug him again."));
	}
	
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
	{
		par1NBTTagCompound.setShort("xTile", (short)this.xTile);
		par1NBTTagCompound.setShort("yTile", (short)this.yTile);
		par1NBTTagCompound.setShort("zTile", (short)this.zTile);
		par1NBTTagCompound.setString("tileLodged", Block.blockRegistry.getNameForObject(this.tileLodged));
		par1NBTTagCompound.setByte("inGround", (byte)(this.inGround ? 1 : 0));
		par1NBTTagCompound.setTag("direction", this.newDoubleNBTList(new double[] {this.motionX, this.motionY, this.motionZ}));
	}
	
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		this.xTile = par1NBTTagCompound.getShort("xTile");
		this.yTile = par1NBTTagCompound.getShort("yTile");
		this.zTile = par1NBTTagCompound.getShort("zTile");
		this.tileLodged = Block.getBlockFromName(par1NBTTagCompound.getString("tileLodged"));
		this.inGround = par1NBTTagCompound.getByte("inGround") == 1;

		if (par1NBTTagCompound.hasKey("direction"))
		{
			NBTTagList nbttaglist = par1NBTTagCompound.getTagList("direction", 10);
			this.motionX = nbttaglist.func_150309_d(0);
			this.motionY = nbttaglist.func_150309_d(1);
			this.motionZ = nbttaglist.func_150309_d(2);
		}
		else
		{
			this.setDead();
		}
	}
	
	@Override
	public boolean canBeCollidedWith()
	{
		return false;
	}
	
	@Override
	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		return 0.0F;
	}
	
	@Override
	public float getBrightness(float par1)
	{
		return 1.0F;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public int getBrightnessForRender(float par1)
	{
		return 15728880;
	}
	
	public static DamageSource causeFireballDamage(EntityCustomBalls entityBall, Entity entityShooter)
    {
        return entityShooter == null ? (new EntityDamageSourceIndirect("onFire", entityBall, entityBall)).setFireDamage().setProjectile() : (new EntityDamageSourceIndirect("fireball", entityBall, entityShooter)).setFireDamage().setProjectile();
    }
}
