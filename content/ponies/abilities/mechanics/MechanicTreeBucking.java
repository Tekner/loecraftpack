package loecraftpack.content.ponies.abilities.mechanics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import loecraftpack.content.blocks.BlockAppleBloomLeaves;
import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

/**
 * This class handles code regarding the interactions with custom apple trees
 */
public class MechanicTreeBucking
{	
	/**
	 * handles gentle Tree bucking
	 */
	public static boolean buckTree(World world, int xCoord, int yCoord, int zCoord, int skill, int fortune)
	{
		if ((world.getBlockMetadata(xCoord, yCoord, zCoord)&2) == 1)
			return false;//not a natural tree
		
		Block woodID = world.getBlock(xCoord, yCoord, zCoord);
		if (woodID != LOE_Blocks.appleBloomLog && woodID != LOE_Blocks.zapAppleLog)
			return false;//not a buck-able tree
		
		//find base of tree
		while(true)
		{
			if (world.blockExists(xCoord, --yCoord, zCoord) &&
			    world.getBlock(xCoord, yCoord, zCoord) == woodID &&
			    (world.getBlockMetadata(xCoord, yCoord, zCoord)&2) == 0)
				continue;
			yCoord++;
			break;
		}
		
		//buck possible leaves
		for (int xi = xCoord-2; xi <= xCoord+2; xi++)
		{
			for (int yi = yCoord+2 ; yi <= yCoord+8; yi++)
			{
				for (int zi = zCoord-2; zi <= zCoord+2; zi++)
				{
					buckLeaf(world, xi, yi, zi, skill, fortune);
				}
			}
		}
		
		
		return true;
	}
	
	/**
	 * handles gentle leaf bucking
	 */
	public static void buckLeaf(World world, int xCoord, int yCoord, int zCoord, int skill, int fortune)
	{
		if (!world.blockExists(xCoord, yCoord, zCoord))
			return;//out of bounds
		Block hold = world.getBlock(xCoord, yCoord, zCoord);
		if (!(hold instanceof BlockAppleBloomLeaves))
			return;// not a leaf
		BlockAppleBloomLeaves leaf = (BlockAppleBloomLeaves) hold;
		int meta = world.getBlockMetadata(xCoord, yCoord, zCoord);
		if ((meta&4)==1)
			return;//placed by player
		
		if (hold == LOE_Blocks.zapAppleLeavesCharged)
		{
			//charged Zap-Apple leaves scenario
			leaf.dropBlockAsItemWithChance(world, xCoord, yCoord, zCoord, meta, skill, fortune);
			if (world.setBlock(xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeaves, 0, 2))
				leaf.tellClientOfChange(world, xCoord, yCoord, zCoord, LOE_Blocks.zapAppleLeaves);
		}
		
		if ((meta&3) < leaf.bloomStage)
			return;//no apples
		
		leaf.dropBlockAsItemWithChance(world, xCoord, yCoord, zCoord, meta, skill, fortune);
		if (world.setBlock(xCoord, yCoord, zCoord, leaf, 0, 2))
			leaf.tellClientOfChange(world, xCoord, yCoord, zCoord, leaf);
	}

}
