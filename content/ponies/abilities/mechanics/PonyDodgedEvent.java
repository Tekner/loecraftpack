package loecraftpack.content.ponies.abilities.mechanics;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

//Raised when an attack was dodged and contains all of the information of the attack
public class PonyDodgedEvent extends LivingHurtEvent
{
	public PonyDodgedEvent(LivingHurtEvent event)
	{
		super(event.entityLiving, event.source, event.ammount);
	}
}
