package loecraftpack.content.ponies.abilities.mechanics;

import java.util.ArrayList;

import loecraftpack.content.blocks.BlockHiddenOre;
import loecraftpack.proxies.ClientProxy;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.Vec3;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * This class handles code regarding the visibility of hidden ores.
 */
public class MechanicHiddenOres
{
	@SideOnly(Side.CLIENT)
	public static boolean revealHiddenGems = false;
	@SideOnly(Side.CLIENT)
	public static double rangeHiddenGems = 5;
	@SideOnly(Side.CLIENT)
	public static double rangeOtherGems = 0;
	@SideOnly(Side.CLIENT)
	public static double rangeDeformaties = 0;
	@SideOnly(Side.CLIENT)
	public static int xPos;
	@SideOnly(Side.CLIENT)
	public static int yPos;
	@SideOnly(Side.CLIENT)
	public static int zPos;
	
	public static void refreshRenderWithRange(EntityPlayer player)
	{
		ClientProxy.renderHiddenOre.phantomBlocks = new ArrayList<int[]>();
		
		double pX = player.posX;
		double pY = player.posY;
		double pZ = player.posZ;
		
		pX = pX<0? pX-1: pX;
		pY = pY<0? pY-1: pY;
		pZ = pZ<0? pZ-1: pZ;
		
		xPos = (int) pX;
		yPos = (int) pY;
		zPos = (int) pZ;
		
		if (revealHiddenGems)
		{
			int[] bounds = getBoundingBox();
			
			for (int x=bounds[0]; x<=bounds[3]; x++)
			{
				for (int y=bounds[1]; y<=bounds[4]; y++)
				{
					for (int z=bounds[2]; z<=bounds[5]; z++)
					{
						if (isVisible(player, x, y, z))
							ClientProxy.renderHiddenOre.phantomBlocks.add(new int[]{x, y, z});
					}
				}
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public static int[] getBoundingBox()
	{
		int range2 = (int)(rangeHiddenGems+1);
		return new int[] {xPos-range2, yPos-range2, zPos-range2, xPos+range2, yPos+range2, zPos+range2};
	}
	
	@SideOnly(Side.CLIENT)
	public static boolean isVisible(EntityPlayer player, int x, int y, int z)
	{
		Block block = player.worldObj.getBlock(x, y, z);
		int meta = player.worldObj.getBlockMetadata(x, y, z);
		if (block != Blocks.air && block instanceof BlockHiddenOre)
		{
			return inRangeofClientPlayer(x, y, z, rangeHiddenGems);
		}
		else if (block == Blocks.diamond_ore || block == Blocks.lapis_ore || block == Blocks.emerald_ore)
			return inRangeofClientPlayer(x, y, z, rangeOtherGems);
		
		return false;
	}
	
	//blocks in range of the player
	@SideOnly(Side.CLIENT)
	public static boolean inRangeofClientPlayer(int x, int y, int z, double range)
	{
		return inRange(xPos+0.5, yPos+0.5, zPos+0.5,
					  x, y, z, x+1, y+1, z+1,
				       range);
	}
	
	/**
	 * This methods finds if a square intersects a sphere
	 * @param xPos - sphere
	 * @param yPos - sphere
	 * @param zPos - sphere
	 * @param xMinus - square
	 * @param yMinus - square
	 * @param zMinus - square
	 * @param xPlus - square
	 * @param yPlus - square
	 * @param zPlus - square
	 * @param range - sphere
	 * @return
	 */
	protected static boolean inRange(double xPos,   double yPos,   double zPos,
									 double xMinus, double yMinus, double zMinus,
									 double xPlus,  double yPlus,  double zPlus,
									 double range)
	{
		byte closestX = (byte)(xMinus>xPos ? -1 : xPlus<xPos ? 1 : 0);
		if (closestX==1? xPlus+range<xPos : xMinus-range>xPos)return false;
		byte closestY = (byte)(yMinus>yPos ? -1 : yPlus<yPos ? 1 : 0);
		if (closestY==1? yPlus+range<yPos : yMinus-range>yPos)return false;
		byte closestZ = (byte)(zMinus>zPos ? -1 : zPlus<zPos ? 1 : 0);
		if (closestZ==1? zPlus+range<zPos : zMinus-range>zPos)return false;
		
		Vec3 center;
		Vec3 target;
		
		if(closestX==0)
		{
			if(closestY==0)
			{
				if(closestZ==0)
				{
					return true;
				}
				else
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(xPos, yPos, closestZ==1? zPlus: zMinus);
					if (center.distanceTo(target) <= range)return true;
				}
			}
			else
			{
				if(closestZ==0)
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(xPlus, closestY==1? yPlus: yPos, zPos);
					if (center.distanceTo(target) <= range)return true;
				}
				else
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(xPos, closestY==1? yPlus: yMinus, closestZ==1? zPlus: zMinus);
					if (center.distanceTo(target) <= range)return true;
				}
			}
		}
		else
		{
			if(closestY==0)
			{
				if(closestZ==0)
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(closestX==1? xPlus: xMinus, yPos, zPos);
					if (center.distanceTo(target) <= range)return true;
				}
				else
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(closestX==1? xPlus: xMinus, yPos, closestZ==1? zPlus: zMinus);
					if (center.distanceTo(target) <= range)return true;
				}
			}
			else
			{
				if(closestZ==0)
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(closestX==1? xPlus: xMinus, closestY==1? yPlus: yMinus, zPos);
					if (center.distanceTo(target) <= range)return true;
				}
				else
				{
					center = Vec3.createVectorHelper(xPos, yPos, zPos);
					target = Vec3.createVectorHelper(closestX==1? xPlus: xMinus, closestY==1? yPlus: yMinus, closestZ==1? zPlus: zMinus);
					if (center.distanceTo(target) <= range)return true;
				}
			}
		}
		
		return false;
	}
}
