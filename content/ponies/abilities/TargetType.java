package loecraftpack.content.ponies.abilities;

public enum TargetType
{
	AOE,	//Area of effect around the player
	AIMED,	//Projectile or otherwise aimed abilities
	BLAST,  //Area of effect infront of the player
	GENERIC	//Everything else - usually just abilities that don't directly interact with the world
}
