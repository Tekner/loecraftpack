package loecraftpack.content.ponies.abilities;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.network.packets.PacketStatUpdate;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class AbilityPlayerData
{
	@SideOnly(Side.CLIENT)
	public static AbilityPlayerData clientData;
	
	@SideOnly(Side.CLIENT)
	protected static EnergyUsePiece<Float> useDrawBackRising;
	@SideOnly(Side.CLIENT)
	protected static EnergyUsePiece<Float> useDrawBackFalling;
	
	@SideOnly(Side.CLIENT)
	protected static EnergyUsePiece<Float> afterImageDrawBack;
	@SideOnly(Side.CLIENT)
	protected static HashMap<Long, Float> afterImageStorage;
	@SideOnly(Side.CLIENT)
	protected static float afterImageHold;
	
	@SideOnly(Side.CLIENT)
	protected static EnergyUsePiece<Float> restoreDrawBack;
	@SideOnly(Side.CLIENT)
	protected static EnergyUsePiece<Float> drainDrawBack;
	
	static
	{
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			clientData = new AbilityPlayerData();
			useDrawBackRising = new EnergyUsePiece<Float>(0,0.0f,0);
			useDrawBackFalling = new EnergyUsePiece<Float>(0,0.0f,0);
			afterImageDrawBack = new EnergyUsePiece<Float>(0,0.0f,0);
			afterImageStorage = new HashMap<Long, Float>();
			afterImageHold = 0;
			restoreDrawBack = new EnergyUsePiece<Float>(0,0.0f,0);
			drainDrawBack = new EnergyUsePiece<Float>(0,0.0f,0);
		}
	}
	
	public String playerName;
	public EntityPlayer player = null; //This is correctly set to the correct entity on player respawn
	public Stats playerStats = null;
	
	public float energyRegenNatural = 10;//Multiples of 5, for max accuracy
	public int energyMax = 100;
	public float energy;
	
	public float chargeMax = 100.0f;
	public float charge;
	
	public final ActiveAbility[] activeAbilities;
	public final PassiveAbility[] passiveAbilities;
	
	private static HashMap<String, AbilityPlayerData> map = new HashMap<String, AbilityPlayerData>();
	private static HashMap<String, Object> sharedAbilityData = new HashMap<String, Object>();
	
	@SideOnly(Side.CLIENT)
	public AbilityPlayerData()
	{
		this.activeAbilities = ActiveAbility.NewAbilityArray();
		this.passiveAbilities = PassiveAbility.NewAbilityArray();
		playerName = "";
	}
	
	public AbilityPlayerData(String player, ActiveAbility[] activeAbilities, PassiveAbility[] passiveAbilities)
	{
		this.activeAbilities = activeAbilities;
		this.passiveAbilities = passiveAbilities;
		playerName = player;
	}
	
	/**************************************/
	/****  External Access / Register  ****/
	/**************************************/
	
	public static AbilityPlayerData Get(String player)
	{
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
			return clientData;
		else
			return map.get(player);
	}
	
	public static boolean PassPassiveAbilityEvent(String player, Event event)
	{
		for(PassiveAbility ability : AbilityPlayerData.Get(player).passiveAbilities)
		{
			if (!ability.PassEvent(event))
				return false;
		}
		return true;
	}
	
	public static boolean HasPlayer(String player)
	{
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT && player == Minecraft.getMinecraft().thePlayer.getGameProfile().getId())
			return true;
		else
			return map.containsKey(player);
	}
	
	//Client only
	@SideOnly(Side.CLIENT)
	public static AbilityPlayerData RegisterPlayer(String player)
	{
		AbilityPlayerData playerData;

		if (Minecraft.getMinecraft().thePlayer.getGameProfile().getId().equals(player))
			playerData = clientData = new AbilityPlayerData(player, ActiveAbility.NewAbilityArray(), PassiveAbility.NewAbilityArray());
		else
			map.put(player, playerData = new AbilityPlayerData(player, ActiveAbility.NewAbilityArray(), PassiveAbility.NewAbilityArray()));
		
		return playerData;
	}
	
	public static AbilityPlayerData RegisterPlayer(EntityPlayer player)
	{	
		for(int i = 0; i < 36; i++)
		{
			ItemStack stack = player.inventory.mainInventory[i];
			if (stack != null)
			{
				if (stack.getItem() instanceof ItemActiveAbility && stack.getItemDamage() >= ActiveAbility.abilityClasses.length)
					stack.setItemDamage(ActiveAbility.abilityClasses.length - 1);
				if (stack.getItem() instanceof ItemPassiveAbility && stack.getItemDamage() >= PassiveAbility.abilityClasses.length)
					stack.setItemDamage(PassiveAbility.abilityClasses.length - 1);
			}
		}
		
		AbilityPlayerData playerData = new AbilityPlayerData(player.getGameProfile().getId(), ActiveAbility.NewAbilityArray(), PassiveAbility.NewAbilityArray());

		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT && Minecraft.getMinecraft().thePlayer.getGameProfile().getId().equals(player.getGameProfile().getId()))
		{
			clientData = new AbilityPlayerData(player.getGameProfile().getId(), ActiveAbility.NewAbilityArray(), PassiveAbility.NewAbilityArray());
			
			for(AbilityBase ability : clientData.activeAbilities)
				ability.SetPlayer(player.getGameProfile().getId(), clientData);
			
			for(AbilityBase ability : clientData.passiveAbilities)
				ability.SetPlayer(player.getGameProfile().getId(), clientData);
			
			clientData.bindPlayerStats(player.getGameProfile().getId());
		}
			map.put(player.getGameProfile().getId(), playerData);
		
		playerData.player = player;
		
		return playerData;
	}
	
	public void setPlayer(EntityPlayer player)
	{
		this.player = player;
	}
	
	//Order: None, Unicorn, Pegasus, Earth, Alicorn
	
	public void fixRaceStats()
	{
		if (player != null)
		{
			int r = playerStats.getRaceOrdinal();
			energyRegenNatural = playerStats.getEnergyRegen();
			energyMax = playerStats.getMaxEnergy();
			if (r > 0)
			{
				player.capabilities.walkSpeed = playerStats.getMoveSpeed();
				player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(player.capabilities.walkSpeed);
			}
			else
			{
				player.capabilities.walkSpeed = 0.1f;
				player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.1f);
			}
			if (r != 1 && r != 4)
				player.setAbsorptionAmount(0);
			if (r == 3)
				player.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1);
			else
				player.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(0);
			player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(playerStats.getMaxHP());
		}
	}
	
	public void bindPlayerStats(String player)
	{
		playerStats = (Stats)LoECraftPack.statHandler.stats.get(player);
		fixRaceStats();
	}
	
	public static void UnregisterPlayer(String player)
	{
		map.remove(player);
	}
	
	public Stats getPlayerStats()
	{
		return playerStats;
	}
	
	/**************************************/
	/************  UPDATES  ***************/
	/**************************************/
	
	public void onUpdateSERVER(EntityPlayer player)
	{
		addEnergy(energyRegenNatural/20f, false);
		
		for(ActiveAbility ability : activeAbilities)
			ability.onUpdate(player);
		
		for(PassiveAbility ability : passiveAbilities)
			ability.onTick(player);
	}
	
	public void onUpdateCLIENT(EntityPlayer player)
	{
		restoreOrDrainEnergy(energyRegenNatural/20f);
		
		long currentTime = System.currentTimeMillis();
		Object[] timeStamps = afterImageStorage.keySet().toArray();
		for (Object obj : timeStamps)
		{
			long timeStamp = Long.valueOf(obj.toString());
			if (currentTime - timeStamp >= 500)
			{
				float cost = afterImageStorage.get(timeStamp);
				afterImageStorage.remove(timeStamp);
				
				afterImageDrawBack.cost = getDrawBack(afterImageDrawBack) + cost;
				afterImageDrawBack.timestamp = System.currentTimeMillis();
				afterImageHold -= cost;
			}
		}
		
	}
	
	/**************************************/
	/*****  GET/SET - ENERGY/CHARGE  ******/
	/**************************************/
	
	/**
	 * set energy
	 */
	public void setEnergy(float newEnergy, boolean isClient)
	{
		if (isClient)
		{
			float oldEnergy = clientData.energy;
			clientData.energy = Math.min(energyMax, Math.max(0, newEnergy));
			float energyDifference = clientData.energy-oldEnergy;
			
			if (clientData.energy-oldEnergy>0)
			{
				useDrawBackRising.cost = getDrawBack(useDrawBackRising) + energyDifference;
				useDrawBackRising.timestamp = System.currentTimeMillis();
			}
			else
			{
				useDrawBackFalling.cost = getDrawBack(useDrawBackFalling) - energyDifference;
				useDrawBackFalling.timestamp = System.currentTimeMillis();
			}
		}
		else
		{
			energy = Math.min(energyMax, Math.max(0, newEnergy));
		}
	}
	
	/**
	 * add/subtract energy
	 */
	public void addEnergy(float energyDifference, boolean isClient)
	{
		if (isClient)
		{
			clientData.energy = Math.min(energyMax, Math.max(0, clientData.energy + energyDifference));
			if (energyDifference>0)
			{
				useDrawBackRising.cost = getDrawBack(useDrawBackRising) + energyDifference;
				useDrawBackRising.timestamp = System.currentTimeMillis();
			}
			else
			{
				useDrawBackFalling.cost = getDrawBack(useDrawBackFalling) - energyDifference;
				useDrawBackFalling.timestamp = System.currentTimeMillis();
			}
		}
		else
		{
			energy = Math.min(energyMax, Math.max(0, energy + energyDifference));
		}
	}
	
	/**
	 * add/subtract energy - used by regen, restore, drain, etc.
	 */
	@SideOnly(Side.CLIENT)
	public void restoreOrDrainEnergy(float energyDifference)
	{
		float oldEnergy = clientData.energy;
		clientData.energy = Math.min(energyMax, Math.max(0, energy + energyDifference));
		
		if (energyDifference>0)
		{
			if (clientData.energy >= clientData.energyMax)
				restoreDrawBack.cost = clientData.energy - (oldEnergy - getDrawBack(restoreDrawBack));
			else
				restoreDrawBack.cost = getDrawBack(restoreDrawBack) + energyDifference;
			restoreDrawBack.timestamp = System.currentTimeMillis();
		}
		else
		{
			if (clientData.energy <= 0)
				drainDrawBack.cost = oldEnergy + getDrawBack(drainDrawBack);
			else
				drainDrawBack.cost = getDrawBack(drainDrawBack) - energyDifference;
			drainDrawBack.timestamp = System.currentTimeMillis();
		}
	}
	
	/**
	 * set charge
	 */
	public void setCharge(float partial, float max)
	{
		charge = partial;
		chargeMax = max;
	}
	
	/**************************************/
	/********  GET RENDER INFO  ***********/
	/**************************************/
	
	/**
	 * get the position of the charge
	 */
	@SideOnly(Side.CLIENT)
	public static float getClientCastTimeRatio()
	{
		if (clientData == null)
			return 0;
		
		if (clientData.charge >= clientData.chargeMax)
			return 1;
		else if (clientData.charge <= 0)
			return 0;
		else
			return (clientData.charge) / clientData.chargeMax;
	}
	
	/**
	 * get the position of the afterImage
	 */
	@SideOnly(Side.CLIENT)
	public static float getClientEnergyAfterImageRatio()
	{
		if (clientData == null)
			return 0;
		
		float goal = clientData.energy + afterImageHold + getDrawBack(afterImageDrawBack) - getDrawBack(useDrawBackRising) + getDrawBack(drainDrawBack) - getDrawBack(restoreDrawBack);
		
		return clampRatio(goal);
	}
	
	/**
	 * get the position of the estimated remaining energy
	 */
	@SideOnly(Side.CLIENT)
	public static float getClientEffectiveEnergyRatio()
	{
		if (clientData == null)
			return 0;
		
		float goal = clientData.energy + getDrawBack(useDrawBackFalling) - getDrawBack(useDrawBackRising) + getDrawBack(drainDrawBack) - getDrawBack(restoreDrawBack);
		
		return clampRatio(goal);
	}
	
	/**
	 * get the position of the regeneration / restore
	 */
	@SideOnly(Side.CLIENT)
	public static float getClientRegenEnergyRatio()
	{
		if (clientData == null)
			return 0;
		
		float goal = clientData.energy + getDrawBack(useDrawBackFalling) - getDrawBack(useDrawBackRising) + getDrawBack(drainDrawBack);
		
		return clampRatio(goal);
	}
	
	/**
	 * get the position of the drain / deplete
	 */
	@SideOnly(Side.CLIENT)
	public static float getClientDrainEnergyRatio()
	{
		if (clientData == null)
			return 0;
		
		float goal = clientData.energy + getDrawBack(useDrawBackFalling) - getDrawBack(useDrawBackRising) - getDrawBack(restoreDrawBack);
		
		return clampRatio(goal);
	}
	
	/**
	 * used to clamp the ratio position to a 0.0-1.0 value, in regards to max energy
	 */
	protected static float clampRatio(float goal)
	{
		if (goal >= clientData.energyMax)
			return 1;
		else if (goal <= 0)
			return 0;
		else
			return goal / (float) clientData.energyMax;
	}
	
	/**
	 * get the render equivalent of the gradual change due to some parameter.
	 */
	@SideOnly(Side.CLIENT)
	protected static float getDrawBack(EnergyUsePiece<Float> meter)
	{
		float progress = (float)(System.currentTimeMillis() - meter.timestamp);
		if (progress>=400.0f)
			return 0.0f;
		return (1-(progress / 400.0f))*meter.cost;
	}
	
	/****************************/
	/**********  OTHER  *********/
	/****************************/
	
	/**
	 *  store a after-image by sequential id
	 */
	@SideOnly(Side.CLIENT)
	public static void addAfterImage(float cost)
	{
		afterImageHold += cost;
		long timeStamp = System.currentTimeMillis();
		if (afterImageStorage.containsKey(timeStamp))
			cost+= afterImageStorage.get(timeStamp);
		afterImageStorage.put(timeStamp, cost);
	}
	
	/**
	 * restore position of charge, if server denied the use of a non-toggle ability
	 */
	@SideOnly(Side.CLIENT)
	public static void handleDeny(int activeID, float setEnergy)
	{
		if (activeID >= 0 && activeID < clientData.activeAbilities.length)
		{
			ActiveAbility ability = clientData.activeAbilities[activeID];
			ability.cooldown = 0;
			clientData.setEnergy(setEnergy, true);
			if (ability.isToggleable())
				ability.toggled = false;
		}
	}
	
	public void applyGlobalCooldown()
	{
		for (ActiveAbility ability : activeAbilities)
			ability.applyGlobalCooldown();
	}
	
	public void sendChangingPlayerStatPacket()
	{
		LoECraftPack.packetHandler.sendTo(new PacketStatUpdate().INIT(energy), (EntityPlayerMP) player);
	}
	
	public void sendChangingPlayerStatPacket(int statID, int stat)
	{
		if (statID == -1)
			sendChangingPlayerStatPacket();
		else
			LoECraftPack.packetHandler.sendTo(new PacketStatUpdate().INIT((byte)statID, (short) stat), (EntityPlayerMP) player);
	}
	
	@SideOnly(Side.CLIENT)
	public static void recieveChangingPlayerStatPacket(PacketStatUpdate data)
	{
		if (data.id == -1)
			clientData.energy = (Float)data.value;
		else if (clientData.playerStats != null)
			clientData.playerStats.getStatList()[data.id].setValue((Short)data.value);
	}
	
	public void SetSharedData(String name, Object data)
	{
		sharedAbilityData.put(name, data);
	}
	
	private Object GetData(String name)
	{
		return sharedAbilityData.get(name);
	}
	
	private Object GetValueOrDefault(Object value, Object def)
	{
		if (value == null)
			return def;
		else
			return value;
	}
	
	public Boolean GetSharedFlag(String name)
	{
		return (Boolean)GetValueOrDefault(GetData(name), false);
	}
	
	public Integer GetSharedInt(String name)
	{
		return (Integer)GetValueOrDefault(GetData(name), 0);
	}
	
	public Float GetSharedFloat(String name)
	{
		return (Float)GetValueOrDefault(GetData(name), 0f);
	}
}
