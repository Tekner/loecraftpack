package loecraftpack.content.ponies.abilities.renders;

import loecraftpack.content.ponies.abilities.projectiles.EntityCustomBalls;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderFlatProjectile extends Render
{
	private static float scale = 0.5f;
	
	public void doRenderCommon(Entity entityCommon, double relativeX, double relativeY, double relativeZ, float par8, float par9)
	{
		GL11.glPushMatrix();
		this.bindEntityTexture(entityCommon);
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		float f2 = this.scale;
		GL11.glScalef(f2 / 1.0F, f2 / 1.0F, f2 / 1.0F);
		Tessellator tessellator = Tessellator.instance;
		float minU = 0.0f;
		float maxU = 1.0f;
		float minV = 0.0f;
		float maxV = 1.0f;
		float f7 = 1.0F;
		float f8 = 0.5F;
		float f9 = 0.25F;
		GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		tessellator.addVertexWithUV((double)(0.0F - f8), (double)(0.0F - f9), 0.0D, (double)minU, (double)maxV);
		tessellator.addVertexWithUV((double)(f7 - f8), (double)(0.0F - f9), 0.0D, (double)maxU, (double)maxV);
		tessellator.addVertexWithUV((double)(f7 - f8), (double)(1.0F - f9), 0.0D, (double)maxU, (double)minV);
		tessellator.addVertexWithUV((double)(0.0F - f8), (double)(1.0F - f9), 0.0D, (double)minU, (double)minV);
		tessellator.draw();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		if(entity instanceof EntityCustomBalls)
			return ((EntityCustomBalls)entity).getTexture();
		else
			return null;
	}
	
	@Override
	public void doRender(Entity entity, double relativeX, double relativeY, double relativeZ, float par8, float par9)
	{
		this.doRenderCommon(entity, relativeX, relativeY, relativeZ, par8, par9);
	}

}
