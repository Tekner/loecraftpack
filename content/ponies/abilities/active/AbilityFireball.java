package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.content.ponies.abilities.projectiles.Fireball;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class AbilityFireball extends ActiveAbility
{
	public AbilityFireball()
	{
		super("Fireball", Race.UNICORN, 50, 3, 1);
		alignment = Elemental;
		targetType = TargetType.AIMED;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		if(super.CastSpellServer())
		{
			Fireball.SpawnFireballOnPlayer(playerData.player, 3, 2, getDamage(), true, true);
			return true;
		}
		return false;
	}
	
	private float getDamage()
	{
		return 4 + playerStats.getStatValueWithModifiers(playerStats.magic)/27.75f;
	}
	
	@Override
	public float getMaxCooldown()
	{
		if (getAlignmentPoints() >= 99)
			return 1;
		else
			return 1 + (99-getAlignmentPoints()) * 0.0202f;
	}
	
	@Override
	public float getCasttime()
	{
		if (getAlignmentPoints() >= 100)
			return 0.25f;
		else
			return (float)(Math.ceil(4f - getAlignmentPoints()/33)*0.25f);
	}
	
	@Override
	public String GetDescription()
	{
		return "Fireball shoots a projectile that damages and sets fire to\nthe ground and all enemies in a small area around where it hits.";
	}
}
