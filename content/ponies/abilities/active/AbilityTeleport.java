package loecraftpack.content.ponies.abilities.active;

import io.netty.buffer.Unpooled;
import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

public class AbilityTeleport extends ActiveAbility
{
	public AbilityTeleport()
	{
		super("Teleport", Race.UNICORN, 0, 10);
		targetType = TargetType.AIMED;
		alignment = Arcane;
	}

	@Override
	protected boolean CastSpellClient()
	{
		MovingObjectPosition target = playerData.player.rayTrace(getMaxDistance(), 1);
		if (target == null)
			return false;
		else
		{
			double x = target.hitVec.xCoord,
				   y = target.hitVec.yCoord,
				   z = target.hitVec.zCoord;
			
			if(target.entityHit == null)
			{
				switch(target.sideHit)
				{
					case 0: y -= 0.5d; break;
					case 1: y += 0.5d; break;
					case 2: z -= 0.5d; break;
					case 3: z += 0.5d; break;
					case 4: x -= 0.5d; break;
					case 5: x += 0.5d; break;
				}
			}
			
			if (!playerData.player.capabilities.isCreativeMode)
			{
				AbilityPlayerData.clientData.addEnergy(-energyCost, true);
				AbilityPlayerData.clientData.addAfterImage(energyCost);
			}
			
			PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
			buffer.writeDouble((int)x);
			buffer.writeDouble((int)y);
			buffer.writeDouble((int)z);
			LoECraftPack.packetHandler.sendToServer(new PacketUseAbility().INIT(getId(), buffer));
		}
		
		return true;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		return false;
	}
	
	@Override
	protected boolean castSpellServerPacket(EntityPlayer sender, PacketBuffer data)
	{
		double x = data.readDouble();
		double y = data.readDouble();
		double z = data.readDouble();
		
		System.out.println("   targ "+x+" "+y+" "+z);
		
		if (sender.capabilities.isCreativeMode)
		{
			sender.setPositionAndUpdate(x, y, z);
			return true;
		}
		else
		{
			double distance = getPosition(sender, 1.0f).distanceTo(Vec3.createVectorHelper(x, y, z));
			if (distance <= getMaxDistance())
			{
				int energyCost = (int)(getEnergyCostRate() * distance);
				if (energyCost <= playerData.energy)
				{
					sender.setPositionAndUpdate(x, y, z);
					playerData.addEnergy(-energyCost, false);
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public float getEnergyCost()
	{
		if (!playerData.player.worldObj.isRemote)
			return 0;
		MovingObjectPosition target = playerData.player.rayTrace(getMaxDistance(), 1);
		if (target == null)
			return Float.MAX_VALUE;
		else
		{
			double distance = playerData.player.getPosition(1.0f).distanceTo(target.hitVec);
			return energyCost = (int)(getEnergyCostRate() * distance);
		}
	}
	
	@Override
	public String getEnergyCostString()
	{
		return "1-" + (int)(getEnergyCostRate()*getMaxDistance()) + " Energy (based on distance)";
	}
	
	private float getEnergyCostRate()
	{
		return 10 - 8 * (getAlignmentPoints() / 99f);
	}
	
	public float getMaxDistance()
	{
		return 10 + 90 * (getAlignmentPoints() / 99f);
	}

	@Override
	public float getMaxCooldown()
	{
		if (getAlignmentPoints() >= 99)
			return 2;
		else
			return 2 + 8 * ((99-getAlignmentPoints()) / 99f);
	}
	
	@Override
	public boolean isUsable()
	{
		return super.isUsable() && getEnergyCost() <= playerData.energy;
	}
	
	///// player vec3 function  (imported because old function became client sided)
	
	public Vec3 getPosition(EntityPlayer player, float par1)
    {
        if (par1 == 1.0F)
        {
            return player.worldObj.getWorldVec3Pool().getVecFromPool(player.posX, player.posY, player.posZ);
        }
        else
        {
            double d0 = player.prevPosX + (player.posX - player.prevPosX) * (double)par1;
            double d1 = player.prevPosY + (player.posY - player.prevPosY) * (double)par1;
            double d2 = player.prevPosZ + (player.posZ - player.prevPosZ) * (double)par1;
            return player.worldObj.getWorldVec3Pool().getVecFromPool(d0, d1, d2);
        }
    }
	
	@Override
	public String GetDescription()
	{
		return "Teleport lets you instantly position yourself\nwherever you're currently looking.\nThe energy it takes to use Teleport increases\nthe farther you travel.\nMax Distance: " + getMaxDistance();
	}
}
