package loecraftpack.content.ponies.abilities.active;

import io.netty.buffer.Unpooled;

import java.io.DataInputStream;
import java.io.IOException;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class AbilityBusterHoof extends ActiveAbility
{
	private final float dtr = (float)Math.PI/180f; //Degree To Radian
	
	public AbilityBusterHoof()
	{
		super("Buster Hoof", Race.EARTH, 25, 0.5f);
		targetType = TargetType.AIMED;
	}

	@Override
	protected boolean CastSpellClient()
	{
		MovingObjectPosition target = Minecraft.getMinecraft().objectMouseOver;
		if (target == null)
			return false;
		else
		{
			if(target.entityHit != null && target.entityHit instanceof EntityLivingBase)
			{
				if (playerData.player.capabilities.isCreativeMode || playerData.energy >= energyCost)
				{
					PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
					buffer.writeInt(target.entityHit.getEntityId());
					LoECraftPack.packetHandler.sendToServer(new PacketUseAbility().INIT(getId(), buffer));
					if (!playerData.player.capabilities.isCreativeMode)
					{
						playerData.addEnergy(-energyCost, true);
						playerData.addAfterImage(energyCost);
					}
					return true;
				}
			}
		}
		
		return false;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		return false;
	}
	
	@Override
	protected boolean castSpellServerPacket(EntityPlayer sender, PacketBuffer data)
	{
		EntityLivingBase target = (EntityLivingBase)sender.worldObj.getEntityByID(data.readInt());
		if (target != null)
		{
			float pitch = sender.rotationPitch*dtr, yaw = sender.rotationYaw*dtr;
			float force = 1f;
			
			if (sender.capabilities.isCreativeMode)
			{
				//target.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 200, 1));
				//target.addPotionEffect(new PotionEffect(Potion.weakness.id, 200, 1));
				target.attackEntityFrom((new EntityDamageSource("busterHoof", sender)), playerStats.getMeleeMultiplier() * 6);
				target.motionX -= force * Math.cos(pitch) * Math.sin(yaw);
				target.motionY += force * 0.4d;
				target.motionZ += force * Math.cos(pitch) * Math.cos(yaw);
				return true;
			}
			else if (playerData.energy >= energyCost)
			{
				//target.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 200, 1));
				//target.addPotionEffect(new PotionEffect(Potion.weakness.id, 200, 1));
				target.attackEntityFrom((new EntityDamageSource("busterHoof", sender)), playerStats.getMeleeMultiplier() * 6);
				target.motionX -= force * Math.cos(pitch) * Math.sin(yaw);
				target.motionY += force * 0.4d;
				target.motionZ += force * Math.cos(pitch) * Math.cos(yaw);
				playerData.addEnergy(-energyCost, false);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "";
	}
}
