package loecraftpack.content.ponies.abilities.active;

import io.netty.buffer.Unpooled;

import java.io.DataInputStream;
import java.io.IOException;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class AbilityFireWard extends ActiveAbility
{
	public AbilityFireWard()
	{
		super("Fire Ward", Race.UNICORN, 100, 1);
		targetType = TargetType.AIMED;
	}

	@Override
	protected boolean CastSpellClient()
	{
		MovingObjectPosition target = Minecraft.getMinecraft().objectMouseOver;
		if (target == null)
			return false;
		else
		{
			EntityPlayer playerHit;
			if(target.entityHit != null && target.entityHit instanceof EntityPlayer)
				playerHit = (EntityPlayer)target.entityHit;
			else
				playerHit = playerData.player;
			if (playerData.player.capabilities.isCreativeMode || playerData.energy >= energyCost)
			{
				PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
				buffer.writeInt(playerHit.getEntityId());
				LoECraftPack.packetHandler.sendToServer(new PacketUseAbility().INIT(getId(), buffer));
				if (!playerData.player.capabilities.isCreativeMode)
				{
					playerData.addEnergy(-energyCost, true);
					playerData.addAfterImage(energyCost);
				}
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		return false;
	}
	
	@Override
	protected boolean castSpellServerPacket(EntityPlayer sender, PacketBuffer data)
	{
		EntityPlayer target = (EntityPlayer)sender.worldObj.getEntityByID(data.readInt());
		
		if (sender.capabilities.isCreativeMode)
		{
			target.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 6000, 0));
			return true;
		}
		else if (playerData.energy >= energyCost)
		{
			target.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 6000, 0));
			playerData.addEnergy(-energyCost, false);
			return true;
		}
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "Fire Ward grants either yourself or a player\nfire resistance for five minutes.\nYou must be within 10m and aim at the\nother player for it to affect them.";
	}
}
