package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.world.World;

public class AbilityHomewardBound extends ActiveAbility
{
	public AbilityHomewardBound()
	{
		super("Homeward Bound", Race.UNICORN, 70, 60, 10);
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		EntityPlayer player = playerData.player;
		World world = player.worldObj;
		ChunkCoordinates currentDimBedCoords = player.getBedLocation(world.provider.dimensionId);
		ChunkCoordinates defaultDimBedCoords = player.getBedLocation(0);
		if (currentDimBedCoords != null)
		{
			if (super.CastSpellServer())
			{
				player.setPositionAndUpdate(currentDimBedCoords.posX, currentDimBedCoords.posY, currentDimBedCoords.posZ);
				return true;
			}
		}
		else if (defaultDimBedCoords != null)
		{
			if (super.CastSpellServer())
			{
				MinecraftServer.getServer().getConfigurationManager().transferPlayerToDimension((EntityPlayerMP)player, 0);
				player.setPositionAndUpdate(defaultDimBedCoords.posX, defaultDimBedCoords.posY, defaultDimBedCoords.posZ);
				return true;
			}
		}
		else if (world.getSpawnPoint() != null)
		{
			if (super.CastSpellServer())
			{
				player.setPositionAndUpdate(world.getSpawnPoint().posX, world.getSpawnPoint().posY, world.getSpawnPoint().posZ);
				
				player.addChatMessage(new ChatComponentText("Your bed could not be found. We know ...we checked. Instead we sent you to the spawn area."));
				return true;
			}
		}
		else
			player.addChatMessage(new ChatComponentText("You were pulled through the dimensions in search of a place to teleport to safely, but you failed. The Doctor had to bring you back. Go sleep in a bed or go to a world with a proper spawn point and don't bug him again."));
		
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "Homeward Bound pulls you through spacetime in search of a safe place.\nIt will send you to your bed if it can find it,\nbut if not, you will be sent to Spawn.";
	}
}
