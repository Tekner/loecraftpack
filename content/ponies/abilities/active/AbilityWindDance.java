package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.referrance.enums.Race;

public class AbilityWindDance extends ActiveAbility
{
	
	public AbilityWindDance()
	{
		super("Wind Dance", Race.PEGASUS, 0);
	}
	
	@Override
	protected boolean CastSpellClient()
	{
		if (!GetSharedFlag("flying") && playerData.energy >= 50)
		{
			SetSharedData("evasion", 0.33f);
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		if (!GetSharedFlag("flying") && playerData.energy >= 50)
		{
			SetSharedData("evasion", 0.33f);
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean CastSpellToggledClient()
	{
		return GetSharedFloat("evasion") > 0;
	}
	
	@Override
	protected boolean CastSpellToggledServer()
	{
		return GetSharedFloat("evasion") > 0;
	}
	
	@Override
	protected void CastSpellUntoggledClient()
	{
		SetSharedData("evasion", 0f);
	}
	
	@Override
	protected void CastSpellUntoggledServer()
	{
		SetSharedData("evasion", 0f);
	}
	
	@Override
	public float getEnergyCostToggled()
	{
		if (!GetSharedFlag("flying"))
			return playerStats.getEnergyRegen() / 20f;
		else
			return 0;
	}
	
	@Override
	public boolean isUsable()
	{
		return super.isUsable() && !GetSharedFlag("flying") && playerData.energy >= 50;
	}
	
	@Override
	public String GetDescription()
	{
		return "You do the dance of your people, increasing\nyour dodge chance by 33% while not flying.\nEvery attack successfully dodged while\nunder the effect of Wind Dance consumes 50 energy.";
	}
}
