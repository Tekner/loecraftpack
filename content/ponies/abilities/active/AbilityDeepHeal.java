package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class AbilityDeepHeal extends ActiveAbility
{
	public AbilityDeepHeal()
	{
		super("Deep Heal", Race.EARTH, 50, 90, 3);
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		if (super.CastSpellServer())
		{
			playerData.player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 600, 3));
			return true;
		}
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "Deep Heal gives you fast health regeneration for 30 seconds.\nIt's long cast time and cooldown make it very situational, however.";
	}
}
