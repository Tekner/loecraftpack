package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.content.ponies.abilities.projectiles.EntityCustomBalls;
import loecraftpack.content.ponies.stats.Stats;
import loecraftpack.referrance.enums.Race;

public class AbilitySonicBreath extends ActiveAbility
{
	int duration = 0;
	
	public AbilitySonicBreath()
	{
		super("Fus Ro Yay", Race.DRAGON, 50, 3, 1);
		alignment = 1;
		isToggleable = true;
		targetType = TargetType.BLAST;
	}
	
	@Override
	protected boolean CastSpellClient()
	{
		duration = 40;
		return super.CastSpellClient();
	}

	@Override
	protected boolean CastSpellServer()
	{
		duration = 40;
		if(super.CastSpellServer())
		{
			playerData.player.addExhaustion(2);
			Stats stats = playerData.getPlayerStats();
			stats.dragonFuel.setValue(Math.max(stats.dragonFuel.getValue()-1, 0));
			EntityCustomBalls.SpawnSprayOnPlayer(playerData.player, 10, getForce(), getDamage(), EntityCustomBalls.Type.WIND);
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean CastSpellToggledClient()
	{
		duration--;
		return duration != 0;
	}
	
	@Override
	protected boolean CastSpellToggledServer()
	{
		duration--;
		if (duration == 0)
			return false;
		if (duration % 3 == 0)
		{
			EntityCustomBalls.SpawnSprayOnPlayer(playerData.player, 10, getForce(), getDamage(), EntityCustomBalls.Type.WIND);
		}
		return true;
	}
	
	protected float getForce()
	{
		return 0.15f + getAlignmentPoints()/11f;
	}
	
	protected float getDamage()
	{
		return 0.5f + playerStats.getStatValueWithModifiers(playerStats.magic)/222f;
	}
	
	@Override
	public float getMaxCooldown()
	{
		if (getAlignmentPoints() >= 99)
			return 3;
		else
			return 3 + (99-getAlignmentPoints()) * 0.0202f;
	}
	
	@Override
	public float getCasttime()
	{
		if (getAlignmentPoints() >= 100)
			return 0.25f;
		else
			return (float)(Math.ceil(4f - getAlignmentPoints()/33)*0.25f);
	}
	
	@Override
	public String GetDescription()
	{
		return "Release your enthusiasim upon unsuspecting foes.";
	}
}
