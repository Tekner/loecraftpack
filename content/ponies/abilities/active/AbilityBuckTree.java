package loecraftpack.content.ponies.abilities.active;

import io.netty.buffer.Unpooled;

import java.io.DataInputStream;
import java.io.IOException;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.content.ponies.abilities.mechanics.MechanicTreeBucking;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.network.packets.PacketUseAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class AbilityBuckTree extends ActiveAbility
{
	public AbilityBuckTree()
	{
		super("Buck Tree", Race.EARTH, 20, 3);
		targetType = TargetType.AIMED;
	}
	
	@Override
	protected boolean CastSpellClient()
	{
		MovingObjectPosition target = playerData.player.rayTrace(2, 1);
		if (target == null)
			return false;
		else
		{
			double x = target.hitVec.xCoord;
			double y = target.hitVec.yCoord;
			double z = target.hitVec.zCoord;
			
			if(target.entityHit == null)
			{
				switch(target.sideHit)
				{
					case 0: y += 0.5d; break;
					case 1: y -= 0.5d; break;
					case 2: z += 0.5d; break;
					case 3: z -= 0.5d; break;
					case 4: x += 0.5d; break;
					case 5: x -= 0.5d; break;
				}
			}
			
			if (x<0) x-=1;
			if (y<0) y-=1;
			if (z<0) z-=1;
			
			
			Block block = playerData.player.worldObj.getBlock((int)x, (int)y, (int)z);
			
			if (Block.isEqualTo(block, LOE_Blocks.zapAppleLog) ||
					Block.isEqualTo(block, LOE_Blocks.appleBloomLog))
			{
				PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
				buffer.writeInt((int)x);
				buffer.writeInt((int)y);
				buffer.writeInt((int)z);
				LoECraftPack.packetHandler.sendToServer(new PacketUseAbility().INIT(getId(), buffer));
				return true;
			}
			return false;
		}
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		return false;
	}
	
	@Override
	protected boolean castSpellServerPacket(EntityPlayer sender, PacketBuffer data)
	{
		//debug:attepmt buck
		System.out.println("----BUCK? 4----");
		EntityPlayer player = (EntityPlayer) sender;
		int energyCost = (int)(this.getEnergyCost());
		int skill = 2;//Do: AbilityBuckTree - might want to increase this with certain ability levels
		int fortune = 0;//Do: AbilityBuckTree - might want to increase this with certain accessories
		
		
		if ((player.capabilities.isCreativeMode || playerData.energy>=energyCost) && MechanicTreeBucking.buckTree(player.worldObj, data.readInt(), data.readInt(), data.readInt(), skill, fortune))
		{
			if (!player.capabilities.isCreativeMode)
			{
				playerData.addEnergy(-energyCost, false);
				playerData.sendChangingPlayerStatPacket();
			}
			return true;
		}
		return false;
	}

	@Override
	public String GetDescription()
	{
		return "Buck Tree allows you to buck Zap Apple and Apple Bloom\ntrees, which makes them drop all of their apples.";
	}
}
