package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.passive.AbilityMagicBarrier;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class AbilityMagicBarrierRecharge extends ActiveAbility
{
	
	public AbilityMagicBarrierRecharge()
	{
		super("Magic Barrier: Recharge", Race.UNICORN, 0, 10);
	}
	
	private int getMaxShieldAmount()
	{
		return AbilityMagicBarrier.getMaxShieldAmount(getAlignmentPoints());
	}
	
	@Override
	protected boolean CastSpellClient()
	{
		float energyCost = getEnergyCost();
		playerData.player.setAbsorptionAmount(Math.min(playerData.player.getAbsorptionAmount() + ((int)playerData.energy / getEnergyCostRate()), getMaxShieldAmount()));
		playerData.addEnergy(-energyCost, true); //We must reduce energy here because the energy cost is dependent on the shield amount, which just got changed.
		return true;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		float energyCost = getEnergyCost();
		playerData.player.setAbsorptionAmount(Math.min(playerData.player.getAbsorptionAmount() + ((int)playerData.energy / getEnergyCostRate()), getMaxShieldAmount()));
		playerData.addEnergy(-energyCost, false); //We must reduce energy here because the energy cost is dependent on the shield amount, which just got changed.
		return true;
	}
	
	@Override
	public float getEnergyCost()
	{
		return Math.min((int)playerData.energy / getEnergyCostRate(), getMaxShieldAmount() - playerData.player.getAbsorptionAmount()) * getEnergyCostRate();
	}
	
	private int getEnergyCostRate()
	{
		return 10;
	}
	
	@Override
	public String getEnergyCostString()
	{
		return getEnergyCostRate() + "-" + Math.min((playerStats.getMaxEnergy() / getEnergyCostRate()) * getEnergyCostRate(), getMaxShieldAmount() * getEnergyCostRate());
	}
	
	@Override
	public boolean isUsable()
	{
		return super.isUsable() && playerData.energy >= getEnergyCostRate() && playerData.player.getAbsorptionAmount() < getMaxShieldAmount();
	}
	
	@Override
	public String GetDescription()
	{
		return "You deplete your entire energy reserve and\nrecharge as much of your Magic Barrier as possible.";
	}
}