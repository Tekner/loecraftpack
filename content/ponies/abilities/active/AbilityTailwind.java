package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class AbilityTailwind extends ActiveAbility
{
	public AbilityTailwind()
	{
		super("Tail Wind", Race.PEGASUS, 60, 5);
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		if (super.CastSpellServer())
		{
			playerData.player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, (int)getMaxCooldown() * 20, Math.min(getAlignmentPoints(), 30) / 15));
			return true;
		}
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "Tailwind increases your speed greatly for five seconds.";
	}
}
