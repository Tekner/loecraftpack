package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.TargetType;
import loecraftpack.content.ponies.abilities.projectiles.Fireball;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class AbilityFrostbolt extends ActiveAbility
{
	public AbilityFrostbolt()
	{
		super("Frostbolt", Race.UNICORN, 50, 3, 1);
		targetType = TargetType.AIMED;
	}
	
	@Override
	protected boolean CastSpellServer()
	{
		if(super.CastSpellServer())
		{
			Fireball.SpawnFireballOnPlayer(playerData.player, 3, 0, 2, false, false, false, false, 0, false, false, new PotionEffect[] {new PotionEffect(Potion.moveSlowdown.id, 200, 1)});
			//Waterball.SpawnWaterballOnPlayer(player, 1, 0, 2);
			return true;
		}
		return false;
	}
	
	@Override
	public String GetDescription()
	{
		return "Frostbolt shoots a projectile that damages and slows\nall enemies in a small area around where it hits.";
	}
}