package loecraftpack.content.ponies.abilities.active;

import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.abilities.ActiveAbility;
import loecraftpack.content.ponies.abilities.mechanics.MechanicHiddenOres;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;

public class AbilityOreVision extends ActiveAbility
{
	public static long refreshtime;
	
	public AbilityOreVision()
	{
		super("Gem Vision", Race.UNICORN, 10);
		alignment = Arcane;
	}
	
	@Override
	public float getEnergyCostToggled()
	{
		return 0.5f;
	}
	

	@Override
	protected boolean CastSpellClient()
	{
		refreshtime = Minecraft.getSystemTime();
		MechanicHiddenOres.revealHiddenGems = true;
		MechanicHiddenOres.rangeHiddenGems = Math.min(50, 5 + getAlignmentPoints()*1.5d);
		MechanicHiddenOres.rangeOtherGems = Math.min(50, 1 + Math.max(0, (getAlignmentPoints()-5)*1.1d));
		//not implemented
		MechanicHiddenOres.rangeDeformaties = Math.min(30, Math.max(0, (getAlignmentPoints()-15)*2.0d));
		MechanicHiddenOres.refreshRenderWithRange(playerData.player);
		return true;
	}
	
	@Override
	protected boolean CastSpellToggledClient()
	{
		double pX = playerData.player.posX;
		double pY = playerData.player.posY;
		double pZ = playerData.player.posZ;
		
		pX = pX<0? pX-1: pX;
		pY = pY<0? pY-1: pY;
		pZ = pZ<0? pZ-1: pZ;
		
		if (MechanicHiddenOres.xPos != (int) pX ||
			MechanicHiddenOres.yPos != (int) pY ||
			MechanicHiddenOres.zPos != (int) pZ ||
			refreshtime + 2000/*2 sec.*/< Minecraft.getSystemTime())
		{
			refreshtime = Minecraft.getSystemTime();
			MechanicHiddenOres.refreshRenderWithRange(playerData.player);
		}
		
		float cost = getEnergyCostToggled();
		AbilityPlayerData.addAfterImage(cost);
		
		return true;
	}
	
	@Override
	protected void CastSpellUntoggledClient()
	{
		MechanicHiddenOres.revealHiddenGems = false;
		MechanicHiddenOres.refreshRenderWithRange(playerData.player);
	}
	
	@Override
	public String GetDescription()
	{
		return "Gem Vision lets you see all gems that\nare near you, even through other blocks.";
	}
}
