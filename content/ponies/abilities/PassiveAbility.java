package loecraftpack.content.ponies.abilities;


import java.util.ArrayList;
import java.util.HashMap;

import loecraftpack.content.ponies.abilities.mechanics.PonyDodgedEvent;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.common.registry.LanguageRegistry;

public abstract class PassiveAbility extends AbilityBase
{
	public static Class[] abilityClasses = new Class[0];
	private static HashMap abilityIdByName = new HashMap<String, Integer>();
	
	protected int period = 0; //tick delay before the ability performs logic
	private float count = 1; //count to keep track of when to perform logic
	
	private long time = System.currentTimeMillis();
	
	public PassiveAbility(String name, Race race)
	{
		super(name, race);
	}
	
	public PassiveAbility(String name, Race race, int period)
	{
		super(name, race);
		this.period = period;
	}
	
	public static void RegisterAbilities(Class... classes)
	{
		System.out.println("Registering New Passive Abilities...");
		abilityClasses = classes;
		ItemPassiveAbility.num = Math.max(0, abilityClasses.length - 1);
		
		for(PassiveAbility ability : NewAbilityArray())
		{
			abilityIdByName.put(ability.name, ability.ID);
			System.out.println("Successfully registered \"" + ability.name + "\"");
			LanguageRegistry.instance().addStringLocalization("item.itemPassiveAbility." + ability.icon + ".name", ability.name);
		}
	}
	
	public static PassiveAbility[] NewAbilityArray()
	{
		ArrayList<PassiveAbility> abilityList = new ArrayList<PassiveAbility>();
		for (int i = 0; i < abilityClasses.length; i++)
		{
			Class c = abilityClasses[i];
			try
			{
				PassiveAbility ability = (PassiveAbility)c.getConstructor().newInstance();
				abilityList.add(ability);
				ability.ID = i;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return abilityList.toArray(new PassiveAbility[0]);
	}
	
	public void onTick(EntityPlayer player)
	{
		if (period > 0 && playerStats != null && (race == Race.NONE || playerStats.isRace(race)) && count++ >= period)
		{
			if (isClient())
				onUpdateClient(player);
			else
				onUpdateServer(player);
			
			count = 1;
		}
		time = System.currentTimeMillis();
	}
	
	protected float deltaTime()
	{
		return (System.currentTimeMillis() - time) / 1000f;
	}
	
	public void onUpdateClient(EntityPlayer player) {}
	public void onUpdateServer(EntityPlayer player) {}
	
	private interface PassedEvent {boolean Pass(Event event);}
	private static Class[] eventClasses = {LivingFallEvent.class, LivingHurtEvent.class, PlayerInteractEvent.class, PonyDodgedEvent.class};
	private PassedEvent[] passedEvents = {new PassedEvent() {public boolean Pass(Event event) {FallEvent((LivingFallEvent)event); return !event.isCanceled();}},
										  new PassedEvent() {public boolean Pass(Event event) {HurtEvent((LivingHurtEvent)event); return !event.isCanceled();}},
										  new PassedEvent() {public boolean Pass(Event event) {InteractEvent((PlayerInteractEvent)event); return !event.isCanceled();}},
										  new PassedEvent() {public boolean Pass(Event event) {DodgedEvent((PonyDodgedEvent)event); return !event.isCanceled();}}};
	public final boolean PassEvent(Event event) //Returns if the event was successful (not canceled)
	{
		if (playerStats != null && (race == Race.NONE || playerStats.isRace(race)))
		{
			for(int i = 0; i < eventClasses.length; i++)
			{
				if (event.getClass() == eventClasses[i])
				{
					return passedEvents[i].Pass(event);
				}
			}
		}
		
		return true;
	}
	
	protected void FallEvent(LivingFallEvent event) {}
	protected void HurtEvent(LivingHurtEvent event) {}
	protected void InteractEvent(PlayerInteractEvent event) {}
	protected void DodgedEvent(PonyDodgedEvent event) {}
	
	public static int getIdByName(String abilityName)
	{
		return abilityIdByName.get(abilityName) == null ? -1 : (Integer)abilityIdByName.get(abilityName);
	}
}
