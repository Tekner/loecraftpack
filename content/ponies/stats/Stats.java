package loecraftpack.content.ponies.stats;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.items.accessories.ItemAccessory;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.content.ponies.stats.components.Stat;
import loecraftpack.content.ponies.stats.components.Stat.IStats;
import loecraftpack.content.ponies.stats.components.StatFlags;
import loecraftpack.content.ponies.stats.components.StatType;
import loecraftpack.referrance.enums.Race;
import net.minecraft.nbt.NBTTagCompound;

public class Stats implements IStats
{
	private static int nextID = 0;
	
	public static final StatType Experiance = new StatType("Experiance", nextID++);
	public static final StatType PonyLevel  = new StatType("PonyLevel", nextID++);
	
	public static final StatType Offense    = new StatType("Offense", nextID++, StatFlags.Alignment);
	public static final StatType Defense    = new StatType("Defense", nextID++, StatFlags.Alignment);
	public static final StatType Utility    = new StatType("Utility", nextID++, StatFlags.Alignment);
	
	public static final StatType Strength   = new StatType("Strength", nextID++, StatFlags.PonyLevel);
	public static final StatType Magic      = new StatType("Magic"   , nextID++, StatFlags.PonyLevel);
	public static final StatType Agility    = new StatType("Agility" , nextID++, StatFlags.PonyLevel);
	
	public static final StatType DragonFuel = new StatType("DragonBreath", nextID++);
	
	public final Stat experiance = Experiance.Instance(this);
	public final Stat ponyLevel  = PonyLevel.Instance(this);
	
	public final Stat offense    = Offense.Instance(this);
	public final Stat defense    = Defense.Instance(this);
	public final Stat utility    = Utility.Instance(this);
	
	public final Stat strength   = Strength.Instance(this);
	public final Stat magic      = Magic.Instance(this);
	public final Stat agility    = Agility.Instance(this);
	
	public final Stat dragonFuel = DragonFuel.Instance(this);
	
	private final Stat[] statList = new Stat[] {experiance, ponyLevel, offense, defense, utility, strength, magic, agility, dragonFuel};
	public Stat[] getStatList() { return statList; }
	
	public AbilityPlayerData abilityData;
	private Race race = Race.NONE;
	
	public Stats(AbilityPlayerData abilityData)
	{
		this.abilityData = abilityData;
	}
	
	public Stats(AbilityPlayerData abilityData, Race race, float energy)
	{
		this.abilityData = abilityData;
		this.race = race;
		this.abilityData.energy = energy;
	}
	
	public void setStatValues(int... values)
	{
		for(int i = 0; i < values.length && i < statList.length; i++)
		{
			statList[i].setValue(values[i]);
		}
	}
	
	public byte getStatsSize()
	{
		return (byte)statList.length;
	}
	
	public int[] getStatValues()
	{
		int[] values = new int[statList.length];
		
		for (int i=0; i<statList.length; i++)
		{
			values[i]=statList[i].getValue();
		}
		
		return values;
	}
	
	public void loadStatsFromNBT (NBTTagCompound nbt)
	{
		if (nbt.hasKey("Stats") && nbt.getTag("Stats") instanceof NBTTagCompound)
		{
			NBTTagCompound statCompound = nbt.getCompoundTag("Stats");
			if (statCompound != null)
			{
				for (Stat stat :statList)
				{
					stat.setValue(statCompound.getInteger(stat.getName()));
				}
			}
		}
	}
	
	public void saveStatsToNBT (NBTTagCompound nbt)
	{
		NBTTagCompound statCompound = new NBTTagCompound();
		for (Stat stat :statList)
		{
			statCompound.setInteger(stat.getName(), stat.getValue());
		}
		nbt.setTag("Stats", statCompound);
	}
	
	/*************************************/
	
	public void SetRace(Race race)
	{
		this.race = race;
		abilityData.fixRaceStats();
	}
	
	public boolean isRace(Race race)
	{
		return this.race == race || (this.race == Race.ALICORN && race != Race.NONE) || (race == Race.DRAGON && dragonFuel.getValue()>0);
	}
	
	public Race getRace()
	{
		return race;
	}
	
	public int getRaceOrdinal()
	{
		return race.ordinal();
	}
	
	
	/*************************************/
	
	private String[][] raceAliText = {{""   , ""   , ""   },
									  {"ELE", "RST", "ARC"},
									  {"STM", "REF", "FLT"},
									  {"COM", "WIL", "NAT"},
									  {"OFF", "DEF", "UTI"}};
	
	public String getAlignmentText(int alignment)
	{
		return raceAliText[getRaceOrdinal()][alignment];
	}
	
	public void addExp(int amount)
	{
		int newValue = experiance.getValue()+amount;
		int cost = 0;
		while (newValue >= (cost = getLevelupCost()))
		{
			newValue -= cost;
			ponyLevel.setValue(1+ponyLevel.getValue());
		}
		experiance.setValue(newValue);
	}
	
	public int getLevelupCost()
	{
		return 17 + (int)(34 * (getPonyLevel() + 1));
	}
	
	public int getMaxEnergy()
	{
		return Math.round(GetStatLinear(getPonyLevel(), 100, 333) * GetStatLinear(magic.getValue(), 1f, 1.5f));
	}
	
	public float getEnergyRegen()
	{
		return GetStatLinear(magic.getValue(), 10f, 20f) * (isRace(Race.UNICORN) ? GetAlignmentLinear(defense.getValue(), 1f, 1.5f) : 1);
	}
	
	public float getMoveSpeed()
	{
		return GetStatLinear(getPonyLevel(), 0.1f, 0.16f) * GetStatLinear(agility.getValue(), 1f, 1.5f);
	}
	
	public float getEvasion()
	{
		return 1-(1/(  GetStatLinear(agility.getValue(), 1.0f, 1.5f)*(isRace(Race.PEGASUS)? GetAlignmentLinear(defense.getValue(), 1.0f, (4f/3f)): 1)  ));
	}
	
	public float getMeleeMultiplier()
	{
		return GetStatLinear(getPonyLevel(), 1f, 8f) * GetStatLinear(strength.getValue(), 1f, 1.5f);
	}
	
	public float GetMiningSpeed()
	{
		return GetStatLinear(strength.getValue(), 1f, 5f);
	}
	
	public float getMaxHP()
	{
		return GetStatLinear(getPonyLevel(), 20f, 200f) * (isRace(Race.EARTH) ? GetAlignmentLinear(defense.getValue(), 1f, 1.5f) : 1);
	}
	
	public static float GetStatCurve(int stat, float a, float b, float c)
	{
		float statDiv = stat / 999f;
		return a * statDiv*statDiv + b * statDiv + c;
	}
	
	public static float GetStatLinear(int stat, float start, float end)
	{
		return (end - start) * (stat / 999f) + start;
	}
	
	public static float GetAlignmentCurve(int alignment, float a, float b, float c)
	{
		float statDiv = alignment / 99f;
		return a * statDiv*statDiv + b * statDiv + c;
	}
	
	public static float GetAlignmentLinear(int alignment, float start, float end)
	{
		return (end - start) * (alignment / 99f) + start;
	}
	
	
	/*******************************************/
	/*******************************************/
	
	/**the amount of alignment spent**/
	public int getAlignmentTotal()
	{
		return getStatTotal(StatFlags.Alignment);
	}
	
	/**the amount of points the player hasn't spent for alignment**/
	public int getAlignmentPoints()
	{
		return Math.max(0, getStatTotal(StatFlags.PonyLevel)/10 - getStatTotal(StatFlags.Alignment));
	}
	
	/**the amount of pony acquired**/
	public int getPonyLevel()
	{
		return ponyLevel.getValue();
	}
	
	/**the amount of points the player hasn't spent for pony level**/
	public int getPonyPoints()
	{
		return Math.max(0, ponyLevel.getValue() - getStatTotal(StatFlags.PonyLevel));
	}
	
	public int getStatTotal(int flags)
	{
		int result = 0;
		Stat[] stats = getStatsWithAllFlags(flags);
		for (Stat stat : stats)
			result += stat.getValue();
		return result;
	}
	
	public int getStatWithModifiersTotal(int flags)
	{
		return getStatTotal(flags) + getStatModifierTotal(flags);
	}
	
	public int getStatModifierTotal(int flags)
	{
		int result = 0;
		Stat[] stats = getStatsWithAllFlags(flags);
		
		for (Stat stat: stats)
			result += ItemAccessory.getTotalStatBoostforStat(abilityData.player, getRace(), stat);
		return result;
	}
	
	public int getStatValueWithModifiers(Stat stat)
	{
		if (stat == null)
			return 0;
		return stat.getValue() + getStatModifier(stat);
	}
	
	public int getStatModifier(Stat stat)
	{
		return ItemAccessory.getTotalStatBoostforStat(abilityData.player, getRace(), stat);
	}
	
	/*************************************/
	
	public Stat[] getStatsWithAnyFlag(int... flag)
	{
		List<Stat> foundStats = new ArrayList<Stat>();
		for(Stat stat : statList)
		{
			if (stat.hasAnyFlag(flag))
				foundStats.add(stat);
		}
		
		return foundStats.toArray(new Stat[0]);
	}
	
	public Stat[] getStatsWithAllFlags(int... flag)
	{
		List<Stat> foundStats = new ArrayList<Stat>();
		for(Stat stat : statList)
		{
			if (stat.hasAnyFlag(flag))
				foundStats.add(stat);
		}
		
		return foundStats.toArray(new Stat[0]);
	}

	@Override
	public void UpdateClientOfStatChanges(int statID, int newValue)
	{
		abilityData.sendChangingPlayerStatPacket(statID, newValue);
	}
	
	/*public void debug()
	{
		Stat[] stats = values.keySet().toArray(new Stat[values.size()]);
		Integer[] vals = values.getValue()s().toArray(new Integer[values.size()]);
		for (int i=0; i<values.size(); i++)
		{
			System.out.println("STat "+stats[i].name+" "+vals[i]);
		}
	}*/
}