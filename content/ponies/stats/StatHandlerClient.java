package loecraftpack.content.ponies.stats;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityBase;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.referrance.enums.Race;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class StatHandlerClient extends StatHandlerServer
{
	@Override
	public void addPlayer(String player)
	{
		if (Minecraft.getMinecraft().thePlayer.getGameProfile().getId().equals(player))
		{
			AbilityPlayerData data = AbilityPlayerData.RegisterPlayer(player);
			if (!LoECraftPack.isSinglePlayer())
				stats.put(player, new Stats(data));
			
			for(AbilityBase ability : data.activeAbilities)
				ability.SetPlayer(player, data);
			
			for(AbilityBase ability : data.passiveAbilities)
				ability.SetPlayer(player, data);
			
			data.bindPlayerStats(player);
			
			data.setPlayer(Minecraft.getMinecraft().thePlayer);
		}
	}
	
	//used by update packet
	@Override
	public void updatePlayerData(String player, Race race, float energy, int[] statValues)
	{
		if (Minecraft.getMinecraft().thePlayer.getGameProfile().getId().equals(player))
		{
			if (stats.containsKey(player))
			{
				//single-player load  OR  stat update
				Stats playerStats = (Stats)stats.get(player);
				playerStats.SetRace(race);
				playerStats.setStatValues(statValues);
				AbilityPlayerData abilityData = AbilityPlayerData.Get(player);
				abilityData.energy = energy;
				abilityData.fixRaceStats();
			}
			else
			{
				//multi-player load
				AbilityPlayerData abilityData = AbilityPlayerData.RegisterPlayer(player);
				abilityData.energy = energy;
				Stats playerStats = new Stats(abilityData, race, energy);
				stats.put(player, playerStats);
				playerStats.setStatValues(statValues);
				abilityData.fixRaceStats();
			}
		}
	}
	
	//used by update packet
	public void updatePlayerRace(String player, Race race)
	{
		if (stats.containsKey(player))
		{
			//race change
			Stats playerStats = (Stats)stats.get(player);
			playerStats.SetRace(race);
			AbilityPlayerData.Get(player).fixRaceStats();
		}
	}
	
	@Override
	public boolean isRace(EntityPlayer player, Race race)
	{
		return isRace(player.getGameProfile().getId(), race);
	}
	
	public boolean isRace(String player, Race race)
	{
		if (stats.containsKey(player))
		{
			Stats playerStats = (Stats)stats.get(player);
			return playerStats.isRace(race); //Alicorn is master race
		}
		
		return false;
	}
	
	public Race getRace(String player)
	{
		if (stats.containsKey(player))
			return ((Stats)stats.get(player)).getRace();
		else
			return Race.NONE;
	}
	
	@Override
	public void setRace(EntityPlayer player, Race race)
	{
		if (Minecraft.getMinecraft().isSingleplayer())
			super.setRace(player, race);
		else
			setRace(player.getGameProfile().getId(), race);
	}
	
	public void setRace(String player, Race race)
	{
		if (stats.containsKey(player))
			((Stats)stats.get(player)).SetRace(race);
	}
}