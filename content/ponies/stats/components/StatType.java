package loecraftpack.content.ponies.stats.components;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.ponies.stats.components.Stat.IStats;

public class StatType
{
	public final int statID;
	public final String name;
	public final int flags;
	
	public StatType(String name, int ID)
	{
		this(name, ID, 0);
	}
	
	public StatType(String name, int ID, int flags)
	{
		this.name = name;
		this.statID = ID;
		this.flags = flags;
	}
	
	public Stat Instance(IStats statsInterface)
	{
		return new Stat(this, statsInterface);
	}
}
