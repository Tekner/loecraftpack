package loecraftpack.content.ponies.stats.components;

import cpw.mods.fml.common.FMLCommonHandler;

public class Stat
{
	private final StatType type;
	private int value;
	private IStats statsInterface = null;
	
	Stat(StatType type, IStats statsInterface)
	{
		this.type = type;
		this.statsInterface = statsInterface;
	}
	
	public boolean isType(StatType statType)
	{
		return type == statType;
	}
	
	public String getName()
	{
		return type.name;
	}
	
	public int getID()
	{
		return type.statID;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public void setValue(int newValue)
	{
		value = newValue;
		
		if (FMLCommonHandler.instance().getEffectiveSide().isServer())
			statsInterface.UpdateClientOfStatChanges(type.statID, value);
	}
	
	public boolean hasAnyFlag(int... flag)
	{
		int _flags = type.flags;
		for(int _flag : flag)
			_flags &= _flag;
		
		return _flags > 0;
	}
	
	public boolean hasAllFlags(int... flag)
	{
		int _flags = type.flags;
		for(int _flag : flag)
			_flags |= _flag;
		
		return type.flags == _flags;
	}
	
	public interface IStats
	{
		public void UpdateClientOfStatChanges(int statID, int newValue);
		public Stat[] getStatList();
	}
}
