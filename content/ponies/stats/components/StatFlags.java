package loecraftpack.content.ponies.stats.components;

public class StatFlags
{
	private static int powerCounter = 0;
	
	public static final int Alignment  = nextPowerOfTwo();
	public static final int PonyLevel = nextPowerOfTwo();
	
	private static int nextPowerOfTwo()
	{
		if (powerCounter == 0)
		{
			return powerCounter = 1;
		}
		return powerCounter = powerCounter << 1;
	}
}
