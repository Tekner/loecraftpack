package loecraftpack.content.ponies.stats;

import java.util.HashMap;
import java.util.Map;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.abilities.AbilityBase;
import loecraftpack.content.ponies.abilities.AbilityPlayerData;
import loecraftpack.network.packets.PacketAddPlayerToStatsHandler;
import loecraftpack.network.packets.PacketApplyStats;
import loecraftpack.referrance.enums.Race;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class StatHandlerServer
{
	public static Map<String, Stats> stats = new HashMap<String, Stats>();
	
	public void addPlayer(String player) {} //to be overridden by the client stat handler
	
	public static void addPlayer(EntityPlayer player)
	{
		String ID = player.getGameProfile().getId();
		String username = player.getGameProfile().getName();
		System.out.println("Login-ing player ID:"+ID+"  username:"+username);
		
		NBTTagCompound nbtMaster = player.getEntityData();
		if (nbtMaster.hasNoTags())System.out.println("no custom nbt");
		NBTTagCompound nbt = nbtMaster.getCompoundTag("LoePlayerStats");
		if (nbtMaster.hasNoTags())System.out.println("no LOE nbt");
		
		AbilityPlayerData data = AbilityPlayerData.RegisterPlayer(player);
		
		float energy = nbt.getFloat("Energy");
		data.energy = energy;
		Race race = Race.values()[nbt.getByte("Race")];
		data.playerStats = new Stats(data, race, energy);
		data.playerStats.loadStatsFromNBT(nbt);
		
		stats.put(player.getGameProfile().getId(), data.playerStats);
		LoECraftPack.packetHandler.sendTo(new PacketAddPlayerToStatsHandler().INIT(ID), (EntityPlayerMP) player);
		LoECraftPack.packetHandler.sendTo(new PacketApplyStats().INIT(ID, data.playerStats), (EntityPlayerMP) player);
		for(AbilityBase ability : data.activeAbilities)
			ability.SetPlayer(ID, data);
		
		for(AbilityBase ability : data.passiveAbilities)
			ability.SetPlayer(ID, data);
		
		data.bindPlayerStats(ID);
		System.out.println("NBT: "+nbt.toString());
	}
	
	public static void savePlayer(EntityPlayer player)
	{
		System.out.println("SAVE");
		
		NBTTagCompound nbt = new NBTTagCompound();
		
		Stats playerStats = (Stats)stats.get(player.getGameProfile().getId());
		nbt.setByte("Race", (byte)playerStats.getRaceOrdinal());
		System.out.println("SAVE");
		playerStats.saveStatsToNBT(nbt);
		System.out.println("SAVE");
		nbt.setFloat("Energy", AbilityPlayerData.Get(player.getGameProfile().getId()).energy);
		
		player.getEntityData().setTag("LoePlayerStats", nbt);
		
		AbilityPlayerData.UnregisterPlayer(player.getGameProfile().getId());
	}
	
	public static boolean sendRaceStatToPlayer(EntityPlayer player)
	{
		if (stats.containsKey(player.getGameProfile().getId()))
		{
			Stats stat = (Stats)stats.get(player.getGameProfile().getId());
			LoECraftPack.packetHandler.sendTo(new PacketApplyStats().INIT(player.getGameProfile().getId(), stat.getRace()), (EntityPlayerMP) player);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//used by client side
	public void updatePlayerData(String player, Race race, float energy, int[] statValues){}
	
	//used by client side
	public void updatePlayerRace(String player, Race race){}
	
	public void updatePlayerStatBoosts(String player)
	{
		
	}
	
	public boolean isRace(EntityPlayer player, Race race)
	{
		Stats playerStats = (Stats)stats.get(player.getGameProfile().getId());
		//debug: check server side race
		//System.out.println("The Server IS racist: "+playerStats.race.toString());
		
		return playerStats.isRace(race); //Alicorn is master race
	}
	
	public Race getRace(EntityPlayer player)
	{
		Stats stat = (Stats)stats.get(player.getGameProfile().getId());
		if (stat == null)
			return Race.NONE;
		
		return stat.getRace();
	}
	
	public void setRace(EntityPlayer player, Race race)
	{
		((Stats)stats.get(player.getGameProfile().getId())).SetRace(race);
		//Do: make this packet more efficient
		sendRaceStatToPlayer(player);
	}
}