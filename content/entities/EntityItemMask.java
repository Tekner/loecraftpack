package loecraftpack.content.entities;

import loecraftpack.content.registery.LOE_Items;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityItemMask extends EntityItemHangingCustom
{

	public EntityItemMask(World world)
	{
		super(world);
	}
	
	public EntityItemMask(World world, int xPos, int yPos, int zPos, int direction)
	{
		super(world, xPos, yPos, zPos, direction);
	}
	
	@Override
	protected void entityInit()
	{
		this.getDataWatcher().addObject(3, Byte.valueOf((byte)0));
	}
	
	@Override
	public int getWidthPixels()
	{
		return 9;
	}

	@Override
	public int getHeightPixels()
	{
		return 15;
	}
	
	@Override
	public float getCenterOffsetX()
	{
		return 0;
	}
	
	@Override
	public float getCenterOffsetY()
	{
		return 0;
	}

	@Override
	public void onBroken(Entity entity)
	{
		if (entity instanceof EntityPlayer)
		{
			EntityPlayer entityplayer = (EntityPlayer)entity;

			if (entityplayer.capabilities.isCreativeMode)
			{
				return;
			}
		}
		this.entityDropItem(new ItemStack(LOE_Items.mask, 1, getMaskType()), 0.0F);
	}
	
	public void setMaskType(int type)
	{
		this.getDataWatcher().updateObject(3, Byte.valueOf((byte)type));
	}
	
	public int getMaskType()
	{
		return (int)this.getDataWatcher().getWatchableObjectByte(3);
	}
	
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		compound.setByte("masktype", (byte)getMaskType());
	}
	
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);
		setMaskType(compound.getByte("masktype"));
	}
	
	public ItemStack getPickedResult(MovingObjectPosition target)
	{
		return new ItemStack(LOE_Items.mask, 1, getMaskType());
	}
}
