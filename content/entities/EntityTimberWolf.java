package loecraftpack.content.entities;

import loecraftpack.content.entities.ai.EntityAIPlayDead;
import loecraftpack.content.items.ItemBits;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Enchantment;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class EntityTimberWolf extends EntityMob implements IPossum
{
	public static final int formStateMax = 10;
	protected int dying = 0;
	protected int regenBuffer = 0;
	
	protected static float size = 3.0f;
	
	private float newInterest;
	private float oldInterest;
	
	/** true is the wolf is wet else false */
	private boolean isShaking;
	private boolean field_70928_h;
	
	/** This time increases while wolf is shaking and emitting water particles. */
	private float timeWolfIsShaking;
	private float prevTimeWolfIsShaking;
	
	public EntityTimberWolf(World par1World)
	{
		super(par1World);
		
		this.setSize(size*0.6F, size*0.8F);
		this.experienceValue = 8;
		this.getNavigator().setAvoidsWater(true);
		this.tasks.addTask(1, new EntityAISwimming(this));
		this.tasks.addTask(2, new EntityAIPlayDead(this));
		this.tasks.addTask(3, new EntityAILeapAtTarget(this, 0.4F));
		this.tasks.addTask(4, new EntityAIAttackOnCollide(this, EntityPlayer.class, 1.0d, false));
		this.tasks.addTask(5, new EntityAIAttackOnCollide(this, EntityVillager.class, 1.0d, true));
		this.tasks.addTask(6, new EntityAIAttackOnCollide(this, EntitySheep.class, 1.0d, true));
		this.tasks.addTask(7, new EntityAIWander(this, 1.0d));
		this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(8, new EntityAILookIdle(this));
		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true));
		this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
		this.targetTasks.addTask(3, new EntityAINearestAttackableTarget(this, EntityVillager.class, 0, false));
		this.targetTasks.addTask(4, new EntityAINearestAttackableTarget(this, EntitySheep.class, 0, true));
	}
	
	//init attributes
	@Override
	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		
		//move speed
		this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.30000001192092896D);
		
		//max health
		this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(40.0D);
		
		//damage
		this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(5.0D);
		
	}
	
	@Override
	protected void entityInit()
	{
		super.entityInit();
		//this.dataWatcher.addObject(18, this.getHealth());
		//beg   : this.dataWatcher.addObject(19, new Byte((byte)0));
		//Collar: this.dataWatcher.addObject(20, new Byte((byte)BlockCloth.getBlockFromDye(1)));
		
		this.dataWatcher.addObject(21, Byte.valueOf((byte)1));//Active Form
		this.dataWatcher.addObject(22, Byte.valueOf((byte)0));//form state
	}
	
	@Override
	protected void updateAITick()
	{
		//this.dataWatcher.updateObject(18, this.getHealth());
	}
	
	@Override
	public boolean isAIEnabled()
	{
		return true;
	}
	
	@Override
	protected boolean isValidLightLevel()
	{
		return true;
	}
	
	@Override
	public boolean getCanSpawnHere()
	{
		return this.posY > 60 && super.getCanSpawnHere();
	}
	
	public boolean getActiveForm()
	{
		return dataWatcher.getWatchableObjectByte(21)!= 0;
	}
	
	protected void setActiveForm(boolean state)
	{
		if (state)
			dataWatcher.updateObject(21, Byte.valueOf((byte) 1));
		else
			dataWatcher.updateObject(21, Byte.valueOf((byte) 0));
	}
	
	public byte getFormState()
	{
		return dataWatcher.getWatchableObjectByte(22);
	}
	
	protected void setFormState(int state)
	{
		dataWatcher.updateObject(22, Byte.valueOf((byte) state));
	}
	
	@Override
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		compound.setBoolean("Possum", !getActiveForm());
		compound.setByte("FormState", getFormState());
		compound.setByte("Dying", (byte)dying);
	}
	
	@Override
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);
		setActiveForm(!compound.getBoolean("Possum"));
		setFormState(compound.getByte("FormState"));
		dying = compound.getByte("Dying");
	}
	
	/*******************************************************/
	/******************** Behavior *************************/
	/*******************************************************/
	
	public float getBlockPathWeight(int par1, int par2, int par3)
	{
		return 0.0F;
	}
	
	@Override
	public void onLivingUpdate()
	{
		//effect of banish wears off a little
		if (dying>0)
		{
			dying--;
		}
		
		int formState = getFormState();
		
		//body rebuilds a bit
		if (!getActiveForm())
		{
			regenBuffer=(regenBuffer+1)%10;
			if (regenBuffer==0)
			{
				heal(1);
				if (this.getHealth()==this.getMaxHealth()/2)
				{
					//body reactivates
					setActiveForm(true);
				}
			}
			if (formState < formStateMax)
				formState++;
		}
		else if (formState > 0)
			formState--;
		
		setFormState(formState);
		
		super.onLivingUpdate();
		
		if (!this.worldObj.isRemote && this.isShaking && !this.field_70928_h && !this.hasPath() && this.onGround)
		{
			this.field_70928_h = true;
			this.timeWolfIsShaking = 0.0F;
			this.prevTimeWolfIsShaking = 0.0F;
			this.worldObj.setEntityState(this, (byte)8);
		}
	}
	
	@Override
	public void onUpdate()
	{
		super.onUpdate();
		this.oldInterest = this.newInterest;
		
		if (this.isBegging())
		{
			this.newInterest += (1.0F - this.newInterest) * 0.4F;
		}
		else
		{
			this.newInterest += (0.0F - this.newInterest) * 0.4F;
		}
		
		if (this.isBegging())
		{
			this.numTicksToChaseTarget = 10;
		}
		
		if (this.isWet())
		{
			this.isShaking = true;
			this.field_70928_h = false;
			this.timeWolfIsShaking = 0.0F;
			this.prevTimeWolfIsShaking = 0.0F;
		}
		else if ((this.isShaking || this.field_70928_h) && this.field_70928_h)
		{
			if (this.timeWolfIsShaking == 0.0F)
			{
				this.playSound("mob.wolf.shake", this.getSoundVolume(), (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
			}
			
			this.prevTimeWolfIsShaking = this.timeWolfIsShaking;
			this.timeWolfIsShaking += 0.05F;
			
			if (this.prevTimeWolfIsShaking >= 2.0F)
			{
				this.isShaking = false;
				this.field_70928_h = false;
				this.prevTimeWolfIsShaking = 0.0F;
				this.timeWolfIsShaking = 0.0F;
			}
			
			if (this.timeWolfIsShaking > 0.4F)
			{
				float f = (float)this.boundingBox.minY;
				int i = (int)(MathHelper.sin((this.timeWolfIsShaking - 0.4F) * (float)Math.PI) * 7.0F);
				
				for (int j = 0; j < i; ++j)
				{
					float f1 = (this.rand.nextFloat() * 2.0F - 1.0F) * this.width * 0.5F;
					float f2 = (this.rand.nextFloat() * 2.0F - 1.0F) * this.width * 0.5F;
					this.worldObj.spawnParticle("splash", this.posX + (double)f1, (double)(f + 0.8F), this.posZ + (double)f2, this.motionX, this.motionY, this.motionZ);
				}
			}
		}
	}
	
	public boolean canBreatheUnderwater()
	{
		return true;
	}
	
	/*******************************************************/
	/******************** Item Drop ************************/
	/*******************************************************/
	
	@Override
	protected Item getDropItem()
	{
		return Items.stick;
	}
	
	@Override
	protected void dropFewItems(boolean par1, int par2)
	{
		int j = this.rand.nextInt(3) + this.rand.nextInt(1 + par2);
		
		if (j<4)
		{
			for (int k = 0; k < j; ++k)
			{
				this.entityDropItem(new ItemStack(Items.stick, 1, 0), 0);
			}
		}
		else
		{
			this.entityDropItem(new ItemStack(Blocks.log, j-3, 0), 0);
		}
	}
	
	@Override
	protected void dropRareDrop(int par1)
	{
		switch (this.rand.nextInt(2))
		{
			case 0:
				this.entityDropItem(new ItemStack(LOE_Items.zapApple, 1, 1), 0);
				break;
			case 1:
				this.entityDropItem(new ItemStack(LOE_Blocks.zapAppleSapling, 1, 0), 0);
		}
	}
	
	@Override
	public void onKillEntity(EntityLivingBase par1EntityLiving)
	{
		if (par1EntityLiving instanceof EntityVillager)
			ItemBits.DropBits(5, par1EntityLiving);
		super.onKillEntity(par1EntityLiving);
	}
	
	/*******************************************************/
	/********************* States **************************/
	/*******************************************************/
	
	public boolean isBegging()//func_70922_bv()
	{
		return false;//this.dataWatcher.getWatchableObjectByte(19) == 1;
	}
	
	@SideOnly(Side.CLIENT)
	public float getTailRotation()
	{
		return this.isAngry() ? 1.5393804F : ((float)Math.PI / 5F);
	}
	
	public boolean isAngry()
	{
		return true;//(this.dataWatcher.getWatchableObjectByte(16) & 2) != 0;
	}
	
	public boolean isSitting()
	{
		return false;//(this.dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}
	
	@SideOnly(Side.CLIENT)
	public boolean getWolfShaking()
	{
		return this.isShaking;
	}
	
	/*******************************************************/
	/***************** Render checks ***********************/
	/*******************************************************/
	
	@SideOnly(Side.CLIENT)
	public float getInterestedAngle(float par1)
	{
		return (this.oldInterest + (this.newInterest - this.oldInterest) * par1) * 0.15F * (float)Math.PI;
	}
	
	@SideOnly(Side.CLIENT)
	/**
	 * Used when calculating the amount of shading to apply while the wolf is shaking.
	 */
	public float getShadingWhileShaking(float par1)
	{
		return 0.75F + (this.prevTimeWolfIsShaking + (this.timeWolfIsShaking - this.prevTimeWolfIsShaking) * par1) / 2.0F * 0.25F;
	}
	
	@SideOnly(Side.CLIENT)
	public float getShakeAngle(float par1, float par2)
	{
		float f2 = (this.prevTimeWolfIsShaking + (this.timeWolfIsShaking - this.prevTimeWolfIsShaking) * par1 + par2) / 1.8F;
		
		if (f2 < 0.0F)
		{
			f2 = 0.0F;
		}
		else if (f2 > 1.0F)
		{
			f2 = 1.0F;
		}
		
		return MathHelper.sin(f2 * (float)Math.PI) * MathHelper.sin(f2 * (float)Math.PI * 11.0F) * 0.15F * (float)Math.PI;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void handleHealthUpdate(byte par1)
	{
		if (par1 == 8)
		{
			this.field_70928_h = true;
			this.timeWolfIsShaking = 0.0F;
			this.prevTimeWolfIsShaking = 0.0F;
		}
		else
		{
			super.handleHealthUpdate(par1);
		}
	}
	
	/*******************************************************/
	/********************** Sound **************************/
	/*******************************************************/
	
	/**playStepSound**/
	@Override
	protected void func_145780_a(int posX, int posY, int posZ, Block block)
	{
		super.func_145780_a(posX, posY, posZ, block);
		Block.SoundType soundtype = Block.soundTypeWood;
		this.playSound(soundtype.getStepResourcePath(), soundtype.getVolume() * 0.15F, soundtype.getPitch());
	}
	
	@Override
	protected String getHurtSound()
	{
		return "mob.wolf.hurt";
	}
	
	@Override
	protected String getDeathSound()
	{
		return "mob.wolf.death";
	}
	
	@Override
	protected float getSoundVolume()
	{
		return 0.4F;
	}
	
	/*******************************************************/
	/***************** Death & Injury **********************/
	/*******************************************************/
	
	@Override
	public void onDeath(DamageSource damageSource)
	{
		if (dying>0)
		{
			super.onDeath(damageSource);
		}
		else
		{
			//false death
			setActiveForm(false);
			isJumping = false;
			setHealth(1);
		}
	}
	
	@Override
	public boolean attackEntityFrom(DamageSource damageSource, float par2)
	{
		if (damageSource == DamageSource.cactus)
			return false;
		
		Entity sourceEntity = damageSource.getSourceOfDamage();
		if (sourceEntity!= null)
		{
			ItemStack tool = null;
			
			if (sourceEntity instanceof EntityLivingBase)
			{
				if (sourceEntity instanceof EntityPlayer)
				{
					EntityPlayer attackingPlayer = (EntityPlayer)sourceEntity;
					tool = attackingPlayer.getHeldItem();
				}
			}
			
			if (tool != null)
			{
				  /****************************************/
				 /**Handle the effects of each type here**/
				/****************************************/
				int banishLevel = EnchantmentHelper.getEnchantmentLevel(LOE_Enchantment.enchantBanish.effectId, tool);
				if (dying < banishLevel*20 && getActiveForm())
				{
					dying = banishLevel*20;
				}
			}
		}
		return super.attackEntityFrom(damageSource, par2);
	}
	
	public boolean isPlayingDead()
	{
		return !getActiveForm();
	}
}