package loecraftpack.content.entities;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.entities.render.ModelTapestry.Shape;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityItemTapestry extends EntityItemHangingCustom {
	
	public static List<Shape> shape = new ArrayList<Shape>();
	static
	{
		shape.add(Shape.NORMAL);//celly
		shape.add(Shape.NORMAL);//luna
		shape.add(Shape.NORMAL);//twi
		shape.add(Shape.NORMAL);//rare
		shape.add(Shape.NORMAL);//AJ
		shape.add(Shape.NORMAL);//pie
		shape.add(Shape.NORMAL);//dash
		shape.add(Shape.NORMAL);//shy
		shape.add(Shape.GRAND);//faust
	}
	
	public EntityItemTapestry(World world)
	{
		super(world);
	}
	
	public EntityItemTapestry(World world, int xPos, int yPos, int zPos, int direction)
	{
		super(world, xPos, yPos, zPos, direction);
	}
	
	@Override
	protected void entityInit()
	{
		this.getDataWatcher().addObject(3, Byte.valueOf((byte)0));
	}
	
	@Override
	public int getWidthPixels()
	{
		switch(this.getTapestryShape())
		{
		case NORMAL:
			return 14;
		case DOUBLE:
			return 30;
		case GRAND:
			return 62;
		}
		return 16;
	}

	@Override
	public int getHeightPixels()
	{
		switch(this.getTapestryShape())
		{
		case NORMAL:
			return 62;
		case DOUBLE:
			return 62;
		case GRAND:
			return 94;
		}
		return 16;
	}
	
	@Override
	public float getCenterOffsetX()
	{
		return 0;
	}
	
	@Override
	public float getCenterOffsetY()
	{
		switch(this.getTapestryShape())
		{
		case NORMAL:
			return -1.5f;
		case DOUBLE:
			return -1.5f;
		case GRAND:
			return -2.5f;
		}
		return 0.0f;
	}
	/*
	@Override
	public int getWidthPixelsHanging()
	{
		switch(this.getTapestryShape())
		{
		case NORMAL:
			return 14;
		case DOUBLE:
			return 30;
		case GRAND:
			return 62;
		}
		return 16;
	}*/

	@Override
	public int getHeightPixelsHanging()
	{
		return 14;
	}
	/*
	@Override
	public float getCenterOffsetXHanging()
	{
		return 0.0f;
	}*/
	
	@Override
	public float getCenterOffsetYHanging()
	{
		return 0.0f;
	}
	
	@Override
	public void onBroken(Entity entity)
	{
		if (entity instanceof EntityPlayer)
		{
			EntityPlayer entityplayer = (EntityPlayer)entity;

			if (entityplayer.capabilities.isCreativeMode)
			{
				return;
			}
		}
		this.entityDropItem(new ItemStack(LOE_Items.tapestry, 1, getTapestryType()), 0.0F);
	}
	
	public void setTapestryType(int type)
	{
		Shape oldShape = getTapestryShape();
		this.getDataWatcher().updateObject(3, Byte.valueOf((byte)type));
		
		//this step updates the boundingBox and pos, if needed
		if (oldShape != getTapestryShape())
			setDirection(this.hangingDirection);
	}
	
	public int getTapestryType()
	{
		return (int)this.getDataWatcher().getWatchableObjectByte(3);
	}
	
	public Shape getTapestryShape()
	{
		int type = getTapestryType();
		if (type<0 || type>=shape.size())
			return Shape.NORMAL;
		else
			return shape.get(type);
	}
	
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		compound.setByte("tapestryType", (byte)getTapestryType());
	}
	
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);
		setTapestryType(compound.getByte("tapestryType"));
	}
	
	public ItemStack getPickedResult(MovingObjectPosition target)
	{
		return new ItemStack(LOE_Items.tapestry, 1, getTapestryType());
	}

}
