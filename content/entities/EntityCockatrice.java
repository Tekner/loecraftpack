package loecraftpack.content.entities;

import loecraftpack.LoECraftPack;
import loecraftpack.content.entities.ai.EntityAIArtiliary;
import loecraftpack.content.registery.LOE_Items;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class EntityCockatrice extends EntityMob implements IArtiliary
{
	protected EntityAIAttackOnCollide aiAttackOnCollide = new EntityAIAttackOnCollide(this, EntityPlayer.class, 1.2D, false);
	protected EntityAITaskEntry aiAttackOnCollideHolder;
	protected float flap = 0.0f;
	public final float flapSpeed = 0.22f;
	
	public EntityCockatrice(World world)
	{
		super(world);
		this.setSize(0.7f, 1.0f);
		this.tasks.addTask(1, new EntityAISwimming(this));
		aiAttackOnCollideHolder = tasks.new EntityAITaskEntry(2, aiAttackOnCollide);
		this.tasks.addTask(3, new EntityAIArtiliary(this, 10.0F, 1.5D, 1.0D, 5.0f, 15.0F, 30, 50, EntityPlayer.class));
		this.tasks.addTask(4, new EntityAIWander(this, 0.8D));
		this.tasks.addTask(5, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(5, new EntityAILookIdle(this));
		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
		this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
	}
	
	public boolean isAIEnabled()
	{
		return true;
	}

	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(8.0D);
		this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.30D);
	}
	
	@Override
	public void onLivingUpdate()
	{
		super.onLivingUpdate();
		
		if (!this.onGround)
		{
			if (this.motionY < 0.0D)
			{
				this.motionY *= 0.7D;
			}
			
			flap += flapSpeed;
			
			if (flap >= 1.0f)
				flap -= 1.0f;
		}
		else if (flap != 0.0f)
		{
			if (flap >= 1.0f - flapSpeed)
				flap = 0.0f;
			else
				flap += flapSpeed;
		}
	}
	
	@Override
	protected void fall(float par1) {}
	
	@Override
	protected String getLivingSound()
	{
		return "mob.chicken.say";
	}
	
	@Override
	protected String getHurtSound()
	{
		return "mob.chicken.hurt";
	}
	
	@Override
	protected String getDeathSound()
	{
		return "mob.chicken.hurt";
	}
	
	/**playStepSound**/
	@Override
	protected void func_145780_a(int posX, int posY, int posZ, Block block)
	{
		this.playSound("mob.chicken.step", 0.15F, 1.0F);
	}
	
	public float getFlap()
	{
		return flap;
	}
	
	@Override
	protected void dropFewItems(boolean par1, int par2)
	{
		int k = this.rand.nextFloat() < 0.3F? 1: 0;
		
		if (par2 > 0)
		{
			k += this.rand.nextInt(par2 + 1);
		}

		for (int l = 0; l < k; ++l)
		{
			this.dropItem(Item.getItemFromBlock(Blocks.stone), 1);
		}
	}
	
	public EntityItem dropItemWithOffset(int par1, int par2, float par3)
	{
		return this.entityDropItem(new ItemStack(LOE_Items.ingredients, par2, 10/*Cockatrice Eye*/), par3);
	}
	
	@Override
	protected void updateAITick()
	{
		updateCombatStratagy();
	}
	
	public void updateCombatStratagy()
	{
		EntityLivingBase target = getAttackTarget();
		if (target != null)
		{
			PotionEffect status = target.getActivePotionEffect(LoECraftPack.potionPetrified);
			if (status != null && status.getAmplifier() > 0)
			{
				// move in for kill
				if (!tasks.taskEntries.contains(aiAttackOnCollideHolder))
				{
					tasks.taskEntries.add(1, aiAttackOnCollideHolder);
				}
			}
			else
			{
				// attack form distance
				tasks.removeTask(aiAttackOnCollide);
			}
		}
	}
	
	@Override
	public void attackEntityWithRangedAttack(EntityLivingBase target, float rangePercent)
	{
		if (!(target instanceof EntityPlayer) || !((EntityPlayer)target).capabilities.isCreativeMode)
		{
			PotionEffect status = target.getActivePotionEffect(LoECraftPack.potionPetrified);
			
			if (status != null)
			{
				if (status.getAmplifier() > 0? true: status.getDuration()+100 >= 800)
				{
					target.addPotionEffect(new PotionEffect(LoECraftPack.potionPetrified.id, status.getDuration()+100, 1));
				}
				else
				{
					target.addPotionEffect(new PotionEffect(LoECraftPack.potionPetrified.id, status.getDuration()+100, 0));
				}
			}
			else
				target.addPotionEffect(new PotionEffect(LoECraftPack.potionPetrified.id, 100, 0));
		}
	}

}
