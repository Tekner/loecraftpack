package loecraftpack.content.entities;

import loecraftpack.content.registery.LOE_Blocks;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityHarmonyTree extends Entity {

	public EntityHarmonyTree(World world)
	{
		super(world);
	}
	
	public EntityHarmonyTree(World world, double xPos, double yPos, double zPos)
	{
		this(world);
		this.setPosition(xPos, yPos+0.5f, zPos);
	}
	
	@Override
	protected void entityInit()
	{
		
	}
	
	@Override
	public void onUpdate()
	{
		this.setDead();
		super.onUpdate();
	}
	
	@Override
	public void setDead()
	{
		int x = (int)posX;
		int y = (int)posY;
		int z = (int)posZ;
		
		if (posX <0) x--;
		if (posZ <0) z--;
		
		if (!isValidTree(x, y, z))
		{
			super.setDead();
		}
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound nbttagcompound)
	{
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound nbttagcompound)
	{
	}
	
	protected boolean isValidTree(int x, int y, int z)
	{
		return Block.isEqualTo(worldObj.getBlock(x,   y, z), LOE_Blocks.harmonyLog) &&
			   Block.isEqualTo(worldObj.getBlock(x, 1+y, z), LOE_Blocks.harmonyLog) &&
			   Block.isEqualTo(worldObj.getBlock(x, 2+y, z), LOE_Blocks.harmonyLog) &&
			   Block.isEqualTo(worldObj.getBlock(x, 3+y, z), LOE_Blocks.harmonyLog);
	}

}
