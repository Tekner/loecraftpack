package loecraftpack.content.entities.ai;

import java.util.List;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class EntityAIArtiliarySelector implements IEntitySelector
{
	protected EntityAIArtiliary ai;
	protected List<Class> targetEntityClasses;
	
	public EntityAIArtiliarySelector (EntityAIArtiliary ai, List<Class> targetEntityClasses)
	{
		this.ai = ai;
		this.targetEntityClasses = targetEntityClasses;
	}
	
	@Override
	public boolean isEntityApplicable(Entity entity)
	{
		if (entity.isEntityAlive())
		{
			//ignore creative mode players
			if (entity instanceof EntityPlayer && ((EntityPlayer)entity).capabilities.isCreativeMode)
				return false;
			
			// include current threat with a little paranoia
			if (ai.closestThreateningEntity == entity)
			{
				if ( !(entity instanceof EntityLivingBase) || (((EntityLivingBase)entity).getRNG().nextFloat() < 0.3f) )
				{
					return true;
				}
				else
					return ai.parent.getEntitySenses().canSee(entity);
			}
			
			//include current target, if visible
			if (ai.parent.getAttackTarget() == entity)
				return ai.parent.getEntitySenses().canSee(entity);
			
			//include anything that uses one of the listed classes, if visible
			for (Class targetClass: targetEntityClasses)
			{
				if (targetClass.isInstance(entity))
				{
					return ai.parent.getEntitySenses().canSee(entity);
				}
			}
		}
		return false;
	}
}
