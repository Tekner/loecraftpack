package loecraftpack.content.entities.ai;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIBase;

public class EntityAIEntitySelector implements IEntitySelector
{
	public final EntityAIBase parent;
	
	EntityAIEntitySelector(EntityAIBase parent)
	{
		this.parent = parent;
	}
	
	public boolean isEntityApplicable(Entity par1Entity)
	{
		return par1Entity.isEntityAlive();
	}
}
