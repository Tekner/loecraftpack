package loecraftpack.content.entities.ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import loecraftpack.content.entities.IArtiliary;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;

public class EntityAIArtiliary extends EntityAIBase
{
	/**filter used for finding entities to avoid*/
	public final IEntitySelector selector;
	/**the entity that uses this ai*/
	public final EntityCreature parent;
	/**the entity that uses this ai*/
	public final IArtiliary parentRanged;
	/**Closest Threat*/
	protected Entity closestThreateningEntity;
	/**Current Target*/
	protected EntityLivingBase attackTarget;
	/**Range to keep between*/
	protected float safetyRange;
	/**Point-Blank attack range*/
	protected float minRange;
	/**Max range to attack from*/
	protected float maxRange;
	protected float maxRangeSq;
	/**speed mod to flee*/
	protected double nearSpeed;
	/**speed mod to flee while to close*/
	protected double farSpeed;
	/**speed to fire at close range*/
	protected int minAttackTime;
	/**speed to fire at far range*/
	protected int maxAttackTime;
	/** The PathEntity for our entity */
	protected PathEntity newPathEntity;
	/** The PathNavigate of our entity */
	protected PathNavigate pathNavigate;
	/** timer is used to reduce excessive calls to, searching for paths*/
	protected int fearTimer;
	/** the time remaining, to recover from attacking*/
	protected int recoverTimer;
	/** the time spent, while in line of sight*/
	protected int sightTimer;
	/** used to flag that it's the first tick of this AI's run*/
	protected boolean boot;
	/** the list path is valid*/
	protected boolean pathIsValid;
	
	public EntityAIArtiliary(IArtiliary parent, float safetyRange, double nearSpeed, double farSpeed, float minRange, float maxRange, int minAttackTime, int maxAttackTime, Class... ClassesToAvoid)
	{
		this.parent = (EntityCreature) parent;
		this.parentRanged = parent;
		pathNavigate = this.parent.getNavigator();
		this.safetyRange = safetyRange;
		this.nearSpeed = nearSpeed;
		this.farSpeed = farSpeed;
		this.minRange = minRange;
		this.maxRange = maxRange;
		this.maxRangeSq = maxRange*maxRange;
		this.minAttackTime = minAttackTime;
		this.maxAttackTime = maxAttackTime;
		selector = new EntityAIArtiliarySelector(this, new ArrayList<Class>(Arrays.asList(ClassesToAvoid)));
		setMutexBits(3);
	}
	
	@Override
	public boolean shouldExecute()
	{
		boot = true;
		
		findNearestThreat();
		attackTarget = parent.getAttackTarget();
		
		if (attackTarget != null || closestThreateningEntity != null)
			return true;
		else
		{
			//reset fear timer to 0 because AI is not going to start
			fearTimer = 0;
			return false;
		}
	}
	
	@Override
	public boolean continueExecuting()
	{
		boot = false;
		
		attackTarget = parent.getAttackTarget();
		
		return attackTarget != null || closestThreateningEntity != null || !pathNavigate.noPath();
	}
	
	@Override
	public void resetTask()
	{
		newPathEntity = null;
		closestThreateningEntity = null;
		attackTarget = null;
		recoverTimer = -1;
		sightTimer = 0;
		fearTimer = 0;
	}
	
	@Override
	public void updateTask()
	{
		if (boot)
		{
			if (closestThreateningEntity != null && findEvasionPath())
			{
				pathNavigate.setPath(newPathEntity, farSpeed);
				fearTimer = 2;
			}
		}
		else if (findThreateningEnemyAndPath())
			pathNavigate.setPath(newPathEntity, farSpeed);
		
		if (!pathNavigate.noPath())
		{
			if (closestThreateningEntity != null && parent.getDistanceSqToEntity(closestThreateningEntity) < 49.0D)
			{
				pathNavigate.setSpeed(this.nearSpeed);
			}
			else
			{
				pathNavigate.setSpeed(this.farSpeed);
			}
		}
		
		if (attackTarget != null)
		{
			double distance = parent.getDistanceSq(attackTarget.posX, attackTarget.boundingBox.minY, attackTarget.posZ);
			boolean visible = parent.getEntitySenses().canSee(attackTarget);
			
			if (visible)
				sightTimer++;
			else
				sightTimer = 0;
			
			if(closestThreateningEntity == null)
			{
				if (distance <= (double)maxRangeSq && sightTimer >= 10)
					pathNavigate.clearPathEntity();
				else
					pathNavigate.tryMoveToEntityLiving(attackTarget, farSpeed);
			}
			
			parent.getLookHelper().setLookPositionWithEntity(attackTarget, 30.0F, 30.0F);
			
			float rangePercent;
			if (--recoverTimer == 0)
			{
				if (distance <= maxRangeSq && visible)
				{
					rangePercent = MathHelper.sqrt_double(distance) / maxRange;
					
					float rangePercent2 = rangePercent;
					if (rangePercent < 0.1f)
						rangePercent2 = 0.1f;
					else if (rangePercent > 1.0f)
						rangePercent2 = 1.0f;
					
					parentRanged.attackEntityWithRangedAttack(this.attackTarget, rangePercent2);
					recoverTimer = MathHelper.floor_float(rangePercent * (float)(maxAttackTime - minAttackTime) + (float)minAttackTime);
				}
			}
			else if (recoverTimer < 0)
			{
				rangePercent = MathHelper.sqrt_double(distance) / maxRange;
				recoverTimer = MathHelper.floor_float(rangePercent * (float)(maxAttackTime - minAttackTime) + (float)minAttackTime);
			}
		}
	}
	
	/**returns true when new evasion path is found*/
	protected boolean findThreateningEnemyAndPath()
	{
		if (fearTimer > 0)
		{
			fearTimer--;
		}
		else
		{
			fearTimer = 2;
			
			findNearestThreat();
			
			if (closestThreateningEntity != null)
				if (pathNavigate.noPath() || !pathIsValid)
				{
					return findEvasionPath();
				}
		}
		
		return false;
	}
	
	protected void findNearestThreat()
	{
		Entity closestLivingEntity = null;
		
		List list = parent.worldObj.selectEntitiesWithinAABB(Entity.class, parent.boundingBox.expand((double)safetyRange, 4.0D, (double)safetyRange), selector);
		
		if (!list.isEmpty())
		{
			closestLivingEntity = (Entity)list.get(0);
		}
		
		if (closestLivingEntity != closestThreateningEntity)
		{
			closestThreateningEntity = closestLivingEntity;
			pathIsValid = false;
		}
	}
	
	/**Used to locate a new Escape Path.  Returns true when new Escape Path is found.   Also resets fearTimer to 0 if it can't find a path*/
	protected boolean findEvasionPath()
	{
		
		if (closestThreateningEntity != null)
		{
			Vec3 vec3 = RandomPositionGenerator.findRandomTargetBlockAwayFrom(parent, 16, 7, parent.worldObj.getWorldVec3Pool().getVecFromPool(closestThreateningEntity.posX, closestThreateningEntity.posY, closestThreateningEntity.posZ));
			
			if (vec3 != null && closestThreateningEntity.getDistanceSq(vec3.xCoord, vec3.yCoord, vec3.zCoord) >= closestThreateningEntity.getDistanceSqToEntity(parent))
			{
				newPathEntity = pathNavigate.getPathToXYZ(vec3.xCoord, vec3.yCoord, vec3.zCoord);
				if (newPathEntity != null && newPathEntity.isDestinationSame(vec3))
				{
					pathIsValid = true;
					return true;
				}
			}
		}
		pathIsValid = false;
		fearTimer = 0;
		return false;
	}
}
