package loecraftpack.content.entities.ai;

import java.util.List;

import loecraftpack.content.entities.EntityTimberWolf;

//import loecraftpack.content.entities.EntityTimberWolf;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.Vec3;


//modified vanilla code
public class EntityAIAvoidEntityLocation extends EntityAIBase
{
	public final IEntitySelector selector = new EntityAIEntitySelector(this);
	
	/** The entity we are attached to */
	protected EntityCreature parentEntity;
	protected double farSpeed;
	protected double nearSpeed;
	protected Entity closestLivingEntity;
	protected float distanceFromEntity;

	/** The PathEntity of our entity */
	protected PathEntity entityPathEntity;

	/** The PathNavigate of our entity */
	protected PathNavigate entityPathNavigate;

	/** The class of the entity we should avoid */
	public Class targetEntityClass;
	
	public EntityAIAvoidEntityLocation(EntityCreature entity, Class par2Class, float par3, double par4, double par6)
	{
		this.parentEntity = entity;
		this.targetEntityClass = par2Class;
		this.distanceFromEntity = par3;
		this.farSpeed = par4;
		this.nearSpeed = par6;
		this.entityPathNavigate = entity.getNavigator();
		this.setMutexBits(1);
	}
	
	@Override
	public boolean shouldExecute()
	{
		List list = parentEntity.worldObj.selectEntitiesWithinAABB(targetEntityClass, parentEntity.boundingBox.expand((double)distanceFromEntity, 3.0D, (double)distanceFromEntity), selector);
		
		if (list.isEmpty())
		{
			return false;
		}
		
		closestLivingEntity = (Entity)list.get(0);
		
		Vec3 vec3 = RandomPositionGenerator.findRandomTargetBlockAwayFrom(parentEntity, 16, 7, parentEntity.worldObj.getWorldVec3Pool().getVecFromPool(closestLivingEntity.posX, closestLivingEntity.posY, closestLivingEntity.posZ));
		
		if (vec3 == null)
		{
			return false;
		}
		else if (closestLivingEntity.getDistanceSq(vec3.xCoord, vec3.yCoord, vec3.zCoord) < closestLivingEntity.getDistanceSqToEntity(parentEntity))
		{
			return false;
		}
		else
		{
			entityPathEntity = entityPathNavigate.getPathToXYZ(vec3.xCoord, vec3.yCoord, vec3.zCoord);
			if (entityPathEntity == null)
				return false;
			else
			{
				if (entityPathEntity.isDestinationSame(vec3))
					return true;
				else if (parentEntity instanceof EntityTimberWolf)
				{
					return true;
				}
				return false;
			}
		}
	}
	
	public boolean continueExecuting()
	{
		return !entityPathNavigate.noPath();
	}
	
	public void startExecuting()
	{
		entityPathNavigate.setPath(entityPathEntity, farSpeed);
	}
	
	public void resetTask()
	{
		closestLivingEntity = null;
	}
	
	public void updateTask()
	{
		if (parentEntity.getDistanceSqToEntity(closestLivingEntity) < 49.0D)
		{
			parentEntity.getNavigator().setSpeed(nearSpeed);
		}
		else
		{
			parentEntity.getNavigator().setSpeed(farSpeed);
		}
	}

	public EntityCreature getEntity()
	{
		return parentEntity;
	}
}
