package loecraftpack.content.entities.ai;

import loecraftpack.content.entities.IPossum;
import net.minecraft.entity.ai.EntityAIBase;


/**
 * this task never ends, and can only be stopped by removing it from the active task list, or by giving it a possum instance
 */
public class EntityAIPlayDead extends EntityAIBase
{
	IPossum parent;
	
	public EntityAIPlayDead(IPossum parent)
	{
		this();
		this.parent = parent;
	}
	
	public EntityAIPlayDead()
	{
		this.setMutexBits(3);
	}
	
	@Override
	public boolean shouldExecute()
	{
		return parent == null? true: parent.isPlayingDead();
	}
	
	public void updateTask() 
	{
		
	}

}
