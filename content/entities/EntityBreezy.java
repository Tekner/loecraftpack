package loecraftpack.content.entities;

import java.util.Calendar;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityBreezy extends EntityFlyingCreature implements IAnimals
{
	public EntityBreezy(World world)
	{
		super(world);
		setSize(0.375F, 0.375F);
		
		detachHome();
		
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(1, new EntityAIWander(this, 0.3D));
		tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
		tasks.addTask(2, new EntityAILookIdle(this));
	}
	
	//TODO: adjust this
	@Override
	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(6.0D);
	}
	
	//TODO: adjust this
	@Override
	protected float getSoundVolume()
	{
		return 0.1F;
	}
	
	//TODO: adjust this
	@Override
	protected float getSoundPitch()
	{
		return super.getSoundPitch() * 0.95F;
	}
	
	//TODO: adjust this
	@Override
	protected String getLivingSound()
	{
		return getIsStanding() && rand.nextInt(4) != 0 ? null : "mob.bat.idle";
	}
	
	//TODO: adjust this
	@Override
	protected String getHurtSound()
	{
		return "mob.bat.hurt";
	}
	
	//TODO: adjust this
	@Override
	protected String getDeathSound()
	{
		return "mob.bat.death";
	}
	
	@Override
	protected boolean interact(EntityPlayer par1EntityPlayer)
	{
		return false;
	}
	
	@Override
	public boolean allowLeashing()
	{
		return false;
	}
	
	@Override
	public boolean canBePushed()
	{
		return false;
	}
	
	@Override
	protected void collideWithEntity(Entity par1Entity) {}
	
	@Override
	protected void collideWithNearbyEntities() {}
	
	@Override
	protected boolean canTriggerWalking()
	{
		return false;
	}
	
	@Override
	public boolean doesEntityNotTriggerPressurePlate()
	{
		return true;
	}
	
	protected boolean updateToFly()
	{
		if (!worldObj.isBlockNormalCubeDefault(MathHelper.floor_double(posX), (int)posY - 1, MathHelper.floor_double(posZ), false) ||
				(rand.nextInt(1200) == 0 && !isWet()))
		{
			worldObj.playAuxSFXAtEntity((EntityPlayer)null, 1015, (int)posX, (int)posY, (int)posZ, 0);
			return true;
		}
		else
			return false;
	}
	
	protected boolean updateToStand()
	{
		return (rand.nextInt(100) == 0 || isWet()) && worldObj.isBlockNormalCubeDefault(MathHelper.floor_double(posX), (int)posY - 1, MathHelper.floor_double(posZ), false);
	}
	
	@Override
	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
	{
		if (isEntityInvulnerable())
		{
			return false;
		}
		else
		{
			if (!worldObj.isRemote && getIsStanding())
			{
				setIsStanding(false);
			}
			
			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}
	
	public float getBlockPathWeight(int par1, int par2, int par3)
	{
		Block blockUnder = worldObj.getBlock(par1, par2-1, par3);
		if (blockUnder != null && blockUnder.isFertile(worldObj, par1, par2-1, par3))
			return 15.0F;
		
		if (getIsStanding())
		{
			if (blockUnder != null && Block.isEqualTo(blockUnder, Blocks.grass))
				return 10.0F;
			else
			{
				float light = worldObj.getLightBrightness(par1, par2, par3);
				if (light < 13.0F)
				{
					if (blockUnder != null && blockUnder.isNormalCube())
						return 8.0F;
					else
						return blockUnder == null || blockUnder.isAir(worldObj, par1, par2-1, par3)? -99999.0F: 7.0F;
				}
				else
					return 0.0f; 
			}
		}
		else
		{
			return Math.min(7.0F ,15.0f - worldObj.getLightBrightness(par1, par2, par3));
		}
	}
	
	@Override
	public boolean getCanSpawnHere()
	{
		int i = MathHelper.floor_double(boundingBox.minY);

		if (i >= 63)
		{
			return false;
		}
		else
		{
			int posX = MathHelper.floor_double(this.posX);
			int posY = MathHelper.floor_double(this.posY);
			int posZ = MathHelper.floor_double(this.posZ);
			
			if (posY >= worldObj.getHeightValue(posX, posZ)+20)
				return false;
			
			int light = worldObj.getBlockLightValue(posX, posY, posZ);
			
			if (light <= 3)
				return false;
			
			Calendar calendar = worldObj.getCurrentDate();

			if (calendar.get(2) + 1 != 4)
			{
				if (rand.nextBoolean())
				{
					return false;
				}
			}

			return super.getCanSpawnHere();
		}
	}
}
