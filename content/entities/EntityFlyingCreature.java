package loecraftpack.content.entities;

import net.minecraft.entity.EntityCreature;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityFlyingCreature extends EntityCreature
{
	public ChunkCoordinates currentFlightTarget;
	
	public EntityFlyingCreature(World par1World)
	{
		super(par1World);
		setIsStanding(true);
	}
	
	@Override
	protected void entityInit()
	{
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte)0));
	}
	
	public boolean getIsStanding()
	{
		return (dataWatcher.getWatchableObjectByte(16) & 1) != 0;
	}
	
	public void setIsStanding(boolean par1)
	{
		byte b0 = dataWatcher.getWatchableObjectByte(16);

		if (par1)
		{
			dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 | 1)));
		}
		else
		{
			dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 & -2)));
		}
	}
	
	@Override
	protected void fall(float par1) {}
	
	@Override
	protected void updateFallState(double par1, boolean par3) {}
	
	@Override
	protected boolean isAIEnabled()
	{
		return true;
	}
	
	@Override
	public void onUpdate()
	{
		super.onUpdate();
		
		if (!getIsStanding())
		{
			motionY *= 0.6000000238418579D;
		}
	}
	
	//TODO: modified vanilla code, rewrite this
	@Override
	protected void updateAITasks()
	{
		if (getIsStanding())
		{
			super.updateAITasks();
			
			if (updateToFly())
			{
				setIsStanding(false);
			}
		}
		else
		{
			++entityAge;
			worldObj.theProfiler.startSection("checkDespawn");
			despawnEntity();
			worldObj.theProfiler.endSection();
			worldObj.theProfiler.startSection("sensing");
			getEntitySenses().clearSensingCache();
			worldObj.theProfiler.endSection();
			worldObj.theProfiler.startSection("targetSelector");
			targetTasks.onUpdateTasks();
			worldObj.theProfiler.endSection();
			worldObj.theProfiler.startSection("mob tick");
			updateAITick();
			worldObj.theProfiler.endSection();
			
			fly();
			
			if (updateToStand())
			{
				setIsStanding(true);
			}
		}
	}
	
	//TODO: modified vanilla, rewrite
	protected void fly()
	{
		//clear path if invalid
		if (currentFlightTarget != null && (!worldObj.isAirBlock(currentFlightTarget.posX, currentFlightTarget.posY, currentFlightTarget.posZ) || currentFlightTarget.posY < 1))
		{
			currentFlightTarget = null;
		}
		
		//establish path if none present, you're close to the destination, or roughly every 1.5 secs.  ----  used to keep flight path fairly random
		if (currentFlightTarget == null || rand.nextInt(30) == 0 || currentFlightTarget.getDistanceSquared((int)posX, (int)posY, (int)posZ) < 4.0F)
		{
			currentFlightTarget = new ChunkCoordinates((int)posX + rand.nextInt(7) - rand.nextInt(7), (int)posY + rand.nextInt(6) - 2, (int)posZ + rand.nextInt(7) - rand.nextInt(7));
		}
		
		//apply hover effects & additional drag
		double disX = (double)currentFlightTarget.posX + 0.5D - posX;
		double disY = (double)currentFlightTarget.posY + 0.1D - posY;
		double disZ = (double)currentFlightTarget.posZ + 0.5D - posZ;
		motionX += (Math.signum(disX) * 0.5D - motionX) * 0.10000000149011612D;
		motionY += (Math.signum(disY) * 0.699999988079071D - motionY) * 0.10000000149011612D;
		motionZ += (Math.signum(disZ) * 0.5D - motionZ) * 0.10000000149011612D;
		
		//set forward direction and thrust 100%
		float heading = (float)(Math.atan2(motionZ, motionX) * 180.0D / Math.PI) - 90.0F;
		float headingChange = MathHelper.wrapAngleTo180_float(heading - rotationYaw);
		moveForward = 0.5F;
		rotationYaw += headingChange;
	}
	
	protected boolean updateToFly()
	{
		return false;
	}
	
	protected boolean updateToStand()
	{
		return false;
	}
	
	@Override
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.readEntityFromNBT(par1NBTTagCompound);
		dataWatcher.updateObject(16, Byte.valueOf(par1NBTTagCompound.getByte("FlyingFlags")));
	}
	
	@Override
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setByte("FlyingFlags", dataWatcher.getWatchableObjectByte(16));
	}

}
