package loecraftpack.content.entities.render;

import loecraftpack.content.blocks.render.ModelPotion;
import loecraftpack.content.entities.EntityThrowableItem;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderThrowableItem extends Render {
	
	protected static final ResourceLocation RES_POTION = new ResourceLocation("loecraftpack:entities/potion.png");
	
	protected ModelPotion modelPotion = new ModelPotion(Items.potionitem.getColorFromDamage(0));
	
	public void doRenderThrowableItem(EntityThrowableItem entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		
		bindEntityTexture(entity);
		if (entity.refferance == null)
			entity.refferance = new ItemStack(Item.getItemById(entity.getItemID()), 1, entity.getMetaID());
		//Do: if potion
		modelPotion.color = entity.refferance.getItem().getColorFromItemStack(entity.refferance, -1);
		
		modelPotion.renderAll();
		
		GL11.glPopMatrix();
	}

	@Override
	public void doRender(Entity entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		this.doRenderThrowableItem((EntityThrowableItem)entity, relativeX, relativeY, relativeZ, f, f1);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		//Do: check if not a potion
		return RES_POTION;
	}

}
