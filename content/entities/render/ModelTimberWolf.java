package loecraftpack.content.entities.render;

import loecraftpack.content.entities.EntityTimberWolf;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelTimberWolf extends ModelBase
{
	/** main box for the wolf head */
	public ModelRenderer wolfHeadMain;
	
	/** The wolf's body */
	public ModelRenderer wolfBody;
	
	/** Wolf'se first leg */
	public ModelRenderer wolfLeg1;
	
	/** Wolf's second leg */
	public ModelRenderer wolfLeg2;
	
	/** Wolf's third leg */
	public ModelRenderer wolfLeg3;
	
	/** Wolf's fourth leg */
	public ModelRenderer wolfLeg4;
	
	/** The wolf's tail */
	ModelRenderer wolfTail;
	
	/** The wolf's mane */
	ModelRenderer wolfMane;
	
	protected float formState = 0;
	
	public ModelTimberWolf()
	{
		float f = 0.0F;
		float f1 = 13.5F;
		this.wolfHeadMain = new ModelRenderer(this, 0, 0);
		this.wolfHeadMain.addBox(-3.0F, -3.0F, -2.0F, 6, 6, 4, f);
		this.wolfHeadMain.setRotationPoint(-1.0F, f1, -7.0F);
		this.wolfBody = new ModelRenderer(this, 18, 14);
		this.wolfBody.addBox(-4.0F, -2.0F, -3.0F, 6, 9, 6, f);
		this.wolfBody.setRotationPoint(0.0F, 14.0F, 2.0F);
		this.wolfMane = new ModelRenderer(this, 21, 0);
		this.wolfMane.addBox(-4.0F, -3.0F, -3.0F, 8, 6, 7, f);
		this.wolfMane.setRotationPoint(-1.0F, 14.0F, 2.0F);
		this.wolfLeg1 = new ModelRenderer(this, 0, 18);
		this.wolfLeg1.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg1.setRotationPoint(-2.5F, 16.0F, 7.0F);
		this.wolfLeg2 = new ModelRenderer(this, 0, 18);
		this.wolfLeg2.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg2.setRotationPoint(0.5F, 16.0F, 7.0F);
		this.wolfLeg3 = new ModelRenderer(this, 0, 18);
		this.wolfLeg3.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg3.setRotationPoint(-2.5F, 16.0F, -4.0F);
		this.wolfLeg4 = new ModelRenderer(this, 0, 18);
		this.wolfLeg4.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfLeg4.setRotationPoint(0.5F, 16.0F, -4.0F);
		this.wolfTail = new ModelRenderer(this, 9, 18);
		this.wolfTail.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, f);
		this.wolfTail.setRotationPoint(-1.0F, 12.0F, 8.0F);
		this.wolfHeadMain.setTextureOffset(16, 14).addBox(-3.0F, -5.0F, 0.0F, 2, 2, 1, f);
		this.wolfHeadMain.setTextureOffset(16, 14).addBox(1.0F, -5.0F, 0.0F, 2, 2, 1, f);
		this.wolfHeadMain.setTextureOffset(0, 10).addBox(-1.5F, 0.0F, -5.0F, 3, 3, 4, f);
	}
	
	public void render(Entity entity, float limbSwingOLD, float limbSwingChange, float rotation, float headYaw, float pitch, float scale)
	{
		if (formState <= 0.0F)
			this.setRotationAngles(limbSwingOLD, limbSwingChange, rotation, headYaw, pitch, scale, entity);
		
		float size = 3.0F;
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0F, -1.5F * (size-1.0f), 0.0F);
		GL11.glScalef(size, size, size);
		
		if (this.isChild)
		{
			float f6 = 2.0F;
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, 5.0F * scale, 2.0F * scale);
			this.wolfHeadMain.renderWithRotation(scale);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glScalef(1.0F / f6, 1.0F / f6, 1.0F / f6);
			GL11.glTranslatef(0.0F, 24.0F * scale, 0.0F);
			this.wolfBody.render(scale);
			this.wolfLeg1.render(scale);
			this.wolfLeg2.render(scale);
			this.wolfLeg3.render(scale);
			this.wolfLeg4.render(scale);
			this.wolfTail.renderWithRotation(scale);
			this.wolfMane.render(scale);
			GL11.glPopMatrix();
		}
		else
		{
			this.wolfHeadMain.render(scale);
			this.wolfBody.render(scale);
			this.wolfLeg1.render(scale);
			this.wolfLeg2.render(scale);
			this.wolfLeg3.render(scale);
			this.wolfLeg4.render(scale);
			this.wolfTail.render(scale);
			this.wolfMane.render(scale);
		}
		
		GL11.glPopMatrix();
	}
	
	@Override
	public void setLivingAnimations(EntityLivingBase entityLiving, float par2, float par3, float par4)
	{
		EntityTimberWolf timber = (EntityTimberWolf)entityLiving;
		
		if (timber.isAngry())
		{
			this.wolfTail.rotateAngleY = 0.0F;
		}
		else
		{
			this.wolfTail.rotateAngleY = MathHelper.cos(par2 * 0.6662F) * 1.4F * par3;
		}
		
		if (timber.isSitting())
		{
			this.wolfMane.setRotationPoint(-1.0F, 16.0F, -3.0F);
			this.wolfMane.rotateAngleX = ((float)Math.PI * 2F / 5F);
			this.wolfMane.rotateAngleY = 0.0F;
			this.wolfBody.setRotationPoint(0.0F, 18.0F, 0.0F);
			this.wolfBody.rotateAngleX = ((float)Math.PI / 4F);
			this.wolfTail.setRotationPoint(-1.0F, 21.0F, 6.0F);
			this.wolfLeg1.setRotationPoint(-2.5F, 22.0F, 2.0F);
			this.wolfLeg1.rotateAngleX = ((float)Math.PI * 3F / 2F);
			this.wolfLeg2.setRotationPoint(0.5F, 22.0F, 2.0F);
			this.wolfLeg2.rotateAngleX = ((float)Math.PI * 3F / 2F);
			this.wolfLeg3.rotateAngleX = 5.811947F;
			this.wolfLeg3.setRotationPoint(-2.49F, 17.0F, -4.0F);
			this.wolfLeg4.rotateAngleX = 5.811947F;
			this.wolfLeg4.setRotationPoint(0.51F, 17.0F, -4.0F);
		}
		else
		{
			this.wolfBody.setRotationPoint(0.0F, 14.0F, 2.0F);
			this.wolfBody.rotateAngleX = ((float)Math.PI / 2F);
			this.wolfMane.setRotationPoint(-1.0F, 14.0F, -3.0F);
			this.wolfMane.rotateAngleX = this.wolfBody.rotateAngleX;
			this.wolfTail.setRotationPoint(-1.0F, 12.0F, 8.0F);
			this.wolfLeg1.setRotationPoint(-2.5F, 16.0F, 7.0F);
			this.wolfLeg2.setRotationPoint(0.5F, 16.0F, 7.0F);
			this.wolfLeg3.setRotationPoint(-2.5F, 16.0F, -4.0F);
			this.wolfLeg4.setRotationPoint(0.5F, 16.0F, -4.0F);
			this.wolfLeg1.rotateAngleX = MathHelper.cos(par2 * 0.6662F) * 1.4F * par3;
			this.wolfLeg2.rotateAngleX = MathHelper.cos(par2 * 0.6662F + (float)Math.PI) * 1.4F * par3;
			this.wolfLeg3.rotateAngleX = MathHelper.cos(par2 * 0.6662F + (float)Math.PI) * 1.4F * par3;
			this.wolfLeg4.rotateAngleX = MathHelper.cos(par2 * 0.6662F) * 1.4F * par3;
		}
		
		this.wolfHeadMain.rotateAngleZ = timber.getInterestedAngle(par4) + timber.getShakeAngle(par4, 0.0F);
		this.wolfMane.rotateAngleZ = timber.getShakeAngle(par4, -0.08F);
		this.wolfBody.rotateAngleZ = timber.getShakeAngle(par4, -0.16F);
		this.wolfTail.rotateAngleZ = timber.getShakeAngle(par4, -0.2F);
		
		
		 /**************************/
		/*** DEATH STATE **********/
		
		formState = (float)timber.getFormState();
		if (timber.getActiveForm())
		{
			if (formState > 0)
				formState += par4 - 1.0f;
		}
		else
		{
			if (formState < (float)timber.formStateMax)
				formState -= par4 - 1.0f;
		}
		formState = formState/((float)timber.formStateMax);
		
		if (formState > 0)
		{
			this.wolfHeadMain.offsetY = formState*0.46875F;
			this.wolfBody.offsetY = formState*0.4375F;
			this.wolfLeg1.offsetX = -formState*0.1875F;
			this.wolfLeg2.offsetX = formState*0.1875F;
			this.wolfTail.offsetY = formState*0.75F;
			this.wolfMane.offsetY = formState*0.4375F;
			
			float rotateZ = (float) (formState*0.5f*Math.PI);
			float shiftX = (float) (Math.sin(rotateZ)*0.5f);
			float shiftY = (float) ((1.0f-Math.cos(rotateZ))*0.5f);
			
			this.wolfLeg1.offsetX += -shiftX;
			this.wolfLeg2.offsetX += shiftX;
			this.wolfLeg3.offsetX = wolfLeg1.offsetX;
			this.wolfLeg4.offsetX = wolfLeg2.offsetX;
			
			this.wolfLeg1.offsetY = shiftY;
			this.wolfLeg2.offsetY = shiftY;
			this.wolfLeg3.offsetY = wolfLeg1.offsetY;
			this.wolfLeg4.offsetY = wolfLeg2.offsetY;
			
			this.wolfLeg1.rotateAngleZ = -rotateZ;
			this.wolfLeg2.rotateAngleZ = rotateZ;
			this.wolfLeg3.rotateAngleZ = -rotateZ;
			this.wolfLeg4.rotateAngleZ = rotateZ;
			
			this.wolfHeadMain.rotateAngleZ = rotateZ*0.3f;
			this.wolfHeadMain.rotateAngleY = -rotateZ*0.1f;
		}
		else
		{
			this.wolfHeadMain.offsetY = 0;
			this.wolfBody.offsetY = 0;
			this.wolfLeg1.offsetY = 0;
			this.wolfLeg2.offsetY = 0;
			this.wolfLeg3.offsetY = 0;
			this.wolfLeg4.offsetY = 0;
			this.wolfTail.offsetY = 0;
			this.wolfMane.offsetY = 0;
			
			this.wolfLeg1.offsetX = 0;
			this.wolfLeg2.offsetX = 0;
			this.wolfLeg3.offsetX = 0;
			this.wolfLeg4.offsetX = 0;
			
			this.wolfLeg1.rotateAngleZ = 0;
			this.wolfLeg2.rotateAngleZ = 0;
			this.wolfLeg3.rotateAngleZ = 0;
			this.wolfLeg4.rotateAngleZ = 0;
			
			this.wolfHeadMain.rotateAngleZ = 0;
			this.wolfHeadMain.rotateAngleY = 0;
		}
	}
	
	public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
	{
		this.wolfHeadMain.rotateAngleX = par5 / (180F / (float)Math.PI);
		this.wolfHeadMain.rotateAngleY = par4 / (180F / (float)Math.PI);
		this.wolfTail.rotateAngleX = par3;
	}
}
