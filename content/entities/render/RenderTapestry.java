package loecraftpack.content.entities.render;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.entities.EntityItemTapestry;
import loecraftpack.content.entities.render.ModelTapestry.Shape;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderTapestry extends Render {
	
	private static final List<ResourceLocation> images = new ArrayList<ResourceLocation>();
	static
	{
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_Celestia.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_Luna.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_TwilightSparkle.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_Rarity.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_AppleJack.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_PinkiePie.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_RainbowDash.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_FlutterShy.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/tapestry_Grand_Faust.png"));
	}
	
	private ModelTapestry modelTapestryNormal = new ModelTapestry(Shape.NORMAL);
	private ModelTapestry modelTapestryDouble = new ModelTapestry(Shape.DOUBLE);
	private ModelTapestry modelTapestryGrand = new ModelTapestry(Shape.GRAND);
	
	public void doRenderTapestry(EntityItemTapestry entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		//FING BUG FIX: client not receiving the bloody bounding box.
		if (!entity.correctedClientBB)
			entity.correctBB();
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		
		GL11.glPushMatrix();
		
		GL11.glRotatef(entity.rotationYaw, 0.0F, 1.0F, 0.0F);
		doRenderTapestry(getEntityTexture(entity), entity.getTapestryShape());
		
		GL11.glPopMatrix();
		
		GL11.glPopMatrix();
	}
	
	public void doRenderTapestry(ResourceLocation image, Shape shape)
	{
		this.renderManager.renderEngine.bindTexture(image);
		switch (shape)
		{
		case NORMAL:
			modelTapestryNormal.renderAll();
			break;
			
		case DOUBLE:
			modelTapestryDouble.renderAll();
			break;
			
		case GRAND:
			modelTapestryGrand.renderAll();
			break;
		}
		
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		int type = ((EntityItemTapestry)entity).getTapestryType();
		if (type < images.size())
		{
			return images.get(type);
		}
		return images.get(0);
	}

	@Override
	public void doRender(Entity entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		doRenderTapestry((EntityItemTapestry)entity, relativeX, relativeY, relativeZ, f, f1);
	}

	

}