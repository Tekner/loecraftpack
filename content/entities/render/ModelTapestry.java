package loecraftpack.content.entities.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelTapestry extends ModelBase
{
	int textureSizeX;
	int textureSizeY;
	
	public ModelRenderer mainBar;
	public ModelRenderer cloth;
	
	protected Shape shape;
	public enum Shape
	{
		NORMAL,
		DOUBLE,
		GRAND
	}
	
	public ModelTapestry(Shape shape)
	{
		this.shape = shape;
		switch(shape)
		{
		case NORMAL:
			textureSizeX = 96;
			textureSizeY = 128;
			this.mainBar = (new ModelRenderer(this, 64, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.mainBar.addBox(58.0F, -17.0F, -2.0F, 4, 34, 4, 0.0F);
			this.mainBar.rotationPointX = 0.0F;
			this.mainBar.rotationPointY = 0.0F;
			this.mainBar.rotationPointZ = 0.0F;
			
			this.cloth = (new ModelRenderer(this, 0, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.cloth.addBox(-16.0F, -64.0F, -2.0F, 32, 128, 0, 0.0F);
			this.cloth.rotationPointX = 0.0F;
			this.cloth.rotationPointY = 0.0F;
			this.cloth.rotationPointZ = 0.0F;
			
			break;
			
		case DOUBLE:
			textureSizeX = 160;
			textureSizeY = 128;
			this.mainBar = (new ModelRenderer(this, 128, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.mainBar.addBox(58.0F, -17.0F, -2.0F, 4, 34, 4, 0.0F);//Do: adjust this
			this.mainBar.rotationPointX = 0.0F;
			this.mainBar.rotationPointY = 0.0F;
			this.mainBar.rotationPointZ = 0.0F;
			
			this.cloth = (new ModelRenderer(this, 0, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.cloth.addBox(-32.0F, -64.0F, -2.0F, 64, 128, 0, 0.0F);
			this.cloth.rotationPointX = 0.0F;
			this.cloth.rotationPointY = 0.0F;
			this.cloth.rotationPointZ = 0.0F;
			
			break;
			
		case GRAND:
			textureSizeX = 288;
			textureSizeY = 192;
			this.mainBar = (new ModelRenderer(this, 256, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.mainBar.addBox(87.0F, -67.0F, -4.0F, 6, 134, 6, 0.0F);
			this.mainBar.rotationPointX = 0.0F;
			this.mainBar.rotationPointY = 0.0F;
			this.mainBar.rotationPointZ = 0.0F;
			
			this.cloth = (new ModelRenderer(this, 0, 0)).setTextureSize(textureSizeX, textureSizeY);
			this.cloth.addBox(-64.0F, -96.0F, -4.0F, 128, 192, 0, 0.0F);
			this.cloth.rotationPointX = 0.0F;
			this.cloth.rotationPointY = 0.0F;
			this.cloth.rotationPointZ = 0.0F;
			
			break;
		}
	}
	
	public void renderAll()
	{
		float scale = 0.0625F / 2.0f/*resolution*/;
		
		switch(shape)
		{
		case GRAND:
		case DOUBLE:
		case NORMAL:
			this.mainBar.rotateAngleZ = ((float) Math.PI)*0.5f;
			this.mainBar.render(scale);
			
			this.cloth.rotateAngleZ = ((float) Math.PI);
			this.cloth.render(scale);
			
			break;
		}
	}

}
