package loecraftpack.content.entities.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelMask extends ModelBase
{
	
	public ModelRenderer mask = (new ModelRenderer(this, 0, 0)).setTextureSize(32, 32);
	
	public ModelMask()
	{
		this.mask = (new ModelRenderer(this, 0, 0)).setTextureSize(32, 32);
		this.mask.addBox(-4.0F, -7.0F, -1.0F, 8, 14, 2, 1.0F);
		this.mask.rotationPointX = 0.0F;
		this.mask.rotationPointY = 0.0F;
		this.mask.rotationPointZ = 0.0F;
	}
	
	public void renderAll()
	{
		this.mask.rotateAngleZ = (float) Math.PI;
		this.mask.render(0.0625F);
	}
}
