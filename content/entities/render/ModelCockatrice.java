package loecraftpack.content.entities.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

//TODO: modified chicken model, rewrite
@SideOnly(Side.CLIENT)
public class ModelCockatrice extends ModelBase
{
	public ModelRenderer head;
	public ModelRenderer body;
	public ModelRenderer rightLeg;
	public ModelRenderer leftLeg;
	public ModelRenderer rightWing;
	public ModelRenderer leftWing;
	public ModelRenderer bill;
	public ModelRenderer chin;
	public ModelRenderer tail;
	
	public ModelCockatrice()
	{
		byte b0 = 16;
		head = new ModelRenderer(this, 0, 0);
		head.addBox(-2.0F, -6.0F, -2.0F, 4, 6, 3, 0.0F);
		head.setRotationPoint(0.0F, (float)(-1 + b0), -4.0F);
		bill = new ModelRenderer(this, 14, 0);
		bill.addBox(-2.0F, -4.0F, -4.0F, 4, 2, 2, 0.0F);
		bill.setRotationPoint(0.0F, (float)(-1 + b0), -4.0F);
		chin = new ModelRenderer(this, 14, 4);
		chin.addBox(-1.0F, -2.0F, -3.0F, 2, 2, 2, 0.0F);
		chin.setRotationPoint(0.0F, (float)(-1 + b0), -4.0F);
		body = new ModelRenderer(this, 0, 9);
		body.addBox(-3.0F, -4.0F, -3.0F, 6, 8, 6, 0.0F);
		body.setRotationPoint(0.0F, (float)b0, 0.0F);
		tail = new ModelRenderer(this, 38, 13);
		tail.addBox(-1.0F, 3.0F, -1.0F, 2, 8, 2, 0.0F);
		tail.setRotationPoint(0.0F, (float)(-1 + b0), 0.0F);
		rightLeg = new ModelRenderer(this, 26, 0);
		rightLeg.addBox(-1.0F, 0.0F, -3.0F, 3, 5, 3);
		rightLeg.setRotationPoint(-2.0F, (float)(3 + b0), 1.0F);
		leftLeg = new ModelRenderer(this, 26, 0);
		leftLeg.addBox(-1.0F, 0.0F, -3.0F, 3, 5, 3);
		leftLeg.setRotationPoint(1.0F, (float)(3 + b0), 1.0F);
		rightWing = new ModelRenderer(this, 24, 13);
		rightWing.addBox(0.0F, 0.0F, -3.0F, 1, 4, 6);
		rightWing.setRotationPoint(-4.0F, (float)(-3 + b0), 0.0F);
		leftWing = new ModelRenderer(this, 24, 13);
		leftWing.addBox(-1.0F, 0.0F, -3.0F, 1, 4, 6);
		leftWing.setRotationPoint(4.0F, (float)(-3 + b0), 0.0F);
	}
	
	public void render(Entity par1Entity, float limbSwingOLD, float limbSwingChange, float rotation, float headYaw, float pitch, float scale)
	{
		setRotationAngles(limbSwingOLD, limbSwingChange, rotation, headYaw, pitch, scale, par1Entity);
		
		head.render(scale);
		bill.render(scale);
		chin.render(scale);
		body.render(scale);
		tail.render(scale);
		rightLeg.render(scale);
		leftLeg.render(scale);
		rightWing.render(scale);
		leftWing.render(scale);
	}
	
	public void setRotationAngles(float limbSwingOLD, float limbSwingChange, float rotation, float headYaw, float pitch, float scale, Entity entity)
	{
		head.rotateAngleX = pitch / (180F / (float)Math.PI);
		head.rotateAngleY = headYaw / (180F / (float)Math.PI);
		bill.rotateAngleX = head.rotateAngleX;
		bill.rotateAngleY = head.rotateAngleY;
		chin.rotateAngleX = head.rotateAngleX;
		chin.rotateAngleY = head.rotateAngleY;
		body.rotateAngleX = ((float)Math.PI / 2F);
		tail.rotateAngleX = ((float)Math.PI / 2F);
		rightLeg.rotateAngleX = MathHelper.cos(limbSwingOLD * 0.6662F) * 1.4F * limbSwingChange;
		leftLeg.rotateAngleX = MathHelper.cos(limbSwingOLD * 0.6662F + (float)Math.PI) * 1.4F * limbSwingChange;
		rightWing.rotateAngleZ = rotation;
		leftWing.rotateAngleZ = -rotation;
	}
}