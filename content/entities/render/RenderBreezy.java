package loecraftpack.content.entities.render;

import org.lwjgl.opengl.GL11;

import loecraftpack.content.entities.EntityBreezy;
import loecraftpack.content.entities.EntityTimberWolf;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.util.ResourceLocation;

public class RenderBreezy extends RenderLiving
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:mob/breezy.png");
	
	public RenderBreezy(ModelBase modelBase, float shadowSize)
	{
		super(modelBase, shadowSize);
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return mainImage;
	}
	
	protected int shouldRenderPass(EntityLivingBase entityLivingBase, int par2, float par3)
	{
		if (entityLivingBase.isInvisible())
		{
			return 0;
		}
		else if (par2 == 0)
		{
			if (mainModel instanceof ModelBreezy)
				((ModelBreezy)mainModel).renderPass2 = true;
			this.setRenderPassModel(mainModel);
			GL11.glEnable(GL11.GL_NORMALIZE);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			return 1;
		}
		else
		{
			if (par2 == 1)
			{
				if (mainModel instanceof ModelBreezy)
					((ModelBreezy)mainModel).renderPass2 = false;
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			}
			
			return -1;
		}
	}

}
