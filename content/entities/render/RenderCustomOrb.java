package loecraftpack.content.entities.render;

import loecraftpack.content.entities.EntityCustomExp;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Altered version of vanilla exp orb
 */
@SideOnly(Side.CLIENT)
public class RenderCustomOrb extends Render
{

	private static final ResourceLocation customExperienceTextures = new ResourceLocation("loecraftpack:entities/experience_star.png");
	
	public RenderCustomOrb()
	{
		shadowSize = 0.15F;
		shadowOpaque = 0.75F;
	}
	
	public void doRender(EntityCustomExp entityExp, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		//position
		GL11.glPushMatrix();
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		
		//texture
		bindEntityTexture(entityExp);
		int frame = entityExp.getTextureByXP();
		float xMin = (float)(frame % 4 * 16 + 0) / 64.0F;
		float xMax = (float)(frame % 4 * 16 + 16) / 64.0F;
		float yMin = (float)(frame / 4 * 16 + 0) / 64.0F;
		float yMax = (float)(frame / 4 * 16 + 16) / 64.0F;
		
		//lighting
		int brightness = entityExp.getBrightnessForRender(f1);
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)(brightness%65536)/1.0F, (float)(brightness/65536)/1.0F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		//color shift state
		float state = ((float)entityExp.xpColor + f1) / 2.0F;
		int red = (int)((MathHelper.sin(state + 4.1887903F) + 1.0F) * 0.1F * 255.0F);
		int green = (int)((MathHelper.sin(state + 0.0F) + 1.0F) * 0.5F * 255.0F);
		int k1 = red << 16 | green << 8 | 255;
		
		//angle
		GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		
		//scale
		float scale = 0.3F;
		GL11.glScalef(scale, scale, scale);
		
		//draw
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.setColorRGBA_I(k1, 128);
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		tessellator.addVertexWithUV(-0.5d, -0.25d, 0.0D, (double)xMin, (double)yMin);
		tessellator.addVertexWithUV( 0.5d, -0.25d, 0.0D, (double)xMax, (double)yMin);
		tessellator.addVertexWithUV( 0.5d,  0.75d, 0.0D, (double)xMax, (double)yMax);
		tessellator.addVertexWithUV(-0.5d,  0.75d, 0.0D, (double)xMin, (double)yMax);
		tessellator.draw();
		
		//end
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}
	
	protected ResourceLocation getCustomExpTexture(EntityCustomExp entity)
	{
		return customExperienceTextures;
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return getCustomExpTexture((EntityCustomExp) entity);
	}
	
	@Override
	public void doRender(Entity entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		this.doRender((EntityCustomExp)entity, relativeX, relativeY, relativeZ, f, f1);
	}

}
