package loecraftpack.content.entities.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import loecraftpack.content.entities.EntityBreezy;
import loecraftpack.content.entities.EntityCockatrice;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

@SideOnly(Side.CLIENT)
public class RenderCockatrice extends RenderLiving
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:mob/cockatrice.png");
	
	public RenderCockatrice(ModelBase modelBase, float shadowSize)
	{
		super(modelBase, shadowSize);
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return mainImage;
	}
	
	@Override
	protected float handleRotationFloat(EntityLivingBase par1EntityLivingBase, float par2)
	{
		EntityCockatrice bird = (EntityCockatrice)par1EntityLivingBase;
		float flap = bird.getFlap() + (!bird.onGround || bird.getFlap() < bird.flapSpeed? 0: par2*bird.flapSpeed);
		if (flap > 1.0f)
			flap -= 1.0f;
		return (1.0f - MathHelper.cos(flap*(float)Math.PI))*0.4f*(float)Math.PI;
	}
}
