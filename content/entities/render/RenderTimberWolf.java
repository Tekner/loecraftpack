package loecraftpack.content.entities.render;

import loecraftpack.content.entities.EntityTimberWolf;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderTimberWolf extends RenderLiving
{
	private static final ResourceLocation mainImage = new ResourceLocation("loecraftpack:mob/timberwolf.png");
	
	public RenderTimberWolf(ModelBase par1ModelBase, float shadowSize)
	{
		super(par1ModelBase, shadowSize);
		renderPassModel = par1ModelBase;
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return mainImage;
	}
	
	protected float getTailRotation(EntityTimberWolf entityTimberWolf, float par2)
	{
		return entityTimberWolf.getTailRotation();
	}
	
	protected int shouldRemderShake(EntityTimberWolf entityTimberWolf, int par2, float par3)
	{
		float f1;
		
		if (par2 == 0 && entityTimberWolf.getWolfShaking())
		{
			f1 = entityTimberWolf.getBrightness(par3) * entityTimberWolf.getShadingWhileShaking(par3);
			this.bindTexture(mainImage);
			GL11.glColor3f(f1, f1, f1);
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	/**
	 * Queries whether should render the specified pass or not.
	 */
	@Override
	protected int shouldRenderPass(EntityLivingBase entityLiving, int par2, float par3)
	{
		return this.shouldRemderShake((EntityTimberWolf)entityLiving, par2, par3);
	}
	
	/**
	 * Defines what float the third param in setRotationAngles of ModelBase is
	 */
	@Override
	protected float handleRotationFloat(EntityLivingBase entityLiving, float par2)
	{
		return this.getTailRotation((EntityTimberWolf)entityLiving, par2);
	}
}
