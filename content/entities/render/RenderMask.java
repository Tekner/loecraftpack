package loecraftpack.content.entities.render;

import java.util.ArrayList;
import java.util.List;

import loecraftpack.content.entities.EntityItemMask;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderMask extends Render {

	private static final List<ResourceLocation> images = new ArrayList<ResourceLocation>();
	static
	{
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/maskDeath.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/maskHappy.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/maskHungry.png"));
		images.add(new ResourceLocation("loecraftpack:textures/blocks/decor/maskSteve.png"));
	}
	
	private ModelMask modelMask = new ModelMask();
	
	public void doRenderEntityItemMask(EntityItemMask entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		//FING BUG FIX: client not receiving the bloody bounding box.
		if (!entity.correctedClientBB)
			entity.correctBB();
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		
		GL11.glPushMatrix();
		
		GL11.glRotatef(entity.rotationYaw, 0.0F, 1.0F, 0.0F);
		doRenderMask(getEntityTexture(entity));
		
		GL11.glPopMatrix();
		
		GL11.glPopMatrix();
	}
	
	public void doRenderItemMask(ItemStack stack, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)relativeX, (float)relativeY, (float)relativeZ);
		
		GL11.glPushMatrix();
		
		GL11.glRotatef(f1, 0.0F, 0.0F, 1.0F);
		doRenderMask(getItemTexture(stack));
		
		GL11.glPopMatrix();
		
		GL11.glPopMatrix();
	}
	
	public void doRenderMask(ResourceLocation image)
	{
		this.renderManager.renderEngine.bindTexture(image);
		modelMask.renderAll();
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		int type = ((EntityItemMask)entity).getMaskType();
		if (type < images.size())
		{
			return images.get(type);
		}
		return images.get(0);
	}
	
	protected ResourceLocation getItemTexture(ItemStack stack)
	{
		int type = stack.getItemDamage();
		if (type < images.size())
		{
			return images.get(type);
		}
		return images.get(0);
	}
	
	@Override
	public void doRender(Entity entity, double relativeX, double relativeY, double relativeZ, float f, float f1)
	{
		doRenderEntityItemMask((EntityItemMask)entity, relativeX, relativeY, relativeZ, f, f1);
	}

}
