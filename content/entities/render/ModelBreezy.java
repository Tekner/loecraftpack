package loecraftpack.content.entities.render;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import loecraftpack.content.entities.EntityBreezy;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

@SideOnly(Side.CLIENT)
public class ModelBreezy extends ModelBase
{
	private ModelRenderer head;
	private ModelRenderer hair;
	private ModelRenderer antennaRight;
	private ModelRenderer antennaRightTip;
	private ModelRenderer antennaLeft;
	private ModelRenderer antennaLeftTip;
	private ModelRenderer body;
	private ModelRenderer tail;
	private ModelRenderer foreLimbRight;
	private ModelRenderer foreLimbLeft;
	private ModelRenderer backLimbRight;
	private ModelRenderer backLimbLeft;
	private ModelRenderer rightWing;
	private ModelRenderer leftWing;
	
	public boolean renderPass2 = false;
	
	public ModelBreezy()
	{
		textureWidth = 64;
		textureHeight = 64;
		
		head = new ModelRenderer(this, 0, 0).addBox(-3.0F, -3.0F, -3.0F, 6, 6, 6);
		
		hair = new ModelRenderer(this, 32, 0).addBox(-1.0F, 0.0F, 1.0F, 2, 8, 3);
		hair.rotateAngleX = (float)Math.PI/2.0f;
		head.addChild(hair);
		
		antennaRight = new ModelRenderer(this, 24, 3).addBox(-2.0F, -7.0F, -3.0F, 1, 4, 1);
		head.addChild(antennaRight);
		
		antennaRightTip = new ModelRenderer(this, 24, 0).addBox(-2.0F, -4.0F, 7.0F, 1, 2, 1);
		antennaRightTip.rotateAngleX = (float)Math.PI/2.0f;
		antennaRight.addChild(antennaRightTip);
		
		antennaLeft = new ModelRenderer(this, 28, 3).addBox(1.0F, -7.0F, -3.0F, 1, 4, 1);
		head.addChild(antennaLeft);
		
		antennaLeftTip = new ModelRenderer(this, 28, 0).addBox(1.0F, -4.0F, 7.0F, 1, 2, 1);
		antennaLeftTip.rotateAngleX = (float)Math.PI/2.0f;
		antennaLeft.addChild(antennaLeftTip);
		
		body = new ModelRenderer(this, 0, 16).addBox(-3.0F, 4.0F, -3.0F, 6, 12, 6);
		
		tail = new ModelRenderer(this, 32, 11).addBox(-1.0F, -2.0F, -1.5F, 2, 12, 3);
		tail.setRotationPoint(0.0F, 16.0F, 3.0F);
		body.addChild(tail);
		
		foreLimbRight = new ModelRenderer(this, 24, 8).addBox(-0.5F, 0.0F, -0.5F, 1, 11, 1);
		foreLimbRight.setRotationPoint(-2.5F, 4.5F, -3.0F);
		body.addChild(foreLimbRight);
		
		foreLimbLeft = new ModelRenderer(this, 28, 8).addBox(-0.5F, 0.0F, -0.5F, 1, 11, 1);
		foreLimbLeft.setRotationPoint(2.5F, 4.5F, -3.0F);
		body.addChild(foreLimbLeft);
		
		backLimbRight = new ModelRenderer(this, 24, 20).addBox(-0.5F, 0.0F, -0.5F, 1, 11, 1);
		backLimbRight.setRotationPoint(-2.5F, 15.5F, -3.0F);
		body.addChild(backLimbRight);
		
		backLimbLeft = new ModelRenderer(this, 28, 20).addBox(-0.5F, 0.0F, -0.5F, 1, 11, 1);
		backLimbLeft.setRotationPoint(2.5F, 15.5F, -3.0F);
		body.addChild(backLimbLeft);
		
		
		
		rightWing = new ModelRenderer(this, 42, 17).addBox(-10.0F, 1.0F, 0.0F, 10, 16, 1);
		rightWing.mirror = true;
		rightWing.setRotationPoint(-3.0F, 0.0f, 3.0F);
		body.addChild(rightWing);
		
		leftWing = new ModelRenderer(this, 42, 0).addBox(0.0F, 1.0F, 0.0F, 10, 16, 1);
		leftWing.mirror = true;
		leftWing.setRotationPoint(3.0F, 0.0f, 3.0F);
		body.addChild(leftWing);
	}
	
	public void render(Entity entity, float limbSwingOLD, float limbSwingChange, float rotation, float headYaw, float pitch, float scale)
	{
		if (renderPass2)
		{
			rightWing.isHidden = false;
			leftWing.isHidden = false;
			
			head.isHidden = true;
			tail.isHidden = true;
			foreLimbRight.isHidden = true;
			foreLimbLeft.isHidden = true;
			backLimbRight.isHidden = true;
			backLimbLeft.isHidden = true;
		}
		else
		{
			rightWing.isHidden = true;
			leftWing.isHidden = true;
			
			head.isHidden = false;
			tail.isHidden = false;
			foreLimbRight.isHidden = false;
			foreLimbLeft.isHidden = false;
			backLimbRight.isHidden = false;
			backLimbLeft.isHidden = false;
		}
		
		
		GL11.glPushMatrix();
		
		float sizeRatio = 0.25f;
		scale *= sizeRatio;
		
		EntityBreezy entityBreezy = (EntityBreezy)entity;
		float toDegrees = (180F / (float)Math.PI);

		if (entityBreezy.getIsStanding())
		{
			GL11.glTranslatef(0.0F, 1.2875F, 0.0F);
			
			head.offsetZ = -0.5f * sizeRatio;
			head.setRotationPoint(0.0F, -2.0F, 0.0F);
			head.rotateAngleX = pitch / toDegrees;
			head.rotateAngleY = headYaw / toDegrees;
			
			body.offsetZ = -0.5625f * sizeRatio;
			body.setRotationPoint(0.0F, 0.25f, 0.0F);
			body.rotateAngleX = (float)Math.PI/2.0f;
			
			rightWing.rotateAngleY = 0.35f*(float)Math.PI;
			leftWing.rotateAngleY = -rightWing.rotateAngleY;
			
			tail.rotateAngleX = -(float)Math.PI*0.1f;
			
			foreLimbRight.rotateAngleX = -(float)Math.PI*0.5f + MathHelper.cos(limbSwingOLD * 0.6662F) * 1.4F * limbSwingChange;
			foreLimbLeft.rotateAngleX = -(float)Math.PI*0.5f + MathHelper.cos(limbSwingOLD * 0.6662F + (float)Math.PI) * 1.4F * limbSwingChange;
			backLimbRight.rotateAngleX = foreLimbLeft.rotateAngleX;
			backLimbLeft.rotateAngleX = foreLimbRight.rotateAngleX;
		}
		else
		{
			GL11.glTranslatef(0.0F, 1.1625F, 0.0F);
			
			head.setRotationPoint(0.0F, 0.0F, 0.0F);
			head.rotateAngleX = pitch / toDegrees;
			head.rotateAngleY = headYaw / toDegrees;
			
			body.offsetY = 0.0f;
			body.rotateAngleX = ((float)Math.PI / 4F) + MathHelper.cos(rotation * 0.1F) * 0.15F;
			body.rotateAngleY = 0.0F;
			
			rightWing.rotateAngleY = ((MathHelper.cos(rotation * 1.3F) * 0.25F)+ 0.1F) * (float)Math.PI ;
			leftWing.rotateAngleY = -rightWing.rotateAngleY;
			
			tail.rotateAngleX = (float)Math.PI*0.2f;
			
			foreLimbRight.rotateAngleX = -(float)Math.PI*0.1f;
			foreLimbLeft.rotateAngleX = -(float)Math.PI*0.1f;
			backLimbRight.rotateAngleX = -(float)Math.PI*0.1f;
			backLimbLeft.rotateAngleX = -(float)Math.PI*0.1f;
		}

		head.render(scale);
		body.render(scale);
		
		GL11.glPopMatrix();
	}
}
