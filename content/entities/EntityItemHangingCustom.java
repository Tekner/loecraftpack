package loecraftpack.content.entities;

import java.util.Iterator;
import java.util.List;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Direction;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class EntityItemHangingCustom extends EntityItemFrame
{
	public boolean correctedClientBB = false;
	
	public EntityItemHangingCustom(World world)
	{
		super(world);
	}
	
	public EntityItemHangingCustom(World world, int xPos, int yPos, int zPos, int direction)
	{
		super(world, xPos, yPos, zPos, direction);
	}
	
	@Override
	protected abstract void entityInit();
	
	@Override
	public abstract int getWidthPixels();

	@Override
	public abstract int getHeightPixels();
	
	public abstract float getCenterOffsetX();

	public abstract float getCenterOffsetY();
	
	public int getWidthPixelsHanging()
	{
		return getWidthPixels();
	}

	
	public int getHeightPixelsHanging()
	{
		return getHeightPixels();
	}
	
	public float getCenterOffsetXHanging()
	{
		return getCenterOffsetX();
	}

	public float getCenterOffsetYHanging()
	{
		return getCenterOffsetY();
	}
	
	@Override
	public void setDirection(int par1)
	{
		this.hangingDirection = par1;
		this.prevRotationYaw = this.rotationYaw = (float)(par1 * 90);
		float f = (float)this.getWidthPixels();
		float f1 = (float)this.getHeightPixels();
		float f2 = (float)this.getWidthPixels();

		if (par1 != 2 && par1 != 0)
		{
			f = 0.5F;
		}
		else
		{
			f2 = 0.5F;
			this.rotationYaw = this.prevRotationYaw = (float)(Direction.rotateOpposite[par1] * 90);
		}

		f /= 32.0F;
		f1 /= 32.0F;
		f2 /= 32.0F;
		float f3 = (float)this.field_146063_b + 0.5F;
		float f4 = (float)this.field_146064_c + 0.5F;
		float f5 = (float)this.field_146062_d + 0.5F;
		float f6 = 0.5625F;

		if (par1 == 2)
		{
			f5 -= f6;
		}

		if (par1 == 1)
		{
			f3 -= f6;
		}

		if (par1 == 0)
		{
			f5 += f6;
		}

		if (par1 == 3)
		{
			f3 += f6;
		}

		if (par1 == 2)
		{
			f3 -= getCenterOffsetX();
		}

		if (par1 == 1)
		{
			f5 += getCenterOffsetX();
		}

		if (par1 == 0)
		{
			f3 += getCenterOffsetX();
		}

		if (par1 == 3)
		{
			f5 -= getCenterOffsetX();
		}

		f4 += getCenterOffsetY();
		this.setPosition((double)f3, (double)f4, (double)f5);
		float f7 = -0.03125F;
		this.boundingBox.setBounds((double)(f3 - f - f7), (double)(f4 - f1 - f7), (double)(f5 - f2 - f7), (double)(f3 + f + f7), (double)(f4 + f1 + f7), (double)(f5 + f2 + f7));
	}
	
	//FING BUG FIX: client not receiving the bloody bounding box.  (activated during render)
	@SideOnly(Side.CLIENT)
	public void correctBB()
	{
		int dir = (int)(this.rotationYaw%360)/90;
		if (dir < 0) dir += 4;
		if (dir == 0 || dir == 2) dir = Direction.rotateOpposite[dir];
		
		float f = (float)this.getWidthPixels();
		float f1 = (float)this.getHeightPixels();
		float f2 = (float)this.getWidthPixels();

		if (dir != 2 && dir != 0)
		{
			f = 0.5F;
		}
		else
		{
			f2 = 0.5F;
		}

		f /= 32.0F;
		f1 /= 32.0F;
		f2 /= 32.0F;
		float f3 = (float)this.posX;
		float f4 = (float)this.posY;
		float f5 = (float)this.posZ;
		
		if (dir == 2)
		{
			f3 -= getCenterOffsetX();
		}

		if (dir == 1)
		{
			f5 += getCenterOffsetX();
		}

		if (dir == 0)
		{
			f3 += getCenterOffsetX();
		}

		if (dir == 3)
		{
			f5 -= getCenterOffsetX();
		}
		
		float f7 = -0.03125F;
		this.boundingBox.setBounds((double)(f3 - f - f7), (double)(f4 - f1 - f7), (double)(f5 - f2 - f7), (double)(f3 + f + f7), (double)(f4 + f1 + f7), (double)(f5 + f2 + f7));
		correctedClientBB = true;
	}

	@Override
	public void onBroken(Entity entity)
	{
	}
	
	@Override
	public ItemStack getDisplayedItem()
	{
		return null;
	}
	
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		compound.setByte("Direction", (byte)this.hangingDirection);
		compound.setInteger("TileX", this.field_146063_b);
		compound.setInteger("TileY", this.field_146064_c);
		compound.setInteger("TileZ", this.field_146062_d);

		switch (this.hangingDirection)
		{
			case 0:
				compound.setByte("Dir", (byte)2);
				break;
			case 1:
				compound.setByte("Dir", (byte)1);
				break;
			case 2:
				compound.setByte("Dir", (byte)0);
				break;
			case 3:
				compound.setByte("Dir", (byte)3);
		}
	}
	
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		if (compound.hasKey("Direction"))
		{
			this.hangingDirection = compound.getByte("Direction");
		}
		else
		{
			switch (compound.getByte("Dir"))
			{
				case 0:
					this.hangingDirection = 2;
					break;
				case 1:
					this.hangingDirection = 1;
					break;
				case 2:
					this.hangingDirection = 0;
					break;
				case 3:
					this.hangingDirection = 3;
			}
		}

		this.field_146063_b = compound.getInteger("TileX");
		this.field_146064_c = compound.getInteger("TileY");
		this.field_146062_d = compound.getInteger("TileZ");
		this.setDirection(this.hangingDirection);
	}
	
	public boolean interactFirst(EntityPlayer par1EntityPlayer)
	{
		return false;
	}
	
	@Override
	public boolean onValidSurface()
    {
        if (!this.worldObj.getCollidingBoundingBoxes(this, this.boundingBox).isEmpty())
        {
            return false;
        }
        else
        {
            int i = Math.max(1, this.getWidthPixelsHanging() / 16);
            int j = Math.max(1, this.getHeightPixelsHanging() / 16);
            int k = this.field_146063_b;
            int l = this.field_146064_c;
            int i1 = this.field_146062_d;
            
            if (this.getWidthPixelsHanging() > 16)
            { 
            	int offset = ((int)(MathHelper.abs(this.getCenterOffsetXHanging()*16.0f)+8) % 16);
            	if (offset + 8 + (this.getWidthPixelsHanging() % 16)/2 > 16)
            		i++;
            	if (offset - 8 - (this.getWidthPixelsHanging() % 16)/2 < 0)
            		i++;
            }
            
            if (this.getHeightPixelsHanging() > 16)
            { 
            	int offset = ((int)(MathHelper.abs(this.getCenterOffsetYHanging()*16.0f)+8) % 16);
            	if (offset + (this.getHeightPixelsHanging() % 16)/2 > 16)
            		j++;
            	if (offset - (this.getHeightPixelsHanging() % 16)/2 < 0)
            		j++;
            }

            if (this.hangingDirection == 2)
            {
            	double posX = this.posX + (double)(this.getCenterOffsetX()-this.getCenterOffsetXHanging());
                k = MathHelper.floor_double(posX - (double)((float)this.getWidthPixelsHanging() / 32.0F));
            }

            if (this.hangingDirection == 1)
            {
            	double posZ = this.posZ - (double)(this.getCenterOffsetX()-this.getCenterOffsetXHanging());
                i1 = MathHelper.floor_double(posZ - (double)((float)this.getWidthPixelsHanging() / 32.0F));
            }

            if (this.hangingDirection == 0)
            {
            	double posX = this.posX - (double)(this.getCenterOffsetX()-this.getCenterOffsetXHanging());
                k = MathHelper.floor_double(posX - (double)((float)this.getWidthPixelsHanging() / 32.0F));
            }

            if (this.hangingDirection == 3)
            {
            	double posZ = this.posZ + (double)(this.getCenterOffsetX()-this.getCenterOffsetXHanging());
                i1 = MathHelper.floor_double(posZ - (double)((float)this.getWidthPixelsHanging() / 32.0F));
            }
            
            double posY = this.posY - (double)(this.getCenterOffsetY()-this.getCenterOffsetYHanging());
            l = MathHelper.floor_double(posY - (double)((float)this.getHeightPixelsHanging() / 32.0F));

            for (int j1 = 0; j1 < i; ++j1)
            {
                for (int k1 = 0; k1 < j; ++k1)
                {
                    Material material;

                    if (this.hangingDirection != 2 && this.hangingDirection != 0)
                    {
                        material = this.worldObj.getBlock(this.field_146063_b, l + k1, i1 + j1).getMaterial();
                    }
                    else
                    {
                        material = this.worldObj.getBlock(k + j1, l + k1, this.field_146062_d).getMaterial();
                    }

                    if (!material.isSolid())
                    {
                        return false;
                    }
                }
            }

            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox);
            Iterator iterator = list.iterator();
            Entity entity;

            do
            {
                if (!iterator.hasNext())
                {
                    return true;
                }

                entity = (Entity)iterator.next();
            }
            while (!(entity instanceof EntityHanging));

            return false;
        }
    }
}
