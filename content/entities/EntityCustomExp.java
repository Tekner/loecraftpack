package loecraftpack.content.entities;

import loecraftpack.LoECraftPack;
import loecraftpack.content.ponies.stats.Stats;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class EntityCustomExp extends EntityXPOrb
{
	public EntityCustomExp(World world)
	{
		super(world);
	}
	
	public EntityCustomExp(World world, double xPos, double yPos, double zPos, int value)
	{
		super(world, xPos, yPos, zPos, value);
	}
	
	@Override
	public void onCollideWithPlayer(EntityPlayer player)
	{
		if (!worldObj.isRemote)
		{
			if (field_70532_c == 0 && player.xpCooldown == 0)
			{
				player.xpCooldown = 2;
				playSound("random.orb", 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
				player.onItemPickup(this, 1);
				Stats playerStats = (Stats)LoECraftPack.statHandler.stats.get(player.getGameProfile().getId());
				if (playerStats != null)
					playerStats.addExp(xpValue);
				setDead();
			}
		}
	}
}
