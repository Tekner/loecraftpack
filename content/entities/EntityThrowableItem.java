package loecraftpack.content.entities;

import java.util.Iterator;
import java.util.List;

import loecraftpack.LoECraftPack;
import loecraftpack.content.items.IThrowableItem;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityThrowableItem extends EntityThrowable {
	
	public ItemStack refferance;
	
	public EntityThrowableItem(World world)
	{
		super(world);
	}

	public EntityThrowableItem(World world, EntityLivingBase entityLivingBase, Item item, int meta)
	{
		super(world, entityLivingBase);
		setItemID(Item.getIdFromItem(item));
		setMetaID(meta);
		
	}

	@SideOnly(Side.CLIENT)
	public EntityThrowableItem(World world, double xCoord, double yCoord, double zCoord, Item item, int meta)
	{
		super(world, xCoord, yCoord, zCoord);
		setItemID(Item.getIdFromItem(item));
		setMetaID(meta);
	}
	
	@Override
	public void entityInit()
	{
		super.entityInit();
		this.dataWatcher.addObject(2, 0);
		this.dataWatcher.addObject(3, 0);
	}
	
	public void setItemID(int value)
	{
		this.dataWatcher.updateObject(2, value);
	}
	
	public int getItemID()
	{
		return this.dataWatcher.getWatchableObjectInt(2);
	}
	
	public void setMetaID(int value)
	{
		this.dataWatcher.updateObject(3, value);
	}
	
	public int getMetaID()
	{
		return this.dataWatcher.getWatchableObjectInt(3);
	}
	
	@Override
	protected float getGravityVelocity()
	{
		return 0.05F;
	}
	
	@Override
	protected float func_70182_d()
	{
		return 0.5F;
	}
	
	@Override
	protected float func_70183_g()
	{
		return -20.0F;
	}
	
	@Override
	protected void onImpact(MovingObjectPosition par1MovingObjectPosition)
	{
		if (!this.worldObj.isRemote)
		{
			Item item = Item.getItemById(getItemID());
			int metaID = getMetaID();
			if (item != null && (item instanceof IThrowableItem))
			{
				IThrowableItem throwable = (IThrowableItem)item;
				
				List list = throwable.getPotionEffects(metaID);
				
				boolean flagPotion = list != null && !list.isEmpty();
				boolean flagOther = throwable.hasNonPotionEffects(metaID);
				if (flagPotion || flagOther)
				{
					AxisAlignedBB axisalignedbb = this.boundingBox.expand(4.0D, 2.0D, 4.0D);
					List list1 = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, axisalignedbb);
					
					if (list1 != null && !list1.isEmpty())
					{
						Iterator iterator = list1.iterator();
						
						while (iterator.hasNext())
						{
							EntityLivingBase entitylivingbase = (EntityLivingBase)iterator.next();
							double d0 = this.getDistanceSqToEntity(entitylivingbase);
							
							if (d0 < 16.0D)
							{
								double distanceInEdge = 1.0D - Math.sqrt(d0) / 4.0D;
								
								if (entitylivingbase == par1MovingObjectPosition.entityHit)
								{
									distanceInEdge = 1.0D;
								}
								
								if (flagPotion)
								{
									Iterator iterator1 = list.iterator();
									
									while (iterator1.hasNext())
									{
										PotionEffect potioneffect = (PotionEffect)iterator1.next();
										int i = potioneffect.getPotionID();
										
										if (Potion.potionTypes[i].isInstant())
										{
											Potion.potionTypes[i].affectEntity(this.getThrower(), entitylivingbase, potioneffect.getAmplifier(), distanceInEdge);
										}
										else
										{
											int j = (int)(distanceInEdge * (double)potioneffect.getDuration() + 0.5D);
											
											if (j > 20)
											{
												entitylivingbase.addPotionEffect(new PotionEffect(i, j, potioneffect.getAmplifier()));
											}
										}
									}
								}
								
								if (flagOther)
									throwable.applyNonPotionEffects(entitylivingbase, metaID, distanceInEdge);
							}
						}
					}
					
					throwable.enviromentEffect(worldObj, posX, posY, posZ, metaID);
				}
			}
			//Do: custom splash?
			this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), metaID);
			this.setDead();
		}
	}
	
	@Override
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.readEntityFromNBT(par1NBTTagCompound);
		
		setItemID(par1NBTTagCompound.getInteger("itemID"));
		setMetaID(par1NBTTagCompound.getInteger("metaID"));
	}
	
	@Override
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.writeEntityToNBT(par1NBTTagCompound);
		
		par1NBTTagCompound.setInteger("itemID", getItemID());
		par1NBTTagCompound.setInteger("metaID", getMetaID());
	}
	
}