package loecraftpack.content.entities.arrow;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S0DPacketCollectItem;
import net.minecraft.network.play.server.S2BPacketChangeGameState;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

//modified vanilla code - because of excessive use of private, and no sub methods for the update method.
public abstract class EntityCustomArrow extends Entity implements IProjectile
{
	protected int xTile = -1;
	protected int yTile = -1;
	protected int zTile = -1;
	protected Block tileLodged = Blocks.air;
	protected int inData = 0;
	protected boolean inGround = false;
	
	/** 1 if the player can pick up the arrow */
	public int canBePickedUp = 0;
	
	/** Seems to be some sort of timer for animating an arrow. */
	public int arrowShake = 0;
	
	/** The owner of this arrow. */
	public Entity shootingEntity;
	protected int ticksInGround;
	protected int lifeSpan = 1200;
	protected int ticksInAir = 0;
	protected double damage = 2.0D;
	
	/** The amount of knockback an arrow applies when it hits a mob. */
	protected int knockbackStrength;
	
	public EntityCustomArrow(World par1World)
	{
		super(par1World);
		renderDistanceWeight = 10.0D;
		setSize(0.5F, 0.5F);
	}
	
	public EntityCustomArrow(World par1World, double xPos, double yPos, double zPos)
	{
		super(par1World);
		renderDistanceWeight = 10.0D;
		setSize(0.5F, 0.5F);
		setPosition(xPos, yPos, zPos);
		yOffset = 0.0F;
	}
	
	public EntityCustomArrow(World par1World, EntityLivingBase par2EntityLiving, EntityLivingBase par3EntityLiving, float par4, float par5)
	{
		super(par1World);
		renderDistanceWeight = 10.0D;
		shootingEntity = par2EntityLiving;
		
		if (par2EntityLiving instanceof EntityPlayer)
		{
			canBePickedUp = 1;
		}
		
		posY = par2EntityLiving.posY + (double)par2EntityLiving.getEyeHeight() - 0.10000000149011612D;
		double d0 = par3EntityLiving.posX - par2EntityLiving.posX;
		double d1 = par3EntityLiving.boundingBox.minY + (double)(par3EntityLiving.height / 3.0F) - posY;
		double d2 = par3EntityLiving.posZ - par2EntityLiving.posZ;
		double d3 = (double)MathHelper.sqrt_double(d0 * d0 + d2 * d2);
		
		if (d3 >= 1.0E-7D)
		{
			float f2 = (float)(Math.atan2(d2, d0) * 180.0D / Math.PI) - 90.0F;
			float f3 = (float)(-(Math.atan2(d1, d3) * 180.0D / Math.PI));
			double d4 = d0 / d3;
			double d5 = d2 / d3;
			setLocationAndAngles(par2EntityLiving.posX + d4, posY, par2EntityLiving.posZ + d5, f2, f3);
			yOffset = 0.0F;
			float f4 = (float)d3 * 0.2F;
			setThrowableHeading(d0, d1 + (double)f4, d2, par4, par5);
		}
	}
	
	public EntityCustomArrow(World par1World, EntityLivingBase par2EntityLiving, float par3)
	{
		super(par1World);
		renderDistanceWeight = 10.0D;
		shootingEntity = par2EntityLiving;
		
		if (par2EntityLiving instanceof EntityPlayer)
		{
			canBePickedUp = 1;
		}
		
		setSize(0.5F, 0.5F);
		setLocationAndAngles(par2EntityLiving.posX, par2EntityLiving.posY + (double)par2EntityLiving.getEyeHeight(), par2EntityLiving.posZ, par2EntityLiving.rotationYaw, par2EntityLiving.rotationPitch);
		posX -= (double)(MathHelper.cos(rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
		posY -= 0.10000000149011612D;
		posZ -= (double)(MathHelper.sin(rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
		setPosition(posX, posY, posZ);
		yOffset = 0.0F;
		motionX = (double)(-MathHelper.sin(rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(rotationPitch / 180.0F * (float)Math.PI));
		motionZ = (double)(MathHelper.cos(rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(rotationPitch / 180.0F * (float)Math.PI));
		motionY = (double)(-MathHelper.sin(rotationPitch / 180.0F * (float)Math.PI));
		setThrowableHeading(motionX, motionY, motionZ, par3 * 1.5F, 1.0F);
	}
	
	protected void entityInit()
	{
		dataWatcher.addObject(16, Byte.valueOf((byte)0));
	}
	
	/**
	 * Similar to setArrowHeading, it's point the throwable entity to a x, y, z direction.
	 */
	public void setThrowableHeading(double par1, double par3, double par5, float par7, float par8)
	{
		float f2 = MathHelper.sqrt_double(par1 * par1 + par3 * par3 + par5 * par5);
		par1 /= (double)f2;
		par3 /= (double)f2;
		par5 /= (double)f2;
		par1 += rand.nextGaussian() * (double)(rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double)par8;
		par3 += rand.nextGaussian() * (double)(rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double)par8;
		par5 += rand.nextGaussian() * (double)(rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double)par8;
		par1 *= (double)par7;
		par3 *= (double)par7;
		par5 *= (double)par7;
		motionX = par1;
		motionY = par3;
		motionZ = par5;
		float f3 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
		prevRotationYaw = rotationYaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI);
		prevRotationPitch = rotationPitch = (float)(Math.atan2(par3, (double)f3) * 180.0D / Math.PI);
		ticksInGround = 0;
	}
	
	@SideOnly(Side.CLIENT)
	
	/**
	 * Sets the position and rotation. Only difference from the other one is no bounding on the rotation. Args: posX,
	 * posY, posZ, yaw, pitch
	 */
	public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9)
	{
		setPosition(par1, par3, par5);
		setRotation(par7, par8);
	}
	
	@SideOnly(Side.CLIENT)
	
	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	public void setVelocity(double par1, double par3, double par5)
	{
		motionX = par1;
		motionY = par3;
		motionZ = par5;
		
		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F)
		{
			float f = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
			prevRotationYaw = rotationYaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch = (float)(Math.atan2(par3, (double)f) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch;
			prevRotationYaw = rotationYaw;
			setLocationAndAngles(posX, posY, posZ, rotationYaw, rotationPitch);
			ticksInGround = 0;
		}
	}
	
	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate()
	{
		super.onUpdate();
		
		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F)
		{
			float f = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
			prevRotationYaw = rotationYaw = (float)(Math.atan2(motionX, motionZ) * 180.0D / Math.PI);
			prevRotationPitch = rotationPitch = (float)(Math.atan2(motionY, (double)f) * 180.0D / Math.PI);
		}
		
		Block blockTarget = worldObj.getBlock(xTile, yTile, zTile);
		Boolean isAir = worldObj.isAirBlock(xTile, yTile, zTile);
		
		if (!isAir)
		{
			blockTarget.setBlockBoundsBasedOnState(worldObj, xTile, yTile, zTile);
			AxisAlignedBB axisalignedbb = blockTarget.getCollisionBoundingBoxFromPool(worldObj, xTile, yTile, zTile);
			
			if (axisalignedbb != null && axisalignedbb.isVecInside(worldObj.getWorldVec3Pool().getVecFromPool(posX, posY, posZ)))
			{
				inGround = true;
			}
		}
		
		if (arrowShake > 0)
		{
			--arrowShake;
		}
		
		if (inGround)
		{
			whileInGround();
		}
		else
		{
			++ticksInAir;
			Vec3 vecPos = worldObj.getWorldVec3Pool().getVecFromPool(posX, posY, posZ);
			Vec3 vecMove = worldObj.getWorldVec3Pool().getVecFromPool(posX + motionX, posY + motionY, posZ + motionZ);
			MovingObjectPosition interceptioningObject = worldObj.func_147447_a(vecPos, vecMove, false, true, false);//raytrace blocks
			vecPos = worldObj.getWorldVec3Pool().getVecFromPool(posX, posY, posZ);
			vecMove = worldObj.getWorldVec3Pool().getVecFromPool(posX + motionX, posY + motionY, posZ + motionZ);
			
			if (interceptioningObject != null)
			{
				vecMove = worldObj.getWorldVec3Pool().getVecFromPool(interceptioningObject.hitVec.xCoord, interceptioningObject.hitVec.yCoord, interceptioningObject.hitVec.zCoord);
			}
			
			Entity interceptingEntity = null;
			List list = worldObj.getEntitiesWithinAABBExcludingEntity(this, boundingBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));
			double d0 = 0.0D;
			int l;
			float f1;
			
			for (l = 0; l < list.size(); ++l)
			{
				Entity entity1 = (Entity)list.get(l);
				
				if (entity1.canBeCollidedWith() && (entity1 != shootingEntity || ticksInAir >= 5))
				{
					f1 = 0.3F;
					AxisAlignedBB axisalignedbb1 = entity1.boundingBox.expand((double)f1, (double)f1, (double)f1);
					MovingObjectPosition movingobjectposition1 = axisalignedbb1.calculateIntercept(vecPos, vecMove);
					
					if (movingobjectposition1 != null)
					{
						double d1 = vecPos.distanceTo(movingobjectposition1.hitVec);
						
						if (d1 < d0 || d0 == 0.0D)
						{
							interceptingEntity = entity1;
							d0 = d1;
						}
					}
				}
			}
			
			if (interceptingEntity != null)
			{
				interceptioningObject = new MovingObjectPosition(interceptingEntity);
			}
			
			if (interceptioningObject != null && interceptioningObject.entityHit != null && interceptioningObject.entityHit instanceof EntityPlayer)
			{
				EntityPlayer entityplayer = (EntityPlayer)interceptioningObject.entityHit;
				
				if (entityplayer.capabilities.disableDamage || shootingEntity instanceof EntityPlayer && !((EntityPlayer)shootingEntity).canAttackPlayer(entityplayer))
				{
					interceptioningObject = null;
				}
			}
			
			float f2;
			float f3;
			
			if (interceptioningObject != null)
			{
				if (interceptioningObject.entityHit != null)
				{
					onEntityHit(interceptioningObject);
				}
				else
				{
					xTile = interceptioningObject.blockX;
					yTile = interceptioningObject.blockY;
					zTile = interceptioningObject.blockZ;
					tileLodged = blockTarget;
					inData = worldObj.getBlockMetadata(xTile, yTile, zTile);
					motionX = (double)((float)(interceptioningObject.hitVec.xCoord - posX));
					motionY = (double)((float)(interceptioningObject.hitVec.yCoord - posY));
					motionZ = (double)((float)(interceptioningObject.hitVec.zCoord - posZ));
					f2 = MathHelper.sqrt_double(motionX * motionX + motionY * motionY + motionZ * motionZ);
					posX -= motionX / (double)f2 * 0.05000000074505806D;
					posY -= motionY / (double)f2 * 0.05000000074505806D;
					posZ -= motionZ / (double)f2 * 0.05000000074505806D;
					playSound("random.bowhit", 1.0F, 1.2F / (rand.nextFloat() * 0.2F + 0.9F));
					inGround = true;
					arrowShake = 7;
					setIsCritical(false);
					
					if (!isAir)
					{
						tileLodged.onEntityCollidedWithBlock(worldObj, xTile, yTile, zTile, this);
					}
				}
			}
			
			if (getIsCritical())
			{
				for (l = 0; l < 4; ++l)
				{
					worldObj.spawnParticle("crit", posX + motionX * (double)l / 4.0D, posY + motionY * (double)l / 4.0D, posZ + motionZ * (double)l / 4.0D, -motionX, -motionY + 0.2D, -motionZ);
				}
			}
			
			posX += motionX;
			posY += motionY;
			posZ += motionZ;
			f2 = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
			rotationYaw = (float)(Math.atan2(motionX, motionZ) * 180.0D / Math.PI);
			
			for (rotationPitch = (float)(Math.atan2(motionY, (double)f2) * 180.0D / Math.PI); rotationPitch - prevRotationPitch < -180.0F; prevRotationPitch -= 360.0F)
			{
				;
			}
			
			while (rotationPitch - prevRotationPitch >= 180.0F)
			{
				prevRotationPitch += 360.0F;
			}
			
			while (rotationYaw - prevRotationYaw < -180.0F)
			{
				prevRotationYaw -= 360.0F;
			}
			
			while (rotationYaw - prevRotationYaw >= 180.0F)
			{
				prevRotationYaw += 360.0F;
			}
			
			rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch) * 0.2F;
			rotationYaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * 0.2F;
			float f4 = 0.99F;
			f1 = 0.05F;
			
			if (isInWater())
			{
				for (int j1 = 0; j1 < 4; ++j1)
				{
					f3 = 0.25F;
					worldObj.spawnParticle("bubble", posX - motionX * (double)f3, posY - motionY * (double)f3, posZ - motionZ * (double)f3, motionX, motionY, motionZ);
				}
				
				f4 = 0.8F;
			}
			
			motionX *= (double)f4;
			motionY *= (double)f4;
			motionZ *= (double)f4;
			motionY -= (double)f1;
			setPosition(posX, posY, posZ);
			func_145775_I();//do collisions
		}
	}
	
	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	public void writeEntityToNBT(NBTTagCompound nBTTagCompound)
	{
		nBTTagCompound.setShort("xTile", (short)xTile);
		nBTTagCompound.setShort("yTile", (short)yTile);
		nBTTagCompound.setShort("zTile", (short)zTile);
		nBTTagCompound.setString("tileLodged", Block.blockRegistry.getNameForObject(tileLodged));
		nBTTagCompound.setByte("inData", (byte)inData);
		nBTTagCompound.setByte("shake", (byte)arrowShake);
		nBTTagCompound.setByte("inGround", (byte)(inGround ? 1 : 0));
		nBTTagCompound.setByte("pickup", (byte)canBePickedUp);
		nBTTagCompound.setDouble("damage", damage);
	}
	
	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	public void readEntityFromNBT(NBTTagCompound nBTTagCompound)
	{
		xTile = nBTTagCompound.getShort("xTile");
		yTile = nBTTagCompound.getShort("yTile");
		zTile = nBTTagCompound.getShort("zTile");
		tileLodged = Block.getBlockFromName(nBTTagCompound.getString("tileLodged"));
		inData = nBTTagCompound.getByte("inData") & 255;
		arrowShake = nBTTagCompound.getByte("shake") & 255;
		inGround = nBTTagCompound.getByte("inGround") == 1;
		
		if (nBTTagCompound.hasKey("damage"))
		{
			damage = nBTTagCompound.getDouble("damage");
		}
		
		if (nBTTagCompound.hasKey("pickup"))
		{
			canBePickedUp = nBTTagCompound.getByte("pickup");
		}
		else if (nBTTagCompound.hasKey("player"))
		{
			canBePickedUp = nBTTagCompound.getBoolean("player") ? 1 : 0;
		}
	}
	
	/**
	 * Called by a player entity when they collide with an entity
	 */
	public void onCollideWithPlayer(EntityPlayer entityPlayer)
	{
		if (!worldObj.isRemote && inGround && arrowShake <= 0)
		{
			boolean flag = canBePickedUp == 1 || canBePickedUp == 2 && entityPlayer.capabilities.isCreativeMode;
			
			if (canBePickedUp == 1 && !entityPlayer.inventory.addItemStackToInventory(onPickup()))
			{
				flag = false;
			}
			
			if (flag)
			{
				playSound("random.pop", 0.2F, ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
				//par1EntityPlayer.onItemPickup(this, 1);
				if (!this.isDead && !entityPlayer.worldObj.isRemote)
				{
					EntityTracker entitytracker = ((WorldServer)entityPlayer.worldObj).getEntityTracker();
					entitytracker.func_151247_a(this, new S0DPacketCollectItem(this.getEntityId(), entityPlayer.getEntityId()));
				}
				setDead();
				onHitEntity();
			}
		}
	}
	
	protected abstract void onHitEntity();

	public abstract ItemStack onPickup();
	
	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they walk on. used for spiders and wolves to
	 * prevent them from trampling crops
	 */
	protected boolean canTriggerWalking()
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		return 0.0F;
	}
	
	public void setDamage(double par1)
	{
		damage = par1;
	}
	
	public double getDamage()
	{
		return damage;
	}
	
	/**
	 * Sets the amount of knockback the arrow applies when it hits a mob.
	 */
	public void setKnockbackStrength(int par1)
	{
		knockbackStrength = par1;
	}
	
	/**
	 * If returns false, the item will not inflict any damage against entities.
	 */
	public boolean canAttackWithItem()
	{
		return false;
	}
	
	/**
	 * Whether the arrow has a stream of critical hit particles flying behind it.
	 */
	public void setIsCritical(boolean par1)
	{
		byte b0 = dataWatcher.getWatchableObjectByte(16);
		
		if (par1)
		{
			dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 | 1)));
		}
		else
		{
			dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 & -2)));
		}
	}
	
	/**
	 * Whether the arrow has a stream of critical hit particles flying behind it.
	 */
	public boolean getIsCritical()
	{
		byte b0 = dataWatcher.getWatchableObjectByte(16);
		return (b0 & 1) != 0;
	}
	
	
	
	
	
	/******************************/
	/**** UPDATE SUB FUNCTIONS ****/
	/******************************/
	
	public void whileInGround()
	{
		Block block = worldObj.getBlock(xTile, yTile, zTile);
		Boolean isAir = worldObj.isAirBlock(xTile, yTile, zTile);
		int meta = worldObj.getBlockMetadata(xTile, yTile, zTile);
		
		if (!isAir && block == tileLodged && meta == inData)
		{
			++ticksInGround;
			
			if (ticksInGround >= lifeSpan)
			{
				setDead();
			}
		}
		else
		{
			inGround = false;
			motionX *= (double)(rand.nextFloat() * 0.2F);
			motionY *= (double)(rand.nextFloat() * 0.2F);
			motionZ *= (double)(rand.nextFloat() * 0.2F);
			ticksInGround = 0;
			ticksInAir = 0;
		}
	}
	
	public void onEntityHit(MovingObjectPosition movingobjectposition)
	{
		float f2;
		float f3;
		
		f2 = MathHelper.sqrt_double(motionX * motionX + motionY * motionY + motionZ * motionZ);
		int i1 = MathHelper.ceiling_double_int((double)f2 * damage);
		
		if (getIsCritical())
		{
			i1 += rand.nextInt(i1 / 2 + 2);
		}
		
		DamageSource damagesource = getDamageScource();
		
		if (isBurning() && !(movingobjectposition.entityHit instanceof EntityEnderman))
		{
			movingobjectposition.entityHit.setFire(5);
		}
		
		if (movingobjectposition.entityHit.attackEntityFrom(damagesource, i1))
		{
			if (movingobjectposition.entityHit instanceof EntityLiving)
			{
				EntityLiving entityliving = (EntityLiving)movingobjectposition.entityHit;
				
				if (!worldObj.isRemote)
				{
					entityliving.setArrowCountInEntity(entityliving.getArrowCountInEntity() + 1);
				}
				
				if (knockbackStrength > 0)
				{
					f3 = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
					
					if (f3 > 0.0F)
					{
						movingobjectposition.entityHit.addVelocity(motionX * (double)knockbackStrength * 0.6000000238418579D / (double)f3, 0.1D, motionZ * (double)knockbackStrength * 0.6000000238418579D / (double)f3);
					}
				}
				
				if (shootingEntity != null)
				{
					entityliving.attackEntityFrom(DamageSource.causeThornsDamage(shootingEntity), 1 + rand.nextInt(4));
				}
				
				if (shootingEntity != null && movingobjectposition.entityHit != shootingEntity && movingobjectposition.entityHit instanceof EntityPlayer && shootingEntity instanceof EntityPlayerMP)
				{
					((EntityPlayerMP)shootingEntity).playerNetServerHandler.sendPacket(new S2BPacketChangeGameState(6, 0));
				}
			}
			
			playSound("random.bowhit", 1.0F, 1.2F / (rand.nextFloat() * 0.2F + 0.9F));
			
			if (!(movingobjectposition.entityHit instanceof EntityEnderman))
			{
				setDead();
				onHitEntity();
			}
		}
		else
		{
			motionX *= -0.10000000149011612D;
			motionY *= -0.10000000149011612D;
			motionZ *= -0.10000000149011612D;
			rotationYaw += 180.0F;
			prevRotationYaw += 180.0F;
			ticksInAir = 0;
		}
	}
	
	protected DamageSource getDamageScource()
	{
		if (shootingEntity == null)
		{
			return (new EntityDamageSourceIndirect("arrow", this, this)).setProjectile();
		}
		else
		{
			return (new EntityDamageSourceIndirect("arrow", this, shootingEntity)).setProjectile();
		}
	}
}
