package loecraftpack.content.entities.arrow;

import loecraftpack.content.registery.LOE_Items;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.world.World;

public class EntityIronArrow extends EntityCustomArrow{
	
	public EntityIronArrow(World par1World)
	{
		super(par1World);
	}

	public EntityIronArrow(World par1World, double xPos, double yPos, double zPos)
	{
		super(par1World, xPos, yPos, zPos);
	}

	public EntityIronArrow(World par1World, EntityLivingBase par2EntityLiving, EntityLivingBase par3EntityLiving, float par4, float par5)
	{
		super(par1World, par2EntityLiving, par3EntityLiving, par4, par5);
	}
	
	public EntityIronArrow(World par1World, EntityLivingBase par2EntityLiving, float par3)
	{
		super(par1World, par2EntityLiving, par3);
	}
	
	@Override
	protected DamageSource getDamageScource()
	{
		return (new EntityDamageSourceIndirect("arrow", this, shootingEntity == null? this: shootingEntity)).setProjectile();
	}
	
	@Override
	public void onHitEntity()
	{
		if (!worldObj.isRemote && (shootingEntity != null? shootingEntity.rand: rand).nextInt(30) != 0) this.dropItem(LOE_Items.ironArrow, 1);
	}
	
	@Override
	public ItemStack onPickup()
	{
		return new ItemStack(LOE_Items.ironArrow, 1);
	}

}
