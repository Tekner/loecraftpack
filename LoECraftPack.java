package loecraftpack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.blocks.tile.TileColoredBed;
import loecraftpack.content.blocks.tile.TileDungeonChest;
import loecraftpack.content.blocks.tile.TileProjectTable;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.content.commands.CommandPrintSchematic;
import loecraftpack.content.commands.CommandStat;
import loecraftpack.content.commands.CommandStatRace;
import loecraftpack.content.entities.EntityCustomExp;
import loecraftpack.content.entities.EntityHarmonyTree;
import loecraftpack.content.entities.EntityItemMask;
import loecraftpack.content.entities.EntityItemTapestry;
import loecraftpack.content.entities.EntityPedestal;
import loecraftpack.content.entities.EntityThrowableItem;
import loecraftpack.content.entities.arrow.EntityIceArrow;
import loecraftpack.content.entities.arrow.EntityIronArrow;
import loecraftpack.content.entities.arrow.EntityPhantomArrow;
import loecraftpack.content.gui.CreativeTabBlock;
import loecraftpack.content.materials.MaterialCloud;
import loecraftpack.content.ponies.abilities.projectiles.EntityCustomBalls;
import loecraftpack.content.ponies.inventory.gui.ContainerAbilities;
import loecraftpack.content.ponies.stats.StatHandlerServer;
import loecraftpack.content.potions.PotionCharged;
import loecraftpack.content.potions.PotionEnergy;
import loecraftpack.content.potions.PotionEnergyRegen;
import loecraftpack.content.potions.PotionPetrified;
import loecraftpack.content.potions.PotionWellFed;
import loecraftpack.content.registery.LOE_Abilities;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.content.registery.LOE_Enchantment;
import loecraftpack.content.registery.LOE_Items;
import loecraftpack.content.registery.LOE_Mobs;
import loecraftpack.content.registery.LOE_Settings;
import loecraftpack.content.registery.LOE_WorldGen;
import loecraftpack.content.worldgen.HandlerTerrain;
import loecraftpack.logic.attributes.HandlerAttribute;
import loecraftpack.logic.handlers.HandlerExtendedInventory;
import loecraftpack.logic.handlers.HandlerGui;
import loecraftpack.logic.handlers.HandlerPlayer;
import loecraftpack.logic.handlers.HandlerRecipes;
import loecraftpack.logic.handlers.event.HandlerCommonEvent;
import loecraftpack.logic.handlers.event.HandlerTick;
import loecraftpack.network.PacketHandler;
import loecraftpack.proxies.CommonProxy;
import loecraftpack.referrance.externalaccess.ExternalAccessHandler;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * 
 * @author Tenker, Rundo
 *
 */
@Mod(modid = LoECraftPack.MODID, name = LoECraftPack.NAME, version = LoECraftPack.VERSION)
public class LoECraftPack
{
	//Testing something
	public static final String MODID = "loecraftpack";
	public static final String NAME = "LoECraft Pack";
	public static final String VERSION = "1.0";
	
	@Instance(LoECraftPack.MODID)
	public static LoECraftPack instance = new LoECraftPack();
	
	@SidedProxy(clientSide = "loecraftpack.proxies.ClientProxy", serverSide = "loecraftpack.proxies.CommonProxy")
	public static CommonProxy proxy;
	@SidedProxy(clientSide = "loecraftpack.content.ponies.stats.StatHandlerClient", serverSide = "loecraftpack.content.ponies.stats.StatHandlerServer")
	public static StatHandlerServer statHandler;
	
	public static final PacketHandler packetHandler = new PacketHandler();
	public static final HandlerPlayer playerHandler = new HandlerPlayer();
	
	//Create our own creative tab
	public static CreativeTabBlock LoECraftTabBlock = new CreativeTabBlock("LoECraftTabBlocks");
	
	public static CreativeTabs LoECraftTabItem = new CreativeTabs("LoECraftTabItems")
	{
		//Set the icon - Do: CreativeTab - Add new item with custom icon for use here 
		@Override
		public Item getTabIconItem()
		{
			return Items.writable_book;
		}
	};
	
	public static CreativeTabs LoECraftTabAbility = new CreativeTabs("LoECraftTabAbility")
	{
		//Set the icon - Do: CreativeTab - Add new item with custom icon for use here 
		public Item getTabIconItem()
		{
				return Items.writable_book;
		}
	};
	
	//Declare Materials
	public static final Material materialCloud = new MaterialCloud();
	
	//Declare Potions
	public static final PotionCharged potionCharged = ((PotionCharged)(new PotionCharged(31, 16776960)).setPotionName("Charged")).setIconIndex(0, 0);
	public static final PotionEnergy potionEnergy = (PotionEnergy)(new PotionEnergy(30, true, 16776960)).setPotionName("Energy");
	public static final PotionEnergyRegen potionEnergyRegen = ((PotionEnergyRegen)(new PotionEnergyRegen(29, true, 16776960)).setPotionName("EnergyRegen")).setIconIndex(0, 0);
	public static final PotionWellFed potionWellFed = ((PotionWellFed)(new PotionWellFed(28)).setPotionName("WellFed")).setIconIndex(0, 0);
	public static final PotionPetrified potionPetrified = ((PotionPetrified)(new PotionPetrified(27, 10526880)).setPotionName("Petrified")).setIconIndex(0, 0);
	
	/****************************/
	/**Forge Pre-Initialization**/
	/****************************/
	
	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		/************************/
		/**Load Config  &  INIT**/
		/************************/
		
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		
		config.load();
		
		LOE_Settings.init(config);
		LOE_Abilities.init(config);
		LOE_Blocks.init(config);
		LOE_Items.init(config);
		LOE_Enchantment.init(config);
		LOE_WorldGen.init(config);
		LOE_Mobs.init(config);
		
		config.save();
		
		LOE_Abilities.updateDependants();
		LOE_Blocks.updateDependants();
		LOE_Items.updateDependants();
		LOE_Enchantment.updateDependants();
		LOE_WorldGen.updateDependants();
		LOE_Mobs.updateDependants();
		
		
		/************************/
		/*** Register ***********/
		/************************/
		
		LOE_Abilities.register();
		LOE_Blocks.register();
		LOE_Items.register();
		LOE_Enchantment.register();
		LOE_WorldGen.register();
		LOE_Mobs.register();
		
		
		/************************/
		/**Initialize Variables**/
		/************************/
		
		packetHandler.initialise("loecraftpack");
		
	}
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		/****************************/
		/**Register everything else**/
		/****************************/
		
///////// Tile Entities /////////
		GameRegistry.registerTileEntity(TileProtectionMonolith.class, "ProtectionMonolithTileEntity");
		GameRegistry.registerTileEntity(TileColoredBed.class, "ColoredBedTileEntity");
		GameRegistry.registerTileEntity(TileProjectTable.class, "ProjectTableTileEntity");
		GameRegistry.registerTileEntity(TileDungeonChest.class, "DungeonChestTileEntity");
		GameRegistry.registerTileEntity(TileBrewingStation.class, "BrewingStationTileEntity");
		
///////// Entities /////////
		EntityRegistry.registerModEntity(EntityPhantomArrow.class, "phantomarrow", 1, this, 50, 2, true);
		EntityRegistry.registerModEntity(EntityPedestal.class, "pedestal", 2, this, 50, 2, false);
		EntityRegistry.registerModEntity(EntityItemMask.class, "mask", 3, this, 50, 100, false);
		EntityRegistry.registerModEntity(EntityItemTapestry.class, "tapestry", 4, this, 50, 100, false);
		EntityRegistry.registerModEntity(EntityThrowableItem.class, "throwable_drink", 5, this, 50, 2, true);
		EntityRegistry.registerModEntity(EntityCustomBalls.class, "balls", 6, this, 50, 2, true);
		EntityRegistry.registerModEntity(EntityCustomExp.class, "LoeExp", 7, this, 50, 2, true);
		EntityRegistry.registerModEntity(EntityHarmonyTree.class, "harmonytree", 8, this, 50, 100, false);
		EntityRegistry.registerModEntity(EntityIronArrow.class, "iton_arrow", 9, this, 50, 2, true);
		EntityRegistry.registerModEntity(EntityIceArrow.class, "ice_arrow", 10, this, 50, 2, true);
		
///////// Handlers /////////
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new HandlerGui());
		FMLCommonHandler.instance().bus().register(playerHandler);
		MinecraftForge.EVENT_BUS.register(new HandlerCommonEvent());
		MinecraftForge.EVENT_BUS.register(new HandlerAttribute());
		MinecraftForge.EVENT_BUS.register(new HandlerExtendedInventory());
		//MinecraftForge.EVENT_BUS.register(new HandlerBucket());
		HandlerTerrain terrainHandle = new HandlerTerrain();
		MinecraftForge.TERRAIN_GEN_BUS.register(terrainHandle);
		MinecraftForge.EVENT_BUS.register(terrainHandle);
		FMLCommonHandler.instance().bus().register(new HandlerTick());
		
		
		
		
		/******************/
		/**Dictionary******/
		/******************/
		
		//logs
		OreDictionary.registerOre("logWood", new ItemStack(LOE_Blocks.appleBloomLog, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("logWood", new ItemStack(LOE_Blocks.zapAppleLog, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("logWood", new ItemStack(LOE_Blocks.harmonyLog, 1, OreDictionary.WILDCARD_VALUE));
		
		//saplings
		OreDictionary.registerOre("treeSapling", new ItemStack(LOE_Blocks.appleBloomSapling, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("treeSapling", new ItemStack(LOE_Blocks.zapAppleSapling, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("treeSapling", new ItemStack(LOE_Blocks.harmonySapling, 1, OreDictionary.WILDCARD_VALUE));
		
		//leaves
		OreDictionary.registerOre("treeLeaves", new ItemStack(LOE_Blocks.appleBloomLeaves, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("treeLeaves", new ItemStack(LOE_Blocks.zapAppleLeaves, 1, OreDictionary.WILDCARD_VALUE));
		OreDictionary.registerOre("treeLeaves", new ItemStack(LOE_Blocks.harmonyLeaves, 1, OreDictionary.WILDCARD_VALUE));
		
		//dusts
		OreDictionary.registerOre("dustDiamond", new ItemStack(LOE_Items.ingredients, 1, 7));
		
		//clouds
		OreDictionary.registerOre("cloud", new ItemStack(LOE_Blocks.cloud, 1, OreDictionary.WILDCARD_VALUE));
		
		
		
		
		/******************/
		/**Do Proxy Stuff**/
		/******************/
		
		proxy.doProxyStuff();
		
		
		/******************/
		/**Update Recipes**/
		/******************/
		
		HandlerRecipes.loadCommonRecipes();
		
		
		/********************/
		/**Update abilities**/
		/********************/
		
		//Update Ability Page
		ContainerAbilities.registerAbilities();
	}
	
	
	
	/*****************************/
	/**Forge Post-Initialization**/
	/*****************************/
	
	@EventHandler
	public void postLoad(FMLPostInitializationEvent event)
	{
		
		ExternalAccessHandler.FindMods();
		HandlerRecipes.init();
		HandlerRecipes.loadConditionalRecipes();
		
		proxy.doProxyStuffPost();
		
		
		
		
		///null biome fix?
		/*
		BiomeGenBase[] biomes = BiomeGenBase.getBiomeGenArray();
		for (int i=0; i< biomes.length; i++)
		{
			if (biomes[i]==null)
			{
				System.out.println("filling null biome");
				biomes[i] = BiomeGenBase.ocean;
			}
		}*/
	}
	
	
	
	
	/****************/
	/**Server Start**/
	/****************/
	@EventHandler
	public void serverLoad(FMLServerStartingEvent event)
	{
		//load server commands
		event.registerServerCommand(new CommandStat());
		event.registerServerCommand(new CommandStatRace());
		event.registerServerCommand(new CommandPrintSchematic());
	}
	
	@EventHandler
	public void serverStopping(FMLServerStoppingEvent event)
	{
		if (!isClient() || isSinglePlayer())
		{
			List<EntityPlayer> players = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().playerEntityList;
			for (EntityPlayer player : players)
			{
				playerHandler.onPlayerLogout(new PlayerLoggedOutEvent(player));
			}
		}
	}
	
	
	
	/*****************/
	/***Identifiers***/
	/*****************/
	public static boolean isSinglePlayer()
	{
		return proxy.isSinglePlayer();
	}
	
	public static boolean isClient()
	{
		return proxy.isClient();
	}
	
	public static Map<String, String> getPlayerID_UsernameMap()
	{
		List<EntityPlayer> players = MinecraftServer.getServer().getConfigurationManager().playerEntityList;
		Map<String, String> result = new HashMap<String, String>();
		for (int i=0; i<players.size(); i++)
		{
			result.put(players.get(i).getGameProfile().getId(), players.get(i).getGameProfile().getName());
		}
		return result;
	}
	
	public static boolean isPlayerOP(EntityPlayer player)
	{
		return MinecraftServer.getServer().getConfigurationManager().getOps().contains(player.getGameProfile().getId().toString());
	}
	
	public static EntityPlayerMP getPlayerForUsername(String username)
	{
		return MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(username);
	}
	
	
	
	/*****************/
	/***DEBUG TOOLS***/
	/*****************/
	public static void printStackTrace()
	{
		for (StackTraceElement ste : Thread.currentThread().getStackTrace())
		{
			System.out.println(ste);
		}
	}
}
