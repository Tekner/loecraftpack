LoECraft Pack
============

This is the Git Repository for the LoECraft Mod to be used on the Minecraft server, *LoECraft*.  
Please visit us at [http://loecraft.enjin.com](http://loecraft.enjin.com)!

-

Suggestions
===========

If you have any suggestions of things to add, questions about the server/mods, or anything else pertaining to LoECraft,  
please do not hesistate to contact us via [our forums](http://loecraft.enjin.com/forum). We'd love to hear from you!

-

Developers:
==========
* Tekner *(Author/Programmer)*
* Rundo *(Programmer)*