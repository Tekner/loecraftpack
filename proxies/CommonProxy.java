package loecraftpack.proxies;

import java.lang.reflect.Field;

import net.minecraft.potion.Potion;
import net.minecraft.server.MinecraftServer;

public class CommonProxy
{
	public void doProxyStuff()
	{
		if (!isSinglePlayer())
			setNightVisionColor();
	}
	
	public void doProxyStuffPost()
	{
		
	}
	
	public boolean isSinglePlayer()
	{
		return !MinecraftServer.getServer().isDedicatedServer();
	}
	
	public boolean isClient()
	{
		return false;
	}

	@SuppressWarnings("finally")
	protected void setNightVisionColor()
	{
		//Make Night Vision potion particles invisible
		try
		{
			//For compiled mod
			Field liquidColorField  = Potion.class.getDeclaredField("field_76414_N");
			liquidColorField.setAccessible(true);
			liquidColorField.setInt(Potion.nightVision, 0);
		}
		catch (Exception _e)
		{
			try
			{
				//For uncompiled mod
				Field liquidColorField  = Potion.class.getDeclaredField("liquidColor");
				liquidColorField.setAccessible(true);
				liquidColorField.setInt(Potion.nightVision, 0);
			}
			catch (Exception e)
			{
				System.out.println("Error while removing night vision particles:");
				e.printStackTrace();
			}
			System.out.println("Error while removing night vision particles:");
			_e.printStackTrace();
		}
	}
}
