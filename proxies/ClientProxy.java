package loecraftpack.proxies;

import loecraftpack.LoECraftPack;
import loecraftpack.content.blocks.render.RenderBrewingStation;
import loecraftpack.content.blocks.render.RenderColoredBed;
import loecraftpack.content.blocks.render.RenderDungeonChest;
import loecraftpack.content.blocks.render.RenderHiddenOre;
import loecraftpack.content.blocks.render.RenderProtectionMonolith;
import loecraftpack.content.blocks.tile.TileBrewingStation;
import loecraftpack.content.blocks.tile.TileDungeonChest;
import loecraftpack.content.blocks.tile.TileProtectionMonolith;
import loecraftpack.content.entities.EntityBreezy;
import loecraftpack.content.entities.EntityCockatrice;
import loecraftpack.content.entities.EntityCustomExp;
import loecraftpack.content.entities.EntityElectricBlock;
import loecraftpack.content.entities.EntityItemMask;
import loecraftpack.content.entities.EntityItemTapestry;
import loecraftpack.content.entities.EntityPedestal;
import loecraftpack.content.entities.EntityThrowableItem;
import loecraftpack.content.entities.EntityTimberWolf;
import loecraftpack.content.entities.arrow.EntityPhantomArrow;
import loecraftpack.content.entities.render.ModelBreezy;
import loecraftpack.content.entities.render.ModelCockatrice;
import loecraftpack.content.entities.render.ModelTimberWolf;
import loecraftpack.content.entities.render.RenderBreezy;
import loecraftpack.content.entities.render.RenderCockatrice;
import loecraftpack.content.entities.render.RenderCustomOrb;
import loecraftpack.content.entities.render.RenderElectricBlock;
import loecraftpack.content.entities.render.RenderMask;
import loecraftpack.content.entities.render.RenderPedestal;
import loecraftpack.content.entities.render.RenderPhantomArrow;
import loecraftpack.content.entities.render.RenderTapestry;
import loecraftpack.content.entities.render.RenderThrowableItem;
import loecraftpack.content.entities.render.RenderTimberWolf;
import loecraftpack.content.ponies.abilities.projectiles.EntityCustomBalls;
import loecraftpack.content.ponies.abilities.projectiles.Waterball;
import loecraftpack.content.ponies.abilities.renders.RenderFlatProjectile;
import loecraftpack.content.ponies.abilities.renders.RenderFrostbolt;
import loecraftpack.content.registery.LOE_Blocks;
import loecraftpack.logic.attributes.HandlerSpecialRender;
import loecraftpack.logic.handlers.HandlerExtendedPotions;
import loecraftpack.logic.handlers.HandlerHud;
import loecraftpack.logic.handlers.event.HandlerKey;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;

public class ClientProxy extends CommonProxy
{
	public static RenderElectricBlock renderEleBlock;
	public static RenderHiddenOre renderHiddenOre;
	public static RenderMask renderMask;
	public static RenderFlatProjectile renderFlatProjectile;
	public static RenderCustomOrb renderCustomOrb;
	
	public void doProxyStuff()
	{
		/** HUD, etc. **/
		MinecraftForge.EVENT_BUS.register(new HandlerHud(Minecraft.getMinecraft()));
		MinecraftForge.EVENT_BUS.register(new HandlerSpecialRender());
		
		
		/** KEY BINDING **/
		FMLCommonHandler.instance().bus().register(new HandlerKey());
		
		
		
		/** TILES **/
		
		ClientRegistry.bindTileEntitySpecialRenderer(TileProtectionMonolith.class, new RenderProtectionMonolith());
		
		RenderDungeonChest rdc = new RenderDungeonChest();
		ClientRegistry.bindTileEntitySpecialRenderer(TileDungeonChest.class, rdc);
		
		RenderBrewingStation rbs = new RenderBrewingStation();
		ClientRegistry.bindTileEntitySpecialRenderer(TileBrewingStation.class, rbs);
		
		
		
		/** BLOCKS **/
		
		RenderColoredBed cbr = new RenderColoredBed();
		RenderingRegistry.registerBlockHandler(cbr.renderID = RenderingRegistry.getNextAvailableRenderId(), (ISimpleBlockRenderingHandler)cbr);
		LOE_Blocks.bed.renderID = cbr.renderID;
		
		renderHiddenOre = new RenderHiddenOre();
		RenderingRegistry.registerBlockHandler(renderHiddenOre.renderID = RenderingRegistry.getNextAvailableRenderId(), (ISimpleBlockRenderingHandler)renderHiddenOre);
		LOE_Blocks.gemOre.renderID = renderHiddenOre.renderID;
		
		RenderingRegistry.registerBlockHandler(rdc.renderID = RenderingRegistry.getNextAvailableRenderId(), (ISimpleBlockRenderingHandler)rdc);
		LOE_Blocks.bossChest.renderID = rdc.renderID;
		LOE_Blocks.coffin.renderID = rdc.renderID;
		
		RenderingRegistry.registerBlockHandler(rbs.renderID = RenderingRegistry.getNextAvailableRenderId(), (ISimpleBlockRenderingHandler)rbs);
		LOE_Blocks.brewingStation.renderID = rbs.renderID;
		
		
		/** ENTITIES **/
		RenderingRegistry.registerEntityRenderingHandler(EntityElectricBlock.class, renderEleBlock = new RenderElectricBlock());
		RenderingRegistry.registerEntityRenderingHandler(EntityTimberWolf.class, new RenderTimberWolf(new ModelTimberWolf(), 0.5F));
		RenderingRegistry.registerEntityRenderingHandler(EntityBreezy.class, new RenderBreezy(new ModelBreezy(), 0.18F));
		RenderingRegistry.registerEntityRenderingHandler(EntityCockatrice.class, new RenderCockatrice(new ModelCockatrice(), 0.5F));
		RenderingRegistry.registerEntityRenderingHandler(EntityPhantomArrow.class, new RenderPhantomArrow());
		//RenderingRegistry.registerEntityRenderingHandler(EntityIronArrow.class, new RenderIronArrow());
		//RenderingRegistry.registerEntityRenderingHandler(EntityIceArrow.class, new RenderIceArrow());
		RenderingRegistry.registerEntityRenderingHandler(EntityPedestal.class, new RenderPedestal());
		RenderingRegistry.registerEntityRenderingHandler(EntityItemMask.class, renderMask = new RenderMask());
		RenderingRegistry.registerEntityRenderingHandler(EntityItemTapestry.class, new RenderTapestry());
		RenderingRegistry.registerEntityRenderingHandler(Waterball.class, new RenderFrostbolt(2));
		RenderingRegistry.registerEntityRenderingHandler(EntityThrowableItem.class, new RenderThrowableItem());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomBalls.class, renderFlatProjectile = new RenderFlatProjectile());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomExp.class, renderCustomOrb = new RenderCustomOrb());
		
		
		/** OTHER **/
		
		//add potionEffects to handler
		HandlerExtendedPotions.addPotion(LoECraftPack.potionCharged);
		HandlerExtendedPotions.addPotion(LoECraftPack.potionEnergyRegen);
		HandlerExtendedPotions.addPotion(LoECraftPack.potionWellFed);
		HandlerExtendedPotions.addPotion(LoECraftPack.potionPetrified);
	}
	
	public void doProxyStuffPost()
	{
		
	}
	
	@Override
	public boolean isSinglePlayer()
	{
		return Minecraft.getMinecraft().isSingleplayer();
	}
	
	@Override
	public boolean isClient()
	{
		return true;
	}
}
