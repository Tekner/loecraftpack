package loecraftpack.referrance.enums;

public enum Race
{
	NONE, //This should only be a valid race for players who just joined the server
	UNICORN,
	PEGASUS,
	EARTH,
	ALICORN,
	DRAGON; //used to identify that the ability is not standard, and only usable under certain conditions
}
