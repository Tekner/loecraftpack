package loecraftpack.referrance.externalaccess;

import java.util.List;
import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModContainer;


public class ExternalAccessHandler
{
	private static ModContainer IndustrialCraft = null;
	private static ModContainer NEI = null;

	public static void FindMods()
	{
		List<ModContainer> modList = Loader.instance().getModList();
		for (ModContainer mod : modList)
		{
			//FMLLog.log("LoECraft Pack", Level.INFO, "Scanning mod for connectivity:   %s : %s ", mod.getName(), mod.getModId());
			String modID = mod.getModId();
			if (modID.contains("IC2"))
				IndustrialCraft = mod;
			else if (modID.contains("NotEnoughItems"))
				NEI = mod;
				
		}
		
		if (IndustrialCraft == null)
			;//FMLLog.log("LoECraft Pack", Level.WARNING, "cannot find:  IndustrialCraft : IC2   ...but, that's ok if you are not using it.");
		
		if (NEI == null)
			;//FMLLog.log("LoECraft Pack", Level.WARNING, "cannot find:  Not Enough Items : NotEnoughItems   ...but, that's ok if you are not using it.");
	}
	
	public static boolean isICPresent()
	{
		return IndustrialCraft != null;
	}
	public static boolean isNEIPresent()
	{
		return NEI != null;
	}

}
